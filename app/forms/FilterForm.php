<?php
namespace Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;

class FilterForm extends Form
{
    /**
     * Inicializa Formulario de Filtros de Origenes
     */
    public function initialize($options = [])
    {
        //Campo titulo obligatorio
        $title = new Text("title");
        $title->setLabel("Título");
        $title->setFilters([
                "striptags",
                "string",
        ]);
        $title->addValidators([
            new PresenceOf([
                "message" => "El título es obligatorio",
            ])
        ]);
        $this->add($title);
        
        //Campo descripcion
        $description = new Text("description");
        $description->setLabel("Descripción");
        $description->setFilters([
                "striptags",
                "string",
        ]);
        $this->add($description);
        
        //Campo tipo de dato
        $dataType = new Select('idDataType', \Models\LedDataType::find(), [
            'using'      => ['id', 'dataType'],
            'useEmpty'   => false,
            'emptyText'  => '...',
            'emptyValue' => '',
            'class'      => 'form-control'
        ]);
        $dataType->setLabel('Tipo de dato');
        $this->add($dataType);
        
        //Campo nombre del parametro obligatorio
        $param = new Text("param");
        $param->setLabel("Nombre del parámetro");
        $param->setFilters([
                "striptags",
                "string",
        ]);
        $param->addValidators([
            new PresenceOf([
                "message" => "El nombre del parámetros es obligatorio",
            ])
        ]);
        $this->add($param);
        
        //Campo valor
        $value = new Text("value");
        $value->setLabel("Valor");
        $value->setFilters([
                "striptags",
                "string",
        ]);
        $this->add($value);
    }
}