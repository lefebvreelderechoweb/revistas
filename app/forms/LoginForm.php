<?php

namespace Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Identical;

class LoginForm extends Form {

	public function initialize() {

		$user = new Text('user', [
			'class' => 'form-control',
			'placeholder' => 'Usuario'
		]);
		$user->addValidator(
			new PresenceOf(
				[
				'message' => 'Campo requerido',
				]
			)
		);
		$this->add($user);

		$pwd = new Password('pwd', [
			'class' => 'form-control',
			'placeholder' => 'Contraseña'
		]);
		$pwd->addValidator(
			new PresenceOf(
				[
				'message' => 'Campo requerido',
				]
			)
		);
		$this->add($pwd);

		$csrf = new Hidden(
			'csrf'
		);
		$csrf->addValidator(new Identical([
				'value' => $this->security->getSessionToken(),
				'message' => 'Token inválido'
		]));
		$this->add($csrf);
	}

	/**
	 * Metodo contra el que se valida el token csrf
	 * @return string
	 */
	public function getCsrf() {
		if($this->security->getSessionToken() == null){
			return $this->security->getToken();
		}
		return $this->security->getSessionToken();
	}

}
