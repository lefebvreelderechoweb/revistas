<?php
namespace Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;

class FieldForm extends Form
{
    /**
     * Inicializa Formulario de Origenes
     */
    public function initialize($options = [])
    {
        //Campo nombre obligatorio
        $name = new Text("field");
        $name->setLabel("Nombre");
        $name->setFilters([
                "striptags",
                "string",
        ]);
        $name->addValidators([
            new PresenceOf([
                "message" => "El nombre es obligatorio",
            ])
        ]);
        $this->add($name);
        
        //Campo tipo de dato
        $dataType = new Select('idDataType', \Models\LedDataType::find(), [
            'using'      => ['id', 'dataType'],
            'useEmpty'   => false,
            'emptyText'  => '...',
            'emptyValue' => '',
            'class'      => 'form-control'
        ]);
        $dataType->setLabel('Tipo de dato');
        $this->add($dataType);
        
        //Campo valor por defecto
        $defaultValue = new Text("defaultValue");
        $defaultValue->setLabel("Valor por defecto");
        $defaultValue->setFilters([
                "striptags",
                "string",
        ]);
        $this->add($defaultValue);
        
        //Selector de etiquetas
        $tags = array(
            'title'       => 'Título',
            'description' => 'Descripción',
            'date'        => 'Fecha',
            'image'       => 'Imagen',
            'url'         => 'Url',
            'label'       => 'Etiqueta',
            'author'      => 'Autor',
            'subtitle'    => 'Subtitulo',
            'type'        => 'Tipo'
        );
        $tag = new Select('tag', $tags);
        $tag->setLabel('Tipo de campo');
        $this->add($tag);
    }
}