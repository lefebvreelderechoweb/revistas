<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Numericality;

class MagazineForm extends Form
{
    /**
     * Inicializa Formulario de Revistas
     */
    public function initialize($entity = null, $options = [])
    {
        if (!isset($options["edit"])) {
            $element = new Text("id");

            $element->setLabel("Id");
            
            $element->setFilters(
            [
                "int"
            ]
        );

            $this->add(
                $element
            );
        } else {
            $this->add(
                new Hidden("id")
            );
        }



        $name = new Text("name");

        $name->setLabel("Nombre");

        $name->setFilters(
            [
                "striptags",
                "string",
            ]
        );

        $name->addValidators(
            [
                new PresenceOf(
                    [
                        "message" => "El nombre es obligatorio",
                    ]
                )
            ]
        );

        $this->add($name);
        
        $lastNumber = new Text ("idNumber");
        $lastNumber->setLabel("Último número publicado");
        $lastNumber->setFilters(
            [
                "striptags",
                "string",
            ]
        );
        $this->add($lastNumber);
    }
}