<?php
namespace Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;

class OriginForm extends Form
{
    /**
     * Inicializa Formulario de Origenes
     */
    public function initialize($options = [])
    {
        //Revistas
        $this->add(new Hidden("id"));       
        
        //Campo nombre obligatorio
        $name = new Text("name");
        $name->setLabel("Nombre");
        $name->setFilters([
                "striptags",
                "string",
        ]);
        $name->addValidators([
            new PresenceOf([
                "message" => "El nombre es obligatorio",
            ])
        ]);
        $this->add($name);
        
        //Campo url obligatorio
        $url = new Text("url");
        $url->setLabel("Url");
        $url->setFilters([
                "url"
        ]);
        $url->addValidators([
            new PresenceOf([
                "message" => "Url es obligatorio",
            ])
        ]);
        $this->add($url);
        
        //Campo origen obligatorio (nodo o funcion por defecto)
        $origin = new Text("origin");
        $origin->setLabel("Origen");
        $origin->setFilters([
                "string"
        ]);
        $origin->addValidators([
            new PresenceOf([
                "message" => "El origen es obligatorio",
            ])
        ]);
        $this->add($origin);
        
        //Campo tipo obligatorio
        $originType = new Select('idOriginType', \Models\LedOriginType::find(), [
            'using'      => ['id', 'name'],
            'useEmpty'   => false,
            'emptyText'  => '...',
            'emptyValue' => '',
            'class'      => 'form-control'
        ]);
        $originType->setLabel('Tipo');
        $this->add($originType);
        
        //Campos del origen
        $this->add(new Hidden("fields"));
        
        //Filtros y atributos
        $this->add(new Hidden("filters"));
        
        //Revistas
        $this->add(new Hidden("magazines"));
        
        //Revistas select multiple
        $magazinesSelect = new Select('magazines_id', \Models\LedMagazine::find(), [
            'using'      => ['id', 'name'],
            'useEmpty'   => false,
            'emptyValue' => '',
            'multiple'   => true,
            'class'      => 'form-control'
        ]);
        $magazinesSelect->setLabel('Revistas');
        $this->add($magazinesSelect);
        
        //Componentes
        $this->add(new Hidden("components"));
        
        //Componentes select
        $componentsSelect = new Select('components_id', \Models\LedComponent::find(), [
            'using'      => ['id', 'name'],
            'useEmpty'   => false,
            'emptyValue' => '',
            'multiple'   => true,
            'class'      => 'form-control'
        ]);
        $componentsSelect->setLabel('Componentes');
        $this->add($componentsSelect);
    }
}