<?php
namespace Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;

class FilterAttributeForm extends Form
{
    /**
     * Inicializa Formulario de Filtros de Origenes
     */
    public function initialize($options = [])
    {
        //Campo nombre obligatorio
        $attrName = new Text("name");
        $attrName->setLabel("Nombre");
        $attrName->setFilters([
                "striptags",
                "string",
        ]);
        $attrName->addValidators([
            new PresenceOf([
                "message" => "El nombre es obligatorio",
            ])
        ]);
        $this->add($attrName);
        
        //Campo valor
        $attrValue = new Text("value");
        $attrValue->setLabel("Valor");
        $attrValue->setFilters([
                "striptags",
                "string",
        ]);
        $this->add($attrValue);
    }
}