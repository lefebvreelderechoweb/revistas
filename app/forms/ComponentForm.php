<?php
namespace Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\TextArea;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;

class ComponentForm extends Form
{
    /**
     * Inicializa Formulario de Origenes
     */
    public function initialize($options = [])
    {
        //Componente
        $this->add(new Hidden("id"));       
        
        //Campo nombre obligatorio
        $name = new Text("name");
        $name->setLabel("Nombre");
        $name->setFilters([
                "striptags",
                "string",
        ]);
        $name->addValidators([
            new PresenceOf([
                "message" => "El nombre es obligatorio",
            ])
        ]);
        $this->add($name);
        
        //Campo descripcion
        $description = new TextArea("description");
        $description->setLabel("Descripción");
        $this->add($description);
        
        //Revistas
        $this->add(new Hidden("magazines"));
        
        //Revistas select multiple
        $magazinesSelect = new Select('magazines_id', \Models\LedMagazine::find(), [
            'using'      => ['id', 'name'],
            'useEmpty'   => false,
            'emptyValue' => '',
            'multiple'   => true,
            'class'      => 'form-control'
        ]);
        $magazinesSelect->setLabel('Revistas');
        $this->add($magazinesSelect);
        
        //Zonas
        $this->add(new Hidden("zones"));
        
        //Zonas select
        $zonesSelect = new Select('zones_id', \Models\LedZone::find(), [
            'using'      => ['id', 'name'],
            'useEmpty'   => false,
            'emptyValue' => '',
            'multiple'   => true,
            'class'      => 'form-control'
        ]);
        $zonesSelect->setLabel('Zonas');
        $this->add($zonesSelect);
        
        //Origenes
        $this->add(new Hidden("origins"));
        
        //Zonas select
        $originsSelect = new Select('origins_id', \Models\LedOrigin::find(), [
            'using'      => ['id', 'name'],
            'useEmpty'   => false,
            'emptyValue' => '',
            'multiple'   => true,
            'class'      => 'form-control'
        ]);
        $originsSelect->setLabel('Origenes');
        $this->add($originsSelect);
    }
}