<?php
namespace Forms;

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Validation\Validator\PresenceOf;

class FilterOptionForm extends Form
{
    /**
     * Inicializa Formulario de Filtros de Origenes
     */
    public function initialize($options = [])
    {
        //Campo nombre obligatorio
        $optName = new Text("name");
        $optName->setLabel("Nombre");
        $optName->setFilters([
                "striptags",
                "string",
        ]);
        $optName->addValidators([
            new PresenceOf([
                "message" => "El nombre es obligatorio",
            ])
        ]);
        $this->add($optName);
        
        //Campo valor
        $optValue = new Text("value");
        $optValue->setLabel("Valor");
        $optValue->setFilters([
                "striptags",
                "string",
        ]);
        $this->add($optValue);
    }
}