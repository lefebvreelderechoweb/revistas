<?php

namespace Util;
use Phalcon\DI;

class Rss {
    
    public $idMagazine;
    public $baseDomain;
    
    public function __construct() {
        $di                 = \Phalcon\DI::getDefault();
        $configApp          = $di->getConfig()->application;
        $this->baseDomain   = $configApp->baseDomain;
    }
    
    /**
     * Devuelve la url del contenido en función de su tipo
     * @param int $typeContent
     * @param int $idContent
     * @return string
     */
    public function getURLCorrespondence( $typeContent, $idContent ){
        
        switch ($typeContent) {
            case '24':
                return $this->baseDomain.'tribunas';
                break;
            
            case '25':
                return $this->baseDomain.'foro-abierto';
                break;
            
            case '26':
                return $this->baseDomain.'resenas-de-jurisprudencia/'.$idContent;
                break;
            
            case '27':
                return $this->baseDomain.'novedades-legislativas';
                break;
            
            case '28':
                return $this->baseDomain.'consultas/'.$idContent;
                break;
            
            case '29':
                return $this->baseDomain.'doctrina/'.$idContent;
                break;
        } 
    }
    
    public function renderXML($data, $magazineName){
        
        $root		= '<rss version="2.0"></rss>';

        $postsXML	= new \SimpleXMLElement($root);
        $channel	= $postsXML->addChild('channel');

        // Info principal
        $channel->addChild('title', $magazineName);
        $channel->addChild('link', $this->baseDomain.'rss');
        $channel->addChild('language', 'es');

        $contents	= (count($data) > 0 ) ? $data : null;

        if( $contents ) {
            foreach($contents as $content ) {
                $item	= $channel->addChild('item');
                
                $item->addChild( 'id',          $content->id );
                $item->addChild( 'title',       $content->title );
                $item->addChild( 'description', $content->description );
                $item->addChild( 'category',    $content->category );
                $item->addChild( 'author',      $content->author );
                $item->addChild( 'url',         $content->url );
                $item->addChild( 'pubDate',     $content->pubDate);
            }
        }
        
        return $postsXML->asXML();
    }
}
