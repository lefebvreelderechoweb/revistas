<?php

namespace Util;
use Phalcon\DI;

class Magazines {
    
    public $idMagazine;
    
    public function __construct() {
        $di                 = \Phalcon\DI::getDefault();
        $configApp          = $di->getConfig()->application;
        $this->idMagazine   = $configApp->idMagazineDefault;
    }
    
    public function getMenuNavigation($contentIssue){
        
        //Formamos el array del menu
        $aContentTypes = array();
        
        foreach( $contentIssue as $content ){
            $contentType = $content->idContentType;
            
            if( array_search($contentType, $aContentTypes) == false ){
                $oContentType = \Models\LedContentType::findFirst($contentType);
                $contentTypeName = \Lefebvre\Util\Strings::removeAccents(strtolower($oContentType->name));
                
                $keyArray = str_replace(
                                array('ñ', ' '),
                                array('n', '-'),
                                $contentTypeName
                            );
                
                $aContentTypes[$keyArray] = $oContentType->name;
            }
            
        }
        
        $defaultOrder = array('tribunas', 'foro-abierto', 'consultas', 'respuesta-de-los-tribunales', 'resenas-de-jurisprudencia', 'novedades-legislativas', 'organos-consultivos', 'doctrina-administrativa');
        $menuOrderedArray = array_merge(array_flip($defaultOrder), $aContentTypes);
        
        foreach ($menuOrderedArray as $key => $item){
            if (is_int($item)){
                unset($menuOrderedArray[$key]);
            }
        }
        
        return $menuOrderedArray;
    }
    
    

}
