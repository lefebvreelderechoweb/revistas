<?php

namespace Util;

class Issue {
    
    public $sinSessionService;
    public $session;
    public $user;
    
    public function __construct() {
        $di                         = \Phalcon\DI::getDefault();
        $this->sinSessionService    = $di->get('sinSessionService');
        $this->session              = $di->get('session');
        $this->user                 = $this->session->get('user');
    }
    
    public function getContentIssue($data, $issue){
        
        $onlineLegalContent = new LegalContent();
        $userID = $this->user->id;
        
        $comps = [];
        foreach($data['data'] as $key => $value){

            $order = (int)$key + 1;

            $argsByType = $onlineLegalContent->getArgsByType($value['type'], $data['idMagazine'], true);

            $defaultArgs = array(
                'indice'    => '', //campo obligatorio
                'fulltext'  => $value['nref'],
                'producto'  => null
            );

            $arg = array_merge($defaultArgs, $argsByType);
            $documents = $this->sinSessionService->searchWithoutSession($arg);
            $dataContent = $onlineLegalContent->getFormatLegalData( $value['type'], $documents, $value );

            if ($value['id'] != ''){
                //Es la edición de un contenido ya existente dentro del número
                $comp = \Models\LedContentIssue::findFirstById($value['id']);
                $comp->assign(['ts'=>date("Y:m:d H:i:s")]); //Fecha de modificación
            }else{
                //Contenido nuevo dentro del número
                $comp = new \Models\LedContentIssue();
                $comp->assign(['fc'=>date("Y:m:d H:i:s")]);
                $comp->assign(['ts'=>date("Y:m:d H:i:s")]); //Fecha de modificación
            }

            $comp->assign(['issue'=>$issue]);
            $comp->assign(['idContentType'=>$value['type']]);
            $comp->assign(['nref'=>$value['nref']]);
            $comp->assign(['order'=>$order]);
            $comp->assign(['uic'=>$userID]);
            $comp->assign(['uc'=>$userID]);


            $title              = (isset($dataContent[0]->datas['title']))              ? $dataContent[0]->datas['title']           : '';
            $category           = (isset($dataContent[0]->datas['category']))           ? $dataContent[0]->datas['category']        : '';
            $date               = (isset($dataContent[0]->datas['publish_date']))       ? $dataContent[0]->datas['publish_date']    : '';
            $epigraph           = (isset($dataContent[0]->datas['epigrafe']))           ? $dataContent[0]->datas['epigrafe']        : '';
            $keywords           = (isset($dataContent[0]->datas['keywords']))           ? $dataContent[0]->datas['keywords']        : '';
            $review             = (isset($dataContent[0]->datas['review']))             ? $dataContent[0]->datas['review']          : '';
            $resume             = (isset($dataContent[0]->datas['resume']))             ? $dataContent[0]->datas['resume']          : '';
            $body               = (isset($dataContent[0]->datas['body']))               ? $dataContent[0]->datas['body']            : '';
            $signature          = (isset($dataContent[0]->datas['firma']))              ? $dataContent[0]->datas['firma']           : '';
            $edeDatosBase       = (isset($dataContent[0]->datas['edeDatosBase']))       ? $dataContent[0]->datas['edeDatosBase']    : '';
            $edeBoardSummary    = (isset($dataContent[0]->datas['edeBoardSummary']))    ? $dataContent[0]->datas['edeBoardSummary'] : '';

            $comp->assign(['title'          =>$title]);
            $comp->assign(['category'       =>$category]);
            $comp->assign(['publishDate'    =>$date]);
            $comp->assign(['epigraph'       =>$epigraph]);
            $comp->assign(['keywords'       =>$keywords]);
            $comp->assign(['review'         =>$review]);
            $comp->assign(['signature'      =>$signature]);
            $comp->assign(['resume'         =>$resume]);
            $comp->assign(['body'           =>$body]);
            $comp->assign(['edeBaseDate'    =>$edeDatosBase]);
            $comp->assign(['edeBoardSummary'=>$edeBoardSummary]);

            //Si es una tribuna o foro asocio el autor/es o coordinador a esta
            $authors = [];
            if ($value['type'] == '24' || $value['type'] == '25' ){
                $idAuthors = explode(",", $value['authors']);
                foreach( $idAuthors as $id ){
                    $author = \Models\LedAuthor::findFirst($id);
                    $authors[] = $author;
                }
                $comp->author = $authors;
            }

            if ( $value['type'] == '25' ){

                //Si es un foro abierto genero el planteamiento y resultado
                if ( $value['idapproach'] != '' ){
                    $approachPart = \Models\LedApproachResult::findFirstById($value['idapproach']);
                    $approachPart->assign(['ts' => date("Y:m:d H:i:s")]);
                }else{
                    $approachPart = new \Models\LedApproachResult();
                    $approachPart->assign(['ts' => date("Y:m:d H:i:s")]);
                    $approachPart->assign(['fc' => date("Y:m:d H:i:s")]);
                }

                $approachPart->assign(['idDataTypeForum'    => 1]);
                $approachPart->assign(['contentIssue'       => $comp]);
                $approachPart->assign(['content'            => $value['approach']]);
                $approachPart->assign(['uic'                => $userID]);
                $approachPart->assign(['uc'                 => $userID]);

                if ($value['idresult'] != ''){
                    $resultPart = \Models\LedApproachResult::findFirstById($value['idresult']);
                    $resultPart->assign(['ts' => date("Y:m:d H:i:s")]);
                }else{
                    $resultPart = new \Models\LedApproachResult();
                    $resultPart->assign(['ts' => date("Y:m:d H:i:s")]);
                    $resultPart->assign(['fc' => date("Y:m:d H:i:s")]);
                }

                $resultPart->assign(['idDataTypeForum' => 2]);
                $resultPart->assign(['contentIssue' => $comp]);
                $resultPart->assign(['content' => $value['result']]);
                $resultPart->assign(['uic' => $userID]);
                $resultPart->assign(['uc' => $userID]);

                $approachResult = array(
                    $approachPart,
                    $resultPart
                );

                $comp->approachResult = $approachResult;

                //Si es un foro abierto genero los puntos de vista
                $aPointsViews = [];
                $pointsViews = explode(";", $value['pointsviews']);

                foreach( $pointsViews as $item ){

                    $oPointView = json_decode($item);
                    $author = \Models\LedAuthor::findFirst($oPointView->author);

                    if ($oPointView->id != ''){
                        $pointView = \Models\LedPointView::findFirstById($oPointView->id);
                        $pointView->assign(['ts' => date("Y:m:d H:i:s")]);
                    }else{
                        $pointView = new \Models\LedPointView();
                        $pointView->assign(['ts' => date("Y:m:d H:i:s")]);
                        $pointView->assign(['fc' => date("Y:m:d H:i:s")]);
                    }

                    $pointView->assign(['contentIssue' => $comp]);
                    $pointView->assign(['pointView' => $oPointView->text]);
                    $pointView->assign(['uic' => $userID]);
                    $pointView->assign(['uc' => $userID]);

                    $pointView->author = $author;
                    $aPointsViews[] = $pointView;
                }
                $comp->pointView = $aPointsViews;
            }

            $comps[] = $comp;

        }
        
        return $comps;
    }
}
