<?php

namespace Util;
use Phalcon\DI;
use stdClass;

class LegalContent {
    
    public $cmOnlineService;
    
    const TRIBUNESID        = '24';
    const FORUMID           = '25';
    const JURISPRUDENCEID   = '26';
    const NOVLEGISID        = '27';
    const CONSULTASID       = '28';
    const DOCADMINID        = '29';
    const ORGCONSULTIVOSID  = '30';
    const RESTRIBUNALESID   = '31';
    
    public function __construct() {
        $di = \Phalcon\DI::getDefault();
        
        //Servicio Online
        $config                 = new \Phalcon\Config\Adapter\Php( LEFEBVRE_PATH.'/services/cm/config/onlineService.php' );
        $this->cmOnlineService  = new \Lefebvre\Services\Cm\OnlineService( $config );
    }
    
    /**
     * Tratamos los objetos devueltos por el servicio de Online para acomodarlos a
     * las necesidades de esta APP
     * 
     * @param type $typeDocument
     * @param type $data
     * @return type
     */
    public function getLegalData ($typeDocument, $data){
        
		$result           = $this->handleResult( $typeDocument, $data );
		$mData            = $result->items;
        
        return $mData;
	}
    
    public function getFormatLegalData ($typeDocument, $data, $formValues){
        
        $result     = array();
        $datas      = new stdClass();
        $resultData = array();
        
		$legalData = $this->getLegalData($typeDocument, $data);
        
        switch ($formValues['type']) {
            //Tribunas
            case '24':
                $resultData['nref']         = $legalData[0]->datas['nref'];
                $resultData['title']        = $formValues['title'];
                $resultData['publish_date'] = $legalData[0]->datas['date'];
                $resultData['epigrafe']     = $formValues['epigraph'];
                $resultData['keywords']     = $formValues['keywords'];
                $resultData['body']         = $formValues['review'];
                
                $datas->datas = $resultData;
                $result[] = $datas;
                break;
            
            //Foros Abiertos
            case '25':
                $resultData['nref']         = $legalData[0]->datas['nref'];
                $resultData['title']        = $formValues['title'];
                $resultData['publish_date'] = $legalData[0]->datas['date'];
                $resultData['keywords']     = $formValues['keywords'];
                
                $datas->datas = $resultData;
                $result[] = $datas;
                break;
            
            //Jurisprudencia
            case '26':
                $resultData['nref']         = $legalData[0]->datas['nref'];
                $resultData['title']        = $formValues['title'];
                $resultData['publish_date'] = date("Y-m-d H:i:s");
                $resultData['keywords']     = $formValues['keywords'];
                $resultData['resume']       = $formValues['resume'];
                $resultData['review']       = $formValues['review'];
                $resultData['body']         = $formValues['review'];
                $resultData['edeDatosBase'] = $legalData[0]->datas['tribunal'] . ", Sala " . $legalData[0]->datas['sala'] . ", Sentencia " . $legalData[0]->datas['date'] . ", rec." . $legalData[0]->datas['numero_recurso'] . " Pte.: " . $legalData[0]->datas['ponente'] . " (EDJ " . $legalData[0]->datas['nref'] . " ) ";
                
                $datas->datas = $resultData;
                $result[] = $datas;
                break;
            
            //Novedades Legislativas
            case '27':
                $resultData['nref']         = $legalData[0]->datas['nref'];
                $resultData['title']        = $formValues['title'];
                $resultData['publish_date'] = date("Y-m-d H:i:s");
                $resultData['keywords']     = $formValues['keywords'];
                $resultData['body']         = $formValues['review'];
                
                $datas->datas = $resultData;
                $result[] = $datas;
                break;
            
            //Consultas
            case '28':
                $resultData['nref']         = $legalData[0]->datas['nref'];
                $resultData['title']        = $formValues['title'];
                $resultData['category']     = $legalData[0]->datas['category'];
                $resultData['publish_date'] = $legalData[0]->datas['date'];
                $resultData['keywords']     = $formValues['keywords'];
                $resultData['body']         = $formValues['review'];
                
                $datas->datas = $resultData;
                $result[] = $datas;
                break;
            
            //Doctrina Administrativa
            case '29':
            //Organos Consultivos
            case '30':
                $resultData['nref']         = $legalData[0]->datas['nref'];
                $resultData['title']        = $formValues['title'];
                $resultData['category']     = $legalData[0]->datas['category'];
                $resultData['publish_date'] = date("Y-m-d H:i:s");
                $resultData['keywords']     = $formValues['keywords'];
                $resultData['resume']       = $formValues['resume'];
                $resultData['body']         = $formValues['review'];
                $resultData['edeDatosBase'] = ($formValues['type'] == '29') ? $legalData[0]->datas['organismo'] . ". " . $legalData[0]->datas['title'] . ". " . $legalData[0]->datas['date'] .". " . $legalData[0]->datas['boletin'] . " (EDD " . $legalData[0]->datas['nref'] . " ) " : '';
                
                $datas->datas = $resultData;
                $result[] = $datas;
                break;
            
            //Respuesta de los Tribunales
            case '31':
                $resultData['nref']         = $legalData[0]->datas['nref'];
                $resultData['title']        = $formValues['title'];
                $resultData['publish_date'] = date("Y-m-d H:i:s",strtotime(str_replace('/','-',$legalData[0]->datas['date'])));
                $resultData['keywords']     = $formValues['keywords'];
                $resultData['epigrafe']     = $formValues['epigraph'];
                $resultData['resume']       = $formValues['resume'];
                $resultData['firma']        = $formValues['signature'];
                $resultData['body']         = $formValues['review'];
                
                $datas->datas = $resultData;
                $result[] = $datas;
                break;
        }
        
        return $result;
	}
    /**
     * Formateamos los datos devueltos por el servicio obteniendo un array de datos
     * de cada tipo de documento
     * 
     * @param type $typeDocument
     * @param type $data
     * @return \stdClass
     */
    public function handleResult( $typeDocument, $data ){

		$result       = new stdClass();
		$mData = [];

		$totalEntries = isset( $data->NUMERO_DOCUMENTOS ) ? (int)$data->NUMERO_RESULTADOS : 0;

		foreach( $data->DOCUMENTOS as $document ){
			$el         = new stdClass();
			$el->datas  = array();

			$nref       = isset( $document->NREF ) ? (string)$document->NREF : '';
			$el->datas['nref']     = $nref;
			$el->datas['title']    = isset( $document->TITULO ) ? (string)$document->TITULO : '';

			$this->getExtraDataByTypeDocument( $typeDocument, $el, $document);

			$mData[] = $el;
		}
		$result->items        = $mData;
		$result->totalEntries = $totalEntries;

		return $result;
	}
    
    /**
     * Extraemos las propiedades "especiales" de cada caso de tipo de documento
     * @param type $typeDocument
     * @param type $el
     * @param type $document
     */
    public function getExtraDataByTypeDocument( $typeDocument, &$el, $document ){

		$el->datas['date'] = isset( $document->FECHA ) ? (string) str_replace('-', '/', $document->FECHA ) : '';

		switch ( $typeDocument ){
            
            //Tribunas
			case '24':
                $el->datas['autores']   = isset( $document->AUTORES ) ? (string)implode(',', $document->AUTORES) : '';
                $el->datas['title']     = isset( $document->TITULO ) ? (string)$document->TITULO : '';
                $el->datas['date']      = $document->FECHA;
			break;
            
            //Reseñas Jurisprudencia
			case '26':
				$el->datas['entradilla']             = ( isset( $document->RESUMEN ) && $document->RESUMEN != 'null' ) ? (string)$document->RESUMEN : '';
				$el->datas['jurisdiccion']           = ( isset( $document->JURISDICCION ) && $document->JURISDICCION != 'null' ) ? (string)$document->JURISDICCION : '';
				$el->datas['tipo_jurisprudencia']    = isset( $document->TIPO_JURISPRUDENCIA ) ? (string)$document->TIPO_JURISPRUDENCIA  : '';
				$el->datas['numero']                 = isset( $document->NUMERO ) ? (string)$document->NUMERO  : '';
				$el->datas['numero_recurso']         = isset( $document->NUMERO_RECURSO ) ? (string)$document->NUMERO_RECURSO : '';
				$el->datas['tribunal']				 = isset( $document->TRIBUNAL ) ? (string)$document->TRIBUNAL : '';
				$el->datas['ponente']				 = isset( $document->PONENTE ) ? (string)$document->PONENTE : '';
				$el->datas['sala']				     = isset( $document->SALA ) ? (string)$document->SALA : '';
				$el->datas['seccion']				 = isset( $document->SECCION ) ? (string)$document->SECCION : '';
				$el->datas['dx']					 = isset( $document->TITULO ) ? (string)$document->TITULO : '';
			break;
        
            //Novedades Legislativas
			case '27':
                $el->datas['title']         = $el->datas['title'];
				$el->datas['date']          = $document->FECHA_PUBLICACION;
                $el->datas['body']          = $this->cmOnlineService->getHtmlDocumentByNref($document->NREF);
			break;
        
            //Consultas
            case '28':
				$el->datas['keywords']      = isset( $document->VOCES ) ? (string)$document->VOCES : '';
				$el->datas['category']      = isset( $document->VOCES ) ? (string)$document->VOCES : '';
                $el->datas['body']          = $this->cmOnlineService->getHtmlDocumentByNref($document->NREF);
			break;
        
            //Doctrina Administrativa
            case '29':
            //Organos consultivos
            case '30':
				$el->datas['entradilla']    = isset( $document->RESUMEN )   ? (string)$document->RESUMEN    : '';
                $el->datas['date']          = $document->FECHA;
                $el->datas['organismo']     = isset( $document->ORGANISMO ) ? (string)$document->ORGANISMO  : '';
                $el->datas['boletin']       = isset( $document->BOLETIN )   ? (string)$document->BOLETIN    : '';
                $el->datas['category']      = ( $typeDocument == '29' )     ? 'Doctrina administrativa'     : '';
                $el->datas['body']          = $this->cmOnlineService->getHtmlDocumentByNref($document->NREF);
			break;
        
            //Respuestas de los tribunales
            case '31':
                $el->datas['autores']       = isset( $document->AUTORES ) ? (string)implode(',', $document->AUTORES) : '';
                $el->datas['date']          = $document->FECHA;
                $el->datas['body']          = $this->cmOnlineService->getHtmlDocumentByNref($document->NREF);
			break;
		}
	}
    
    /**
     * Del objeto con el contenido de una ley devuelto por el servicio, extraemos
     * solo el título y nref
     * 
     * @param type $nameDocument
     * @param type $document
     * @return type
     */
    public function getTitleAndNref( $nameDocument, $document){
        $result = array();
        
        $relationTypes = array(
            'tribuna'           => 24,
            'rju'               => 26,
            'nlegislativas'     => 27,
            'consulta'          => 28,
            'dadministrativa'   => 29,
            'oconsultivo'       => 30,
            'respuesta'         => 31,
            
        );
        
        $typeDocument   = $relationTypes[$nameDocument];
        
        $el         = new stdClass();
        $el->datas  = array();
        
        $nref       = isset( $document->NREF ) ? (string)$document->NREF : '';
        $el->datas['nref']     = $nref;
        $el->datas['title']    = isset( $document->TITULO ) ? (string)$document->TITULO : '';
        
        $this->getExtraDataByTypeDocument( $typeDocument, $el, $document );
        
        $result['title']    = (isset($el->datas['title'])) ? $el->datas['title'] : '';
        $result['nref']     = $nref;
        
        return $result;
    }
    
    public function getDataToFormModal( $typeDocument, $document ){
        
        $result = array();
        
        $el         = new stdClass();
        $el->datas  = array();
        
        $nref       = isset( $document->NREF ) ? (string)$document->NREF : '';
        
        $el->datas['title']    = isset( $document->TITULO ) ? (string)$document->TITULO : '';
        
        $this->getExtraDataByTypeDocument( $typeDocument, $el, $document );
        
        $result['title']        = (isset($el->datas['title_front']))    ? $el->datas['title_front'] : $el->datas['title'];
        $result['resume']       = (isset($el->datas['entradilla']))     ? $el->datas['entradilla']  : '';
        $result['epigraph']     = (isset($el->datas['epigrafe']))       ? $el->datas['epigrafe']    : '';
        $result['keywords']     = (isset($el->datas['keywords']))       ? $el->datas['keywords']    : '';
        $result['signature']    = (isset($el->datas['autores']))        ? $el->datas['autores']     : '';       
        $result['review']       = (isset($el->datas['body']))           ? $el->datas['body']        : '';       
        
        return $result;
    }
    
    /**
     * Cada revista tiene un tipo de producto asociado para las consultas.
     * Esta función lo devuelve
     * @param type $idMagazine
     * @return string
     */
    public function getTypeProduct($idMagazine){
        
        $product = '';
        
        switch ($idMagazine) {
            
            //Derecho de circulación
            case '1':
                $product = 'H';
                break;
            
            //Derecho de familia
            case '2':
                $product = 'E';
                break;
            
            //Derecho laboral
            case '3':
                $product = '';
                break;
            
            //Derecho local
            case '4':
                $product = 'P';
                break;
            
            //Inmobiliario y CafMadrid
            case '5':
            case '25':
                $product = 'C';
                break;
            
            //Mercantil
            case '6':
                $product = 'Q';
                break;
            
            //Revista de Jurisprudencia
            case '7':
                $product = '';
                break;
            
            //Urbanismo
            case '8':
                $product = 'G';
                break;
        }
        
        return $product;
    }
    
    /**
     * Función que devuelve los argumentos de llamada al método dependiendo del
     * tipo de contenido
     * 
     * El valor de $type puede ser numérico o de tipo string
     * @param type $type
     * @param type $idMagazine
     * @return string
     */
    public function getArgsByType ($type, $idMagazine, $fromModal = false){
        
        $argsByType = array();
        
        switch ($type) {
            
            //Tribuna
            case '24':
            case 'tribuna':
                $argsByType = array(
                    'indice'            => 'comentarios',
                    'tipoComentario'    => 'articulo-doctrinal'
                );
                break;
            
            //Foro
            case '25':
            case 'foro':
                $argsByType = array(
                    'indice'            => 'comentarios',
                    'tipoComentario'    => 'foro'
                );
                break;
            
            //Reseñas de Jurisprudencia
            case '26':
            case 'rju':
                $argsByType = array(
                    'indice' => 'jurisprudencia',
                );
                break;
            
            //Novedades Legislativas
            case '27':
            case 'nlegislativas':
                $argsByType = array(
                    'indice' => 'legislacion',
                );
                break;
            
            //Consultas
            case '28':
            case 'consulta':
                
                $typeProduct = $this->getTypeProduct($idMagazine);
                
                if ($fromModal){
                    //Es necesario que ponga el fulltext = $nref en el array_merge cuando viene del modal
                    $argsByType = array(
                        'indice'    => 'consultas',
                        'producto'  => $typeProduct
                    );
                }else{
                    $argsByType = array(
                        'indice'    => 'consultas',
                        'fulltext'  => 'derecho',
                        'producto'  => $typeProduct
                    );
                }
                break;
            
            //Doctrina Administrativa
            case '29':
            case 'dadministrativa':
                $argsByType = array(
                    'indice' => 'doctrina',
                );
                break;
            
            //Órganos Consultivos
            case '30':
            case 'oconsultivos':
                $argsByType = array(
                    'indice'    => 'doctrina',
                    'producto'  => 'P',
                    'organismo' => '1',
                    'voces'     => '10300001',
                );
                break;
            
            //Respuesta de los tribunales
            case '31':
            case 'respuesta':
                $argsByType = array(
                    'indice'            => 'comentarios',
                    'tipoComentario'    => 'respuesta-tribunales'
                );
                break;
        }
        
        return $argsByType;
        
    }

}
