<?php

namespace Util;
use Phalcon\DI;

class Pdf {
    
    public function getContentToPDF( $contentsBBDD ){
        
        $infoContents = array();
        
        foreach ($contentsBBDD as $content){
            
            switch ($content->idContentType) {
                
                //Tribunas
                case '24':
                    
                    $oContent               = new \stdClass();
                    $oContent->type         = 'Tribuna';
                    $oContent->category     = $content->category;
                    $oContent->title        = $content->title;
                    $oContent->content      = $content->body;
                    $oContent->subtitule    = '';
                    
                    foreach ($content->author as $author){
                        $oContent->author   = $author->tratamiento . ' ' . $author->fullName;
                        $oContent->job      = $author->job;
                    }
                    
                    $oContent->date         = '';
                    
                    $infoContents[] = $oContent;

                    break;
                
                //Foro Abierto
                case '25':
                    
                    $oContent               = new \stdClass();
                    $oContent->type         = 'Foro Abierto';
                    $oContent->category     = $content->category;
                    
                    $approachResult = $content->approachResult;

                    foreach($approachResult as $topic){
                        if ($topic->idDataTypeForum == 1){
                            $oContent->approach = $topic->content;
                        }else{
                            $oContent->resultTopic = $topic->content;
                        }
                    }
                    
                    $author = $content->author;
                    foreach ($author as $authorData) {
                        $oContent->author = $authorData->tratamiento . " " . $authorData->fullName;
                        $oContent->job = $authorData->job;
                    }
            
                    //Saco puntos de vista
                    $pointsView = $content->pointView;
                    $pointViewFormat = array();
                    
                    foreach($pointsView as $point){
                        $pointViewFormat[] = $point;
                    }
                    
                    $oContent->pointsView = $pointViewFormat;
                    
                    $infoContents[] = $oContent;
                    
                    break;
                
                //Reseña de Jurisprudencia
                case '26':
                    
                    $oContent               = new \stdClass();
                    $oContent->type         = 'Reseña de Jurisprudencia';
                    $oContent->category     = $content->category;
                    $oContent->title        = $content->title;
                    $oContent->content      = $content->resume;
                    $oContent->subtitule    = $content->edeBaseDate;
                    $oContent->author       = '';
                    $oContent->job          = '';
                    $oContent->date         = '';
                    
                    $infoContents[] = $oContent;


                    break;
                
                //Novedades Legislativas
                case '27':
                    
                    $oContent               = new \stdClass();
                    $oContent->type         = 'Novedades Legislativas';
                    $oContent->category     = '';
                    $oContent->title        = $content->title;
                    $oContent->content      = $content->body;
                    $oContent->subtitule    = '';
                    $oContent->author       = '';
                    $oContent->job          = '';
                    $oContent->date         = '';
                    
                    $infoContents[] = $oContent;
                    

                    break;
                
                //Consultas
                case '28':
                    
                    $oContent               = new \stdClass();
                    $oContent->type         = 'Consultas';
                    $oContent->category     = $content->category;
                    $oContent->title        = $content->title;
                    $oContent->content      = $content->body;
                    $oContent->subtitule    = '';
                    $oContent->author       = '';
                    $oContent->job          = '';
                    
                    setlocale(LC_ALL, 'es_ES');
                    $origDate = $content->publishDate;
                    
                    $oContent->date         = strftime("%e de %B de %Y", strtotime($origDate));
                    
                    $infoContents[] = $oContent;
                    
                    break;
                
                //Doctrina Administrativa
                case '29':
                    
                    $oContent               = new \stdClass();
                    $oContent->type         = 'Doctrina Administrativa';
                    $oContent->category     = $content->category;
                    $oContent->title        = $content->title;
                    $oContent->content      = $content->body;
                    $oContent->subtitule    = $content->edeBaseDate;
                    $oContent->author       = '';
                    $oContent->job          = '';
                    
                    setlocale(LC_ALL, 'es_ES');
                    $origDate = $content->publishDate;
                    
                    $oContent->date         = strftime("%e de %B de %Y", strtotime($origDate));
                    
                    $infoContents[] = $oContent;
                    
                    break;
                
                //Órganos Consultivos
                case '30':
                    
                    $oContent               = new \stdClass();
                    $oContent->type         = 'Órganos Consultivos';
                    $oContent->category     = '';
                    $oContent->title        = $content->title;
                    $oContent->content      = $content->body;
                    $oContent->subtitule    = '';
                    $oContent->author       = '';
                    $oContent->job          = '';
                    
                    setlocale(LC_ALL, 'es_ES');
                    $origDate = $content->publishDate;
                    
                    $oContent->date         = strftime("%e de %B de %Y", strtotime($origDate));
                    
                    $infoContents[] = $oContent;
                    
                    break;
                
                //Respuesta de los tribunales
                case '31':
                    
                    $oContent               = new \stdClass();
                    $oContent->type         = 'Respuesta de los tribunales';
                    $oContent->category     = '';
                    $oContent->title        = $content->title;
                    $oContent->content      = $content->body;
                    $oContent->subtitule    = '';
                    $oContent->author       = $content->signature;
                    $oContent->job          = $content->edeJobAuthor;
                    
                    setlocale(LC_ALL, 'es_ES');
                    $origDate = $content->publishDate;
                    
                    $oContent->date         = strftime("%e de %B de %Y", strtotime($origDate));
                    
                    $infoContents[] = $oContent;
                    
                    break;
            }
        }
        
        return $infoContents;
        
    }
}
