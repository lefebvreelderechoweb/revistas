<?php
namespace Library\Builder\Util;

 class Origin {

    /**
     * Obtenemos un objeto del contenido de un RSS
     * 
     * @param string $url
     */
    public function getRSSObject($url, $root = 'item') {
        //Limpiamos los warnings
        libxml_use_internal_errors(true);
        //Obtenemos el contenido desde una url
        $listOrigin = '';
        $xmlOrigin  = $this->curl_get_contents($url);
        
        if ($xmlOrigin) {
            $listOrigin = simplexml_load_string($xmlOrigin);
            if ($listOrigin && ($listOrigin->getName() === 'rss')) {
                $listOrigin = $listOrigin->channel->{$root};
            }else{
                $listOrigin = $listOrigin->{$root};
            }
        }
       
        //Limpiamos los warnings
        libxml_clear_errors();
        return $listOrigin;
    }
    
    /**
     * Obtenemos en objecto del contenido de un web service
     * 
     * @param string $url
     */
    public function getWSObject($url){  
        $client = new SoapClient($originUrl, array('cache_wsdl' => WSDL_CACHE_NONE));
        $result = $client->__call($originWsAction, ['body' =>$argsParamsIn]);

        return $result;
    }
    
    /**
     * Obtenemos el contenido por la url
     * con y sin certificado SSL
     * 
     * @param string $url
     */
    public function curl_get_contents($url)
    {
       $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, utf8_decode($url));
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       curl_setopt($ch, CURLOPT_USERAGENT,
                   'Mozilla/5.0 '.
                   '(Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) '.
                   'Gecko/20080311 Firefox/2.0.0.13');
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
       $data = curl_exec($ch);
       curl_close($ch);
       return $data;
    }
    
    /**
     * Busca en una matriz
     * 
     * @param object $object
     * @param string $key
     * @param boolean $returnAllItems
     */
    public function searchObjectByKey($object, $key, $returnAllItems = false){
        $arrIt = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($object));

        foreach ($arrIt as $sub) {
            $subArray = $arrIt->getSubIterator();
            if (isset($subArray[$key])) {
                $outputArray[] = iterator_to_array($subArray);
            }
        }
        /*
         * Si returnAllItems es true devolvemos un array con las coincidencias
         * si es false devolvemos el valor
         */
        return $returnAllItems ? $outputArray : $outputArray[0][$key];
    }
    
    /**
     * Busca en una matriz y devuelve el número de coincidencias
     * 
     * @param object $object
     * @param string $key
     * @param string $value
     * @return int count
     */
    public function countValuesByKey($object, $key, $value){
        $arrIt = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($object));
        
        foreach ($arrIt as $sub) {
            $subArray = $arrIt->getSubIterator();
            if (isset($subArray[$key]) && $subArray[$key] == $value) {
                $outputArray[] = iterator_to_array($subArray);
            }
        }
        /**
         * Devolvemos el numero de coincidencias
         */
        return is_array($outputArray) ? count($outputArray) : 0;
    }
    
    /**
     * Obtenemos el valor de una propiedad segun la ruta
     * La ruta sera las propiedades separadas por lo que le pasemos
     * por parametro, por defecto dejaremos -> para recalcar
     * que es un objeto
     * 
     * @param object $object
     * @param string $path
     * @param string $separator
     * @return string $object
     */
    public function getValuePropertyObjectByPath($object, $path, $separator = '->'){
        $arrPath = explode($separator, $path);
        foreach ($arrPath as $property){
            $object = $object->{$property};
        }
        return $object;
    }
}
