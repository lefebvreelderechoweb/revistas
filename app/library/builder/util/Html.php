<?php
namespace Library\Builder\Util;

use Library\builder\Components;

class Html {

    /**
     * Minificamos el HTML
     * 
     * @param string $html
     * @return string $min : HTML minificado, valgase la redundancia
     */
    public static function minifyHtml($html) {
        $search = array(
            '/(\n|^)(\x20+|\t)/',
            '/(\n|^)\/\/(.*?)(\n|$)/',
            '/\n/',
            '/\<\!--.*?-->/',
            '/(\x20+|\t)/',   # Delete multispace (Without \n)
            '/\>\s+\</',      # strip whitespaces between tags
            '/(\"|\')\s+\>/', # strip whitespaces between quotation ("') and end tags
            '/=\s+(\"|\')/'); # strip whitespaces between = "'

        $replace = array(
            "\n",
            "\n",
            " ",
            "",
            " ",
            "><",
            "$1>",
            "=$1");

        $html = preg_replace($search, $replace, $html);
        return $html;
    }
    
    /**
     * Obtenemos los HTML a repetir
     * Obviamos los HTML repetidos para no tener que volver a cargar el contenido
     * 
     * @param string $html
     * @param string $elementHtml
     * @param string $attr
     * @param string $value
     * @return array $loops
     */
    public function getLoopsHTML($html, $elementHtml = '*', $attr = 'data-loop', $value = 'true'){
        $dom = $this->initDom($html);
        //Obtenemos el nodo necesario a repetir
        $xp      = new \DOMXPath( $dom );
        $element = $xp->query( '//'.$elementHtml.'[@'.$attr.'="'.$value.'" ]' );
        $loops   = array();
        if( !empty( $element ) ){
            foreach( $element as $key=>$node ){
                array_push($loops, urldecode(trim($dom->saveHTML($node))));
            }
        }
        $dom = null; 
        return array_unique($loops);
    }
    
    /**
     * Obtenemos las etiquetas a remplazar desde un string
     * 
     * @param string $string
     * @return $attributes
     */
    public function stringsByTags($string){
        $attributes = [];
        
        preg_match_all(
			'/\{\{\@\w+\}\}/',
			$string,
			$attributes
		); 
        
        return $attributes;
    }
    
    /**
     * Reemplazamos los tags de contenido
     * 
     * @param string $string
     * @param object $object
     * @return string HTML
     */
    public function replaceStringFields($string, $object){
        $attributes = $this->stringsByTags($string)[0];
        
        $valuesAttributes = array();
        
        foreach ($attributes as $attribute) {
            $cleanAttribute = str_replace(array('{{@', '}}'), '', $attribute);
            $valueAttribute = !empty($object->{$cleanAttribute}) 
                              ? htmlspecialchars($object->{$cleanAttribute}, ENT_QUOTES) 
                              : '';
            array_push($valuesAttributes, $valueAttribute);
        }
        
        return str_replace($attributes, $valuesAttributes, $string);
    }
    
    /**
     * Devolvemos el html con el contenido actual
     * 
     * @param type $html
     * @param type $list
     * @return string $finalHtml
     */
    public function getFullLoopHTML($html, $list){
        $finalHtml = '';
        
        if (is_array($list) && count($list) > 0) {
            foreach ($list as $item) {
                $finalHtml .= $this->replaceStringFields($html, $item);
            }
        } else {
            $finalHtml = _('<p class="component-no-content" data-loop="true">No hay contenido. Pruebe más tarde.</p>');
        }

        return $finalHtml;
    }
    
    /**
     * Actualizamos todos los HTML necesarios con loop
     * 
     * @param string $html
     * @return string $finaHtml
     */
    public function getFullLoopsHTML($html){
        $finalHtml = '';
        //Comprobamos que necesita actulizar algun componente
        if (preg_match("/data-has-loop=\"true\"/i", $html)) {
            $components = new Components(false, false);
            $listViews = array();
            
            $dom = $this->initDom($html);
            //Obtenemos los nodos necesarios a actualizar
            $xp = new \DOMXPath($dom);
            $element = $xp->query('//*[@data-has-loop="true" ]');

            if (!empty($element)) {
                foreach ($element as $key => $node) {
                    //Nodo actual
                    $nodeHtml     = $dom->saveHTML($node);
                    
                    /**
                     * Debemos reemplazar solo los elementos loop para no perder
                     * el contenido anadido manualemente
                     */
                    $domNode      = $this->initDom($nodeHtml);
                    $xpNode       = new \DOMXPath($domNode);
                    $nodeElements = $xpNode->query('//*[@data-loop="true" ]');
                    if(!empty($nodeElements) && count($nodeElements) > 0){
                        //Obtenemos el nombre del componente
                        $componentName = $this->getValueAttr($nodeHtml);
                        
                        /**
                         * Guardamos las vistas en un array
                         * para que si se repite en algun sitio el mismo 
                         * componente, no se necesario volver a instanciar
                         */
                        if (!isset($listViews[$componentName])) {
                            $loopHtml          = '';
                            $component         = $components->getInstance($componentName, false);
                            $domComponent      = $this->initDom($component->view);
                            $xpComponent       = new \DOMXPath($domComponent);
                            $componentElements = $xpComponent->query('//*[@data-loop="true" ]');
                            //Concatenamos el html de los elementos para obtener un html final
                            foreach($componentElements as $componentElement){
                                $loopHtml    .= $domComponent->saveHTML($componentElement);
                            }
                            $listViews[$componentName] = $loopHtml;
                        }
                        
                        //Sustituimos el contenido del HTML del componente por el actual
                        foreach($nodeElements as $key => $nodeElement){
                            if (count($nodeElements) !== ++$key) {
                                $nodeElement->parentNode->removeChild($nodeElement);
                            } else {
                                $htmlNodeElement = $domNode->createTextNode($listViews[$componentName]);
                                $nodeElement->parentNode->replaceChild($htmlNodeElement, $nodeElement);
                            }
                        }
                        
                        //Pasamos el texto del nodo a html y minificamos
                        $finalNodeHtml = html_entity_decode($domNode->saveHTML($domNode->documentElement));
                        $finalNodeHtml = $this->noMainHtmlTags($this->clearHTML($finalNodeHtml));
                        
                        //Creamos el nodo por el que reemplazaremos
                        $nodeLoop = $dom->createTextNode($finalNodeHtml);
                        $node->parentNode->replaceChild($nodeLoop, $node);
                    }
                }
            }
            //Pasamos el texto a html y minificamos
            $finalHtml = html_entity_decode($dom->saveHTML($dom->documentElement));
            $finalHtml = $this->noMainHtmlTags($this->clearHTML($finalHtml));
        } else {
            $finalHtml = $this->minifyHtml($html);
        }

        return $finalHtml;
    }
    
    /**
     * Devolvemos el valor de un attributo 
     * 
     * @param string $html
     * @param string $attr
     * @return string
     */
    public function getValueAttr($html, $attr = 'data-component'){
        preg_match( '@'.$attr.'="([^"]+)"@' , $html, $match );
        return array_pop($match);
    }
    
    /**
     * Limpiamos el html
     * 
     * @param string $html
     * @return string $finalHtml
     */
    public function clearHTML($html){
        $find      = array('@ data-component-name+="[^"]+"@',
                           '@ data-id+="[^"]+"@',
                           '@ data-ver+="[^"]+"@');
        $replace   = '';
        return $this->minifyHtml(preg_replace($find, $replace, $html));
    }
    
    /**
     * Obtenemos los HTML a repetir de los componentes
     * 
     * @param string $html
     * @param object $list
     * @param string $elementHtml
     * @param string $attr
     * @param string $value 
     * @return string $finalHtml
     */
    public function getFinalHtmlbyComponent($html, $list, $elementHtml = '*', $attr = 'data-loop', $value = 'true'){
        $dom = $this->initDom($html);
        //Obtenemos el nodo necesario a repetir
        $xp        = new \DOMXPath( $dom );
        $element   = $xp->query( '//'.$elementHtml.'[@'.$attr.'="'.$value.'" ]' );
        
        $finalHtml = '';
        if( !empty( $element ) ){
            foreach( $element as $key=>$node ){
                $nodeHtml  = urldecode($dom->saveHTML($node));
                //Creamos el nodo por el que reemplazaremos
                $nodeLoop = $dom->createTextNode($this->getFullLoopHTML($nodeHtml, $list));
                //Reemplazamos el nodo por el nuevo
                $node->parentNode->replaceChild($nodeLoop, $node);
            }
        }
        
        //Pasamos el texto a html y minificamos
        $finalHtml = html_entity_decode($dom->saveHTML($dom->documentElement));
        
        return $this->noMainHtmlTags($finalHtml);
    }
    
    /**
     * Eliminamos las etiquedas DOCTYPE, HTML, BODY
     * 
     * @param string $html
     * @return string
     */
    public function noMainHtmlTags($html){
        return preg_replace('~<(?:!DOCTYPE|/?(?:html|body))[^>]*>\s*~i', 
                            '',
                            $html);
    }
    
    /**
     * Para solucionar conflictos con los cierres de etiquetas
     * y todo lo referente DOMDocument dejamos todas las etiquetas
     * con el cierre correspondiente
     * 
     * @param string $html
     * @return string $finalHtml
     */
    public function addClosedTags($html){
        $voidTags = array('area','base','br','col','command','embed','hr','img','input','keygen','link','meta','param','source','track','wbr');
        $regEx = '#<('.implode('|', $voidTags).')(\b[^>]*)>#';
        $html = preg_replace($regEx, '<\\1\\2 />', $html);
        return $html;
    }
    
    /**
     * Inicializamos la clase Domdocument
     * 
     * @return \DOMDocument
     */
    public function initDom($html){
        $dom = new \DOMDocument();
        //Limpiamos los warnings
        libxml_use_internal_errors(true);
        $dom->loadHTML(mb_convert_encoding($html, 'HTML-ENTITIES', 'UTF-8'));
        libxml_clear_errors();

        return $dom;
    }
}
