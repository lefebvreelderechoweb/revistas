<?php
namespace Library\Builder;

class Components{

    /**
     * Como no sabemos si la app tiene definido DS
     * la definimos en la misma clase como constante
     */
    const DS = '/';

    /**
     * El path de la carpeta en la
     * que se encuentran los componentes
     * 
     * @var string
     */
    private $path;

    /**
     * Para evitar hacer comprobaciones
     * antes de un foreach
     * 
     * @var array
     */
    private $components = [];
    
    /**
     * Inicializamos la clase
     * 
     * @param string $zone Filtramos si fuera necesario el tipo de componente
     * @param boolean $getAllComponents True si listamos todo los componentes
     */
    public function __construct($zone = false, $getAllComponents = true) {
        $this->setPath(dirname(__FILE__) . self::DS . 'components' . self::DS);
        if($getAllComponents){
            $this->getComponentsByZone($zone);
        }
    }

    /**
     * Obtenemos $path
     */
    public function getPath(){
        return $this->path;
    }
    
    /**
     * Actualizamos el valor de $path
     * 
     * @param string $path
     */
    public function setPath($path){
        $this->path = $path; 
    }
    
    /**
     * Obtenemos $components
     */
    public function getComponents(){
        return $this->components;
    }
    
    /**
     * Actualizamos $components
     * 
     * @param array $components
     */
    public function setComponents($components){
        $this->components = $components;
    }
    
    /**
     * Devuelve todos los componentes
     * en función del área.
     * 
     * @param string $zone
     */
    public function getComponentsByZone($zonesList = false) {
        //Pasamos $zonesList a un array si fuera necesario
        $zones = [];
        if(is_string($zonesList)){
            $zonesList = explode(',', $zonesList);
        }
        $componentsModel   = new \Models\LedComponent();
        $componentsByZones = $componentsModel->getComponentsByZones($zones);
        
        $components = [];
        foreach ($componentsByZones as $key=>$component) {
            $componentInstance = $this->getInstance($component->folder);
            if (is_object($componentInstance)) {
                $components[$key] = $componentInstance;
            }
        }
        
        $this->setComponents($components);
    }
    
    /**
     * Devuelve todos los componentes
     * en función del área desde el directorio
     * 
     * @param string $zone
     */
    public function getComponentsByZoneFromDir($zone = false) {

        $dirs = glob($this->getPath() . '*', GLOB_ONLYDIR);
        $components = [];

        foreach ($dirs as $key => $dir) {
            $component = $this->getInstance(basename($dir));
            if (is_object($component)) {
                /**
                 * Si no se pasa zona se devuelven todos
                 */				
                if (!$zone || in_array($zone, $component->zones) || $component->zones == 'all') {
                    $components[$key] = $component;
                }
            }
        }
        $this->setComponents($components);
    }

    /**
     * Instanciamos el componente segun id o nombre
     * 
     * @param mixed $id
     * @param boolean $live True inserta las etiquetas necesarioas en el 
     *                      componente para la modificacion de los campos ledLive
     */
    public function getInstance($name, $live = true) {
        $output    = false;
        $className = ucwords($name);
        $classPath = $this->getPath() . $name . self::DS . $className . '.php';
        if (file_exists($classPath)) {
            //Name space de la clase del componente
            $class = 'Library\\builder\\components\\' . $name . '\\' . $className;
            if (class_exists($class)) {
                $output = new $class([], $live);
            }
        }
        return $output;
    }
    
    /**
     * Anadimos donde fuera o fuese necesario los estilos de los componente
     * La hacemos estaticas para no inicializar la clase y no cargar 
     * todos los componentes
     *  
     * @param mixed $css Ejemplo: $css = $this->assets->collection('commonCss');
     */
    public static function addComponentsStyles($css){
        if ($css) {
            //Ruta donde estan los estilos de los componentes
            $baseCssPaths = BASE_PATH . self::DS 
                            . 'html' . self::DS;
            $cssPaths     = 'css' . self::DS
                            . 'library' . self::DS
                            . 'builder' . self::DS
                            . 'components' . self::DS;
            $typeFiles    =  '*.{css}';
            $files        = glob($baseCssPaths.$cssPaths.$typeFiles, GLOB_BRACE);
            
            foreach ($files as $key => $file) {
                $fileName = basename($file);
                $css->addCss($cssPaths.$fileName);
            }
        }
    }
    
    /**
     * Obtenemos una lista con los componentes de la carpeta
     * sin inicializarlos y sin necesidad de filtros
     * 
     * @return array $dirs
     */
    public function getComponentsList(){
        $dirs = array_map('basename', glob($this->getPath() . '*', GLOB_ONLYDIR));
        return $dirs;
    }
    
    /**
     * Comporobamos por el nombre del directorio si existe el componente
     * 
     * @param string $folder
     * @return boolean 
     */
    public function issetComponent($folder){
        return is_dir ($this->getPath().$folder);
    }
}