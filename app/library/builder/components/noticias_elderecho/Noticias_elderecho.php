<?php
namespace Library\Builder\Components\Noticias_elderecho;

use Library\builder\components\AbstractComponent;
use Library\builder\util\Html as htmlUtil;
use Library\builder\util\Origin as originUtil;

/**
 * Clase para el componente 
 */
class Noticias_elderecho extends AbstractComponent{
    
    /**
     * Configuramos el componente
     */
    public function __construct($params = [], $live = true) {
        parent::__construct([
            'version'  => 1,
            'path'     => __DIR__,
            'viewPath' => __DIR__.'/views/noticias_elderecho.phtml',
            'loop'     => 3
        ], $live);
    }

    /**
     * Añadimos los campos que va a tener 
     * el formulario del componente
     */
    public function getFields() {
        return [
            (object)[
                'label' => _('Título'),
                'live' => 'Title',
                'group' => 'Title',
                'name' => 'title',
                'type' => 'text',
                'value' => 'Últimas noticias El Derecho',
            ]
        ];
    }
    
    /**
     * Vista del componente
     */
    public function getView($html, $fields){
        //Clases que nos facilita mostrar el HTML final
        $htmlUtil      = new htmlUtil();
        $originUtil    = new originUtil();
        
        //Url de donde obtenemos el contenido
        $elDerechoFeed = 'https://elderecho.com/feed?limit='.$this->loop;
        
        //Damos la funcionalidad para los campos manuales
        $html = $this->initLedLive($html, $fields);
        
        //Obtenemos el contenido a mostrar
        $elDerechoItems = $originUtil->getRSSObject($elDerechoFeed);
        
        //Creamos un objeto simple para reemplazar las etiquetas del HTML
        $elDerechoList = array();
        $index = 0;
        foreach ($elDerechoItems as $key=>$item){
            //Esto es un ejemplo para acceder a cualquier propiedad de un objeto
            $pathUrlImage = 'image->url';
            
            $elDerechoList[$index] = (object) [
                'order'       => $index + 1,
                'title'       => $item->title,
                'link'        => $item->link,
                'image'       => $originUtil->getValuePropertyObjectByPath($item, $pathUrlImage)
            ];
            $index++;
        }
        
        //Devolvemos el HTML final
        return $htmlUtil->getFinalHtmlbyComponent($html, $elDerechoList);
    }
}
