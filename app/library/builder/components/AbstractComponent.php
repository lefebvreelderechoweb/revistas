<?php
namespace Library\Builder\Components;

use Library\builder\components\ComponentInterface;
use Library\builder\util\Html as UtilHtml;

abstract class AbstractComponent implements ComponentInterface {

    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $version;

    /**
     * @var string
     */
    public $name;

    /**
     * @var array
     */
    public $zones;

    /**
     * @var string
     */
    public $path;

    /**
     * @var string
     */
    public $html;

    /**
     * @see getSettings in any component class
     * @return array|mixed
     * Los objetos que se devueven de este método
     * se pasan a la clase ledFields.js para ser
     * convertidos a formulario
     *
     * Los keys de los objetos que se devuelven
     * siven para lo siguiente:
     *
     * label: es el nombre que genera el <label>
     *
     * live: es el name del input que sirve para
     * hacer el reemplazo de {{ledLive-*-}} con
     * los valores por defecto del componente
     *
     * group: es el nombre que se usa para generar el span
     * que marca el elemento activo dentro del builder.
     *
     * name: genera el atributo name de cualquier input.
     * y que la clase ledFields la aplica solo donde es el caso.
     *
     * typè: es el tipo de input que genera la clase ledFields.
     * hay tipos especiales como por ejemplo image que genera un
     * campo con su botón y clase correspondiente para disparar
     * la librería ledMedia.js
     *
     * value: es el valor por defecto del input.
     *
     * target: en caso de tener html dentro de ledGroup-*-, target
     * te ayuda a seleccionar el DOM correspondiente y evitar
     * que el mismo se reemplace por texto plano. Para entender
     * más a fondo como funciona esta abre ledBuilder.js y mira
     * el evento .on('keyup'....
     *
     * attr: actualiza el atributo del target, si no se rellena
     * actualiza por defecto el texto.
     *
     */
    public $fields;

    /**
     * @var string Vista completa del componente
     */
    public $view;
    
    /**
     * @var int loop Numero de repeticiones del elemento con attr data-loop
     */
    public $loop;

    /**
     * @var \DOMDocument
     */
    protected $dom;
    
    /**
     * @var boolean Si inicializamos a false no mostramos las etiquetas ledLive
     */
    public $live;
    
    /**
     * Inioializamos la clase
     */
    public function __construct($params = [], $live = true) {
        $this->initComponent($params, $live);
    }

    /**
     * Inicializamos el componente
     */
    public function initComponent($params = [], $live = true){
        $params    = (object)$params;
        $this->dom = new \DOMDocument; 
        
        $folder     = end(explode('/',$params->path));
        $component  = \Models\LedComponent::getComponentsByFolder($folder);
        
        //Zonas
        $componentZones = []; 
        foreach($component->getZones() as $zone){
            $componentZones[] = $zone->name;
        }
        
        //paths
        $path     = __DIR__ . '/' .strtolower($params->path);
        $viewPath = $path.'/views/'.strtolower($params->name).'.phtml';
        //Damos la posibilidad de que no sea editable
        $this->live       = $live;
        //Damos valor a las variables
        $this->id         = $component->id;
        $this->version    = $params->version;
        $this->name       = $component->name;
        $this->zones      = $componentZones;
        $this->loop       = $params->loop;
        $this->path       = $params->path ? $params->path : $path;
        $this->viewPath   = $params->viewPath ? $params->viewPath : $viewPath;
        $this->fields     = json_encode($this->getFields());
        $this->html       = $this->getHtml();
        $this->view       = $this->getView($this->html, $this->getFields());
    }
    
    /**
     * Obtenemos el html del componente 
     * 
     * @param $settings
     * @return mixed
     */
    public function getHtml() {
        $view = $this->viewPath;
        if(file_exists($view)){

            $html = file_get_contents($view);
            //No mostramos los warnings para que no cree conflicto si contiene etiquetas de HTML5
            libxml_use_internal_errors(true);
            $this->dom->loadHTML(
                mb_convert_encoding(
                    $html,
                    'HTML-ENTITIES',
                    'UTF-8'
                )
            );
            libxml_clear_errors();

            $output = '';
            $xpath = new \DOMXPath($this->dom);

            $nodes = $xpath->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' ledComponent ')]");

            foreach($nodes as $node){

                $node->setAttribute('id', 'component-'.$this->id);
                $node->setAttribute('data-component-name', $this->name);
                $node->setAttribute('data-id', $this->id);
                $node->setAttribute('data-ver', $this->version);
                if(is_integer($this->loop) && (int)$this->loop > 0){
                    $node->setAttribute('data-has-loop', 'true');
                }
                $node->setAttribute('data-component', end(explode('/', $this->path)));
                
                $output .= $this->dom->saveXML($node);
            }
            
            $content  = UtilHtml::minifyHtml($output);
            return $content;
        }
    }

    /**
     * Remplazamos el contenido por defecto en la vista
     * 
     * @param object $content
     * @param object $fields
     * @return html
     */
    public function initLedLive($content, $fields){
        $find = [];
        $replace = [];
        
        if(!empty($fields)) {
            foreach ($fields as $key => $input) {
                /**
                 * Con ledLive insertamos en el html
                 * los valores por defecto del componente
                 */
                $find[] = '{{ledLive' . $input->live . '}}';
                $replace[] = $input->value ? $input->value : $input->emptyValue;
                
                //Comprobamos que el HTML es editable
                if($this->live){
                    /**
                     * Activa el marcador de elementos
                     * activos dentro del componente
                     */
                    $find[] = '{{ledGroup' . $input->group . '}}';
                    $replace[] = '<span class="ledLive ledLive' . $input->group . '">';

                    /**
                     * Unicamente cierra el span
                     * de ledLive
                     */
                    $find[] = '{{/ledGroup' . $input->group . '}}';
                    $replace[] = '</span>';
                }else{
                    /**
                     * Eliminamos las etiquetas necesarias
                     */
                    $find[] = '{{ledGroup' . $input->group . '}}';
                    $replace[] = '';
                    $find[] = '{{/ledGroup' . $input->group . '}}';
                    $replace[] = '';
                }
            }
        }

        $content = str_replace($find, $replace, $content);
        return $content;
    }
    
    /**
     * Esta función es obligatoria para que devuelva
     * el listado de campos configurados en el componente
     */
    abstract public function getFields();
    
    /**
     * Esta función es obligatoria para que devuelva
     * la vista completa del componente
     * 
     * Ejemplo : return $this->initLedLive($html, $fields);
     */
    abstract public function getView($html, $fields);
}