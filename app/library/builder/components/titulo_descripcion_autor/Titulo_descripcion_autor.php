<?php
namespace Library\Builder\Components\Titulo_descripcion_autor;

use Library\builder\components\AbstractComponent;

/**
 * Clase para el componente 
 */
class Titulo_descripcion_autor extends AbstractComponent{
    
    /**
     * Configuramos el componente
     */
    public function __construct($params = [], $live = true) {
        parent::__construct([
            'version'  => 1,
            'path'     => __DIR__,
            'viewPath' => __DIR__.'/views/titulo_descripcion_autor.phtml'
        ], $live);
    }

    /**
     * Añadimos los campos que va a tener 
     * el formulario del componente
     */
    public function getFields() {
        return [
            (object)[
                'label' => _('Título'),
                'live'  => 'Title',
                'group' => 'Title',
                'name'  => 'title',
                'type'  => 'text',
                'value' => 'Lorem ipsum dolor sit amet',
            ],
            (object)[
                'label' => _('Contenido'),
                'live'  => 'Description',
                'group' => 'Description',
                'name'  => 'description',
                'type'  => 'textarea',
                'value' => 'Lorem ipsum dolor sit amet, consectetur adipiscing '.
                           'elit, sed do eiusmod tempor incididunt ut labore et '.
                           'dolore magna aliqua. Ut enim ad minim veniam, quis '.
                           'nostrud exercitation ullamco laboris nisi ut aliquip.',
                'editor' => true
            ],
            (object)[
                'label' => _('Autor'),
                'live'  => 'Author',
                'group' => 'Author',
                'name'  => 'author',
                'type'  => 'text',
                'value' => 'John Doe',
            ]
        ];
    }
    
    /**
     * Vista del componente
     */
    public function getView($html, $fields){
        return $this->initLedLive($html, $fields);
    }
}
