<?php
namespace Library\Builder\Components;

interface ComponentInterface {

    /**
     * ComponentInterface constructor.
     * @param array $params
     *
     * Se implementa en cada uno de los componentes
     */
    public function __construct($params = []);

    /**
     * Se implementa en la clase abstracta
     *
     * @return mixed
     */
    public function initComponent($params = []);

    /**
     * Se implementa en la clae abstracta
     * ya que las vistas tienen el mismo
     * nombre que el propio componente
     *
     * @param object
     * @return mixed
     */
    public function getHtml();

    /**
     * Se implementa en la clase de cada componente
     *
     * @return mixed
     */
    public function getFields();
    
    /**
     * Se implementa en la clase de cada componente
     *
     * @return string $html
     */
    public function getView($html, $fields);
}