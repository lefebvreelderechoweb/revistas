<?php

use Phalcon\Acl;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Acl\Role;
use Phalcon\Acl\Adapter\Memory as AclList;
use Phalcon\Acl\Resource;

class SecurityPlugin extends Plugin {

    public function getAcl() {
        
        if (!isset($this->persistent->acl)) {
            
            // Create the ACL
            $acl = new AclList();

            // The default action is DENY access
            $acl->setDefaultAction(
                Acl::DENY
            );

            //Roles para la aplicación
            $roles = ['guests' => new Role('Guests')];

            //Extraigo todos los roles que hay en bbdd actualmente
            $rolesList = \Models\LedRol::search();

            foreach ($rolesList as $rol) {
                $index = $rol->rolName;
                $index = preg_replace('/\s*/', '', $index);
                $index = strtolower($index);

                //Añado más roles a la aplicación además del de por defecto (guests)
                $newRole = new Role($rol->rolName);
                $roles[$index] = $newRole;
            }

            foreach ($roles as $role) {
                $acl->addRole($role);
            }

            // Private area resources (backend)
            $privateResources = [
                'magazine'   => ['*'],
                'generatepdf'=> ['*'],
                'issue'      => ['*'],
                'author'     => ['*'],
                'online'     => ['*'],
                'media'      => ['*'],
                'origin'     => ['*'],
                'component'  => ['*']
            ];

            foreach ($privateResources as $resourceName => $actions) {
                $acl->addResource(new Resource($resourceName), $actions);
            }

            // Public area resources (frontend)
            $publicResources = [
                'auth'      => ['index'],
                'error'     => ['error404'],
                'session'   => ['login'],
                'import'    => ['changeDate'],
                'rss'       => ['*']
            ];

            foreach ($publicResources as $resourceName => $actions) {
                $acl->addResource(
                    new Resource($resourceName), $actions
                );
            }

            // Grant access to public areas to both users and guests
            foreach ($roles as $role) {
                foreach ($publicResources as $resource => $actions) {
                    $acl->allow( $role->getName(), $resource, '*' );
                }
            }

            // Grant access to private area only to role Users
            foreach ($privateResources as $resource => $actions) {
                foreach ($actions as $action) {
                    $acl->allow('Admin', $resource, $action);
                }
            }
            
            $this->persistent->acl = $acl;
        }
        return $this->persistent->acl;
    }

    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher) {
        
        // Check whether the 'auth' variable exists in session to define the active role
        $auth = $this->session->get('user');

        if (!$auth) {
            $role = 'Guests';
        } else {
            $role = $auth->rolName;
        }
        
        // Take the active controller/action from the dispatcher
        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();

        // Obtain the ACL list
        $acl = $this->getAcl();

        // Check if the Role have access to the controller (resource)
        $allowed = $acl->isAllowed($role, $controller, $action);
        
        if (!$allowed) {
            // Si no tiene acceso, redirigimos al login
            //$this->flashSession->error("No tienes acceso");

            $dispatcher->forward(
                [
                    'controller' => 'auth',
                    'action' => 'index',
                ]
            );

            // Returning 'false' we tell to the dispatcher to stop the current operation
            return false;
        }
    }

}
