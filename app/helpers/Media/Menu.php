<?php
namespace Helpers\Media;
use Phalcon\Mvc\User\Component;
use mikehaertl\wkhtmlto\Pdf;

/**
 * Created at: 15/03/2018 15:40 with PhpStorm by ext-jgafiu
 * @author Julian Gafiu <weblysolutions@gmail.com>
 * @author website: https://www.clickmedia.es
 * @copyright © 2017 Lefebvre – El Derecho S.A.
 */
class Menu extends Component {


    public static function Build($dirs, $args = [], $index = null) {
        /**
         * Convertimos a objeto para
         * hacer la llamada más simple
         */
        $params = (object)$args;

        /**
         * En el primer loop se asigna
         * una clase por defecto y a partir
         * de la primera llamada recursiva
         * se carga la clase que se para
         * por parámetro.
         */
        $ulClass = isset($params->ulClass) ? $params->ulClass : 'content-tree';

        /**
         * A partir del segundo loop agregamos
         * los atributos necesarios para generar
         * el menu desplegable.
         */
        $ulSuffix = isset($params->ulSuffix) ? $params->ulSuffix : '';

        /**
         * La clase por defecto de todos
         * los menus. se utiliza como selector
         * general en css
         */
        $liClass = 'menu-';

        /**
         * Si el menu que se genera no trae elementos
         * en el array de submenus asignamos el href
         */
        $anchor = 'javascript:;';

        /**
         * La clase del anchos sigue las mismas
         * reglas que los demás elementos: Parámetro
         * por defecto o parámetro que se para
         * por referencia.
         */
        $anchorClass = isset($params->anchorClass) ? $params->anchorClass : 'media-dir';

        /**
         * Los anchors que tienen un submenu deben
         * tener disponibles los atributos data-target
         * y demás. mira la llamada recursiva más abajo
         */
        $anchorSuffix = isset($params->anchorSuffix) ? $params->anchorSuffix : '';





		/*
		 * Recogemos las áreas del usuario para pintar el árbol de carpetas
		 * correspondientes
		 */

		$di				= \Phalcon\DI::getDefault();
		$actualArea		= $di->get('config')->repository->subDir;
        
        $tools = new \Helpers\Tools\Tools();
        $actualAreaName = $tools->clearString(html_entity_decode($actualArea));

        echo '<ul class="' . $ulClass . '"' . $ulSuffix . '>';

        foreach( $dirs as $key => $dir ) {

			$principalParent = isset($params->principalParent) ? $params->principalParent : $dir->name;

			$areaNameComparision = str_replace( array(" ", "_"), "", $actualAreaName );
			$areaNameComparision = $tools->clearString($areaNameComparision);

			/*
			 * Comparamos por el nombre de carpeta ya que el servicio web
			 * no nos devuelve el 'id' de la carpeta ya que no existe
			 */
			if( $principalParent == $areaNameComparision ):

            /**
             * El current key se tiene que generar
             * dentro del loop.
             */
            $currentKey = isset($params->currentKey) ? $params->currentKey : $key;

            $rootClass = !isset($index) ? ' rootDir' : '';

            $collapse = ' class="media-dir"';

            if (isset($dir->directories->directory)) {
                $collapse = ' data-toggle="collapse" aria-expanded="false" class="collapsed media-dir"';
                if (is_object($dir->directories->directory)) {
                    $dir->directories->directory = array($dir->directories->directory);
                }
            }

            echo '
            <li class="active">
            <a href="#' . self::getSubmenuName($dir) . '" data-path="' . Menu::cleanPath($dir->relpath) . '"'.$collapse.'>
            <span>';
            echo isset($dir->name) ? $dir->name : 'error';
            echo '
            </span>
            </a>';

            /**
             * Entramos en la parte recursiva
             * llamando el function dentro de
             * su propio cuerpo y pasando los
             * parámetros correspondientes a los
             * submenus.
             */

            if (isset($dir->directories->directory) && is_array($dir->directories->directory)) {
                \Helpers\Media\Menu::Build($dir->directories, [
                    'ulClass'			=> 'submenu collapse',
                    'liClass'			=> 'child-of-' . $key . ' submenu-',
                    'ulSuffix'			=> ' id="'.self::getSubmenuName($dir).'" aria-expanded="false" style="height: 0px;"',
                    'currentKey'		=> $key,
                    'anchorClass'		=> 'collapsed media-dir',
                    'parent'			=> $dir->name,
					'principalParent'	=> $principalParent
                ], 1);

            } else {

                echo '</li>';
            }

			endif;

        }
        echo '</ul>';

    }


    private static function getSubmenuName($dir) {
        $output = 'javascript:;';
        if (isset($dir->directories->directory)) {
            $instance = self::getInstance();
            $output = 'submenu-' . $instance->filter->sanitize($dir->name, 'string');
        }
        return $output;
    }


    private static function cleanPath($path){
        return str_replace(['paginaspublicas\\'], [''], $path);
    }


    private static function getInstance() {
        return new self();
    }
    
    /**
	 * Create PDF & Image PDF
     * Necesitamos la libreria phpwkhtmltopdf : composer require mikehaertl/phpwkhtmltopdf
	 */
	public function generatePdfImage( $id = null, $html, $tmpDir, $pdfDir, $previewNewsDir ){

		if( !$id ){
			return false;
		}else{

			$result = array();
			$result['pdf'] = false;
			$result['img'] = false;

			/*
			 * Cramos objeto PDF
			 */

			$path            = $pdfDir.$id.'.pdf';
            $pathPdfToRemove = $path;

			// Borramos siempre fichero para actualizarlo
			if(file_exists($path) ){
				unlink($path);
			}

			$pdf = new Pdf(array(
				'binary' => '/opt/wkhtmltopdf/bin/wkhtmltopdf',
				'tmpDir' => $tmpDir,
				'commandOptions' => array(
					'useExec' => true,
					'procEnv' => array(
						'LANG' => 'en_US.utf-8',
					),
				),
				'page-height'	=> '1800px',
				'page-width'	=> 650,
				'no-outline',
				'margin-top'    => 10,
				'margin-right'  => 20,
				'margin-bottom' => 10,
				'margin-left'   => 20,
				// Default page options
				'enable-smart-shrinking',
                'user-style-sheet' => 'css/libraries/pdf/pdf.css',
                'ignoreWarnings' => true,
			));

			$ret = $pdf->addPage($html);
            
            if (!$ret) {
                $html = $this->clearHtml($html);
                $ret = $pdf->addPage($html);
            }
            
            /*if (!$pdf->saveAs($path)) {
                $error = $pdf->getError();
                // ... handle error here
                echo $error;
                die();
            }*/
            
			$ret = $pdf->saveAs($path);
			$result['pdf'] = $ret;


			// Creamos imagen
			$pathImg = $previewNewsDir.$id.'.jpg';

			// Borramos siempre fichero para actualizarlo
			if(file_exists($pathImg) ){
				unlink($pathImg);
			}

			$im = new \Imagick();
			$pathPDF = $path.'[0]';   //[0] for the first page

			$im->readimage($pathPDF);
			$im->setImageFormat('jpg');

			// Realizamos recorte
			$width		= 395;
			$height		= 600;
			$posX		= 45;
			$posY		= 15;
			$im->cropImage( $width, $height, $posX, $posY );

			// Ruta de la imagen
			$r = file_put_contents($pathImg, $im);
			$result['img'] = $r;

            //Eliminamos el pdf
            if(file_exists($pathPdfToRemove) ){
				unlink($pathPdfToRemove);
			}

			return $result;
		}
	}
    
	public function clearHtml( $html ) {
        $expMedia = '/@media[\w\W]*\}\s*}/i';
        $expCom   = '/<!--(.|\s)*?-->/mi';
        $expFont  = '/<link\s.*(\/\/fonts.*?)+"\s\/>?/mi';
        
        $parseHtml = preg_replace_callback_array(
            [
                $expMedia => function ($matches) {
                    return str_replace($matches[0], '', $matches[0]);
                },
                $expCom => function ($matches) {
                    return str_replace($matches[0], '', $matches[0]);
                },
                $expFont   => function ($matches) {
                    return str_replace($matches[1], '', $matches[0]);
                }
            ],
            $html
        );
            
        return $parseHtml;
	}

}
