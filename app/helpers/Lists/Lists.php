<?php
namespace Helpers\Lists;
use Helpers\Helper\Helper;

class Lists extends Helper {
    
    /**
     * Eliminamos informacion de un objeto
     * 
     * @param array $props
     * @return mixed $obj
     */
    public function unsetProps($obj, $props){
        foreach($props as $prop){
            unset($obj->{$prop});
        }
        return $obj;
    }
    
    /**
     * Eliminamos las propiedades de un listadod e objectos
     * 
     * @param mixed $list
     * @param array $props
     * @return mixed
     */
    public function unsetListProps($list, $props){
        $newList = [];
        
        foreach($list as $key=>$item){
            $newList[] = $this->unsetProps($item, $props);
        }
        
        return $newList;
    }
    
    /**
     * Devolvemos un string de las propiedades de un listado
     * separado por un caracter
     * 
     * @param mixed $list
     * @param string $prop
     * @param string $char
     * @return string
     */
    public function propSeparatedByChar($list, $prop = 'id', $char = ','){
        $ids = '';
        
        foreach($list as $item){
            $ids .= $item->{$prop}.$char;
        }
        $ids = $ids ? rtrim($ids, $char) : $ids;
        
        return $ids; 
    }
    
    /**
     * Compromas en un listado de objetos si todos estan vacios
     * 
     * @param mixed $obj
     * @return boolean $listObjectsIsEmpty
     */
    public function listObjectsIsEmpty($obj){
        $listObjectsIsEmpty = true;
        
        foreach($obj as $item){
            if(count($item) > 0){
                $listObjectsIsEmpty = false;
            }
        }
        
        return $listObjectsIsEmpty;
    }
}