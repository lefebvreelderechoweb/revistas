<?php
namespace Helpers\Tools;
use Helpers\Helper\Helper;

class Tools extends Helper {
    /**
     * @param string $url
     * Obtenemos el contenido por la url
     * con y sin certificado SSL
     */
    public function curl_get_contents($url)
    {
       $ch = curl_init();
       curl_setopt($ch, CURLOPT_URL, $url);
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
       curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
       curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
       $data = curl_exec($ch);
       curl_close($ch);
       return $data;
    }
    
    /**
     * @param array $array
     * @return bool
     */
    public function arrayHasEmptyValues($array = []) {
        $output = false;
        if (!empty($array)) {
            foreach ($array as $key => $value) {
                if ($value == '') {
                    $output = true;
                }
            }
        }

        return $output;
    }

    /**
     * @param string $file
     * Elimina un archivo si existe
     */
    public function deleteFileIfExists($file){
        if(file_exists($file)){
            chown($file, 'www-data');
            unlink($file);
        }
    }

    /**
     * @param string $date
     * Formateamos la fecha según parámetro del config
     */
    public function formatStringToDate($date, $caracter = "-"){
        $output = false;
        
        if(!empty($date)){
            $output = date($this->config->application->formatDateList ,strtotime($date));
            $output = str_replace("-", $caracter, $output);
        }
        
        return $output;
    }
    
    /**
     * $param int $hour
     * Pasamos un entero a formato hora punta     
     */
    public function integerToHour($hour){
        $output = 0;
        
        if(!empty($hour) && is_int((int)$hour)){
            $output = (string)sprintf("%02d", (int)$hour);
            $output .= ":00 h";
        }
        
        return $output;
    }
    
    /**
     * $param string $html
     * Devolvemos el contenido del body
     * para no duplicar header, html, doctype...
     * Dejar en $components false si no queremos 
     * aparezca las acciones de edicion
     */
    public function getBodyContentHtml($html, $components = false){
        $output = "";
        
        if(!empty($html)){
            $dom = new \DOMDocument();
            if(!$components){
                $classReplace = array("ui-selected", "ledComponent", "live-edit-overlay");
                $html = str_replace($classReplace, "", $html);
                //Limpiamos caracteres extraños
                $all = get_html_translation_table(HTML_ENTITIES, ENT_NOQUOTES);
                $tags = get_html_translation_table(HTML_SPECIALCHARS, ENT_NOQUOTES);
                $html = strtr($html, array_diff($all, $tags));
            }
            @$dom->loadHTML(utf8_decode($html));
            foreach($dom->getElementsByTagName("body")->item(0)->childNodes as $child) {
                $output .= $dom->saveHTML($child);
            }
        }
        
        return $output;
    }
    
    /**
    * @param string $string
    * Limpia de acentos y 
    * caracteres extraños 
    */
   function clearString($string, $allCharacter = true)
   {

       $string = trim($string);

       $string = str_replace(
           array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
           array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
           $string
       );

       $string = str_replace(
           array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
           array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
           $string
       );

       $string = str_replace(
           array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
           array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
           $string
       );

       $string = str_replace(
           array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
           array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
           $string
       );

       $string = str_replace(
           array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
           array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
           $string
       );

       $string = str_replace(
           array('ñ', 'Ñ', 'ç', 'Ç'),
           array('n', 'N', 'c', 'C',),
           $string
       );

       if($allCharacter){
            $string = str_replace(
                array("¨", "º", "-", "~",
                     "#", "@", "|", "!", '"',
                     "·", "$", "%", "&", "/",
                     "(", ")", "?", "'", "¡",
                     "¿", "[", "^", "<code>", "]",
                     "+", "}", "{", "¨", "´",
                     ">", "< ", ";", ",", ":",
                     "."),
                '',
                $string
            );
       }
       return $string;
   }
   
   /**
    * Obtenemos el nombre del día
    */
   public function getNameDay($numberDay){
       $nameDays = array(
           1 => _('Lunes'),
           2 => _('Martes'),
           3 => _('Miércoles'),
           4 => _('Jueves'),
           5 => _('Viernes'),
           6 => _('Sabado'),
           7 => _('Domingo')
       );
       return  $nameDays[$numberDay];
   }
   
   /**
    * Obtenemos el nombre del día en ingles
    */
   public function getNameDayEn($numberDay){
       $nameDays = array(
           1 => _('monday'),
           2 => _('tuesday'),
           3 => _('wednesday'),
           4 => _('thursday'),
           5 => _('friday'),
           6 => _('saturday'),
           7 => _('sunday')
       );
       return  $nameDays[$numberDay];
   }
   
    /**
    * @param string $html
    * @param string $element
    * @param string $valueAttr
    * @param string $attr
    * Elminamos del DOM los elementos
    */
    public function removeElementDom($html, $element = "tr", $valueAttr = "removeFromDom", $attr = "id"){
        $dom = new \DOMDocument('1.0', 'UTF-8');
        $dom->validateOnParse = false;
        @$dom->loadHTML('<?xml encoding="UTF-8">' . $html);
        $xp = new \DOMXPath( $dom );
        $col = $xp->query( '//'.$element.'[ @'.$attr.'="'.$valueAttr.'" ]' );
        if( !empty( $col ) ){
            foreach( $col as $node ){
                $node->parentNode->removeChild( $node );
            }
        }
        $html = $dom->saveHTML($dom->documentElement);
        $dom = null;        
        return $html;
    }
    
    /**
     * @param string $html
     * Remplazamos la ruta public a raiz
     * para asgurarnos que no haya imagenes rotas
     * al estar en distintos entornos
     */
    public function replacePublic($html = false){
        return str_replace('/public/assets/img/', '/assets/img/', $html);
    }
    
    /**
     * 
     * @param string $url
     * @return status
     * Devuelve un array con todas las redirecciones que tenga una url
     */
    public function getStatusCodeUrl( $url ){

        $handle     = curl_init();
        $error      = "";
        $httpCode   = "";		
        
        try{
			$opts = array(
                CURLOPT_RETURNTRANSFER  => true,
				CURLOPT_URL             => $url,
				CURLOPT_HEADER          => true,
				CURLOPT_HTTPGET         => true,
				CURLOPT_NOBODY          => true,
                CURLOPT_TIMEOUT         => 30
			);

			curl_setopt_array($handle, $opts);
			curl_exec($handle);

			$urlValues = curl_getinfo($handle);
			$httpCode   = $urlValues["http_code"];

			if($httpCode == 404) {
				$checkedUrl     = self::returnTrueHttpStatusCode($url);
				$httpCode       = $checkedUrl["http_code"];
			}
		}catch( Exception $e ){
			$error = ' Error en Curl:' . $e->getMessage();
		}


        curl_close($handle);

        return $httpCode;
    }
    
    /**
     * 
     * @param string $url
     * @return arr
     */
    public static function returnTrueHttpStatusCode($url){
        //El server detecta curl y devuelve falsos 404
        $headers    = get_headers($url,1);
        $httpCode   = substr($headers[0], 9, 3);
        $redirected = "";

        if ($httpCode == 301){
            $redirected = $headers["Location"];
        }
        $arrayAux = array(
            "http_code"     => $httpCode,
            "redirected"    => $redirected
        );

        return $arrayAux;
    }
    
    /**
     * @param string $url
     * @return boolean
     * Comprobamos que la url es accesible
     */
    public function isValidUrl($url = false){
        $isValidUrl = filter_var($url, FILTER_VALIDATE_URL);
        if($isValidUrl){
            $statusCode = self::getStatusCodeUrl($url);
            if ($statusCode != 200 && $statusCode != 400 && $statusCode != 301 && $statusCode != 302){
                $isValidUrl = false; 
            }else{
                $isValidUrl = true; 
            }
        }else{
            $isValidUrl = false; 
        }
        return $isValidUrl;
    }
    
    /**
     * 
     * @param string $url
     * @return string $url
     */
    public function getDomainFromString($url) {
        $protocolos = array('http://', 'https://', 'ftp://', 'www.');
        $url = explode('/', str_replace($protocolos, '', $url));
        return $url[0];
    }
    
    /**
     * 
     * @param string $url
     * @return string $url
     */
    public function replaceDomainReposImg($url) {

        $protocols     = array('http://', 'https://', 'ftp://', 'www.');
        $urlNoProtocol = str_replace($protocols, '', $url);
        $urlDomain     = explode('/', $urlNoProtocol);
        $url = str_replace($urlDomain[0], $this->getDI()->get('config')->application->urlBaseReposApi, $urlNoProtocol);
        return $url;
    }
    
    /**
     * 
     * @param string $url
     * @return boolean
     * Comprobamos que la url es una imagen
     */
    public function urlIsImage($url) {
        $img = '';
        if($this->isValidUrl($url)){
          $img = is_array(getimagesize($url));  
        }
        return $img;
    }
    
    /**
     * 
     * @param type $string
     * @return type
     */
    public function stringsByTags($string){
        $attributes = [];
        
        preg_match_all(
			'/\{\{\@\w+\}\}/',
			$string,
			$attributes
		); 
        
        return $attributes;
    }
    
    /**
     * Busca en una matriz
     * @param type $object
     * @param type $key
     * @param type $returnAllItems
     */
    public function searchObjectByKey($object, $key, $returnAllItems = false){
        $arrIt = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($object));

        foreach ($arrIt as $sub) {
            $subArray = $arrIt->getSubIterator();
            if (isset($subArray[$key])) {
                $outputArray[] = iterator_to_array($subArray);
            }
        }
        /*
         * Si returnAllItems es true devolvemos un array con las coincidencias
         * si es false devolvemos el valor
         */
        return $returnAllItems ? $outputArray : $outputArray[0][$key];
    }
    
    /**
     * Busca en una matriz y devuelve el número de coincidencias
     * @param object $object
     * @param string $key
     * @param string $value
     * @return int count
     */
    public function countValuesByKey($object, $key, $value){
        $arrIt = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($object));
        
        foreach ($arrIt as $sub) {
            $subArray = $arrIt->getSubIterator();
            if (isset($subArray[$key]) && $subArray[$key] == $value) {
                $outputArray[] = iterator_to_array($subArray);
            }
        }
        /**
         * Devolvemos el numero de coincidencias
         */
        return is_array($outputArray) ? count($outputArray) : 0;
    }
    
    /**
     * Obtenemos el alto y ancho de una imagen segun su origen
     * 
     * @param string $src
     * @return object $sizes
     */
    public function getImgSizes($src){
        list($imgWidth, $imgHeight) = getimagesize($src);
        $sizes = (object) [
            'height' => $imgHeight,
            'width'  => $imgWidth
        ];
          
        return $sizes;
    }
    
    /**
     * Devolvemos un string con formato
     * valido para subir las imagenes 
     * 
     * @param string $string
     * @return string $string
     */
    public function clearNameImage($string){
        $string = $this->clearString($string, false);
        
        $string = str_replace(
                array(" "),
                '-',
                $string
            );
        $string = str_replace(
                array("¨", "º", "~","`",
                     "#", "@", "|", "!", '"',
                     "·", "$", "%", "&", "/",
                     "(", ")", "?", "'", "¡",
                     "¿", "[", "^", "<code>", "]",
                     "+", "}", "{", "¨", "´",
                     ">", "< ", ";", ",", ":",),
                '',
                $string
            );
        return $string;
    }
    
    /**
     * Obtenemos un numero aleatorio mas el timestamp
     * para generar un 'id' unico
     */
    public function getRandomInt(){
        return (string)rand(0,9999).(string)time();
    }

}