<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir,
        $config->application->formsDir,
        $config->application->utilDir,
        $config->application->pluginsDir,
        $config->application->servicesDir,
        $config->application->helpersDir
    ]
);


$loader->registerNamespaces([
	'Models'			=> APP_PATH . '/models/',
	'Services'			=> APP_PATH . '/services/',
	'Forms'             => APP_PATH . '/forms/',
	'Util'              => APP_PATH . '/util/',
	'Helpers'           => APP_PATH . '/helpers/',
    'Library'           => APP_PATH . '/library/'
]);


$loader->register();

/**
 * Carga del bundle de lefebvre
 */
include BASE_PATH.'/vendor/lefebvre/loader.php';
