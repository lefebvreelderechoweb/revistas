<?php
/*
 * Modified: prepend directory path of current file, because of this file own different ENV under between Apache and command line.
 * NOTE: please remove this comment.
 */
defined('BASE_PATH') || define('BASE_PATH', getenv('BASE_PATH') ?: realpath(dirname(__FILE__) . '/../..'));
defined('APP_PATH') || define('APP_PATH', BASE_PATH . '/app');
defined('TMP_PATH') || define('TMP_PATH', BASE_PATH . '/tmp/');

$protocol = $_SERVER['REQUEST_SCHEME']. '://';
$baseUri  = $_SERVER['SERVER_NAME'];
$baseUrl  = $protocol.$baseUri.'/';

/**
 * Comprobamos el nombre del usuario para llamar a la base de datos correcta
 * A tener en cuenta que en integracion no utiliza el nombre del usuario 
 */
$baseUriExplode = explode('-', $baseUri); 
$userJail       = array_pop($baseUriExplode);
$userName       =  $userJail !== 'dev'
                   ? '_' . str_replace('ext-', '', get_current_user())
                   : '';

$userName = ( $userJail == 'qlt' ) ? '_qlt' : $userName;

$DBName         = 'revistas' . $userName;

$aConfig = [
	'database' => [
		'adapter'     => 'Mysql',
		'host'        => 'localhost',
		'username'    => 'led-wordpress',
		'password'    => 'SZDFlj123d+wsd',
		'dbname'      => $DBName,
		'charset'     => 'utf8',
	],
	'application' => [
		'appDir'         => APP_PATH . '/',
		'controllersDir' => APP_PATH . '/controllers/',
		'modelsDir'      => APP_PATH . '/models/',
		'formsDir'       => APP_PATH . '/forms/',
		'utilDir'        => APP_PATH . '/util/',
		'servicesDir'    => APP_PATH . '/services/',
		'migrationsDir'  => APP_PATH . '/migrations/',
		'viewsDir'       => APP_PATH . '/views/',
		'pluginsDir'     => APP_PATH . '/plugins/',
		'libraryDir'     => APP_PATH . '/library/',
		'cacheDir'       => BASE_PATH . '/cache/',
		'publicDir'      => BASE_PATH . '/html/',
		'helpersDir'     => BASE_PATH . '/helpers/',

		// This allows the baseUri to be understand project paths that are not in the root directory
		// of the webpspace.  This will break if the public/index.php entry point is moved or
		// possibly if the web server rewrite rules are changed. This can also be set to a static path.
		'baseUri'   => '/',
        'baseDomain'    => $baseUrl

	], 
    // El id es provisional, es el de páginas públicas
    // En principio los componentes no tendran imágenes
    // pero se deja hecho por si fuera o fuese necesario
    'repository' => [
        'area'        => 16,
        'baseUrl'     => 'https://content.lefebvreelderecho.com/getImage.aspx?image=',
        'subDir'      => 'revistas',
        'pagination'  => 18,
        'limitUpload' => 8,
    ],
    
    //Rutas donde se generarán los pdf's de cada numero de revista dividido por proyectos
    'routesPDFs' => [
        'revistas'  => BASE_PATH . '/html/pdfs/',
        'caf'       => '/srv/envs/'.$userJail.'/sites/cafmadrid/html/pdfs/',
    ]
];

switch ( getenv('ENVIRONMENT') ) {
	case 'PROD':
		$aConfig = array_replace_recursive($aConfig,[
            'application' => [
            ],
            'database' => [
                'adapter'     => 'Mysql',
                'host'        => 'localhost',
                'username'    => 'led_wp_revistas',
                'password'    => 'SADnmasd+df33',
                'dbname'      => 'revistas',
                'charset'     => 'utf8',
            ],
            'routesPDFs' => [
                'revistas'  => '/srv/www/revistas-bck.lefebvre.es/html/pdfs/',
                'caf'       => '/srv/www/cafmadrid-revista-derechoinmobiliario.lefebvre.es/html/pdfs/',
            ]
		]);
		break;

}
return new \Phalcon\Config( $aConfig );