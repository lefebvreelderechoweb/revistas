<?php
use Phalcon\Config\Adapter\Ini;
use Phalcon\Di;
use Phalcon\Db\Adapter\Pdo\Factory;
use Phalcon\Mvc\View;
use Phalcon\Mvc\View\Engine\Php as PhpEngine;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Flash\Direct as Flash;
use Phalcon\Flash\Session as FlashSession;
use Phalcon\Security;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Events\Manager as EventsManager;


/**
 * Shared configuration service
 */
$di->setShared('config', function () {
	return include APP_PATH . "/config/config.php";
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->setShared('url', function () {
	$config = $this->getConfig();

	$url = new UrlResolver();
	$url->setBaseUri($config->application->baseUri);

	return $url;
});


$di->set('db', function() {
    $config = $this->getConfig();
    return Factory::load((array)$config->database);
});

$di->set('security', function () {
        $security = new Security();

        // Set the password hashing factor to 12 rounds
        $security->setWorkFactor(12);

        return $security;
    }, true);

/**
 * Setting up the view component
 */
$di->setShared('view', function () {
	$config = $this->getConfig();

	$view = new View();
	$view->setDI($this);
	$view->setViewsDir($config->application->viewsDir);

	$view->registerEngines([
		'.volt' => function ($view) {
			$config = $this->getConfig();

			$volt = new VoltEngine($view, $this);

			$volt->setOptions([
				'compiledPath' => $config->application->cacheDir,
				'compiledSeparator' => '_'
			]);

			return $volt;
		},
		'.phtml' => PhpEngine::class

	]);

	return $view;
});

/**
 * Este componente controla la inicialización de los modelos, manteniendo un 
 * registro de las relaciones entre los diferentes modelos de la aplicación.
 */
$di->set('modelsManager', function(){
    return new Phalcon\Mvc\Model\Manager();
});

/**
 * SetShared es un singleton.
 */
$di->setShared('modelsMetadata', function () {
	return new MetaDataAdapter();
});


/**
 * Start the session the first time some component request the session service
 */
$di->setShared('session', function () {
	$session = new SessionAdapter();
	$session->start();
	return $session;
});

/**
 * Set es una instancia normal y corriente.
 */
$di->set('flash', function () {
	$flash = new Flash([
		'error'   => 'alert alert-danger',
		'success' => 'alert alert-success',
		'notice'  => 'alert alert-info',
		'warning' => 'alert alert-warning'
	]);
	$flash->setImplicitFlush(false);
	return $flash;
});

$di->setShared('flashSession', function () {
	$flash = new FlashSession([
		'error'   => 'alert alert-danger',
		'success' => 'alert alert-success',
		'notice'  => 'alert alert-info',
		'warning' => 'alert alert-warning'
	]);
	return $flash;
});

/**
 * Gestión de acceso a url's por roles // Gestión 404
 */
$di->set('dispatcher', function() {
        // Create an events manager
        $eventsManager = new EventsManager();
        
        // Listen for events produced in the dispatcher using the Security plugin
        $eventsManager->attach(
            'dispatch:beforeExecuteRoute',
            new SecurityPlugin()
        );
        
        // Handle exceptions and not-found exceptions using NotFoundPlugin
        $eventsManager->attach(
            'dispatch:beforeException',
            new NotFoundPlugin()
        );

        $dispatcher = new Dispatcher();

        // Assign the events manager to the dispatcher
        $dispatcher->setEventsManager($eventsManager);

        return $dispatcher;
    }
);

include BASE_PATH.'/vendor/lefebvre/services/online/includes/onlineService.php';
include BASE_PATH.'/vendor/lefebvre/services/online/includes/authService.php';
include BASE_PATH.'/vendor/lefebvre/services/cm/includes/repositoryService.php';
include BASE_PATH.'/vendor/lefebvre/services/dataBase/includes/dataBaseService.php';


$di->setShared('cmOnlineService', function () {
	$config			= new \Phalcon\Config\Adapter\Php( LEFEBVRE_PATH.'/services/cm/config/onlineService.php' );
	$service		= new \Lefebvre\Services\Cm\OnlineService( $config );

	return $service;
});

$di->setShared('sinSessionService', function () {
	$config			= new \Phalcon\Config\Adapter\Php( LEFEBVRE_PATH.'/services/online/config/googleService.php' );
	$service		= new \Lefebvre\Services\Online\GoogleService( $config );

	return $service;
});

$di->setShared('mtdtService', function () {
	$config			= new \Phalcon\Config\Adapter\Php( LEFEBVRE_PATH.'/services/online/config/mtdtService.php' );
	$service		= new \Lefebvre\Services\Online\MtdtService( $config );

	return $service;
});
