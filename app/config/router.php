<?php
$router = $di->getRouter();

/**
 * Controlador y accion por defecto
 */
/*$router->setDefaultController('magazine');
$router->setDefaultAction('list');*/
$router->add(
    '/',
    [
        'controller' => 'magazine',
        'action'     => 'index',
    ]
);

/**
 * Prioridad para el enrutamiento
 * Elimina el /index/
 */
/*$route = $router->add(
    '/login',
    [
        'controller' => 'auth',
        'action'     => 'index',
    ]
);*/

$route = $router->add(
    '/logout',
    [
        'controller' => 'auth',
        'action'     => 'logout',
    ]
);

$router->add(
    "/:controller/:action",
    array(
        "controller" => 1,
        "action"     => 2
    )
);

$router->add(
    "/:controller/:action/:params",
    array(
        "controller" => 1,
        "action"     => 2,
        "params"     => 3,
    )
);

/**
 * Media
 */
$router->add('/media/dir/:params', [
    'controller' => 'media',
    'action' => 'index',
    'dir' => 1
]);

$router->handle();