<?php

namespace Models;

class LedAccess extends \BaseModel {

    public function initialize(){
        $this->setSource('ACCESO');
        
        $this->belongsTo(
            'idUser', 
            __NAMESPACE__ . '\LedUser', 
            'id', 
            array('alias' => 'user')
        );
        
        $this->belongsTo(
            'idRol', 
            __NAMESPACE__ . '\LedRol', 
            'id', 
            array('alias' => 'rol')
        );
        
        $this->belongsTo(
            'idMagazine', 
            __NAMESPACE__ . '\LedMagazine', 
            'id', 
            array('alias' => 'magazine')
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_ACCESO'         => 'id',
            'ID_USUARIO'        => 'idUser',
            'ID_PERFIL'         => 'idRol',
            'ID_REVISTA'        => 'idMagazine',
            'TS'                => 'ts',
            'FC'                => 'fc',
            'UIC'               => 'uic',
            'UC'                => 'ud'
        );
    }
    
}