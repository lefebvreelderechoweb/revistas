<?php

namespace Models;

class LedContentIssue extends \BaseModel {

    public function initialize(){
        $this->setSource('NUMERO_CONTENIDO');
        
        $this->belongsTo(
            'idNumber',
            __NAMESPACE__ . '\LedIssue',
            'id',
            ['alias' => 'issue',
             'foreignKey' => true]
        );
        
        $this->belongsTo(
            'idContentType',
            __NAMESPACE__ . '\LedContentType',
            'id',
            ['alias' => 'contentType']
        );
        
        $this->hasMany(
            'id', 
            __NAMESPACE__ . '\LedPointView', 
            'idContentIssue',
            [
                'alias'        => 'pointView',
                'foreignKey'   => ['action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE]
            ]
        );
        
        $this->hasMany(
            'id', 
            __NAMESPACE__ . '\LedApproachResult', 
            'idContentIssue',
            [
                'alias'         => 'approachResult',
                'foreignKey'    => ['action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE]
            ]
        );
        
        //Relación Número Contenido / Autor
        $this->hasManyToMany(
            'id',                                       // field(s) of this model
            __NAMESPACE__ . '\LedAuthorIssueContent',   // table which stores the n:n relations
            'idContentIssue',                           // columns in intermediate table that refers to this model's fields
            'idAuthor',                                 // columns in intermediate that that refers to the referenced table
            __NAMESPACE__ . '\LedAuthor',               // referenced table
            'id',                                       // referenced table columns
            ['alias' => 'author']                       // array of extra options, for eg alias
        );
    }
    
    public function beforeValidationOnCreate(){
        //Creo campos por defecto
        $this->ts = date("Y:m:d H:i:s");
        $this->fc = date("Y:m:d H:i:s");
   }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_NUMERO_CONTENIDO'   => 'id',
            'ID_NUMERO'             => 'idNumber',
            'ID_TIPOCONTENIDO'      => 'idContentType',
            'REF_BITBAN'            => 'refBitban',
            'NREF'                  => 'nref',
            'TITULO'                => 'title',
            'MATERIA'               => 'category',
            'FECHA_PUBLICACION'     => 'publishDate',
            'EPIGRAFE'              => 'epigraph',
            'KEYWORDS'              => 'keywords',
            'ORDEN'                 => 'order',
            'PORTADA'               => 'cover',
            'LUGAR'                 => 'place',
            'FIRMA'                 => 'signature',
            'RESEÑA'                => 'review',
            'ENTRADILLA'            => 'resume',
            'CUERPO'                => 'body',
            'EDE_DATOS_BASE'        => 'edeBaseDate',
            'EDE_AUTOR_CARGO'       => 'edeJobAuthor',
            'EDE_BOARD_SUMMARY'     => 'edeBoardSummary',
            'META_TITLE'            => 'metaTitle',
            'META_DESCRIPTION'      => 'metaDescription',
            'TS'                    => 'ts',
            'FC'                    => 'fc',
            'UIC'                   => 'uic',
            'UC'                    => 'uc'
        );
    }
}