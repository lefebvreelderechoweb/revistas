<?php

namespace Models;

class LedZone extends \BaseModel {

    public function initialize(){
        $this->setSource('ZONA');
        
        //Relación Zona / Componente
        $this->hasMany(
            'id',
            __NAMESPACE__ . '\LedZoneComponent',
            'idComponent',                               
            [
                'alias'  => 'component'
            ]
        );
        
        $this->hasManyToMany(
            'id',                                // field(s) of this model
            __NAMESPACE__ . '\LedZoneComponent', // table which stores the n:n relations
            'idZone',                            // columns in intermediate table that refers to this model's fields
            'idComponent',                       // columns in intermediate that that refers to the referenced table
            __NAMESPACE__ . '\LedComponent',     // referenced table
            'id',                                // referenced table columns
            ['alias' => 'components']             // array of extra options, for eg alias
        );
        
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_ZONA'           => 'id',
            'NOMBRE'            => 'name', 
            'TS'                => 'ts',
            'FC'                => 'fc',
            'UIC'               => 'uic',
            'UC'                => 'uc'
        );
    }
    
}