<?php

namespace Models;

class LedContentType extends \BaseModel {

    public function initialize(){
        $this->setSource('TIPOCONTENIDO');
        
        $this->hasMany(
            'id', 
            __NAMESPACE__ . '\LedContentIssue', 
            'idContentType',
            ['alias' => 'contentIssue']
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_TIPOCONTENIDO'  => 'id',
            'TIPOCONTENIDO'     => 'contentType',
            'NICENAME'          => 'name',
            'CAMPOSREQUERIDOS'  => 'fieldRequired',
            'TS'                => 'ts',
            'FC'                => 'fc',
            'UIC'               => 'uic',
            'UC'                => 'ud'
        );
    }
    
}