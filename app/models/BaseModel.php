<?php

use Phalcon\Session\Adapter\Files as SessionAdapter;

class BaseModel extends \Phalcon\Mvc\Model {
    
    public static function search( $args = [] ){
        // $args = [ 'id' => value ];
        // $args = [ 'id' => array('operator', 'value') ];
        
        $conditions = '';
        $bing = array();
        foreach ( $args as $key => $arg ){
            
            if ( $conditions != '' )
                $conditions .= ' AND ';
            
            if (!is_array($arg)){
                $conditions .= $key . '=:' . $key . ':';
                $bing[$key] = $arg;
            }  
            else{
                $conditions .= $key . ' ' . $arg[0] . ' :' . $key . ':';
                $bing[$key] = $arg[1];
            } 
        }
        
        if ( !empty( $args ) ){
            
            $args = array (
                'conditions' => $conditions,
                'bind' => $bing
            );
        }
        
        $result = self::find($args);
        
        return $result;
    }
    
    /**
     * Valores por defecto al insertar
     */
    public function beforeValidationOnCreate(){
        $session = new SessionAdapter();
        $user    = $session->get('user');
        $userID  = $user->id;
        
        //Asignamos los valores por defecto
        $this->ts  = date("Y:m:d H:i:s");
        $this->fc  = date("Y:m:d H:i:s");
        $this->uic = $userID;
        $this->uc  = $userID;
    }
}

