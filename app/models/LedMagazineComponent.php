<?php

namespace Models;

class LedMagazineComponent extends \BaseModel {

    public function initialize(){
        $this->setSource('REVISTA_COMPONENTE');
        
        // Relacion con la tabla revista
        $this->belongsTo(
            'idMagazine', 
            __NAMESPACE__ . '\LedMagazine', 
            'id', 
            array('alias' => 'magazine')
        );
        
        // Relacion con la tabla COMPONENTE
        $this->belongsTo(
            'idComponent', 
            __NAMESPACE__ . '\LedComponent', 
            'id', 
            array('alias' => 'component')
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_REVISTA_COMPONENTE' => 'id',
            'ID_REVISTA'            => 'idMagazine',
            'ID_COMPONENTE'         => 'idComponent', 
            'TS'                    => 'ts',
            'FC'                    => 'fc',
            'UIC'                   => 'uic',
            'UC'                    => 'uc'
        );
    }
    
}