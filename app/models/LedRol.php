<?php

namespace Models;

/**
 * @property-read string $idGrupo Description
 */
class LedRol extends \BaseModel {
    
    public $id;
    public $rolName;

    public function initialize(){
        $this->setSource('PERFIL');
        
        $this->hasMany(
            'id',
            __NAMESPACE__ . '\LedUser',
            'idRol',
            ['alias' => 'user']
        );
        
        //Relación Rol / Revista
        $this->hasManyToMany(
            'id',                               // field(s) of this model
            __NAMESPACE__ . '\LedAccess',       // table which stores the n:n relations
            'idRol',                            // columns in intermediate table that refers to this model's fields
            'idMagazine',                       // columns in intermediate that that refers to the referenced table
            __NAMESPACE__ . '\LedMagazine',     // referenced table
            'id',                               // referenced table columns
            ['alias' => 'magazine']             // array of extra options, for eg alias
        );
        
        //Relación Rol / Usuario
        $this->hasManyToMany(
            'id',                               // field(s) of this model
            __NAMESPACE__ . '\LedAccess',       // table which stores the n:n relations
            'idRol',                            // columns in intermediate table that refers to this model's fields
            'idUser',                           // columns in intermediate that that refers to the referenced table
            __NAMESPACE__ . '\LedUser',         // referenced table
            'id',                               // referenced table columns
            ['alias' => 'user']             // array of extra options, for eg alias
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_PERFIL' => 'id',
            'PERFIL'    => 'rolName',
            'TS'        => 'ts',
            'FC'        => 'fc',
            'UIC'       => 'uic',
            'UC'        => 'ud'
        );
    }
    
    public function load($object){
        if(is_array($object)){
            $object = (object)$object;
        }
        
        $this->id           = $object->id;
        $this->rolName      = $object->rolName;
    }
    
    public static function search( $args = [] ){
        $result = parent::search($args);
        
        $r = [];
        
        if( count($result) > 0 ){
            foreach ($result as $stdClassUser) {
                $obj = new self();
                $obj->load($stdClassUser);
                $r[] = $obj;
            }
        }
        
        return $r;
    }
    
    
    
}