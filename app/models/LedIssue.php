<?php

namespace Models;

class LedIssue extends \BaseModel {

    public function initialize(){
        $this->setSource('NUMERO');
        
        $this->belongsTo(
            'idMagazine',
            __NAMESPACE__ . '\LedMagazine',
            'id',
            ['alias' => 'magazine']
        );
        
        $this->hasMany(
            'id', 
            __NAMESPACE__ . '\LedContentIssue', 
            'idNumber',
            ['alias'        => 'contentIssue',
             'foreignKey'   => [
                 'action' => \Phalcon\Mvc\Model\Relation::ACTION_CASCADE
             ]]
        );
        
    }
    
    public function beforeValidationOnCreate(){
        //Creo campos por defecto
        $this->ts = date("Y:m:d H:i:s");
        $this->fc = date("Y:m:d H:i:s");
   }
   
    public function beforeValidationOnUpdate(){
        $this->ts = date("Y:m:d H:i:s"); //Cambio la fecha de modificación
   }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_NUMERO'         => 'id',
            'ID_REVISTA'        => 'idMagazine',
            'REF_BITBAN'        => 'refBitban',
            'NOMBRE'            => 'name',
            'FECHA_PUBLICACION' => 'publishDate',
            'URL'               => 'url',
            'HTML'              => 'html',
            'HTML_FINAL'        => 'finalHtml',
            'SIDEBAR'           => 'sidebar',
            'SIDEBAR_FINAL'     => 'finalSidebar',
            'PREVIEW'           => 'preview',
            'URL_PDF'           => 'urlPdf',
            'TS'                => 'ts',        // Fecha de modificación
            'FC'                => 'fc',        // Fecha de creación
            'UIC'               => 'uic',       //Usuario modificador
            'UC'                => 'uc'         //Usuario creador
        );
    }
    
    /**
     * Devolvemos el contenido asociado a un numero
     * 
     * @param int $issueId
     * @param int $contentTypeId
     * @return mixed $content
     */
    public static function getContentsIssue($issueId, $contentTypeId = false){
        $contents = '';
        
        $issue = self::findFirstById($issueId);
        
        if($issue && $contentTypeId){
            $conditions = 'idContentType=:idContentType:';
            $bind       = ['idContentType'=>$contentTypeId];
            $args       = [
                    'conditions' => $conditions,
                    'bind'       => $bind
            ];
            $contents = $issue->getContentIssue($args);
        }elseif($issue && !$contentTypeId){
            $contents = $issue->getContentIssue();
        }
        
        return $contents;
    }
    
}