<?php

namespace Models;

class LedComponent extends \BaseModel {

    public function initialize(){
        $this->setSource('COMPONENTE');
        
        //Relación Componente / Revista
        $this->hasMany(
            'id',
            __NAMESPACE__ . '\LedMagazineComponent',
            'idComponent',                               
            [
                'alias'  => 'magazineComponent'
            ]
        );
        
        $this->hasManyToMany(
            'id',                                       // field(s) of this model
            __NAMESPACE__ . '\LedMagazineComponent',    // table which stores the n:n relations
            'idComponent',                              // columns in intermediate table that refers to this model's fields
            'idMagazine',                               // columns in intermediate that that refers to the referenced table
            __NAMESPACE__ . '\LedMagazine',             // referenced table
            'id',                                       // referenced table columns
            ['alias' => 'magazines']                     // array of extra options, for eg alias
        );
        
        //Relación con Zona Componente
        $this->hasMany(
            'id',
            __NAMESPACE__ . '\LedZoneComponent',
            'idComponent',                               
            [
                'alias'  => 'zoneComponent'
            ]
        ); 
        
        $this->hasManyToMany(
            'id',                                  // field(s) of this model
            __NAMESPACE__ . '\LedZoneComponent',   // table which stores the n:n relations
            'idComponent',                         // columns in intermediate table that refers to this model's fields
            'idZone',                              // columns in intermediate that that refers to the referenced table
            __NAMESPACE__ . '\LedZone',            // referenced table
            'id',                                  // referenced table columns
            ['alias' => 'zones']                    // array of extra options, for eg alias
        );
        
        //Relación  Componente / Origen
        $this->hasMany(
            'id',
            __NAMESPACE__ . '\LedComponentOrigin',
            'idComponent',
            ['alias' => 'componentOrigin']
        );
        
        $this->hasManyToMany(
            'id',                                    // field(s) of this model
            __NAMESPACE__ . '\LedComponentOrigin',   // table which stores the n:n relations
            'idComponent',                           // columns in intermediate table that refers to this model's fields
            'idOrigin',                              // columns in intermediate that that refers to the referenced table
            __NAMESPACE__ . '\LedOrigin',            // referenced table
            'id',                                    // referenced table columns
            ['alias' => 'origins']                   // array of extra options, for eg alias
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_COMPONENTE' => 'id',
            'DIRECTORIO'    => 'folder',
            'NOMBRE'        => 'name',
            'DESCRIPCION'   => 'description',
            'HTML'          => 'html', 
            'TS'            => 'ts',
            'FC'            => 'fc',
            'UIC'           => 'uic',
            'UC'            => 'uc'
        );
    }
    
    /**
     * Devolvemos el listado de componentes filtrado por zona
     * 
     * @param array $zones
     * @return mixed $result
     */
    public function getComponentsByZones($zones = false){
        $where = '';
        if($zones){
            $where = "LedZone.name ";
            foreach($zones as $zone){
                $where .= "like '$zone' OR"; 
            }
            $where = rtrim($where, ' OR');
        }
        
        $manager = $this->getModelsManager()->createBuilder();
        $manager->columns(['DISTINCT LedComponent.id, LedComponent.folder, LedComponent.name, LedComponent.description, LedComponent.html'])
                ->from(['LedComponent' => __NAMESPACE__ . '\LedComponent'])
                ->leftJoin(__NAMESPACE__ . '\LedZoneComponent', 'LedComponent.id = LedZoneComponent.idComponent', 'LedZoneComponent')
                ->leftJoin(__NAMESPACE__ . '\LedZone', 'LedZoneComponent.idZone = LedZone.id', 'LedZone')
                ->where($where);

        $query = $manager->getQuery();
        $result = $query->execute();
        
        return $result;
    }
    
    /**
     * Devolvemos el componente por su nombre de directorio
     * 
     * @param string $folder
     * @return mixed $component
     */
    public static function getComponentsByFolder($folder = false){
        $component = '';
        if($folder){
            $conditions = ['folder'=>$folder];
            $component = self::findFirst([
                    'conditions' => 'folder=:folder:',
                    'bind' => $conditions
            ]);
        }
        return $component;
    }
}