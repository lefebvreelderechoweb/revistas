<?php

namespace Models;

class LedDataType extends \BaseModel {

    public function initialize(){
        $this->setSource('TIPODATO');
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_TIPODATO'  => 'id',
            'TIPODATO'     => 'dataType',
            'TS'           => 'ts',
            'FC'           => 'fc',
            'UIC'          => 'uic',
            'UC'           => 'ud'
        );
    }
    
}