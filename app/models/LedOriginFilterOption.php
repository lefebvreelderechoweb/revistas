<?php

namespace Models;

class LedOriginFilterOption extends \BaseModel {

    public function initialize(){
        $this->setSource('FUENTED_FILTRO_OPCION');
        
        //Relacion con Fuented
        $this->belongsTo(
            'idOriginFilter',
            __NAMESPACE__ . '\LedOriginFilter',
            'id',
            ['alias' => 'originFilter',
             'foreignKey' => true]
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_FUENTED_FILTRO_OPCION'  => 'id',
            'ID_FUENTED_FILTRO'         => 'idOriginFilter', 
            'NOMBRE'                    => 'name',
            'VALOR'                     => 'value',
            'TS'                        => 'ts',
            'FC'                        => 'fc',
            'UIC'                       => 'uic',
            'UC'                        => 'uc'
        );
    }
    
}