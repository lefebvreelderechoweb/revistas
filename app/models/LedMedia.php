<?php

namespace Models;
use Phalcon\Debug\Dump;

class LedMedia extends \BaseModel{
    
    /**
     * @var object 
     */
    public $params;
    
    /**
     * @var object 
     */
    public $soap;
    
    /**
     * @var int 
     */
    public $area;

    /**
     * Inicializamos las variables
     */
    public function onConstruct() {
        $this->soap = $this->getDI()->get('repositoryService');
        $this->area = $this->getDI()->get('config')->repository->area;
        $this->params = (object) [
                'area' => $this->area
        ];
    }

    /**
     * Instancia el modelo
     */
    public static function getInstance() {
        return new self();
    }
    
    /**
     * Creamos una carpeta en el area seleccionado
     * 
     * @param string $folderName
     * @param string $subfolder
     * @return object
     */
    public function createDirectory( $folderName, $subfolder = ''){

        $this->addParam('folderName', (string)$folderName);
        $this->addParam('subfolder', (string)$subfolder);

        return $this->soap->createDirectory( 
            $this->params->area, 
            $this->params->folderName, 
            $this->params->subfolder);
    }

    /**
     * Comprobamos que el fichero existe
     * 
     * @param string $fileName
     * @param string $subFolder
     * @return mixed
     */
	public function fileExists( $fileName, $subFolder = null ){

		$this->addParam('fileName', (string)$fileName);

		if( $subFolder ){
			$this->addParam('subFolder', (string)$subFolder );
		}
        
		return $this->soap->fileExists( 
            $this->params->area, 
            $this->params->fileName,
            $this->params->subFolder);
	}

    /**
     * Eliminamos
     * @param type $fileName
     * @param type $subFolder
     * @return type
     */
	public function removeFile( $fileName, $subFolder = null ){

		$this->addParam('fileName', (string)$fileName);

		if( $subFolder ){
			$this->addParam('subFolder', (string)$subFolder );
		}
		return $this->soap->removeFile( 
            $this->params->area, 
            $this->params->fileName,
            $this->params->subFolder);
	}

    /**
     * Mostramos un listado de directorios
     * 
     * @param string $dir
     * @return object
     */
    public function getDirs($dir = null) {

        if($dir != null){
            $this->addParam('subfolder', $dir);
        }
        
        return $this->soap->getDirs(
            $this->params->area,
            $this->params->subfolder)->directory;
    }

    /**
     * Subimos un fichero
     * 
     * @param string $content
     * @param string $fileName
     * @param string $subFolder
     * @param boolean $overwrite
     * @return mixed 
     */
	public function uploadFile( $content, $fileName, $subFolder, $overwrite ){

		$this->addParam('fileName', (string)$fileName);
		$this->addParam('subFolder', (string)$subFolder);
		$this->addParam('overwrite', (bool)$overwrite);
		$this->addParam('data', $content);
        
		return $this->soap->uploadFile( $this->params->data, $this->params->fileName, $this->params->area, $this->params->subFolder , $this->params->overwrite );
	}

    public function getFiles( $dir = null, $searchTxt = null, $dateFrom = null, $dateTo = null, $pageSize = null, $pageNumber = null ){

        if( $dir ){
            $this->addParam('subFolder', (string)$dir);
        }

		if( $searchTxt !== "null" ){
            $this->addParam('filterName', (string)$searchTxt);
        }

		if( $dateFrom !== "null" ){
            $this->addParam('dateFrom', (string)$dateFrom);
        }

		if( $dateTo !== "null" ){
            $this->addParam('dateTo', (string)$dateTo);
        }

		if( $pageSize !== "null" ){
            $this->addParam('intPageSize', (int)$pageSize);
        }

		if( $pageNumber !== "null" ){
            $this->addParam('intPageNumber', (int)$pageNumber);
        }

        $allowed = ['.jpg', '.png', '.gif', 'jpeg'];
        
        $files = $this->soap->getFilesWithPagination(
            $this->params->area, 
            $this->params->subFolder, 
            $this->params->filterName, 
            $this->params->param->dateFrom, 
            $this->params->dateTo, 
            $this->params->intPageSize, 
            $this->params->intPageNumber)->file;
        
		/*
		 * Devolvemos resultado siempre en array
		 */
		if( $files ){
			$files = is_object($files) ? array( $files ) : $files;
		}
        
		$totalImages	= isset($files[0]) ? count($files) : 0;
        
		$images		= [];
		$imagesData = [];

        if( is_array( $files )){
            foreach( $files as $file ){
                if( in_array( strtolower( $file->extension ), $allowed )){
                    $images[] = $file;
                }
            }
        }
        
		if( $images ){
			foreach( $images as $image ){
				$imageData = new \stdClass();

				// Propiedades de la imagen
				$imageData->extension			= $image->extension;
				$imageData->name				= $image->name;
				$imageData->url					= $image->url;

				// Tamaños
				$imageData->sizes				= new \stdClass();
				$imageData->sizes->original		= '';
				$imageData->sizes->thumbnail	= '';

				$fileSizes						= isset( $image->sizes->fileSize ) ? (array)$image->sizes->fileSize : [];

				foreach( $fileSizes as $fileSize ){
					$imageData->sizes->{$fileSize->size} = $fileSize->url;
				}

				$imagesData[] = $imageData;
			}
		}
        
		$data					= array();
		$data['images']			= $imagesData;
		$data['totalImages']	= $totalImages;

		return $data;
    }
    
    /**
     * Añadimos una propiedad a $this->params
     * 
     * @param string $key
     * @param string $value
     * @param type $null
     */
    public function addParam($key, $value, $null = false){
        if($key != ''){
            if($null == true && $value == '' || $null == false && $value != '') {
                $this->params->{$key} = $value;
            }
        }
    }
    
    /**
     * Obtenemos el fichero sgun su nombre
     * 
     * @param string $fileName
     * @param int $area
     * @param string $dir
     * @return string
     */
    public function getFile($fileName, $dir = null) {
        
        $this->addParam('fileName', $fileName);
        
        if($dir != null){
            $this->addParam('subfolder', $dir);
        }
        
        return $this->soap->getFile(
            $this->params->fileName,
            $this->params->area,
            $this->params->subfolder);
    }

}