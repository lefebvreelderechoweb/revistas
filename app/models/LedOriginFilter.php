<?php

namespace Models;

use Phalcon\Mvc\Model\Relation;

class LedOriginFilter extends \BaseModel {

    public function initialize(){
        $this->setSource('FUENTED_FILTRO');
        
        //Relacion con Fuented
        $this->belongsTo(
            'idOrigin',
            __NAMESPACE__ . '\LedOrigin',
            'id',
            ['alias' => 'origin',
             'foreignKey' => true]
        );
        
        //Relacion con Tipodato
        $this->belongsTo(
            'idDataType',
            __NAMESPACE__ . '\LedDataType',
            'id',
            ['alias' => 'dataType']
        );
        
        //Relación Filtros / Opciones
        $this->hasMany(
            'id',
            __NAMESPACE__ . '\LedOriginFilterOption',
            'idOriginFilter',                               
            [
                'alias'  => 'options',
                'foreignKey' => [
                    'action' => Relation::ACTION_CASCADE,
                ]
            ]
        );
        
        //Relación Filtros / Atributos
        $this->hasMany(
            'id',
            __NAMESPACE__ . '\LedOriginFilterAttribute',
            'idOriginFilter',                               
            [
                'alias'  => 'attributes',
                'foreignKey' => [
                    'action' => Relation::ACTION_CASCADE,
                ]
            ]
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_FUENTED_FILTRO'  => 'id',
            'ID_FUENTED'         => 'idOrigin', 
            'TITULO'             => 'title',
            'DESCRIPCION'        => 'description',
            'ID_TIPODATO'        => 'idDataType',
            'NOMBRE_PARAMETRO'   => 'param',
            'VALOR'              => 'value',
            'TS'                 => 'ts',
            'FC'                 => 'fc',
            'UIC'                => 'uic',
            'UC'                 => 'uc'
        );
    }
    
}