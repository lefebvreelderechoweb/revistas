<?php

namespace Models;

class LedComponentOrigin extends \BaseModel {

    public function initialize(){
        $this->setSource('COMPONENTE_FUENTED');
        
        $this->belongsTo(
            'idComponent', 
            __NAMESPACE__ . '\LedComponent', 
            'id', 
            array('alias' => 'component')
        );
        
        $this->belongsTo(
            'idOrigin', 
            __NAMESPACE__ . '\LedOrigin', 
            'id', 
            array('alias' => 'origin')
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_COMPONENTE_FUENTED' => 'id',
            'ID_COMPONENTE'         => 'idComponent',
            'ID_FUENTED'            => 'idOrigin',
            'TS'                    => 'ts',
            'FC'                    => 'fc',
            'UIC'                   => 'uic',
            'UC'                    => 'uc'
        );
    }
    
}