<?php

namespace Models;

class LedApproachResult extends \BaseModel {

    public function initialize(){
        $this->setSource('PLANTEAMIENTORESULTADO');
        
        $this->belongsTo(
            'idContentIssue',
            __NAMESPACE__ . '\LedContentIssue',
            'id',
            ['alias' => 'contentIssue']
        );
        
        $this->belongsTo(
            'idDataTypeForum',
            __NAMESPACE__ . '\LedDataTypeForum',
            'id',
            ['alias' => 'dataTypeForum']
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_PLANTEAMIENTORESULTADO' => 'id',
            'ID_TIPODATO'               => 'idDataTypeForum',
            'ID_NUMERO_CONTENIDO'       => 'idContentIssue',
            'CONTENIDO'                 => 'content',
            'TS'                        => 'ts',
            'FC'                        => 'fc',
            'UIC'                       => 'uic',
            'UC'                        => 'uc'
        );
    }
    
}