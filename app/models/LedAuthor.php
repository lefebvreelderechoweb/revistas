<?php

namespace Models;

class LedAuthor extends \BaseModel {

    public function initialize(){
        $this->setSource('AUTOR');
        
        //Relación Número Autor / Contenido
        $this->hasManyToMany(
            'id',                                       // field(s) of this model
            __NAMESPACE__ . '\LedAuthorIssueContent',   // table which stores the n:n relations
            'idAuthor',                                 // columns in intermediate table that refers to this model's fields
            'idContentIssue',                           // columns in intermediate that that refers to the referenced table
            __NAMESPACE__ . '\LedContentIssue',         // referenced table
            'id',                                       // referenced table columns
            ['alias' => 'contentIssue']                 // array of extra options, for eg alias
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_AUTOR'          => 'id',
            'REF_BITBAN'        => 'refBitban',
            'TRATAMIENTO'       => 'tratamiento',
            'NOMBREYAPELLIDOS'  => 'fullName',
            'CARGO'             => 'job',
            'ID_AUTOR_ED'       => 'idAuthorEd',
            'IMAGEN'            => 'image',
            'TS'                => 'ts',
            'FC'                => 'fc',
            'UIC'               => 'uic',
            'UC'                => 'uc'
        );
    }
    
}