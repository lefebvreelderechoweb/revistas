<?php

namespace Models;

class LedZoneComponent extends \BaseModel {

    public function initialize(){
        $this->setSource('ZONA_COMPONENTE');
        
        $this->belongsTo(
            'idZone', 
            __NAMESPACE__ . '\LedZone', 
            'id', 
            array('alias' => 'zone')
        );
        
        $this->belongsTo(
            'idComponent', 
            __NAMESPACE__ . '\LedComponent', 
            'id', 
            array('alias' => 'component')
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_ZONA_COMPONENTE'        => 'id',
            'ID_ZONA'                   => 'idZone',
            'ID_COMPONENTE'             => 'idComponent',
            'TS'                        => 'ts',
            'FC'                        => 'fc',
            'UIC'                       => 'uic',
            'UC'                        => 'uc'
        );
    }
    
}