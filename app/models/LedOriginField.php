<?php

namespace Models;

class LedOriginField extends \BaseModel {

    public function initialize(){
        $this->setSource('FUENTED_CAMPO');
        
        //Relacion con Fuented
        $this->belongsTo(
            'idOrigin',
            __NAMESPACE__ . '\LedOrigin',
            'id',
            ['alias' => 'origin',
             'foreignKey' => true]
        );
        
        //Relacion con Tipodato
        $this->belongsTo(
            'idDataType',
            __NAMESPACE__ . '\LedDataType',
            'id',
            ['alias' => 'dataType']
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_FUENTED_CAMPO'   => 'id',
            'ID_FUENTED'         => 'idOrigin', 
            'NOMBRE_CAMPO'       => 'field',
            'ID_TIPODATO'        => 'idDataType',
            'VALOR_POR_DEFECTO'  => 'defaultValue',
            'TAG'                => 'tag',
            'TS'                 => 'ts',
            'FC'                 => 'fc',
            'UIC'                => 'uic',
            'UC'                 => 'uc'
        );
    }
    
}