<?php

namespace Models;

class LedPointView extends \BaseModel {

    public function initialize(){
        $this->setSource('PUNTOSDEVISTA');
        
        $this->belongsTo(
            'idAuthor',
            __NAMESPACE__ . '\LedAuthor',
            'id',
            ['alias' => 'author']
        );
        
        $this->belongsTo(
            'idContentIssue',
            __NAMESPACE__ . '\LedContentIssue',
            'id',
            ['alias' => 'contentIssue']
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_PUNTODEVISTA'       => 'id',
            'ID_NUMERO_CONTENIDO'   => 'idContentIssue',
            'ID_AUTOR'              => 'idAuthor',
            'FIRMA'                 => 'signature',
            'TRATAMIENTO'           => 'tratamiento',
            'CARGO'                 => 'job',
            'PUNTODEVISTA'          => 'pointView',
            'TS'                    => 'ts',
            'FC'                    => 'fc',
            'UIC'                   => 'uic',
            'UC'                    => 'uc'
        );
    }
    
}