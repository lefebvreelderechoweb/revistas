<?php

namespace Models;

class LedUser extends \BaseModel {
    
    public $id;
    public $fullName;
    public $user;
    public $password;
    /*public $idRol;
    public $rolName;*/

    public function initialize(){
        $this->setSource('USUARIO');
        
        /*$this->belongsTo(
            'idRol',
            __NAMESPACE__ . '\LedRol',
            'id',
            ['alias' => 'rol']
        );*/
        
        //Relación User / Magazine
        $this->hasManyToMany(
            'id',                               // field(s) of this model
            __NAMESPACE__ . '\LedAccess',       // table which stores the n:n relations
            'idUser',                           // columns in intermediate table that refers to this model's fields
            'idMagazine',                       // columns in intermediate that that refers to the referenced table
            __NAMESPACE__ . '\LedMagazine',     // referenced table
            'id',                               // referenced table columns
            ['alias' => 'magazine']             // array of extra options, for eg alias
        );
        
        //Relación User / Rol
        $this->hasManyToMany(
            'id',                               // field(s) of this model
            __NAMESPACE__ . '\LedAccess',       // table which stores the n:n relations
            'idUser',                           // columns in intermediate table that refers to this model's fields
            'idRol',                            // columns in intermediate that that refers to the referenced table
            __NAMESPACE__ . '\LedRol',          // referenced table
            'id',                               // referenced table columns
            ['alias' => 'Rol']                  // array of extra options, for eg alias
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_USUARIO'        => 'id',
            'NOMBREYAPELLIDOS'  => 'fullName',
            'USUARIO'           => 'user',
            'CONTRASEÑA'        => 'password',
            //'ID_ROL'            => 'idRol',
            'TS'                => 'ts',
            'FC'                => 'fc',
            'UIC'               => 'uic',
            'UC'                => 'ud'
        );
    }
    
    public function load($object){
        if(is_array($object)){
            $object = (object)$object;
        }
        
        $this->id           = $object->id;
        $this->fullName     = $object->fullName;
        $this->user         = $object->user;
        $this->password     = $object->password;
        //$this->idRol        = $object->idRol;
        
        //$this->setRolName();
    }
    
    public static function search( $args = [] ){
        $result = parent::search($args);
        
        $r = [];
        
        if( count($result) > 0 ){
            foreach ($result as $stdClassUser) {
                $obj = new self();
                $obj->load($stdClassUser);
                $r[] = $obj;
            }
        }
        
        return $r;
    }
    
    public function setRolName (){
        $result = LedRol::search(['id' => $this->idRol]);
        
        foreach($result as $rol){
            $this->rolName = $rol->rolName;
        }
    }
    
    
    
}