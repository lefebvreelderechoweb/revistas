<?php
namespace Models;

use Phalcon\Mvc\Model\Relation;

class LedOrigin extends \BaseModel {

    public function initialize(){
        $this->setSource('FUENTED');
        
        //Relacion con Tipodato
        $this->belongsTo(
            'idOriginType',
            __NAMESPACE__ . '\LedOriginType',
            'id',
            ['alias' => 'originType']
        );
        
        //Relación Origen / Revista
        $this->hasMany(
            'id',
            __NAMESPACE__ . '\LedMagazineOrigin',
            'idOrigin',                               
            [
                'alias'  => 'magazineOrigin'
            ]
        );
        
        $this->hasManyToMany(
            'id',                                   // field(s) of this model
            __NAMESPACE__ . '\LedMagazineOrigin',   // table which stores the n:n relations
            'idOrigin',                             // columns in intermediate table that refers to this model's fields
            'idMagazine',                           // columns in intermediate that that refers to the referenced table
            __NAMESPACE__ . '\LedMagazine',         // referenced table
            'id',                                   // referenced table columns
            ['alias' => 'magazines']                // array of extra options, for eg alias
        );
        
        //Relación Origen / Componente
        $this->hasMany(
            'id',
            __NAMESPACE__ . '\LedComponentOrigin',
            'idOrigin',                               
            [   
                'alias' => 'componentOrigin'
            ]
        );
        
        $this->hasManyToMany(
            'id',                                   // field(s) of this model
            __NAMESPACE__ . '\LedComponentOrigin',  // table which stores the n:n relations
            'idOrigin',                             // columns in intermediate table that refers to this model's fields
            'idComponent',                          // columns in intermediate that that refers to the referenced table
            __NAMESPACE__ . '\LedComponent',        // referenced table
            'id',                                   // referenced table columns
            ['alias' => 'components']               // array of extra options, for eg alias
        );
        
        //Relación Origen / Campos
        $this->hasMany(
            'id',
            __NAMESPACE__ . '\LedOriginField',
            'idOrigin',                               
            [
                'alias'  => 'fields',
                'foreignKey' => [
                    'action' => Relation::ACTION_CASCADE,
                ]
            ]
        );
        
        //Relación Origen / Filtros
        $this->hasMany(
            'id',
            __NAMESPACE__ . '\LedOriginFilter',
            'idOrigin',                               
            [
                'alias'  => 'filters',
                'foreignKey' => [
                    'action' => Relation::ACTION_CASCADE,
                ]
            ]
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_FUENTED'        => 'id',
            'NOMBRE'            => 'name',
            'URL'               => 'url',
            'ID_FUENTED_TIPO'   => 'idOriginType',
            'BASE'              => 'origin',
            'TS'                => 'ts',
            'FC'                => 'fc',
            'UIC'               => 'uic',
            'UC'                => 'uc'
        );
    } 
}