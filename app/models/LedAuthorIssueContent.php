<?php

namespace Models;

class LedAuthorIssueContent extends \BaseModel {

    public function initialize(){
        $this->setSource('AUTOR_NUMERO_CONTENIDO');
        
        //Relación con autor
        $this->belongsTo(
            'idAuthor', 
            __NAMESPACE__ . '\LedAuthor', 
            'id', 
            array('alias' => 'author')
        );
        
        //Relación con Número Contenido
        $this->belongsTo(
            'idContentIssue', 
            __NAMESPACE__ . '\LedContentIssue', 
            'id', 
            array('alias' => 'contentIssue')
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_AUTOR_NUMERO_CONTENIDO' => 'id',
            'ID_AUTOR'                  => 'idAuthor',
            'ID_NUMERO_CONTENIDO'       => 'idContentIssue',
            'TS'                        => 'ts',
            'FC'                        => 'fc',
            'UIC'                       => 'uic',
            'UC'                        => 'uc'
        );
    }
    
}