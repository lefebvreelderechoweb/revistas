<?php

namespace Models;

/**
 * Forma de sacar los datos relacionales de la tabla (REVISTAS)
 * 
 * $magazine = Models\LedMagazine::findFirst();
 *   foreach ($magazine->user as $user){
 *       var_dump($user->fullName);
 *       die();
 *   }
 */

class LedMagazine extends \BaseModel {

    public function initialize(){
        $this->setSource('REVISTA');
        
        /*$this->hasOne(
            'idNumber', 
            __NAMESPACE__ . '\LedIssue',
            'id',
            ['alias' => 'issue']
        );*/
        
        $this->hasMany(
            'id', 
            __NAMESPACE__ . '\LedIssue', 
            'idMagazine',
            [   
                'alias' => 'issue',
                'params' => array( //here we can pass a params in relations
                    'order' => 'publishDate is null desc, publishDate desc' 
                )
            ]
            );
        
        //Relación Revista / Usuario
        $this->hasManyToMany(
            'id',                               // field(s) of this model
            __NAMESPACE__ . '\LedAccess',       // table which stores the n:n relations
            'idMagazine',                       // columns in intermediate table that refers to this model's fields
            'idUser',                           // columns in intermediate that that refers to the referenced table
            __NAMESPACE__ . '\LedUser',         // referenced table
            'id',                               // referenced table columns
            ['alias' => 'user']                 // array of extra options, for eg alias
        );
        
        //Relación Revista / Perfil
        $this->hasManyToMany(
            'id',                               // field(s) of this model
            __NAMESPACE__ . '\LedAccess',       // table which stores the n:n relations
            'idMagazine',                       // columns in intermediate table that refers to this model's fields
            'idRol',                            // columns in intermediate that that refers to the referenced table
            __NAMESPACE__ . '\LedRol',          // referenced table
            'id',                               // referenced table columns
            ['alias' => 'rol']                  // array of extra options, for eg alias
        );
        
        //Relación Revista / Componente
        $this->hasManyToMany(
            'id',                                       // field(s) of this model
            __NAMESPACE__ . '\LedMagazineComponent',    // table which stores the n:n relations
            'idMagazine',                               // columns in intermediate table that refers to this model's fields
            'idComponent',                              // columns in intermediate that that refers to the referenced table
            __NAMESPACE__ . '\LedComponent',            // referenced table
            'id',                                       // referenced table columns
            ['alias' => 'component']                    // array of extra options, for eg alias
        );
        
        //Relación Revista / Origen
        /*$this->hasManyToMany(
            'id',                                   // field(s) of this model
            __NAMESPACE__ . '\LedMagazineOrigin',   // table which stores the n:n relations
            'idMagazine',                           // columns in intermediate table that refers to this model's fields
            'idOrigin',                             // columns in intermediate that that refers to the referenced table
            __NAMESPACE__ . '\LedOrigin',           // referenced table
            'id',                                   // referenced table columns
            ['alias' => 'origin']                   // array of extra options, for eg alias
        );*/
        $this->hasMany(
            'id',
            __NAMESPACE__ . '\LedMagazineOrigin',
            'idMagazine',
            ['alias' => 'magazineOrigin']
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_REVISTA'        => 'id',
            'NOMBRE'            => 'name',
            'URL'               => 'url',
            'ID_NUMERO'         => 'idNumber', //Es el id del último número publicado (vigente)
            'VISIBLE_WEB'       => 'visibility',
            'ORDEN'             => 'order_index',
            'ALIAS'             => 'alias',
            'SLUG'              => 'slug',
            'TS'                => 'ts',
            'FC'                => 'fc',
            'UIC'               => 'uic',
            'UC'                => 'uc'
        );
    }
    
}