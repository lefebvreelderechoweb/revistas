<?php

namespace Models;

use Phalcon\Session\Adapter\Files as SessionAdapter;

class LedMagazineOrigin extends \BaseModel {

    public function initialize(){
        $this->setSource('REVISTA_FUENTED');
        
        $this->belongsTo(
            'idMagazine', 
            __NAMESPACE__ . '\LedMagazine', 
            'id', 
            ['alias' => 'magazine']
        );
        
        $this->belongsTo(
            'idOrigin', 
            __NAMESPACE__ . '\LedOrigin', 
            'id', 
            ['alias' => 'origin']
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_REVISTA_FUENTED'    => 'id',
            'ID_REVISTA'            => 'idMagazine',
            'ID_FUENTED'            => 'idOrigin',
            'TS'                    => 'ts',
            'FC'                    => 'fc',
            'UIC'                   => 'uic',
            'UC'                    => 'uc'
        );
    }
    
}