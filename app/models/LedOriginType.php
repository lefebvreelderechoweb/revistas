<?php

namespace Models;

class LedOriginType extends \BaseModel {

    public function initialize(){
        $this->setSource('FUENTED_TIPO');
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_FUENTED_TIPO'   => 'id',
            'NOMBRE'            => 'name',
            'TS'                => 'ts',
            'FC'                => 'fc',
            'UIC'               => 'uic',
            'UC'                => 'ud'
        );
    }
    
}