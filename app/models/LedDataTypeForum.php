<?php

namespace Models;

class LedDataTypeForum extends \BaseModel {

    public function initialize(){
        $this->setSource('TIPODATOFORO');
        
        //Relación con Componente Parametro
        $this->hasMany(
            'id', 
            __NAMESPACE__ . '\LedApproachResult', 
            'idDataTypeForum',
            ['alias' => 'approachResult']
        );
    }
    
    public function columnMap(){
        //Keys are the real names in the table and
        //the values their names in the application
        return array(
            'ID_TIPODATOFORO'   => 'id',
            'TIPODATO'          => 'dataType',
            'TS'                => 'ts',
            'FC'                => 'fc',
            'UIC'               => 'uic',
            'UC'                => 'ud'
        );
    }
    
}