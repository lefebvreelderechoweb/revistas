<?php
use Phalcon\DI;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Validation\Message;
use Phalcon\Validation;
use Library\builder\Components as BuilderComponents;

class ComponentController extends BaseController {
    
    public function initialize(){
        $script = $this->assets->collection('commonJs');
        $script->addJs('/js/ledHelper.js');
        $script->addJs('/js/ledComponent.js');
    }
    
    /**
     * Comprobamos que el listado de componentes es correcto tanto
     * a nivel de ficheros como a nivel de base de datos
     * 
     * Mostramos el listado de componentes
     */
    public function indexAction() {
        $messageRegister     = $this->register();
        $messageUnregister   = $this->unregister();
        
        $numberPage = ($this->request->getQuery("page", "int") == null) 
                    ? 1 
                    : $this->request->getQuery("page", "int");
        
        $components = Models\LedComponent::find();

        $paginator = new Paginator(array(
            'data' => $components,
            'limit' => 10,
            'page' => $numberPage,
        ));

        $this->view->page = $paginator->getPaginate();

        $this->view->setVars([
            'messageRegister'   => $messageRegister,
            'messageUnregister' => $messageUnregister,
            'containerClass'    => 'fullContainer'
        ]);
    }
    
    /**
     * Edita un componente en base a su id.
     * 
     * @param int $id
     */
    public function editAction($id){
        //Helpers
        $unsetProps  = ['ts', 'fc', 'uic', 'uc']; 
        $listsHelper = new Helpers\Lists\Lists();
        
        //Objetos necesarios
        $component      = $listsHelper->unsetProps(\Models\LedComponent::findFirstById($id), $unsetProps);
        $magazines      = $listsHelper->propSeparatedByChar($component->getMagazines());
        $magazinesList  = explode(',', $magazines);
        $zones          = $listsHelper->propSeparatedByChar($component->getZones());
        $zonesList      = explode(',', $zones);
        $origins        = $listsHelper->propSeparatedByChar($component->getOrigins());
        $originsList    = explode(',', $origins);
        
        //Pasamos a la vista la informacion necesaria
        $this->view->setVars([ 
            'form'           => new Forms\ComponentForm(),
            'component'      => $component,
            'magazines'      => $magazines,
            'magazinesList'  => $magazinesList,
            'zones'          => $zones,
            'zonesList'      => $zonesList,
            'origins'        => $origins,
            'originsList'    => $originsList,
            'containerClass' => 'fullContainer'
        ]);
    }
    
    /**
     * Actualiza el componente actual
     * 
     * @return JSON
     */
    public function updateAction(){
        $result = new stdClass();
        
        if ($this->request->isPost()) {
            $component = (object) $this->request->getPost();
            //Creamos el origen
            $result = $this->updateComponent($component);
        }
        
        //HEADER JSON
        $this->responseJson();
        return json_encode($result);
    }
    
    /**
     * Actualizamos un componente
     * 
     * @param mixed $component
     */
    private function updateComponent($component){
        //Informacion que pasaremos por ajax para redirigir o mostrar un mensaje
        $result          = new stdClass();
        $result->status  = 'KO';
        $result->msg     = array();
        $result->id      = 0;
        
        //Inicializamos el modelo
        $updateComponent = Models\LedComponent::findFirstById($component->id);
        
        //Asignamos los valores 
        $updateComponent->name        = $component->name; 
        $updateComponent->description = $component->description; 
        
        //Eliminamos y relacionamos con revistas
        if(isset($component->magazines)){
            $updateComponent->magazines = $this->addMagazinesComponent($component, true);
        }
        //Eliminamos y Relacionamos con zonas
        if(isset($component->zones)){
            $updateComponent->zones = $this->addZonesComponent($component, true);
        }
        //Eliminamos y Relacionamos con origenes
        if(isset($component->origins)){
            $updateComponent->origins = $this->addOriginsComponent($component, true);
        }
        
        //Actualizamos y si hay un error devolvemos el mensaje
        if ($updateComponent->save() === false) {
            foreach ($updateComponent->getMessages() as $message) {
                $result->msg[] = $message->getMessage();
            }
        } else {
            $result->status = 'OK';
            $result->msg    = _('Datos guardados correctamente');
            $result->id     = $updateOrigin->id;
        }
        
        return $result;
    }
    
    /**
     * Devolvemos un objeto con todas las relaciones a revistas
     * 
     * @param mixed $component
     */
    private function addMagazinesComponent($component, $remove = false){
        //Limpiamos el contenido
        if(isset($component->id) && $component->id > 0 && $remove){
            $magazinesRemove = Models\LedMagazineComponent::search([
                    "idComponent" => $component->id
                ]
            ); 
            $magazinesRemove->delete();
        }
        
        //Insertamos
        $magazinesComponent = [];
        $magazines       = explode(',', $component->magazines);
        foreach ($magazines as $magazine) {
            $magazineModel     = Models\LedMagazine::findFirstById($magazine);
            $magazinesComponent[] = $magazineModel;
        }
        return $magazinesComponent;
    }
    
    /**
     * Devolvemos un objeto con todas las relaciones a zonas
     * 
     * @param mixed $zones
     */
    private function addZonesComponent($component, $remove = false){
        //Limpiamos el contenido
        if(isset($component->id) && $component->id > 0 && $remove){
            $zonesRemove = Models\LedZoneComponent::search([
                    "idComponent" => $component->id
                ]
            ); 
            $zonesRemove->delete();
        }
        
        //Insertamos
        $zonesComponent = [];
        $zones          = explode(',', $component->zones);
        foreach ($zones as $zone) {
            $zoneModel     = Models\LedZone::findFirstById($zone);
            $zonesComponent[] = $zoneModel;
        }
        return $zonesComponent;
    }
    
    /**
     * Devolvemos un objeto con todas las relaciones a origenes
     * 
     * @param mixed $origins
     */
    private function addOriginsComponent($component, $remove = false){
        //Limpiamos el contenido
        if(isset($component->id) && $component->id > 0 && $remove){
            $originsRemove = Models\LedComponentOrigin::search([
                    "idComponent" => $component->id
                ]
            ); 
            $originsRemove->delete();
        }
        
        //Insertamos
        $originsComponent = [];
        $origins          = explode(',', $component->origins);
        foreach ($origins as $origin) {
            $originModel     = Models\LedOrigin::findFirstById($origin);
            $originsComponent[] = $originModel;
        }
        return $originsComponent;
    }
    
    /**
     * Registramos las zonas
     */
    private function register(){
        $message             = new stdClass();
        $message->msgError   = '';
        $message->msgSuccess = '';
        $message->msgExist   = '';
            
        $user              = $this->session->get('user');
        $userID            = $user->id;
        $builderComponents = new BuilderComponents(false, false);
        $components        = $builderComponents->getComponentsList();
        
        //Recorremos los componentes para registrar los necesarios
        if($components && count($components) > 0){
            foreach($components as $component){
                $args          = array(
                    "folder" => $component
                );
                
                //Comprobamos que el componente no existe
                $findComponent = count(Models\LedComponent::search($args));
                if(!$findComponent){
                    
                    //Guardamos el componente
                    $newComponent = new \Models\LedComponent();

                    $newComponent->folder       = $component;
                    $newComponent->name         = ucfirst(str_replace("_", " ",  $component));
                    $newComponent->description  = "";
                    $newComponent->html         = " ";
                    
                    //Mostramos un mesaje por si da un error el guardado
                    if ( $newComponent->save() === false ){
                        foreach ( $newComponent->getMessages() as $message ){
                            $message->msgError .= $message->getMessage().'<br/>';
                        }
                    }else{
                        $message->msgSuccess .= _("El componente $component ha sido registrado.").'<br/>';
                    }
                }else{
                    $message->msgExist .= _("El componente $component ya existe.").'<br/>';
                }
            }
        }
        
        return $message;
    }
    
    /**
     * Comprobamos si el registro tiene una carpeta definida
     * y si no se da el caso lo eliminamos
     */
    private function unregister(){
        $message             = new stdClass();
        $message->msgError   = '';
        $message->msgSuccess = '';
        
        $builderComponents = new BuilderComponents(false, false);
        $components        = \Models\LedComponent::find();
        
        foreach ($components as $component){
            $issetComponent = $builderComponents->issetComponent($component->folder);
            if(!$issetComponent){
                $componentToDelete = \Models\LedComponent::findFirstById($component->id);
                
                //Mostramos un mesaje por si da un error el guardado
                if ($componentToDelete->delete() === false) {
                    foreach ($componentToDelete->getMessages() as $message) {
                        $message->msgError .= $message->getMessage() . '<br/>';
                    }
                } else {
                    $message->msgSuccess .= _("El componente $component->name ha sido eliminado.") . '<br/>';
                }
            }
        }
        
        return $message;
    }
    
    /**
     * Vista parcial del listado de componentes para
     * el page builder, con la posibilidad de filtar por zona
     * @param type $zone
     */
    public function listAction($zone = false) {
        //No necesita pie y cabecera
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);

        //Utilizamos la vista parcial
        $this->view->pick('common/partials/components');

        //Obtenemos el listado de los componentes
        $builderComponents = new BuilderComponents($zone);
        $components        = $builderComponents->getComponents();

        $this->view->setVars([
            'components' => $components
        ]);
    }
}
