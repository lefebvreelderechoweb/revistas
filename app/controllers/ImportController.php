<?php
use Phalcon\DI;
use Phalcon\Mvc\Model\Criteria;

class ImportController extends BaseController {
    
   public function changeDateAction(){
       
        $days = array(
            'enero'         => 'january',
            'febrero'       => 'february',
            'marzo'         => 'march',
            'abril'         => 'april',
            'mayo'          => 'may',
            'junio'         => 'june',
            'julio'         => 'july',
            'agosto'        => 'august',
            'septiembre'    => 'september',
            'octubre'       => 'october',
            'noviembre'     => 'november',
            'diciembre'     => 'december',
            
        );
        
        $searchParam = array();
        $query = Criteria::fromInput($this->di, "Models\LedIssue", $searchParam);
        $issues = Models\LedIssue::find($query->getParams());
        
        foreach($issues as $issue){
            if ($issue->publishDate == ''){
                
                if ( strpos($issue->name, ',')!== false ){
                    $name = explode(',', $issue->name);
                }else if( strpos($issue->name, '.')!== false ){
                    $name = explode('.', $issue->name);
                }
                
                $dateNotFormat = trim($name[1]);
                $spliteDate = explode(' ', $dateNotFormat);
                $month = strtolower($spliteDate[0]);

                if ( array_key_exists($month, $days) ){
                    $dateNotFormat = '1 '. $days[$month] . ' ' . $spliteDate[1];
                    $time = strtotime($dateNotFormat);
                    $dateFormat = date('Y-m-d',$time);
                    
                    $issueUpdate = Models\LedIssue::findFirstById($issue->id);
                    $issueUpdate->publishDate = $dateFormat;
                    
                    try{
                        if($issueUpdate->save()){
                            echo 'HECHO' . $dateFormat;
                        }
                    }catch(PDOException $e){
                        echo 'Error';
                    }
                }
            }
        }
   }

}
