<?php
use Phalcon\DI;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Validation\Message;
use Phalcon\Validation;

class AuthorController extends BaseController {
    
    
    public function indexAction() {
        
        /**
         * Mostrar listado de autores
         */
        
        $numberPage = ($this->request->getQuery("page", "int") == null) ? 1 : $this->request->getQuery("page", "int");
        
        $authors = Models\LedAuthor::find();        

        $paginator = new Paginator(array(
            "data"  => $authors,
            "limit" => 10,
            "page"  => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();
        $this->view->setVars([
            'idMagazine'    => $magazine->id,
            'routes'        => $routes,
        ]);
            
        
    }
        
    /**
     * Ejecuta la "búsqueda" según los criterios enviados desde el "indexAction".
     * Devuelve un paginador para los resultados.
     * @return type
     */
    public function searchAction() {
        $numberPage = 1;
        
        if ($this->request->isPost()) {
            // Criteria hace una busqueda en el modelo indicado con todos los campos del formulario
            // que coincidan con un campo de la tabla
            $query = Criteria::fromInput($this->di, "Models\LedAuthor", $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            
            $numberPage = $this->request->getQuery("page", "int");
        }
        
        $parameters = array();
        
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }
        
        $authors = Models\LedAuthor::find($parameters);
        
        if (count($authors) == 0) {
            
            $this->flash->notice("La búsqueda no ha producido ningún resultado");
            
            return $this->dispatcher->forward(
                [
                    "controller" => "issue",
                    "action"     => "index",
                ]
            );
        }
        $paginator = new Paginator(array(
            "data"  => $authors,
            "limit" => 10,
            "page"  => $numberPage
        ));
        $this->view->page = $paginator->getPaginate();
    }
    
    public function searchAjaxAction(){
        
        $this->view->disable();
        
        if ($this->request->isAjax()) {
            
            $result             = new stdClass();
            $result->OK         = false;
            $result->authors    = array();
            $data               = $this->request->getPost();
            
            if ( isset( $data['onlyID'] ) && $data['onlyID'] == 'true' ){
                
                //Para las búsquedas solo por ID
                $ids = explode(',', $data['ids']);
                
                foreach ($ids as $id){
                    
                    $author = Models\LedAuthor::findFirst($id);
                    
                    if ( $author ){
                        $result->OK = true;

                        $item           = new stdClass();
                        $item->id       = $author->id;
                        $item->fullName = $author->tratamiento . ' ' . $author->fullName;
                        $item->image    = $author->image;

                        $result->authors[] = $item;
                    }
                }
                
            }else{
                
                //Para un formulario más completo
                $query = Criteria::fromInput($this->di, "Models\LedAuthor", $this->request->getPost());
                $this->persistent->searchParams = $query->getParams();

                $parameters = array();

                if ($this->persistent->searchParams) {
                    $parameters = $this->persistent->searchParams;
                }

                $authors = Models\LedAuthor::find($parameters);

                foreach( $authors as $author ){
                    $result->OK = true;

                    $item           = new stdClass();
                    $item->id       = $author->id;
                    $item->fullName = $author->tratamiento . ' ' . $author->fullName;
                    $item->image    = $author->image;

                    $result->authors[] = $item;
                }
            }
            
            echo json_encode($result);
            exit;
        }
    }
    
    /**
     * Edita un número en base a su id.
     * @param type $id
     * @return type
     */
    public function editAction($id){
        
        $data = $this->request->getPost();
        
        if (!$this->request->isPost() || ( isset($data['edit']) && $data['edit'] == true) ) {
            
            $author = Models\LedAuthor::findFirstById($id);
            
            if (!$author) {
                
                $this->flash->error("Número no encontrado");
                return $this->dispatcher->forward(
                    [
                        "controller" => "author",
                        "action"     => "index",
                    ]
                );
            }
            
            $this->view->setVars([
                'author' => $author
            ]);
        }
    }
    
    /**
     * Actualiza un autor basado en los datos introducidos en "editAction"
     * @return type
     */
    public function saveAction(){
        
        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(
                [
                    "controller" => "author",
                    "action"     => "index",
                ]
            );
        }
        
        $id = $this->request->getPost("idAuthor", "int");
        
        $author = Models\LedAuthor::findFirstById($id);
        
        if (!$author) {
            $this->flashSession->error("El autor no existe");
            return $this->dispatcher->forward(
                [
                    "controller" => "author",
                    "action"     => "index",
                ]
            );
        }
        
        $data                   = $this->request->getPost();
        
        $author->tratamiento    = $data['tratamiento'];
        $author->fullName       = $data['fullName'];
        $author->job            = $data['job'];
        
        if ( $author->save() === false ){
            
            foreach ($author->getMessages() as $message) {
                $this->flashSession->error($message);
            }
            
            return $this->dispatcher->forward(
                [
                    "controller" => "author",
                    "action"     => "edit",
                    "params"     => [$id]
                ]
            );
        }
        
        /**
         * Tratamiento del archivo de imagen de autor (en caso de que se haya adjuntado)
         */
        if ($this->request->hasFiles(true) == true) { //Si no escribo esta sentencia así, siempre daría true aunque no existieran archivos
            $files = $this->request->getUploadedFiles();

            // Print the real file names and sizes
            foreach ($files as $file) {
                
                //La imagen llevará el id del autor recien generado
                $filenameAuthor = $author->id . '.' . $file->getExtension();
                
                
                //Guardo la imagen en el directorio correspondiente a imágenes de autores
                $file->moveTo(
                    BASE_PATH . '/html/images/authors/' . $filenameAuthor
                );
                
                //Actualizamos el registro con el path de la imagen del autor
                $author->image = $filenameAuthor;
                $result = $author->update();
            }
        }else{
            if($data['changeImage'] == '1'){
                // Aunque no se haya adjuntado imagen, se ha eliminado la antigua, de modo que
                // hay que modificarlo en bbdd
                $author->image = '';
                $result = $author->update();
            }
        }
        
        if ( $result === false ){
            
            foreach ($author->getMessages() as $message) {
                $this->flashSession->error($message);
            }
        } else{
            $this->flashSession->success('Autor actualizado correctamente');
        }
        
        return $this->dispatcher->forward(
            [
                'controller' => 'author',
                'action'     => 'edit',
                'params'     => [$id]
            ]
        );
    }
    
    /**
     * Muestra el formulario de creación de nuevo autor
     */
    public function newAction(){
            
        $this->view->setVars([]);
    }
    
    /**
     * Crea un autor basandose en los datos introducidos en 'newAction'
     * @return type
     */
    public function createAction() {
        
        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(
                    [
                        "controller" => "auth",
                        "action" => "index",
                    ]
            );
        }
        
        $data                   = $this->request->getPost();
        
        $author                 = new \Models\LedAuthor();
        
        $author->tratamiento    = $data['tratamiento'];
        $author->fullName       = $data['fullName'];
        $author->job            = $data['job'];
        
        if ( $author->save() === false ){
            
            foreach ($author->getMessages() as $message) {
                $this->flashSession->error($message);
            }
            
            return $this->dispatcher->forward(
                [
                    "controller" => "author",
                    "action"     => "new",
                ]
            );
        }
        
        /**
         * Tratamiento del archivo de imagen de autor (en caso de que se haya adjuntado)
         */
        if ($this->request->hasFiles(true) == true) {
            $files = $this->request->getUploadedFiles();

            // Print the real file names and sizes
            foreach ($files as $file) {
                
                //La imagen llevará el id del autor recien generado
                $filenameAuthor = $author->id . '.' . $file->getExtension();
                
                
                //Guardo la imagen en el directorio correspondiente a imágenes de autores
                $file->moveTo(
                    BASE_PATH . '/html/images/authors/' . $filenameAuthor
                );
                
                //Actualizamos el registro con el path de la imagen del autor
                $author->image = $filenameAuthor;
                $result = $author->update();
            }
        }
        
        if ( $result === false ){
            
            foreach ($author->getMessages() as $message) {
                $this->flashSession->error($message);
            }
        } else{
            $this->flashSession->success('Autor creado correctamente');
        }
        
        return $this->dispatcher->forward(
            [
                'controller' => 'author',
                'action'     => 'new',
            ]
        );
    }
    
    /**
     * Crea un autor basandose en los datos introducidos en 'newAction'
     * @return type
     */
    public function createAjaxAction() {
        
        $this->view->disable();
        
        if ($this->request->isAjax()) {
            
            $result                 = new stdClass();
            $result->OK             = false;
            $result->msg            = array();
            $result->idNew          = 0;
            $result->fullNameNew    = '';
            $data                   = $this->request->getPost();
            
            $author                 = new \Models\LedAuthor();
        
            $author->tratamiento    = $data['tratamiento'];
            $author->fullName       = $data['fullName'];
            $author->job            = $data['job'];

            if ( $author->save() === false ){

                foreach ($author->getMessages() as $message) {
                    $result->msg[] = $message;
                }
                
                echo json_encode($result);
                exit;
            }
            
            /**
             * Tratamiento del archivo de imagen de autor (en caso de que se haya adjuntado)
             */
            if ($this->request->hasFiles(true) == true) {
                $files = $this->request->getUploadedFiles();
                
                foreach ($files as $file) {

                    //La imagen llevará el id del autor recien generado
                    $filenameAuthor = $author->id . '.' . $file->getExtension();
                    
                    //Guardo la imagen en el directorio correspondiente a imágenes de autores
                    $file->moveTo(
                        BASE_PATH . '/html/images/authors/' . $filenameAuthor
                    );

                    //Actualizamos el registro con el path de la imagen del autor
                    $author->image = $filenameAuthor;
                    $resultUpdate = $author->update();
                }
            }
            
            if ( $resultUpdate === false ){

                foreach ($author->getMessages() as $message) {
                    $result->msg[] = $message;
                }
                
            } else{
                $result->OK             = true;
                $result->idNew          = $author->id;
                $result->fullNameNew    = $author->tratamiento . ' ' . $author->fullName;
            }
            
            
            echo json_encode($result);
            exit;
        }
    }
    
    /**
     * Elimina un autor
     * @param type $id
     */
    public function deleteAction(){
        
        $result = new stdClass();
        $result->OK = false;
        $result->msg = array();
        
        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(
                    [
                        "controller" => "auth",
                        "action" => "index",
                    ]
            );
        }
        
        $data = $this->request->getPost();
        
        $author = Models\LedAuthor::findFirstById($data['id']);
        
        if(!$author){
            
            $result->msg[]  = 'El autor no existe';
        }else{
            
            if ( $author->delete() === false ){
                foreach ( $author->getMessages() as $message ){
                    $result->msg[] = $message->getMessage();
                }
            }else{
                //Con esto eliminamos la imagen asociada al autor
                $mask = BASE_PATH . '/html/images/authors/' . $author->id . '*.*';
                array_map('unlink', glob($mask));
                
                $result->OK     = true;
                $result->msg[]  = 'Autor eliminado correctamente';
            }
        }
        
        echo json_encode($result);
        exit;
        
    }

}
