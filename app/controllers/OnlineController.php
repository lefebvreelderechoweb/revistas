<?php

use Phalcon\DI;

class OnlineController extends BaseController {

    public function getDocumentAction() {
        if ($this->request->isPost()) {
            
            $onlineLegalContent = new Util\LegalContent();
            
            $result = new stdClass();
            $result->OK = false;
            $result->msg = '';

            $data       = $this->request->getPost();
            $nref       = $data['nref'];
            $title      = $data['title'];
            $idMagazine = $data['idMagazine'];

            if ($nref == '' && $title == '') {
                $result->msg = 'Debes introducir, al menos, uno de los dos parámetros de búsqueda';
            } else {

                if ($nref != '') {
                    $titleDocument = $this->cmOnlineService->getTitleDocumentByNref($nref);
                    
                    if ($titleDocument != '') {
                        $result->OK = true;
                        $result->msg = $titleDocument;
                    } else {
                        $result->msg = 'El NREF es incorrecto';
                    }
                } else {
                    
                    $argsByType = $onlineLegalContent->getArgsByType($data['type'], $idMagazine);
                    
                    $defaultArgs = array(
                        'indice'    => '', //campo obligatorio
                        'fulltext'  => $title,
                        'producto'  => null
                    );
                    
                    $arg = array_merge($defaultArgs, $argsByType);
                    
                    $documents = $this->sinSessionService->searchWithoutSession($arg);
                    if ( count($documents->DOCUMENTOS) > 0 ){
                        $result->OK = true;
                        $result->msg = array();
                        foreach($documents->DOCUMENTOS as $document){
                            $dataContent = $onlineLegalContent->getTitleAndNref($data['type'], $document);
                            
                            $result->msg[] = $dataContent;
                        }
                        
                    }else{
                        
                        if ( isset($documents->ERROR) && $documents->ERROR ){
                            $result->msg = (isset($documents->MESSAGE)) ? $documents->MESSAGE : 'No hay resultados';
                        }else{
                            $result->msg = 'No hay resultados';
                        }
                    }
                }
            }

            echo json_encode($result);
            exit;
        }
    }
    
    public function getDataToModalAction(){
        if ($this->request->isPost()) {
            
            $onlineLegalContent = new Util\LegalContent();
            
            $result = new stdClass();
            $result->data = array();
            $result->required = '';

            $data       = $this->request->getPost();
            $nref       = $data['nref'];
            $type       = $data['type'];
            $idMagazine = $data['idMagazine'];
            
            $argsByType = $onlineLegalContent->getArgsByType($type, $idMagazine, true);
            
            $defaultArgs = array(
                'indice'    => '', //campo obligatorio
                'fulltext'  => $nref,
                'producto'  => null
            );

            $arg = array_merge($defaultArgs, $argsByType);
            $documents = $this->sinSessionService->searchWithoutSession($arg);
            if ( count($documents->DOCUMENTOS) > 0 ){
                foreach($documents->DOCUMENTOS as $document){
                    $result->data = $onlineLegalContent->getDataToFormModal($type, $document);
                    
                    //Saco los campos requeridos de este tipo de contenido
                    $contentType = Models\LedContentType::findFirstById($type);
                    $result->required = $contentType->fieldRequired;
                }
            }
            
            echo json_encode($result);
            exit;
        }
    }
}
