<?php
use Phalcon\DI;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Validation\Message;
use Phalcon\Validation;

class OriginController extends BaseController {
        
    public function initialize(){
        $script = $this->assets->collection('commonJs');
        $script->addJs('/js/ledHelper.js');
        $script->addJs('/js/ledOrigin.js');
    }
    
    /**
     * Mostrar listado de numeros de una revista
     */
    public function indexAction() {
        $numberPage = ($this->request->getQuery("page", "int") == null) 
                    ? 1 
                    : $this->request->getQuery("page", "int");
        
        $origins = Models\LedOrigin::find();

        $paginator = new Paginator(array(
            'data' => $origins,
            'limit' => 10,
            'page' => $numberPage,
        ));

        $this->view->page = $paginator->getPaginate();

        $this->view->setVars([
            'containerClass' => 'fullContainer'
        ]);
    }
      
    /**
     * Muestra el formulario de creación de un nuevo origen
     */
    public function newAction(){  
        $this->viewFormOrigin();
    }
    
    /**
     * Devuelve un JSON al insertar un origen
     * 
     * @return JSON $result
     */
    public function createAction() {
        $result = new stdClass();
        
        if ($this->request->isPost()) {
            $origin = (object) $this->request->getPost();
            //Creamos el origen
            $result = $this->addOrigin($origin);
        }
        
        //HEADER JSON
        $this->responseJson();
        return json_encode($result);
    }
    
    /**
     * Edita un origen en base a su id.
     * 
     * @param int $id
     */
    public function editAction($id){
        $this->view->pick('origin/new');
        $this->viewFormOrigin();
        
        //Helpers
        $unsetProps  = ['ts', 'fc', 'uic', 'uc']; 
        $listsHelper = new Helpers\Lists\Lists();
        
        //Objetos necesarios
        $origin         = $listsHelper->unsetProps(\Models\LedOrigin::findFirstById($id), $unsetProps);
        $fields         = $listsHelper->unsetListProps($origin->getFields(), $unsetProps);
        $filters        = $this->completeFiltersList($origin->getFilters(), $listsHelper, $unsetProps);
        $magazines      = $listsHelper->propSeparatedByChar($origin->getMagazines());
        $magazinesList  = explode(',', $magazines);
        $components     = $listsHelper->propSeparatedByChar($origin->getComponents());
        $componentsList = explode(',', $components);
        
        //Pasamos a un JSON los objetos necesario
        $originJSON  = json_encode($origin);
        $fieldsJSON  = json_encode($fields);
        $filtersJSON = json_encode($filters);
        
        //Pasamos a la vista la informacion necesaria
        $this->view->setVars([ 
            'origin'         => $origin,
            'originJSON'     => $originJSON,
            'fields'         => $fields,
            'fieldsJSON'     => $fieldsJSON,
            'filters'        => $filters,
            'filtersJSON'    => $filtersJSON,
            'magazines'      => $magazines,
            'magazinesList'  => $magazinesList,
            'components'     => $components,
            'componentsList' => $componentsList,
            'saveAction'     => 'updateOriginContent'
        ]);
    }
    
    /**
     * Actualiza el origen actual
     * 
     * @return type
     */
    public function updateAction(){
        $result = new stdClass();
        
        if ($this->request->isPost()) {
            $origin = (object) $this->request->getPost();
            //Creamos el origen
            $result = $this->updateOrigin($origin);
        }
        
        //HEADER JSON
        $this->responseJson();
        return json_encode($result);
    }
    
    /**
     * Devolvemos un JSON con el resultado de eliminar
     */
    public function removeAction(){
        $result = new stdClass();
        
        if ($this->request->isPost()) {
            $origin = (object) $this->request->getPost();
            //Creamos el origen
            $result = $this->removeOrigin($origin);
        }
        
        //HEADER JSON
        $this->responseJson();
        return json_encode($result);
    }
    
    /**
     * Agregamos los necesario para la vista del formulario de origenes
     */
    private function viewFormOrigin(){
        $this->view->setVars([
            'form'                => new Forms\OriginForm(),
            'fieldForm'           => new Forms\FieldForm(),
            'filterForm'          => new Forms\FilterForm(),
            'filterOptionForm'    => new Forms\FilterOptionForm(),
            'filterAttributeForm' => new Forms\FilterAttributeForm(),
            'dataTypes'           => Models\LedDataType::find(),
            'containerClass'      => 'fullContainer',
            'saveAction'          => 'saveOriginContent'
        ]);
    }
    
    /**
     * Creamos un origen
     * 
     * @param mixed $origin
     */
    private function addOrigin($origin){
        //Informacion que pasaremos por ajax para redirigir o mostrar un mensaje
        $result          = new stdClass();
        $result->status  = 'KO';
        $result->msg     = array();
        $result->id      = 0;
        
        //Inicializamos el modelo
        $newOrigin = new Models\LedOrigin();
        
        //Asignamos los valores 
        $newOrigin->name         = $origin->name; 
        $newOrigin->url          = $origin->url; 
        $newOrigin->idOriginType = $origin->idOriginType;
        $newOrigin->origin       = $origin->origin;
        
        //Relacionamos con revistas
        $newOrigin->magazines = $this->addMagazinesOrigin($origin);
        
        //Relacionamos con componentes
        $newOrigin->components = $this->addComponentsOrigin($origin);
        
        //Agregamos los campos
        $newOrigin->fields = $this->addFieldsOrigin($origin);
        
        //Agregamos los filtros
        $newOrigin->filters = $this->addFiltersOrigin($origin);
        
        //Guardamos y si hay un error devolvemos el mensaje
        if ($newOrigin->save() === false) {
            foreach ($newOrigin->getMessages() as $message) {
                $result->msg[] = $message->getMessage();
            }
        } else {
            $result->status = 'OK';
            $result->msg    = _('Datos guardados correctamente');
            $result->id     = $newOrigin->id;
        }
        
        return $result;
    }
    
    /**
     * Actualizamos un origen
     * 
     * @param mixed $origin
     */
    private function updateOrigin($origin){
        //Informacion que pasaremos por ajax para redirigir o mostrar un mensaje
        $result          = new stdClass();
        $result->status  = 'KO';
        $result->msg     = array();
        $result->id      = 0;
        
        //Inicializamos el modelo
        $updateOrigin = Models\LedOrigin::findFirstById($origin->id);
        
        //Asignamos los valores 
        $updateOrigin->name         = $origin->name; 
        $updateOrigin->url          = $origin->url; 
        $updateOrigin->idOriginType = $origin->idOriginType;
        $updateOrigin->origin       = $origin->origin;
        
        //Eliminamos y relacionamos con revistas
        if(isset($origin->magazines)){
            $updateOrigin->magazines = $this->addMagazinesOrigin($origin, true);
        }
        
        //Eliminamos y Relacionamos con componentes
        if(isset($origin->components)){
            $updateOrigin->components = $this->addComponentsOrigin($origin, true);
        }
        
        //Eliminamos y agregamos los campos
        if(isset($origin->fields)){
            $updateOrigin->fields = $this->addFieldsOrigin($origin, true);
        }
        //Eliminamos y agregamos los filtros
        if(isset($origin->filters)){
            $updateOrigin->filters = $this->addFiltersOrigin($origin, true);
        }
        
        //Actualizamos y si hay un error devolvemos el mensaje
        if ($updateOrigin->save() === false) {
            foreach ($updateOrigin->getMessages() as $message) {
                $result->msg[] = $message->getMessage();
            }
        } else {
            $result->status = 'OK';
            $result->msg    = _('Datos guardados correctamente');
            $result->id     = $updateOrigin->id;
        }
        
        return $result;
    }
    
    /**
     * Devolvemos un objeto con todas las relaciones a revistas
     * 
     * @param mixed $origin
     */
    private function addMagazinesOrigin($origin, $remove = false){
        //Limpiamos el contenido
        if(isset($origin->id) && $origin->id > 0 && $remove){
            $magazinesRemove = Models\LedMagazineOrigin::search([
                    "idOrigin" => $origin->id
                ]
            ); 
            $magazinesRemove->delete();
        }
        
        //Insertamos
        $magazinesOrigin = [];
        $magazines       = explode(',', $origin->magazines);
        foreach ($magazines as $magazine) {
            $magazineModel     = Models\LedMagazine::findFirstById($magazine);
            $magazinesOrigin[] = $magazineModel;
        }
        return $magazinesOrigin;
    }
    
    /**
     * Devolvemos un objeto con todas las relaciones a componentes
     * 
     * @param mixed $origin
     */
    private function addComponentsOrigin($origin, $remove = false){
        //Limpiamos el contenido
        if(isset($origin->id) && $origin->id > 0 && $remove){
            $componentsRemove = Models\LedComponentOrigin::search([
                    "idOrigin" => $origin->id
                ]
            ); 
            $componentsRemove->delete();
        }
        
        //Insertamos
        $componentsOrigin = [];
        $components       = explode(',', $origin->components);
        foreach ($components as $component) {
            $componentModel     = Models\LedComponent::findFirstById($component);
            $componentsOrigin[] = $componentModel;
        }
        return $componentsOrigin;
    }
    
    /**
     * Agregamos los campos relacionados al origen
     * 
     * @param type $origin
     */
    private function addFieldsOrigin($origin, $remove = false){
        //Limpiamos el contenido
        if(isset($origin->id) && $origin->id > 0 && $remove){
            $fieldsRemove = Models\LedOriginField::search([
                    "idOrigin" => $origin->id
                ]
            ); 
            $fieldsRemove->delete();
        }
        
        //Insertamos
        $fieldsOrigin = [];
        $fields       = json_decode($origin->fields);
        
        if($fields && count($fields) > 0){
            foreach ($fields as $field) {
                $fieldModel = new Models\LedOriginField();
                
                $fieldModel->field        = $field->field;
                $fieldModel->idDataType   = $field->idDataType;
                $fieldModel->defaultValue = $field->defaultValue;
                $fieldModel->tag          = $field->tag;

                $fieldsOrigin[] = $fieldModel;
            }
        }
        
        return $fieldsOrigin;
    }
    
    /**
     * Agregamos los filtros relacionados al origen
     * 
     * @param mixed $origin
     * @param object $newOrigin
     */
    private function addFiltersOrigin($origin, $remove = false){
        //Limpiamos el contenido
        if(isset($origin->id) && $origin->id > 0 && $remove){
            $filtersRemove = Models\LedOriginFilter::search([
                    "idOrigin" => $origin->id
                ]
            ); 
            $filtersRemove->delete();
        }
        
        //Insertamos
        $filtersOrigin = [];
        $filters       = json_decode($origin->filters);
        
        if($filters && count($filters) > 0){
            foreach ($filters as $filter) {
                $filterModel = new Models\LedOriginFilter();
                $options     = $this->addOptionsFilter($filter);
                $attributes  = $this->addAttributesFilter($filter);
                
                $filterModel->title       = $filter->title;
                $filterModel->description = $filter->description;
                $filterModel->idDataType  = $filter->idDataType;
                $filterModel->param       = $filter->param;
                $filterModel->value       = $filter->value;
                $filterModel->options     = $options;
                $filterModel->attributes  = $attributes;
                
                $filtersOrigin[] = $filterModel;
            }
        }
        
        return $filtersOrigin;
    }
    
    /**
     * Agregamos las opciones relacionadas al filtro
     * 
     * @param mixed $filter
     */
    private function addAttributesFilter($filter){
        $optionsFilter = [];
        $options       = $filter->options;
        
        if($options && count($options) > 0){
            foreach ($options as $option) {
                $optionFilter = new Models\LedOriginFilterOption();
                $optionFilter->name  = $option->name;
                $optionFilter->value = $option->value;

                $optionsFilter[] = $optionFilter;
            }
        }
        
        return $optionsFilter;
    }
    
    /**
     * Agregamos las opciones relacionadas a los atributos
     * 
     * @param mixed $filter
     */
    private function addOptionsFilter($filter){
        $attributesFilter = [];
        $attributes       = $filter->attributes;
        
        if($attributes && count($attributes) > 0){
            foreach ($attributes as $attribute) {
                $attributeFilter = new Models\LedOriginFilterAttribute();
                $attributeFilter->name  = $attribute->name;
                $attributeFilter->value = $attribute->value;

                $attributesFilter[] = $attributeFilter;
            }
        }
        
        return $attributesFilter;
    }
    
    /**
     * Eliminamos un origen
     * 
     * @param mixed $data
     */
    private function removeOrigin($data){
        //Informacion que pasaremos por ajax para mostrar un mensaje
        $result          = new stdClass();
        $result->status  = 'KO';
        $result->msg     = array();
        
        //Inicializamos el modelo
        $origin = Models\LedOrigin::findFirstById($data->id);
        
        //Guardamos y si hay un error devolvemos el mensaje
        if ($origin->delete() === false) {
            foreach ($origin->getMessages() as $message) {
                $result->msg[] = $message->getMessage();
            }
        } else {
            $result->status = 'OK';
            $result->msg    = _('Origen eliminado');
        }
        
        return $result;
    }
    
    /**
     * Para la vista de editar necesitamos rellenar los items
     * de los filtros con sus atributos y opciones correspondientes
     * 
     * @param mixed $filters
     * @return mixed
     */
    private function completeFiltersList($filters, $listsHelper, $unsetProps){
        $newFilters = [];
        
        foreach($filters as $filter){
            $newFilter              = new stdClass();
            $newFilter->id          = $filter->id;
            $newFilter->idOrigin    = $filter->idOrigin;
            $newFilter->title       = $filter->title;
            $newFilter->description = $filter->description;
            $newFilter->idDataType  = $filter->idDataType;
            $newFilter->param       = $filter->param;
            $newFilter->value       = $filter->value;
            
            //Opciones
            $options = \Models\LedOriginFilterOption::search([
                    "idOriginFilter" => $filter->id
            ]);
            $newFilter->options = $listsHelper->unsetListProps($options, $unsetProps);
            
            //Atributos
            $attributes = \Models\LedOriginFilterAttribute::search([
                    "idOriginFilter" => $filter->id
            ]);
            $newFilter->attributes = $listsHelper->unsetListProps($attributes, $unsetProps);
            
            $newFilters[] = $newFilter;
        }
        
        return $newFilters;
    }
}
