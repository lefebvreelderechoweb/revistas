<?php

class ErrorController extends BaseController
{

	public function error404Action() {
		$this->view->setRenderLevel(
			\Phalcon\Mvc\View::LEVEL_LAYOUT
		);
		$this->view->setLayout('login');
    }
	public function error500Action() {
		$this->view->setRenderLevel(
			\Phalcon\Mvc\View::LEVEL_LAYOUT
		);
		$this->view->setLayout('login');
    }
}