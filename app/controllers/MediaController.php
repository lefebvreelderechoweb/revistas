<?php

class MediaController extends BaseController {

    /**
     * @var \Models\Media
     */
    protected $model;

    /**
     * Inicializamos
     */
    public function onConstruct(){
        parent::onConstruct();
        $this->view->setMainView('plain');
        $this->model = \Models\LedMedia::getInstance();
    }

    /**
     * Comprobamos si existe el fichero
     * 
     * @param string $fileName
     * @param string $subFolder
     * @return mixed
     */
	private function fileExists( $fileName, $subFolder = null ){
		return $this->model->fileExists( $fileName, $subFolder );
	}

    /**
     * Eliminamos un fichero
     * 
     * @param string $fileName
     * @param string $subFolder
     * @return mixed
     */
	private function removeFile( $fileName, $subFolder = null ){
		return $this->model->removeFile( $fileName, $subFolder );
	}

    /**
     * Obtenemos el tamaño de una imagen
     * 
     * @param string $image
     * @return mixed
     */
    public static function getDataSizes($image){
        $sizeData = '';
        if(!empty($image->sizes) && is_object($image->sizes)) {
            foreach ($image->sizes as $key => $size) {
				$sizeData .= ' data-'.$key.'="'.$size.'"';
            }
        }
        return $sizeData;
    }

    /**
     * Lista de imagenes
     * 
     * @param string $dir
     * @param string $searchTxt
     * @param string $dateFrom
     * @param string $dateTo
     * @param type $page
     */
    public function indexAction( $dir = false, $searchTxt = null, $dateFrom = null, $dateTo = null, $page = 1 ){
        $script = $this->assets->collection('commonJs');
        $script->addJs('/js/ledPageBuilder/ledPageBuilderHelper.js');
        $script->addJs('/js/ledMedia.js');
        
        $dir		= $dir ? $dir : $this->config->repository->subDir;
		$dirName	= '';

		$searchTxt	= $searchTxt ? (string)urlencode($searchTxt) : null;

		if(is_numeric($dir)):
            $dirName = $this->config->repository->subDir;
		else:
			$dirName = $dir;
		endif;

		/*
		 * Nombre de las carpetas del repositorio deben de estar sin espacios
		 * ni guiones para que no falle, por lo que a la carpeta
		 * le quitamos dichos carácteres.
		 */
		$dirNameRepository	= str_replace(array(" ", "_"), "", $dirName );
		$dirNameRepository	= $this->tools->clearString($dirNameRepository);
        
		$pagination = (int)$this->config->repository->pagination;

        $data			= $this->model->getFiles( $dirNameRepository, $searchTxt, $dateFrom, $dateTo, $pagination, $page );
		$images			= $data['images'];
		$totalImages	= $data['totalImages'];
		$pages			= (int)ceil($totalImages / $pagination);
        
        $this->view->setVars([
            'directories'	=> $this->model->getDirs(),
            'images'		=> $images,
			'pages'			=> (int)$pages,
			'page'          => (int)$page,
            'tools'         => $this->tools
        ]);
    }

    /**
     * Subir un fichero
     */
	public function uploadFileAction(){

		$tmpDir			= TMP_PATH;
		$subFolder		= $this->config->repository->subDir;

		echo 'subfolder::::'.$subFolder;

		$result			= null;

		/*
		 * Guardamos imagen con las siguientes nomenclaturas:
		 * nombre_imagen_ledSize-original
		 * nombre_imagen_ledSize-thumbnail
		 * nombre_imagen_ledN-id_Newsletter-id_section ( para cada recorte que hagamos )
		 */
		$path			= ( isset( $_FILES['file']['tmp_name'] ) ? $_FILES['file']['tmp_name'] : '' );
		$fileName		= ( isset( $_FILES['file']['name'] ) ? strtolower(basename( $_FILES['file']['name']) ) : '' );

		if( $path && $fileName ){

			// Subimos fichero a plataforma en carpeta temporal 'tmp'
			$fileName   = $fileName;
            
            //Pasamos jpeg a jpg
            $fileName   = $this->strJpegToJpg($fileName);
            
            $tmpFile	= $tmpDir.$fileName;
			move_uploaded_file( $path, $tmpFile );

			$originUrl	= $tmpFile;
            
			$content	= file_get_contents( $originUrl );
			$result		= $this->model->uploadFile( $content, strtolower($this->tools->clearNameImage($fileName)), $subFolder, true );

			$imageSizes			= getimagesize( $originUrl );
			$imageWidth			= $imageSizes[0];
			$imageHeight		= $imageSizes[1];

			/*
			 * Realizamos recorte de miniatura 300x300
			 */
            $imageSizes			= getimagesize( $originUrl );
            $imageWidth			= $imageSizes[0] < 300 ? $imageSizes[0] : 300;
            $imageHeight        = $imageSizes[0] < 300 ? $imageSizes[1] : $imageSizes[1] / ($imageSizes[0] / 300);
            
			$paramsCrop = [
				'originUrl'		=> $originUrl,
				'prefixName'	=> '-ledSize-thumbnail',
				'newWidth'		=> $imageWidth,
				'newHeight'		=> $imageHeight,
                'posX'			=> 0,
				'posY'			=> 0,
				'deleteOrigin'	=> false
			];
			
            $this->createCrop( $paramsCrop );
            
            $path_parts			= pathinfo( $originUrl );
            $originBaseName		= $path_parts['basename'];
            $originFileName		= strtolower($path_parts['filename']);
            $originExtension	= $path_parts['extension'];
            
            $fileNameOrigin     = strtolower($this->tools->clearNameImage($originFileName)).'-ledSize-original.'.$originExtension;
            $result		        = $this->model->uploadFile( $content, $fileNameOrigin, $subFolder, true );

			// Borramos fichero de nuestra carpeta 'tmp'
			unlink( $originUrl );
		}

		$this->view->disable();
	}

	private function createCrop( $params = array() ){

		$result				= [];

		$originUrl			= $params['originUrl'];
		$prefixName			= $params['prefixName'];
		$subFolder			= isset( $params['subFolder'] ) ? $params['subFolder'] : $this->config->repository->subDir;
		$newHeight			= isset( $params['newHeight'] ) ? $params['newHeight'] : null;
		$newWidth			= isset( $params['newWidth'] ) ? $params['newWidth'] : null;
		$cropHeight			= isset( $params['cropHeight'] ) ? $params['cropHeight'] : null;
		$cropWidth			= isset( $params['cropWidth'] ) ? $params['cropWidth'] : null;
		$posX				= isset( $params['posX'] ) ? $params['posX'] : null;
		$posY				= isset( $params['posY'] ) ? $params['posY'] : null;
		$bDeleteOrigin		= isset( $params['deleteOrigin'] ) ? (bool)$params['deleteOrigin'] : true;

		/*
		 * Información de la ruta de la imagen
		 */
		$path_parts			= pathinfo( $originUrl );
		$originBaseName		= $path_parts['basename'];
		$originFileName		= strtolower($this->tools->clearNameImage($path_parts['filename']));
		$originExtension	= $path_parts['extension'];
        
		/*
		 * Información de tamaños de la imagen
		 */
		$imageSizes			= getimagesize( $originUrl );
		$imageWidth			= $imageSizes[0];
		$imageHeight		= $imageSizes[1];

		if( !$newWidth ){
			$newWidth = $imageWidth;
		}

		if( !$newHeight ){
			$newHeight = $imageHeight;
		}

		if( !$cropHeight ){
			$cropHeight = $imageHeight;
		}

		if( !$cropWidth ){
			$cropWidth = $imageWidth;
		}

		/*
		 * Posicionamiento de ejex X,Y sobre la imagen
		 * Sino están definidos el recorte se realizará
		 * en el centro de la imagen
		 */
		if( $posX === null ){
			$posX = ($imageWidth / 2 ) - ( $newWidth / 2 );

		}
		if( $posY === null ){
			$posY = ( $imageHeight / 2 ) - ( $newHeight / 2 );
		}

		/*
		 * Construimos nombre la imagen del recorte con el formato correcto
		 */
		$destinyBaseName	= $originFileName . $prefixName . '.' . $originExtension;
		$destinyUrlFile		= str_replace( $originBaseName, $destinyBaseName, $originUrl );
		$newDimImage		= imagecreatetruecolor( $newWidth, $newHeight );

		/*
		 *  Tratado de la imagen para realizar el recorte
		 */

		$resource = $this->createImageFromMimeType( $originUrl );
		$mimeType = $this->mimeContentType( $originUrl );

		imagealphablending( $newDimImage, false ); // permite mantener la transparencia
		imagecopyresampled( $newDimImage, $resource, 0, 0, $posX, $posY, $newWidth, $newHeight, $cropWidth, $cropHeight );
		$result['saveImage'] = $this->saveImageFromMimeType( $mimeType, $newDimImage, $destinyUrlFile, 100 );

		/*
		 * Borramos imagen previa si existe con el mismo nombre
		 */
		if( $this->fileExists( $destinyBaseName, $subFolder ) ){
			$this->removeFile( $destinyBaseName, $subFolder );
		}
		/*
		 * Subimos fichero al repositorio
		 */
		$result['uploadFile'] = $this->uploadCrop( $destinyUrlFile, $destinyBaseName, $subFolder );

		// Borramos fichero de nuestra carpeta 'tmp'
		unlink( $destinyUrlFile );

		if( file_exists ( $originUrl ) && $bDeleteOrigin ):
			unlink( $originUrl );
		endif;

		return $result;

	}


	/*
	 * Recogemos la imagen del repositorio, para traerla a nuestra carpeta
	 * de la app, de este modo, podremos realizar mejor el recorte y subir de
	 * nuevo al repositorio. Las imágenes generadas posteriormente serán
	 * borradas para librerar espacio ya que se trata de una carpeta temporal.
	 */
    public function uploadAction(){

		$originUrl		= (string)$this->request->getPost('imgSrc');

		$cropBoxData	= (array)$this->request->getPost('cropBoxData');
		$cropData		= (array)$this->request->getPost('cropData');
		$imgData		= (array)$this->request->getPost('imgData');
		$canvasData		= (array)$this->request->getPost('canvasData');

		$tmpDir			= TMP_PATH;
		$subFolder		= (string)$this->request->getPost('subFolder');

		$fileName		= (string)$this->request->getPost('imgName');
		$fileData		= explode('.', $fileName);
		$fileBaseName	= $fileData[0];
		$fileExt		= $fileData[1];

		/*
		 * Generamos los valores para las nuevas dimensiones y los recortes
		 * con posicionamiento XY
		 */
		$newHeight		= intval( $cropBoxData['height']);
		$newWidth		= intval( $cropBoxData['width'] );

		$scaleRatioImg	= $imgData['naturalWidth'] / $imgData['width'];
		$cropWidth		= intval( $scaleRatioImg * $newWidth );
		$cropHeight		= intval( $scaleRatioImg * $newHeight );
		$posX			= intval( $scaleRatioImg * ( $cropBoxData['left'] - $canvasData['left'] ) );
		$posY			= intval( $scaleRatioImg * ( $cropBoxData['top'] - $canvasData['top'] ) );

		/*
		 * Obtenemos el contenido de la imagen del repositorio.
		 * Tener en cuenta que hay que acceder mediante 'http' y
		 * seguir la lógica de la url para una correcta extracción
		 * de la imagen.
		 */
        $urlImage   = $originUrl;
		$tmpFile	= $tmpDir.$fileName;
		$content	= file_get_contents( $urlImage );

		/*
		 * Creamos imagen a través del content y la guardamos temporalmente
		 * en nuestra carpeta tmp para hacer el tratado de la imagen
		 * y el recorte correspondiente
		 */
		$image		= imagecreatefromstring( $content );
		$result		= $this->saveImageFromMimeType( $this->mimeContentType( $fileName ), $image, $tmpFile );

		/*
		 * Liberamos memoria
		 */
		imagedestroy($image);

		$urlImage = $tmpFile;

		$paramsCrop = [
			'originUrl'			=> $urlImage,
			'subFolder'			=> $subFolder,
			'prefixName'		=> '-ledSize-p_'.$newWidth.'-x_'.$newHeight.'_'.$this->tools->getRandomInt(),
			'newWidth'			=> $newWidth,
			'newHeight'			=> $newHeight,
			'cropWidth'			=> $cropWidth,
			'cropHeight'		=> $cropHeight,
			'posX'				=> $posX,
			'posY'				=> $posY
		];


	   $r = $this->createCrop( $paramsCrop );

	   $this->view->disable();
       
       echo json_encode([
           'img' => $this->model->getFile($paramsCrop->prefixName, $this->config->repository->subDir)
       ]);
    }

	/*
	 * Subimos al repositorio la imagen generada del recorte de la
	 * imagen original.
	 */
	private function uploadCrop( $destinyUrlFile, $destinyBaseName, $subFolder ){

		$content = file_get_contents( $destinyUrlFile );
		return $this->model->uploadFile( $content, $this->tools->clearNameImage($destinyBaseName), $subFolder, true );
	}

	/*
	 * Guardamos imagen según el tipo de ésta.
	 */
	private function saveImageFromMimeType( $mimeType, $imageResource, $destFileName, $quality = 100 ) {

		$result = false;


		switch ( $mimeType) :

			case 'image/jpeg':
				$result = imagejpeg( $imageResource, $destFileName, $quality );
				break;

			case 'image/bmp':
				$result = imagebmp( $imageResource, $destFileName );
				break;

			case 'image/gif':
				$result = imagegif( $imageResource, $destFileName );
				break;

			case 'image/png':
				$quality = 0; // Deja la máxima calidad
				imagealphablending($imageResource, false); // permite mantener la transparencia
				imagesavealpha($imageResource, true); // permite mantener la transparencia
				$result = imagepng( $imageResource, $destFileName, $quality );
				break;

			default:
				break;

		endswitch;

		return $result;
	}

	/*
	 * Generamos la imagen a partir del tipo de ésta.
	 */
	private function createImageFromMimeType( $file ) {

		$result = null;
		$mType	= $this->mimeContentType( $file );

		switch ( $mType ):

			case 'image/jpeg':
				$result = imagecreatefromjpeg( $file );
				break;

			case 'image/bmp':
				$result = imagecreatefrombmp( $file );
				break;

			case 'image/gif':
				$result = imagecreatefromgif( $file );
				break;

			case 'image/png':
				$result = imagecreatefrompng( $file );
				break;

			default:
				break;

		endswitch;

		return $result;
	}

	 private function mimeContentType($filename) {

        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );

        $ext = strtolower(array_pop(explode('.',$filename)));
        if (array_key_exists($ext, $mime_types)) {
            return $mime_types[$ext];
        }
        elseif (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mimetype = finfo_file($finfo, $filename);
            finfo_close($finfo);
            return $mimetype;
        }
        else {
            return 'application/octet-stream';
        }
    }
    
    /**
     * Comprobamos que la imagen existe
     * @param string $dir
     * @param string $searchTxt
     * 
     * return json 
     */
    public function issetImageNameAction( $dir = "", $searchTxt = "" ){
        $this->view->disable();
            
        //Limpiamos los textos que pasen por get
        $issetImage      = false;
        $errorMsg        = '';
        
        //Variables si se envia por post o por get
        if($this->request->isPost()){
			$dir	    = $this->request->getPost('subFolder', 'string') ? $this->request->getPost('subFolder', 'string') : "";
            $searchTxt	= $this->request->getPost('imageName', 'string') ? $this->request->getPost('imageName', 'string') : "";
        }else{
            $dir            = filter_var($dir, FILTER_SANITIZE_STRING);
            $searchTxt      = filter_var($searchTxt, FILTER_SANITIZE_STRING);
        }
        
        //Eliminamos la extension del nombre del fichero
        $searchTxt = $this->tools->clearNameImage($this->getFileName($searchTxt));
        
        //Utilizamos el try para obligar a devolver aunque de problemas el servicio
        try {
            $issetImage     = false;
            $data			= $this->model->getFiles( $dir, $searchTxt, null, null, 9999, 0 );
            $images			= $data['images'];

            //Comprobamos que el la imagen no esta en la lista
            foreach ($images as $image){
                if(strtolower($this->getFileName($image->name)) === strtolower($searchTxt)){
                    $issetImage = true;
                    $compareFileName = strtolower($image->name);
                }
            }
        } catch (Exception $exc) {
            $issetImage = false;
            $error      = _('Error en la comprobación de la imagen. Pruebe más tarde');
        }
        
        //devolvemos la respuesta en formato JSON
        echo json_encode([
            'issetImage'    => $issetImage,
            'error'         => $errorMsg,
            'dir'           => $dir,
            'fileName'      => strtolower($this->getFileName($searchTxt)),
            'compare'       => $compareFileName
        ]);
    }
    
    /**
     * Debido a que el servicio da problemas al devolver
     * imagenes jpeg modificamos la extensión para que de los menos
     * problemas posibles
     *  
     * @param string $fileName
     * @return string $fileName
     */
    public function strJpegToJpg($fileName){
        $fileName_parts = pathinfo($fileName);
        $fileNameExtension = $fileName_parts['extension'];

        if (preg_match('/jpeg/i', $fileNameExtension)) {
            $fileName = $fileName_parts['filename'] . '.jpg';
        }
        
        return $fileName;
    }
    
    /**
     * Devolvemos el nombre del fichero 
     * sin la extension
     * 
     * @param string $fileName
     * @return string 
     */
    public function getFileName($fileName){
        $fileName_parts = pathinfo($fileName);
        return $fileName_parts['filename'];
    }
}