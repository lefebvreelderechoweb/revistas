<?php

use Phalcon\Mvc\Controller;

/**
 * Controllador usado solo para extender controllers que tienen que tener sesion iniciada
 * por parte del usuario para que se puedan acceder
 */
class BaseLoggedController extends BaseController {

	/**
	 * Usuario de sessión
	 * @var User|null
	 */
	public $user;

	public function onConstruct() {

		/**
		 * Obtenemos el usuario de sessión, pueder ser un objeto User o null
		 */
		//$this->user = $this->session->get('user');
        
		/**
		 * Si no hay usuario en sesion obligamos a que la inicie
		 */
		/*if ($this->user === null) {
			return $this->dispatcher->forward(array(
					'controller' => 'auth',
					'action' => 'index'
			));
		}*/
        
        /**
		 * Variables comunes para todas las vistas de la app
		 */
        $this->view->setVars([
			'user' => $this->user
		]);
        
		/**
		 * Recursos de la app
		 */
		$this->initAssets();
	}

}