<?php
use Phalcon\DI;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Validation\Message;
use Phalcon\Validation;
use Library\builder\Components as BuilderComponents;

class IssueController extends BaseController {

    public function initialize(){
        $script = $this->assets->collection('commonJs');
        $script->addJs('/js/ledHelper.js');
        $script->addJs('/js/ledIssue.js');
    }
    
    public function indexAction($idMagazine) {

        $commonJs = $this->assets->collection('commonJs');
        $commonJs->addJs('js/pdf.js');

        $di         = \Phalcon\DI::getDefault();
        $routes     = $di->getConfig()->routesPDFs;

        /**
         * Mostrar listado de números de una revista
         */

        $numberPage = ($this->request->getQuery("page", "int") == null) ? 1 : $this->request->getQuery("page", "int");

        $auth = $this->session->get('user');

        $searchParam = array(
            'idUser' => $auth->id,
            'idMagazine' => $idMagazine
        );

        $query = Criteria::fromInput($this->di, "Models\LedAccess", $searchParam);
        $this->persistent->searchParams = $query->getParams();

        $parameters = array();

        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }


        $access = Models\LedAccess::findFirst($parameters);

        if ($access != false){

            $magazine   = $access->magazine;
            $issues     = $magazine->issue;

            if (count($issues) == 0) {

                $this->flash->notice("La búsqueda no ha producido ningún resultado");

                return $this->dispatcher->forward(
                    [
                        "controller" => "magazine",
                        "action"     => "index",
                    ]
                );
            }

            $paginator = new Paginator(array(
                "data"  => $issues,
                "limit" => 10,
                "page"  => $numberPage
            ));

            $paginate           = $paginator->getPaginate();
            $itemsPage          = $paginate->items;
            $aIssues            = array();
            $magazineName       = '';
            $lastNumberMagazine = 0;

            if ( count($itemsPage) > 0 ){
                $magazineName       = $itemsPage[0]->magazine->name;
                $lastNumberMagazine = $itemsPage[0]->magazine->idNumber;

                foreach ( $itemsPage as $key => $issue ){

                    $oIssue             = new stdClass();
                    $oIssue->itemORM    = $issue;
                    $oIssue->class      = '';

                    if ( $lastNumberMagazine == $issue->id ){
                        $oIssue->class = 'last-number';
                    }else if ($issue->publishDate == ''){
                        //Es un borrador
                        $oIssue->class = 'draft-number';
                    }

                    /**
                     * Tratamiento de la URL
                     */
                    $urlBase = 'https://revistas.lefebvre.es/';
                    $baseName = Lefebvre\Util\Strings::removeAccents($issue->magazine->name);
                    $baseName = str_replace(" ", "-", strtolower($baseName));

                    if ( $oIssue->class == '' || $oIssue->class == 'draft-number'){
                        //Estamos en un número pasado o en un borrador
                        $numberName = Lefebvre\Util\Strings::removeAccents($issue->name);
                        $numberName = str_replace(" ", "-", strtolower($numberName));
                        $numberName = str_replace(".", "", $numberName);
                        $numberName = str_replace(",", "", $numberName);

                        $oIssue->finalURL = $urlBase . $baseName . '/' . $numberName;
                        $oIssue->prettyURL = '/' . $baseName . '/' . $numberName;

                    }else{
                        //Estamos en el número activo
                        $oIssue->finalURL = $urlBase . $baseName;
                        $oIssue->prettyURL = '/' . $baseName . '/';
                    }

                    $oIssue->userRol = $issue->magazine->rol[0]->rolName;
                    $aIssues[] = $oIssue;
                }

            }

            $this->view->page = $paginator->getPaginate();
            $this->view->setVars([
                'idMagazine'    => $magazine->id,
                'routes'        => $routes,
                'issues'        => $aIssues,
                'magazineName'  => $magazineName
            ]);
            
        }else{
            //Usuario no válido para ver esta revista
            return $this->dispatcher->forward(
                [
                    "controller" => "auth",
                    "action"     => "index",
                ]
            );
        }
    }

    /**
     * Ejecuta la "búsqueda" según los criterios enviados desde el "indexAction".
     * Devuelve un paginador para los resultados.
     * @return type
     */
    public function searchAction() {
        $numberPage = 1;

        if ($this->request->isPost()) {

            // Criteria hace una busqueda en el modelo indicado con todos los campos del formulario
            // que coincidan con un campo de la tabla
            $query = Criteria::fromInput($this->di, "Models\LedIssue", $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {

            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = array();

        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $issues = Models\LedIssue::find($parameters);

        if (count($issues) == 0) {

            $this->flash->notice("La búsqueda no ha producido ningún resultado");

            return $this->dispatcher->forward(
                [
                    "controller" => "issue",
                    "action"     => "index",
                ]
            );
        }
        $paginator = new Paginator(array(
            "data"  => $issues,
            "limit" => 10,
            "page"  => $numberPage
        ));
        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Edita un número en base a su id.
     * @param type $id
     * @return type
     */
    public function editAction($id){

        $commonJs = $this->assets->collection('commonJs');
        $commonJs->addJs('js/wysihtml5/advanced.js');
        $commonJs->addJs('js/wysihtml5/wysihtml5-0.3.0.min.js');
        $commonJs->addJs('js/wysihtml5/main.js');

        $auth = $this->session->get('user');

        $issue = Models\LedIssue::findFirstById($id);

        $searchParam = array(
            'idUser' => $auth->id,
            'idMagazine' => $issue->idMagazine
        );

        $query = Criteria::fromInput($this->di, "Models\LedAccess", $searchParam);
        $this->persistent->searchParams = $query->getParams();

        $parameters = array();

        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }


        $access = Models\LedAccess::findFirst($parameters);

        if ($access != false){

            $magazine   = $access->magazine;

            $contentsIssue = $issue->contentIssue;

            $tribunes           = array();
            $forums             = array();
            $jurisprudences     = array();
            $novedadLeg         = array();
            $consultas          = array();
            $docAdministrativa  = array();
            $orgConsultivos     = array();
            $resTribunales      = array();

            foreach( $contentsIssue as $contentIssue ){

                switch ($contentIssue->idContentType) {

                    case \Util\LegalContent::TRIBUNESID:
                        $tribunes[] = $contentIssue;
                        break;

                    case \Util\LegalContent::FORUMID:
                        $forums[] = $contentIssue;
                        break;

                    case \Util\LegalContent::JURISPRUDENCEID:
                        $jurisprudences[] = $contentIssue;
                        break;

                    case \Util\LegalContent::NOVLEGISID:
                        $novedadLeg[] = $contentIssue;
                        break;

                    case \Util\LegalContent::CONSULTASID:
                        $consultas[] = $contentIssue;
                        break;

                    case \Util\LegalContent::DOCADMINID:
                        $docAdministrativa[] = $contentIssue;
                        break;

                    case \Util\LegalContent::ORGCONSULTIVOSID:
                        $orgConsultivos[] = $contentIssue;
                        break;

                    case \Util\LegalContent::RESTRIBUNALESID:
                        $resTribunales[] = $contentIssue;
                        break;
                }

            }

            $contents = array();
            $contents[\Util\LegalContent::TRIBUNESID]       = $tribunes;
            $contents[\Util\LegalContent::FORUMID]          = $forums;
            $contents[\Util\LegalContent::JURISPRUDENCEID]  = $jurisprudences;
            $contents[\Util\LegalContent::NOVLEGISID]       = $novedadLeg;
            $contents[\Util\LegalContent::CONSULTASID]      = $consultas;
            $contents[\Util\LegalContent::DOCADMINID]       = $docAdministrativa;
            $contents[\Util\LegalContent::ORGCONSULTIVOSID] = $orgConsultivos;
            $contents[\Util\LegalContent::RESTRIBUNALESID]  = $resTribunales;

            $this->view->setVars([
                'issue'     => $issue,
                'contents'  => $contents,
            ]);

        }else{
            //Usuario no válido para ver esta revista
            return $this->dispatcher->forward(
                [
                    "controller" => "auth",
                    "action"     => "index",
                ]
            );
        }
    }

    /**
     * Guarda la revista actual
     * @return type
     */
    public function saveAction(){

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(
                [
                    "controller" => "issue",
                    "action"     => "index",
                ]
            );
        }

        $id = $this->request->getPost("id", "int");

        $issue = Models\LedIssue::findFirstById($id);

        if (!$issue) {
            $this->flashSession->error("El producto no existe");
            return $this->dispatcher->forward(
                [
                    "controller" => "issue",
                    "action"     => "index",
                ]
            );
        }

        $form = new IssueForm;
        $this->view->form = $form;
        $data = $this->request->getPost();

        if (!$form->isValid($data, $issue)) {

            foreach ($form->getMessages() as $message) {
                $this->flashSession->error($message);
            }

            return $this->dispatcher->forward(
                [
                    "controller" => "issue",
                    "action"     => "edit",
                    "params"     => [$id]
                ]
            );
        }

        try{
            if(!$issue->save()){
                $errors = $issue->getMessages();

                foreach ($issue->getMessages() as $message) {
                    //$this->flash->error($message);
                    $this->flashSession->error($message);
                }
                return $this->dispatcher->forward(
                    [
                        "controller" => "issue",
                        "action"     => "edit",
                        "params"     => [$id]
                    ]
                );
            }

            $form->clear();

            $this->flashSession->success('El número ha sido actualizado correctamente');

            return $this->dispatcher->forward(
                [
                    "controller" => "issue",
                    "action"     => "index",
                ]
            );

        } catch(PDOException $e){
            $error = $e->getMessage();

            $this->flashSession->error($error);

            return $this->dispatcher->forward(
                [
                    "controller" => "issue",
                    "action"     => "edit",
                    "params"     => [$id]
                ]
            );
        }
    }

    /**
     * Muestra el formulario de creación de nuevo número
     */
    public function newAction($idMagazine){

        $commonJs = $this->assets->collection('commonJs');
        $commonJs->addJs('js/wysihtml5/advanced.js');
        $commonJs->addJs('js/wysihtml5/wysihtml5-0.3.0.min.js');
        $commonJs->addJs('js/wysihtml5/main.js');

        $auth = $this->session->get('user');

        $searchParam = array(
            'idUser' => $auth->id,
            'idMagazine' => $idMagazine
        );

        $query = Criteria::fromInput($this->di, "Models\LedAccess", $searchParam);
        $this->persistent->searchParams = $query->getParams();

        $parameters = array();

        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }


        $access = Models\LedAccess::findFirst($parameters);

        if ($access != false){

            $magazine   = $access->magazine;

            $this->view->setVars([
                'form'          => new ContentForm(null, array('edit' => true)),
                'magazine'      => $magazine,
                //'lastTribunes'  => $lastTribunes,
                'containerClass' => 'fullContainer'
            ]);

        }else{
            //Usuario no válido para ver esta revista
            return $this->dispatcher->forward(
                [
                    "controller" => "auth",
                    "action"     => "index",
                ]
            );
        }
    }

    public function createAction() {

        $result = new stdClass();
        $result->OK = false;
        $result->msg = array();

        $onlineLegalContent = new Util\LegalContent();
        $issueUtil = new Util\Issue();

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(
                    [
                        "controller" => "auth",
                        "action" => "index",
                    ]
            );
        }
        $user   = $this->session->get('user');
        $userID = $user->id;

        $urlBase = 'https://revistas.lefebvre.es/';

        $data = $this->request->getPost();

        $magazine = \Models\LedMagazine::findFirst("id = ".$data['idMagazine']);
        if (isset($magazine->name)){
            $baseName = Lefebvre\Util\Strings::removeAccents($magazine->name);
            $baseName = str_replace(" ", "-", strtolower($baseName));
        }

        if(!isset($data['idIssue'])){
            //Es un número nuevo

            //Guardamos primero la revista
            $nameIssue = $data['nameIssue'] . ' Draft' . date("Y-m-d");

            $numberName = Lefebvre\Util\Strings::removeAccents($nameIssue);
            $numberName = str_replace(" ", "-", strtolower($numberName));
            $numberName = str_replace(".", "", $numberName);
            $numberName = str_replace(",", "", $numberName);

            $finalURL = $urlBase . $baseName . '/' . $numberName;

            $issue = new \Models\LedIssue();

            $issue->idMagazine  = $data['idMagazine'];
            $issue->name        = $nameIssue;
            $issue->url         = $finalURL;
            $issue->uic         = $userID;
            $issue->uc          = $userID;
            
            //Compongo los contenidos del número de revista
            $comps = $issueUtil->getContentIssue($data, $issue);
            
            $issue->contentIssue = $comps;
            
        }else{
            //Edición de un número de revista ya creado
            
            $issue = Models\LedIssue::findFirstById($data['idIssue']);
            $issue->uic = $userID;
            $issue->uc  = $userID;

            if ($issue->name != $data['nameIssue']){
                //Ha cambiado el nombre de revista
                $nameIssue = $data['nameIssue'] . ' Draft' . date("Y-m-d");

                $numberName = Lefebvre\Util\Strings::removeAccents($nameIssue);
                $numberName = str_replace(" ", "-", strtolower($numberName));
                $numberName = str_replace(".", "", $numberName);
                $numberName = str_replace(",", "", $numberName);

                $finalURL = $urlBase . $baseName . '/' . $numberName;

                $issue->name        = $nameIssue;
                $issue->url         = $finalURL;
            }
            
            //Compongo los contenidos del número de revista
            $comps = $issueUtil->getContentIssue($data, $issue);

            $issue->contentIssue = $comps;
        }
        
        if ( $issue->save() === false ){
            foreach ( $issue->getMessages() as $message ){

                $result->msg[] = $message->getMessage();
            }
        }else{
            $result->OK     = true;
            $result->msg[]  = 'Datos guardados correctamente';
        }

        echo json_encode($result);
        exit;
    }

    public function deleteAction(){

        $result         = new stdClass();
        $result->OK     = false;
        $result->msg    = array();

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(
                    [
                        "controller" => "auth",
                        "action" => "index",
                    ]
            );
        }

        $data = $this->request->getPost();

        $issue = Models\LedIssue::findFirstById($data['id']);

        if(!$issue){

            $result->msg[]  = 'El número no existe';
        }else{

            if ( $issue->delete() === false ){
                foreach ( $issue->getMessages() as $message ){
                    $result->msg[] = $message->getMessage();
                }
            }else{
                $result->OK     = true;
                $result->msg[]  = 'Número eliminado correctamente';
            }
        }

        echo json_encode($result);
        exit;

    }

    public function cloneAction(){

        $result         = new stdClass();
        $result->OK     = false;
        $result->msg    = array();

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(
                    [
                        "controller" => "auth",
                        "action" => "index",
                    ]
            );
        }

        $data = $this->request->getPost();

        $issue = new Models\LedIssue($di);
        $issue = $issue::findFirstById($data['id']);

        if(!$issue){

            $result->msg[]  = 'El número no existe';

        }else{
            //Llamamos al servicio que clona el número y todas sus relaciones
            $idNewIssue = $this->dataBaseService->cloneRecord($issue);

            $result->OK     = true;
            $result->msg[]  = 'Número clonado correctamente';

            //Vaciaremos siempre la fecha de publicación pues los números de revista
            //clonados quedarán como borradores y cambiamos el nombre del número
            //añadiendo la palabra Clone para distingir
            $newIssue = Models\LedIssue::findFirstById((int)$idNewIssue);
            $newIssue->publishDate = null;
            $newIssue->name = $newIssue->name . " Clon";
            $newIssue->update();
        }

        echo json_encode($result);
        exit;

    }
    
    /**
     * Pagina del page builder desde el back
     * para la portada del numero de la revista
     */
    public function coverAction($issueId = false) {
        
        /**
         *  ANTONIO!!!!! LAS CONSTANTES DE LOS TIPOS DE CONTENIDO ESTÁN EN EL UTIL LEGALCONTENT.PHP
         *  LAS HE ELIMINADO DEL BASE CONTROLLER
         */
        
        
        //Cargamos los ficheros necesarios para el fucionamiento del builder
        $this->initPageBuilder();
        
        //Numero
        $issue = \Models\LedIssue::findFirstById($issueId);
        
        //Contenido del numero
        $contentsIssue = (object) [
            'tribunas'                => \Models\LedIssue::getContentsIssue($issueId, self::TRIBUNAS),
            'foro_abierto'            => \Models\LedIssue::getContentsIssue($issueId, self::FORO_ABIERTO),
            'jurispridencias'         => \Models\LedIssue::getContentsIssue($issueId, self::JURISPRUDENCIAS),
            'legislacion'             => \Models\LedIssue::getContentsIssue($issueId, self::LEGISLACION),
            'consultas'               => \Models\LedIssue::getContentsIssue($issueId, self::CONSULTAS),
            'doctrina_administrativa' => \Models\LedIssue::getContentsIssue($issueId, self::DOCTRINA_ADMINISTRATIVA),
            'organos_consultivos'     => \Models\LedIssue::getContentsIssue($issueId, self::ORGANOS_CONSULTIVOS),
            'respuestas_tribunales'   => \Models\LedIssue::getContentsIssue($issueId, self::RESPUESTAS_TRIBUNALES)
        ];
        
        //Comprobamos que el objeto $contentsIssue no esta vacio
        $listsHelper          = new Helpers\Lists\Lists();
        $contentsIssueIsEmpty = $listsHelper->listObjectsIsEmpty($contentsIssue);
        
        //Obtenemos el listado de los componentes
        $builderComponents = new BuilderComponents('content');
        $components        = $builderComponents->getComponents();

        $this->view->setVars([
            'issue'                => $issue,
            'contentsIssue'        => $contentsIssue,
            'components'           => $components,
            'footerClass'          => 'hidden',
            'containerClass'       => 'fullContainer',
            'tools'                => $this->tools,
            'contentsIssueIsEmpty' => $contentsIssueIsEmpty
        ]);
    }

    /**
     * Añadimos lo necesario para que el builder funcione
     */
    protected function initPageBuilder() {
        //JS necesarios para el builder
        $script = $this->assets->collection('commonJs');
        
        $script->addJs('/js/library/ledpagebuilder/ledPageBuilderFields.js');
        $script->addJs('/js/library/ledpagebuilder/ledPageBuilderComponents.js');
        $script->addJs('/js/library/ledpagebuilder/ledPageBuilder.js');
        $script->addJs('/js/library/ledpagebuilder/ledPageBuilderInit.js');
        $script->addJs('/js/ledMedia.js');
        $script->addJs('/js/cropper.min.js');
        $script->addJs('/js/library/ckeditor/ckeditor.js');

        //CSS necesarios para el builder
        $css = $this->assets->collection('commonCss');

        $css->addCss('/css/library/builder/ledPageBuilderComponents.css');
        $css->addCss('/css/library/builder/ledPageBuilderFields.css');
        $css->addCss('/css/library/builder/ledPageBuilder.css');
        $css->addCss('/css/font-awesome.min.css');
        $css->addCss('/css/cropper.min.css');
        $css->addCss('/css/library/builder/components/components.css');
        
        Library\builder\Components::addComponentsStyles($css);
    }

}
