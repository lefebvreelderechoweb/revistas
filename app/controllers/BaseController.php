<?php

use Phalcon\Mvc\Controller;

class BaseController extends Controller {

    /**
     * Usuario de sessión
     * @var User|null
     */
    public $user;
    public $rootvar;
    public $tools;

    public function onConstruct() {
        
        $this->initAssets();
        
        $this->initVarsJS();
        
        $this->initRootVar();
        
        /**
         * Herramientas disponibles en todos los controladores
         */
        $tools = new \Helpers\Tools\Tools();
        $this->tools = $tools;
        
        $di         = \Phalcon\DI::getDefault();
        $pathPdfs   = $di->getConfig()->routesPDFs;
         

        /**
         * Variables comunes para todas las vistas de la app
         */
        $this->view->setVars([
            'controller'    => $this->dispatcher->getControllerName(),
            'action'        => $this->dispatcher->getActionName(),
            'user'          => $this->session->get('user'),
            'baseUrl'       => $this->config->application->baseDomain,
            'pathPdfs'      => $pathPdfs
        ]);
    }

    /**
     * Recursos de la app
     */
    public function initAssets() {
        $commonCss = $this->assets->collection('commonCss');
        $commonCss->addCss('css/bootstrap.min.css');
        $commonCss->addCss('css/bootstrap-led.css');
        $commonCss->addCss('css/jquery-ui.css');
        $commonCss->addCss('css/icon-styles.css');
        $commonCss->addCss('css/led-colors.css');
        $commonCss->addCss('css/common.css');
        $commonCss->addCss('css/top-bar.css');
        $commonCss->addCss('css/footer.css');
        $commonCss->addCss('//fonts.googleapis.com/css?family=Playfair+Display:400|Raleway:400,600,800', false);
        $commonCss->addCss('https://assets.lefebvre.es/media/css/fonts-mttmilano.css', false);
        $commonCss->addCss('https://assets.lefebvre.es/media/lf-font/lf-font.css', false);
        $commonCss->addCss('css/modal-media.css');
        $commonCss->addCss('css/led-modal-loading.css');
        $commonCss->addCss('https://assets.lefebvre.es/media/lf-font/lf-font.css');
        

        $commonJs = $this->assets->collection('commonJs');
        $commonJs->addJs('js/jquery.min.js');
        $commonJs->addJs('js/bootstrap.min.js');
        $commonJs->addJs('js/jquery-ui.min.js');
        $commonJs->addJs('js/ledBuilder.js');
        $commonJs->addJs('js/ledForm.js');
        $commonJs->addJs('js/ledEvents.js');
        $commonJs->addJs('js/ledFilters.js');
        /*
		 * Loading/Cargando - Corporativo Lefebvre
		 */
        $commonJs->addJs('js/led-modal-loading.js');
        
        $headerJs = $this->assets->collection('headerJs');
    }
    
    /**
     * Variables disponibles en JS
     */
    public function initVarsJS() {
        
        $this->assets->addInlineJs('var APP =  '. json_encode([
				'uri'       => $this->dispatcher->getControllerName(),
                'base_url'  => $this->url->get()
			]));
    }
    
    public function initRootVar() {
        $this->rootvar = [
            'rootvar' => [
                'bodyClass' =>
                    $this->dispatcher->getControllerName() . 'Controller ' .
                    $this->dispatcher->getActionName() . 'Action'
            ]
        ];
        
        $this->view->setVars((array)$this->rootvar);
    }

    protected function getApplication() {
        return $this->config->application;
    }
    
    /**
     * Configuramos el Header para las funciones
     * que devuelven un JSON
     */
    protected function responseJson(){
        header("Content-type: application/json; charset=utf-8");
    }
}
