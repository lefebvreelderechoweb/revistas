
<?php

use Phalcon\DI;
use Library\builder\Components as BuilderComponents;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\Model\Criteria;

class MagazineController extends BaseController {

    public function indexAction() {

        /* $magazine = Models\LedMagazine::findFirst('id = 2');


          foreach($magazine->issue as $issue){

          foreach ($issue->contentIssue as $content){
          var_dump($content->id);
          var_dump($content->contentType->contentType);
          }
          }
          die(); */

        /**
         * Mostrar listado de revistas en función del rol del usuario
         */
        $auth = $this->session->get('user');
        $user = Models\LedUser::findFirst('id =' . $auth->id);

        $magazines = $user->magazine;

        $this->view->setVars([
            'magazines'      => $magazines,
            'containerClass' => 'fullContainer'
        ]);


        /**
         * Muestra la vista "búsqueda"
         */
        /* $this->persistent->searchParams = null;
          $this->view->form = new MagazineForm(); */
    }

    /**
     * Ejecuta la "búsqueda" según los criterios enviados desde el "indexAction".
     * Devuelve un paginador para los resultados.
     * @return type
     */
    public function searchAction() {
        $numberPage = 1;

        if ($this->request->isPost()) {

            // Criteria hace una busqueda en el modelo indicado con todos los campos del formulario
            // que coincidan con un campo de la tabla
            $query = Criteria::fromInput($this->di, "Models\LedMagazine", $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {

            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = array();

        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $magazines = Models\LedMagazine::find($parameters);

        if (count($magazines) == 0) {

            $this->flash->notice("La búsqueda no ha producido ningún resultado");

            return $this->dispatcher->forward(
                    [
                        "controller" => "magazine",
                        "action" => "index",
                    ]
            );
        }
        $paginator = new Paginator(array(
            "data" => $magazines,
            "limit" => 3,
            "page" => $numberPage
        ));
        $this->view->page = $paginator->getPaginate();
    }

    /**
     * Edita una revista en base a su id.
     * @param type $id
     * @return type
     */
    public function editAction($id) {

        if (!$this->request->isPost()) {

            $magazine = Models\LedMagazine::findFirstById($id);

            if (!$magazine) {

                $this->flash->error("Revista no encontrada");
                return $this->dispatcher->forward(
                        [
                            "controller" => "magazine",
                            "action" => "index",
                        ]
                );
            }

            $this->view->form = new MagazineForm($magazine, array('edit' => true));
        }
    }

    /**
     * Guarda la revista actual
     * @return type
     */
    public function saveAction() {

        if (!$this->request->isPost()) {
            return $this->dispatcher->forward(
                    [
                        "controller" => "magazine",
                        "action" => "index",
                    ]
            );
        }

        $id = $this->request->getPost("id", "int");

        $magazine = Models\LedMagazine::findFirstById($id);

        if (!$magazine) {
            $this->flashSession->error("El producto no existe");
            return $this->dispatcher->forward(
                    [
                        "controller" => "magazine",
                        "action" => "index",
                    ]
            );
        }

        $form = new MagazineForm;
        $this->view->form = $form;
        $data = $this->request->getPost();

        if (!$form->isValid($data, $magazine)) {

            foreach ($form->getMessages() as $message) {
                $this->flashSession->error($message);
            }

            return $this->dispatcher->forward(
                    [
                        "controller" => "magazine",
                        "action" => "edit",
                        "params" => [$id]
                    ]
            );
        }

        try {
            if (!$magazine->save()) {
                $errors = $magazine->getMessages();

                foreach ($magazine->getMessages() as $message) {
                    //$this->flash->error($message);
                    $this->flashSession->error($message);
                }
                return $this->dispatcher->forward(
                        [
                            "controller" => "magazine",
                            "action" => "edit",
                            "params" => [$id]
                        ]
                );
            }

            $form->clear();

            $this->flashSession->success('La revista ha sido actualizada correctamente');

            return $this->dispatcher->forward(
                    [
                        "controller" => "magazine",
                        "action" => "index",
                    ]
            );
        } catch (PDOException $e) {
            $error = $e->getMessage();

            $this->flashSession->error($error);

            return $this->dispatcher->forward(
                    [
                        "controller" => "magazine",
                        "action" => "edit",
                        "params" => [$id]
                    ]
            );
        }
    }
}
