<?php
use Phalcon\DI;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\Model\Criteria;
use Mpdf\Mpdf;
 
use mikehaertl\wkhtmlto\Pdf;

class GeneratepdfController extends BaseController {
    
    /**
     * Genera nuevo pdf
     * @return type
     */
    public function generateAction(){
        
        $this->view->disable();
        
        if($this->request->isAjax()){
            
            $idSelectedIssue    = $this->request->getPost('id');
            $typeProject        = $this->request->getPost('type');
            
            $result         = new stdClass();
            $result->ok     = true;
            $result->msg    = '';
            

            $di         = \Phalcon\DI::getDefault();
            $routes     = $di->getConfig()->routesPDFs;
            $pathPdfs   = $routes->$typeProject;
            
            $importedIssue  = Models\LedIssue::findFirst($idSelectedIssue);
            $issueId        = $importedIssue->id;
            $issueName      = $importedIssue->name;
            
            $result->type   = ($typeProject == 'caf') ? 'CAF Madrid' : $importedIssue->magazine->name;
            
            if (file_exists($pathPdfs . $issueId . '.pdf')) {
                $result->ok     = false;
                $result->msg    = 'Duplicate';
            }else{
                
                $html = $this->getHtml( $importedIssue, $issueName, $typeProject );

                $pdf = new Pdf(array(
                        'binary' => '/opt/wkhtmltopdf/bin/wkhtmltopdf',
                        'tmpDir' => TMP_PATH,
                        'commandOptions' => array(
                            'useExec' => true,
                            'procEnv' => array(
                                'LANG' => 'es_ES.utf-8',
                            ),
                        ),

                        'ignoreWarnings'    => true,
                    ));

                $pageOptions = array(
                    'encoding'      => 'UTF-8',
                );

                $tocOptions = array(
                    'user-style-sheet'  => BASE_PATH.'/html/css/pdf/'.$typeProject.'/table-content.css',
                    'xsl-style-sheet'   => BASE_PATH.'/html/xsl/toc.xsl',
                    'no-background',
                    'disable-dotted-lines',
                    );

                $pdf->addToc($tocOptions);

                $pdf->addPage($html, $pageOptions);



                if (!$pdf->saveAs($pathPdfs . $issueId . '.pdf')) {
                    $error = $pdf->getError();

                    $result->ok = false;
                    $result->msg = $error;
                }else{
                    
                    if ($typeProject == 'revistas'){
                        $importedIssue->urlPdf = $pathPdfs . $issueId . '.pdf';
                        $importedIssue->save();
                    }
                }
                
            }
        }
        return json_encode($result);
    }
    
    public function removeAction(){
        
        $this->view->disable();
        
        if($this->request->isAjax()){
            
            $idSelectedIssue    = $this->request->getPost('id');
            $typeProject        = $this->request->getPost('type');
            
            $result         = new stdClass();
            $result->ok     = true;
            $result->msg    = '';
            
            $di         = \Phalcon\DI::getDefault();
            $routes     = $di->getConfig()->routesPDFs;
            $pathPdfs   = $routes->$typeProject;
            
            $importedIssue  = Models\LedIssue::findFirst($idSelectedIssue);
            $issueId        = $importedIssue->id;
            
            if (!unlink($pathPdfs . $issueId . '.pdf')){
                $result->ok     = false;
                $result->msg    = 'Error al borrar el archivo pdf';
            }
        }
        
        return json_encode($result);
    }
    
    /**
     * Obtiene el HTML con el que se generará el pdf
     * @param type $issue
     * @param type $issueName
     * @param type $typeProject
     * @return type
     */
    public function getHtml( $issue, $issueName, $typeProject ){
        
        $application    = $this->getApplication();
        $baseDomain     = $application['baseDomain'];     
		$view = new \Phalcon\Mvc\View();
		$view->setViewsDir($application['viewsDir']);
        
        $utilPdf = new \Util\Pdf();
        
        $infoContents = array();
        $infoContents = $utilPdf->getContentToPDF($issue->contentIssue);
        
        $magazineTitle = '';
        
        if ($typeProject == 'caf'){
            $magazineTitle = 'REVISTA DE DERECHO INMOBILIARIO PARA ADMINISTRADORES DE FINCAS';
        }else{
            $magazineTitle = $issue->magazine->name;
        }
        
        $logoURL = '';
        if( file_exists ( $_SERVER['DOCUMENT_ROOT'] . '/images/pdf/' . $typeProject. '/logo.jpg' ) ){
            $logoURL = $_SERVER['DOCUMENT_ROOT'] . '/images/pdf/' . $typeProject. '/logo.jpg';
        }else{
            $logoURL = $_SERVER['DOCUMENT_ROOT'] . '/images/pdf/' . $typeProject. '/logo.png';
        }
        
        
		$htmlView = $view->getPartial('generatepdf/pdf', [
            'contents'      => $infoContents,
            'issueName'     => $issueName,
            'typeProject'   => $typeProject,
            'baseURL'       => $baseDomain,
            'magazineTitle' => $magazineTitle,
            'logoURL'       => $logoURL
        ]);
        
		return $htmlView;
        
    }
}