<?php

use Phalcon\Mvc\Controller;


class SessionController extends Controller
{
    
    public function loginAction()
    {
        $this->view->disable();
        $form = new Forms\LoginForm();
        $dispacherController = '/';
        
        if ($this->request->isPost()) {
            //if ($this->security->checkToken()) {
            if ($form->isValid($this->request->getPost())) {
                // The token is OK
                $login    = $this->request->getPost('user');
                $password = $this->request->getPost('pwd');
                
                /**
                 * Código para generar contraseña encriptada y guardarla manualmente
                 * en BBDDD (MANTENERLO SIEMPRE COMENTADO Y DESCOMENTAR PARA USAR)
                 */
                /*$pwd = $this->security->hash($password);
                var_dump($pwd);
                die();*/
                
                $users = Models\LedUser::search(array('user' => $login));
                
                if (count($users) == 1) {
                    
                    foreach( $users as $user ){
                        
                        if ($this->security->checkHash($password, $user->password)) {
                            // The password is valid
                            $sessionUser = new stdClass();
                            $sessionUser->id        = $user->id;
                            $sessionUser->fullName  = $user->fullName;
                            $sessionUser->alias     = $user->user;
                            $sessionUser->pass      = $user->password;
                            $sessionUser->idRol     = '';
                            $sessionUser->rolName   = '';
                            
                            foreach ($user->rol as $rol){
                                if ($rol->rolName == 'Admin'){
                                    // En cuanto encuentre un perfil Admin en el usuario, no sigo con el bucle
                                    // El Administrador son todos
                                    $sessionUser->idRol     = $rol->id;
                                    $sessionUser->rolName   = $rol->rolName;
                                    break;
                                }
                                $sessionUser->idRol     = $rol->id;
                                $sessionUser->rolName   = $rol->rolName;
                            }                            
                            
                            $this->session->set('user', $sessionUser);
                            
                        }else{
                            $dispacherController = 'auth/index';
                            $message = _('La contraseña no es correcta');
                            $this->flashSession->error($message);
                        }
                    }
                } else {
                    // To protect against timing attacks. Regardless of whether a user
                    // exists or not, the script will take roughly the same amount as
                    // it will always be computing a hash.
                    $this->security->hash(rand());
                    $dispacherController = 'auth/index';
                    $message = _('El usuario no existe');
                    $this->flashSession->error($message);
                }
            }else{
                $dispacherController = 'auth/index';
                $message = _('El usuario no existe');
                $this->flashSession->error($message);
            }
            return $this->response->redirect( $dispacherController );
        }
    }
}