<?php
use Phalcon\DI;
use Phalcon\Mvc\Controller;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Mvc\Model\Criteria;

class RssController extends Controller {
    
    public $idMagazine;
    public $magazine;
    public $lastIssue;
    public $lastIssuePublish;
    public $utilRss;
    
    public function onConstruct(){
        $di                 = \Phalcon\DI::getDefault();
        $configApp          = $di->getConfig()->application;
        
        $this->utilRss = new \Util\Rss();
    }
    
    
    public function indexAction(){
        
        $args = array();
        $args['title']      = ($this->request->getQuery("title", "string") == null) ? '' : $this->request->getQuery("title", "string");
        $args['magazine']   = ($this->request->getQuery("magazine", "int") == null) ? 0 : $this->request->getQuery("magazine", "int");
        $args['type']       = ($this->request->getQuery("type", "string") == null) ? '' : $this->request->getQuery("type", "string");
        $args['limit']      = ($this->request->getQuery("limit", "int") == null) ? 5 : $this->request->getQuery("limit", "int");
        
        $data = array();
        $data = $this->getDataRss( $args );
        
        $xml = $this->utilRss->renderXML($data, 'Revista '.$this->magazine->name);
        
        
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->partial('feed/rss', [
            'xml' => $xml,
        ]);
        
        $this->response->setHeader('Content-Type', 'text/xml');
    }
    
    public function getDataRss( $args ){
        
        $data = array();
        $conditions = '';
        
        if( $args['magazine'] != 0 ){
            
            $this->magazine         = Models\LedMagazine::findFirst($args['magazine']);
            
            if ( $this->magazine !== false ){
                
                $this->lastIssue        = $this->magazine->idNumber;
                $this->lastIssuePublish = $this->magazine->getIssue(['conditions' => 'id = '.$this->lastIssue]);
                
                if ( $args['type'] != '' ){
                    //Filtro por tipo de contenido
                    $conditions .= 'idContentType = '.$args['type'];

                    if ( $args['title'] != '' ){
                        //Filtro por contenido en título
                        $conditions .= ' AND title LIKE "%'.$args['title'].'%"';
                    }
                }
                
                foreach($this->lastIssuePublish as $item){
                    $issue = $item;
                }
                
                if ($conditions != ''){

                    $contents = $issue->getContentIssue([
                                    'conditions' => $conditions,
                                    'limit' => $args['limit'],
                                ]);
                }else{
                    $contents = $issue->getContentIssue(['limit' => $args['limit']]);
                }
                
                foreach ( $contents as $content ){
            
                    $item = new stdClass();
                    $item->id           = $content->id;
                    $item->title        = $content->title;
                    
                    if ($content->idContentType == 27){
                        //Elimino tag html
                        $txt =  \Lefebvre\Util\Strings::removeTagHtml($content->body);

                        //Reduzco el planteamiento a 400 caracteres para hacer la entradilla
                        $txt = \Lefebvre\Util\Strings::shortenTextWordByLimitChars($txt, 400);

                        $item->description  = html_entity_decode($txt);
                    }else{
                        $item->description  = html_entity_decode($content->resume);
                    }
                    
                    $item->category     = $content->category;
                    $item->pubDate      = $content->publishDate;

                    $item->author       = ''; 
                    foreach ( $content->author as $author ){
                        $item->author       = $author->tratamiento . " " . $author->fullName;
                    }

                    $item->url          = $this->utilRss->getURLCorrespondence($content->idContentType, $content->id);

                    $data[] = $item;
                }
                
            }
        }
        
        return $data;
    }
    

}