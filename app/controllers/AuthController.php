<?php
use Phalcon\DI;

class AuthController extends BaseController {
    
    public function indexAction(){
        $this->view->setRenderLevel(
            \Phalcon\Mvc\View::LEVEL_LAYOUT
        );
        
        $this->view->setLayout('login');
        $form = new Forms\LoginForm();
		$this->view->setVars([
			'form' => $form
		]);
    }
    public function logoutAction(){
        unset($_SESSION['user']);
        unset($_SESSION['SecurityPlugin']);
        $this->session->set('user', null);
        $this->session->destroy();
		return $this->response->redirect( '/' );
    }
}
