<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:outline="http://wkhtmltopdf.org/outline"
                xmlns="http://www.w3.org/1999/xhtml">
    <xsl:output doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
                doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
                indent="yes" />
    <xsl:template match="outline:outline">
        <html>
            <head>
                <title>Sumario</title>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            </head>
            <body>
                <div id="document-pdf" class="pdf-index">
                    <div class="layout">
                        <h1 class="title-index">Sumario</h1>
                        <ul class="lst-index-cnt"><xsl:apply-templates select="outline:item/outline:item"/></ul>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>
    <!--<xsl:template match="outline:item">
        <li class="item-index">
            <xsl:if test="(@title!='') and (@title!='Sumario')">
                <a>
                    <xsl:if test="@link">
                        <xsl:attribute name="href"><xsl:value-of select="@link"/></xsl:attribute>
                    </xsl:if>
                    <xsl:if test="@backLink">
                        <xsl:attribute name="name"><xsl:value-of select="@backLink"/></xsl:attribute>
                    </xsl:if>
                    <span class="page-num"> <xsl:value-of select="@page" /> </span>
                    <p class="it-kicker"> <xsl:value-of select="@title" /> </p>
                </a>
            </xsl:if>
            <ul>
                <xsl:comment>added to prevent self-closing tags in QtXmlPatterns</xsl:comment>
                <xsl:apply-templates select="outline:item"/>
            </ul>
        </li>
    </xsl:template>-->
    <xsl:template match="outline:item[count(ancestor::outline:item)&lt;=3]">
        <li class="item-index">
            <xsl:if test="(@title!='')">
                <div class="book-toc-item-inner">

                    <span class="page-num">
                        <xsl:value-of select="@page"/>
                    </span>

                    <a>
                        <xsl:if test="@link">
                            <xsl:attribute name="href">
                                <xsl:value-of select="@link"/>
                            </xsl:attribute>
                        </xsl:if>

                        <xsl:if test="@backLink">
                            <xsl:attribute name="name">
                                <xsl:value-of select="@backLink"/>
                            </xsl:attribute>
                        </xsl:if>

                        <span class="text">
                            <span class="text-inner">
                                <xsl:value-of select="@title"/>
                            </span>
                        </span>
                    </a>

                </div>
            </xsl:if>

            <ol class="book-toc-list">
                <xsl:comment>added to prevent self-closing tags in QtXmlPatterns</xsl:comment>
                <xsl:apply-templates select="outline:item"/>
            </ol>
        </li>
    </xsl:template>
</xsl:stylesheet>
