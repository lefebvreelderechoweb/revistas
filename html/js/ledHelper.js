/**
 * Utilizamos esta clase para las funciones comunes
 * Al estilo de un helper tipo Tools...
 */
(function ($) {

    $.fn.ledHelper = function (params) {
        var ledHelper  = this;
        var timeOutMsg = '';


        ledHelper.init = function () {
            return this;
        };
        
        /**
         * Convierte la primera letra de
         * una cadena de texto en mayuscuka
         * @param string
         * @returns {string}
         */
        ledHelper.ucFirst = function (string) {
            var str = '';
            try {
                str = string.charAt(0).toUpperCase() + string.slice(1);
            }
            catch(error) {
               str = string;
            }
            return str;
        };

        /**
         * Reemplaza todos los dashes y underscores
         * con espacios. Lo utilizamoa para generar
         * un título por defecto a partir del nombre
         * de una imagen
         * @param filename
         */
        ledHelper.stringFromFileName = function (filename) {
            return filename.replace(/[:._-]/g, ' ');
        };

        /**
         * Convierte el nombre de una imagen en una
         * cadena separada por "dashes". Lo utilizamos
         * para generar las  etiquetas ALT de una iimagen
         * @param filename
         * @returns {string}
         */
        ledHelper.slugFromFileName = function (filename) {
            var output = ledCore.stringFromFileName(filename);
            return output.replace(/ /g, '-').toLowerCase();
        };


        /**
         * Extra el nombre de un fichero a partir
         * de su url o absolute path
         * @param string
         * @returns {string}
         */
        ledHelper.baseName = function (string) {
            var base = new String(string).substring(string.lastIndexOf('/') + 1);
            if (base.lastIndexOf(".") != -1)
                base = base.substring(0, base.lastIndexOf("."));
            return base;
        };


        /**
         * Convierte el rgb en hex
         * Lo usamos dentro del builder
         * @param rgb
         * @returns {*}
         */
        ledHelper.rgbToHex = function (rgb) {
            if (/^#[0-9A-F]{6}$/i.test(rgb)) return rgb;

            rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);

            function hex(x) {
                return ("0" + parseInt(x).toString(16)).slice(-2);
            }

            return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
        };

        /**
         * Comprueba si el parametro pasado es de tipo array
         * 
         * @param mixed params 
         */
        ledHelper.isArray = function (params) {
            if (typeof params !== 'undefined' && params.length > 0) {
                console.log('is array: ' + params.length);
            }
        };
        
        this.isValidJson = function (jsonString) {
            if (/^[\],:{}\s]*$/.test(jsonString.replace(/\\["\\\/bfnrtu]/g, '@').replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {
                return true;
            } else {
                return false;
            }
        };

        ledHelper.showTab = function (container, tab) {
            $(container).find('.tab-pane').removeClass('active');
            $(container).find(tab).addClass('active');
        };


        ledHelper.showModal = function (params) {

            /**
             * Si se llama el método sin haber pasado
             * el selector del modal, mostramos el
             * de por defecto.
             */
            var selector = typeof params.modal != 'undefined' ? params.modal : 'mainModal';
            /**
             * la variable content guarda una url que
             * se nos pasa por parámetro y más abajo
             * carga el contenido de esa url en el
             * body del modal
             * @type {null}
             */
            var content = typeof  params.content != 'undefined' ? params.content : null;

            /**
             * Si pasamos un callback, se ejecutará
             * cuando el modal se cierre.
             */
            var onModalOpen = typeof params.onModalOpen != 'undefined' ? params.onModalOpen : null;
            var onModalClose = typeof params.onModalClose != 'undefined' ? params.onModalClose : null;
            var onAjaxComplete = typeof params.onAjaxComplete != 'undefined' ? params.onAjaxComplete : null;

            var $modal = $(selector);
            $modal.modal();

            if (content) {
                /**
                 * Si se nos pasa una url como parámetro
                 * de content cargamos el contenido por ajax
                 */
                $modal.find('.modal-body').load(params.content + ' .media-library');

                /**
                 * Si necesitamos ejecutar una acción
                 * tras la carga de ajax, ejecutamos
                 * el callback
                 */
                if (onAjaxComplete) {
                    $(document).ajaxComplete(function () {
                        onAjaxComplete();
                    });
                }
            }

            /**
             * Ejecutamos el callback al cerrar
             * el modal.
             */
            if (onModalClose) {
                $modal.on('hidden.bs.modal', function (e) {
                    if (typeof onModalClose == 'function') {
                        onModalClose();
                    }
                });
            }

            if (onModalOpen) {
                onModalOpen();
            }
        };


        ledHelper.randomString = function () {
            var m = m || 9;
            s = '', r = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            for (var i = 0; i < m; i++) {
                s += r.charAt(Math.floor(Math.random() * r.length));
            }
            return s;
        };
        
        ledHelper.defaultDateFormat = function (string) {
            var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
            return dateSearchFormat = string.replace(pattern,'$3-$2-$1');
        };
        
        /**
         * Ajax
         * 
         * @params mixed params 
         */
        this.ajaxCall = function (params) {

            var url = typeof params.url != 'undefined' ? params.url : null;

            if (!url) {
                url = options.ajax_url + '/' + params.controller + '/' + params.method
            }


            var request = $.ajax({
                method: "POST",
                url: url,
                data: params.data,
                async: typeof params.async == "boolean" ? params.async : true
            });

            return request;
        };
        
        /**
         * Obtenemos un numero aleatorio mas el timestamp
         * para generar un 'id' unico
         */
        ledHelper.getRandomInt = function(){
            return Math.floor((Math.random() * 9999) + 1).toString() + Date.now().toString();
        }
        
        /**
         * Datepicker
         * @param {type} format
         * @returns {undefined}
         */
        ledHelper.datePicker = function(format) {
            var minDate = '';
            if ($('.with-min-date').length > 0){
                minDate = 0;
            }
            format = format || 'dd/mm/yy';
            $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: '< Ant',
            nextText: 'Sig >',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            weekHeader: 'Sm',
            dateFormat: format,
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            minDate: minDate,
            };
            $.datepicker.setDefaults($.datepicker.regional['es']);
            
            $( "#dateFrom.led-datepicker, #dateTo.led-datepicker" ).datepicker({
                onSelect: function( selectedDate ) {
                    if(this.id == 'dateFrom'){
                        var dateValue = $('#dateFrom').datepicker().val();
                        
                        var dateFrom = $('#dateFrom').datepicker("getDate");
                        var dateMin = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate());
                        
                        if($('.frequency-form').length > 0){
                            var days = $('.frequency-form input[type=radio]:checked').data('limit-days');
                            dateMin = new Date (dateMin.setDate(dateMin.getDate() + parseInt(days)));
                        }
                        
                        var dateTo = $('#dateTo').datepicker("getDate");
                        var dateToCompare = "";
                        if(dateTo){
                            dateToCompare = new Date(dateTo.getFullYear(), dateTo.getMonth(),dateTo.getDate());
                        }
                        
                        var changeEmptyDateTo = $('#dateFrom').data('change-empty-date');
                        $('#dateTo').datepicker("option","minDate",dateMin); 
                        if((dateFrom > dateToCompare && dateToCompare.lenght > 0) || (!dateToCompare && changeEmptyDateTo)){
                            if($('.frequency-form').length > 0){
                                $('#dateTo').val($.format.date(dateMin, 'dd/MM/yyyy'));
                            }else{
                                $('#dateTo').val(dateValue);
                            }
                        }
                    }
                    
                    if($(this).hasClass('applyDateFilter')){
                        $(this).change();
                    }
                }
            });
            
            $( "#singleDate.led-datepicker" ).datepicker({
                minDate: 0
            });
            
            $( "#defaultDate" ).datepicker();
        };
        
        /**
         * Pasamos desde serialize un JSON con los datos del formulario
         */
        ledHelper.formDataToJson = function ($form) {
            var formSerializeArr = $form.serializeArray(); 
            var jsonObj          = {};
            
            $.map(formSerializeArr, function (n, i) {
                jsonObj[n.name] = n.value;
            });

            return jsonObj;
        }
        
        /**
         * Limpiamos los campos del formulario
         * 
         * @param element $form
         */
        ledHelper.clearForm = function($form){
           $form[0].reset(); 
           $form.find('.form-group.error').removeClass('error');
        }
        
        /**
         * Mostramos un mesaje con sus respectivos estilos segun KO u OK
         * 
         * @param mixed result
         * @param string msg
         */
        ledHelper.showMsgFormAction = function(result, msg) {

            var $ledInfoActions = $('#ledInfoActions');
            $ledInfoActions.html(msg);
            $ledInfoActions.removeClass();

            switch (result) {
                case 'KO':
                    $ledInfoActions.addClass('alert-danger');
                    break;

                case 'OK':
                    $ledInfoActions.addClass('alert-success');
                    break;

                default:
                    break;
            }

            $ledInfoActions.fadeIn('200');
            clearTimeout(timeOutMsg);
            timeOutMsg = window.setTimeout(function () {
                $ledInfoActions.fadeOut('200', function () {
                    $ledInfoActions.removeAttr('class');
                    $ledInfoActions.html('');
                    $ledInfoActions.css('display', 'none');
                });
            }, 4000);
        }
        
        /**
         * Dialog 
         * 
         * @param string title
         * @param string content
         * @param string icon
         * @param mixed options
         */
        ledHelper.addDialog = function(title, content, icon, options){
            icon    = icon || false;
            options = options || false;
            
            var $dialog = $('#led-dialog');
            $dialog.attr('title', title);
            $dialog.find('.led-dialog-content').html(content);
            if(icon){
                $dialog.find('.led-dialog-icon').addClass('ui-icon').addClass('ui-icon-'+icon);
            }
            
            var defaultOptions = {
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                dialogClass: 'led-dialog',
            };
            
            var allOptions = options ? $.extend({}, defaultOptions, options) : defaultOptions;
            $( "#led-dialog" ).dialog(allOptions);
        }
        
        /**
         * Limpiamos el estilo de los campos requeridos
         */
        ledHelper.noErrorField = function(){
            $(document).on('keyup change', '.form-group.error *', function(){
                var $element = $(this);
                $element.closest('.form-group.error').removeClass('error');
            });
        }
        
        /**
         * Reordenamos un array por su
         * 
         * @param mixed arr
         * @param int old_index
         * @param int new_index
         * @returns mixed
         */
        ledHelper.arrayMove = function(arr, fromIndex, toIndex) {
            var element = arr[fromIndex];
            arr.splice(fromIndex, 1);
            arr.splice(toIndex, 0, element);
            return arr; 
        };
        
        /**
         * Validamos el formulario de campos
         * 
         * @param JSON data
         * @returns boolean
         */
        ledHelper.validateRequiredFields = function($form, data){
            var error = false;
            
            $.each(data, function (key, value) {
                var $element   = $form.find('[name='+key+']');
                var isRequired = $element.prop('required') && !$element.val();
                if(isRequired){
                    $element.closest('.form-group').addClass('error');
                    error = true;
                }; 
            });
            
            return !error;
        }
        
        return ledHelper.init();
    };
})(jQuery);