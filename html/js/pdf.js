var pdfGenerator = function(){	
	
	/*
	 * Inicializamos clase
	 * @returns {undefined}
	 */
	this.init = function(  ){
		this.eventsListener();
	},
	
	 /**
      * Generamos pdf
      * @param {type} id
      * @param {type} type
      * @returns {undefined}
      */
	this.generatePdf = function( id, type ){
		
		var self = this;
        
        modalAux = new ledModalLoading('html');
        modalAux.setAnimationTime(500);
        

        var data = {
            id: id,
            type: type
        };			

        request = $.ajax({
            url:  '/generatepdf/generate',
            data: data,
            type: 'POST',
            beforeSend: function () {
                modalAux.show();
            },
            success: function ( dataR ) {

                dataResult = jQuery.parseJSON(dataR);

                if (dataResult.ok){
                    showMsgAction('OK', 'PDF generado correctamente para ' + dataResult.type);
                    setTimeout(function() { location.reload(); }, 1000);
                }else{
                    if( dataResult.msg == 'Duplicate' ){
                        modalAux.hide();
                        self.informDuplicatePdf( id, type );
                        
                    }else{
                        showMsgAction('KO', dataResult.msg);
                    }
                }
                modalAux.hide();

            },
            error: function () {
            }
        }); 
		
	},
            
    this.removePdf = function( id, type ){
        
        var self = this;
        
        var data = {
            id: id,
            type: type
        };			

        request = $.ajax({
            url:  '/generatepdf/remove',
            data: data,
            type: 'POST',
            beforeSend: function () {
            },
            success: function ( dataR ) {

                dataResult = jQuery.parseJSON(dataR);

                if (dataResult.ok){
                    self.generatePdf( id, type );
                }else{
                    showMsgAction('KO', dataResult.msg);
                }

            },
            error: function () {
            }
        });
    },
    
    this.informDuplicatePdf = function( id, type ){
        
        var self = this;
        
        $( "#dialog-confirm-change-pdf" ).dialog({
            classes: {
                "ui-dialog": "led-dialog"
            },
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            show: {
                effect: 'fade',
                duration: 200
            },
            hide: {
                effect: 'fade',
                duration: 200
            },
            buttons: {
                "Confirmar": function() {
                    self.removePdf( id, type );
                    $( this ).dialog( "close" );
                },
                "Cancelar": function() {
                     $( this ).dialog( "close" );
                }
            }
        });
    },
	
	
	/**
	 * Escucha de eventos	 
	 * @returns {undefined}
	 */
    this.eventsListener = function(){
		
		var self = this;
		
		$( document ).on( 'click', '.generator', function(e) {
            
            var idIssue = $(this).attr("data-idx");
            var type    = $(this).attr("data-type");
            self.generatePdf( idIssue, type );
		});
	};	
};

importC = new pdfGenerator();
importC.init();
