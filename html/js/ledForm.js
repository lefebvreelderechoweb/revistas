$(document).ready(function() {

    var timeOutMsg;
    var modalLoading = new ledModalLoading('html');
    modalLoading.setAnimationTime(500);

    /**
     * Cambios de pestaña
     */
    $('#contentForm a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
    });

    /**
     * Botón guardar contenido de un número
     */
    $('#saveEditContent').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        preSaveContentIssue();
    });
    
    /**
     * Botón guardar contenido de un número
     */
    $('#saveContent').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        preSaveContentIssue();
    });

    /**
     * Botón guardar nuevo autor
     */
    $('#createAuthor').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        validateFormAuthor( 'formNewAuthor' );
    });

    /**
     * Botón guardar nuevo autor (desde modal)
     */
    $('#saveAuthorButton').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        validateFormAuthor( 'formCreateAuthor', true );
    });

    /**
     * Botón actualizar autor
     */
    $('#updateAuthor').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        validateFormAuthor( 'formUpdateAuthor' );
    });

    /**
     * Cambio de archivo de imagen de autor
     */
    $('body').on('change', '#urlImageFile', function(){
        if(this.files[0].size > 1048576){
           this.value = "";
           showMsgFormAction('KO', '<p>Archivo demasiado pesado, no debes superar 1MB</p>');

        }else{
            readURL(this);
            $('.img-wrap').removeClass('hidden');
        }
    });

    /**
     * Botón de eliminar autor
     */
    $('body').on('click', '.deleteAuthor', function(e){
        e.preventDefault();
        $this           = $(this);
        var idAuthor    = $this.data('id');

        $( "#dialog-confirm-delete-author" ).dialog({
            classes: {
                "ui-dialog": "led-dialog"
            },
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            show: {
                effect: 'fade',
                duration: 200
            },
            hide: {
                effect: 'fade',
                duration: 200
            },
            buttons: {
                "Confirmar": function() {
                    removeAuthor( idAuthor );
                    $( this ).dialog( "close" );
                },
                "Cancelar": function() {
                     $( this ).dialog( "close" );
                }
            }
        });
    });
    
    /**
     * Botón de eliminar autor
     */
    $('body').on('click', '.deleteIssue', function(e){
        e.preventDefault();
        $this           = $(this);
        var idIssue    = $this.data('id');

        $( "#dialog-confirm-delete-issue" ).dialog({
            classes: {
                "ui-dialog": "led-dialog"
            },
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            show: {
                effect: 'fade',
                duration: 200
            },
            hide: {
                effect: 'fade',
                duration: 200
            },
            buttons: {
                "Confirmar": function() {
                    removeIssue( idIssue );
                    $( this ).dialog( "close" );
                },
                "Cancelar": function() {
                     $( this ).dialog( "close" );
                }
            }
        });
    });

    /**
     * Botón de eliminar imagen de autor
     */
    $('body').on('click', '.delete-action', function(e){
        e.preventDefault();
        $this = $(this);

        $( "#dialog-confirm-delete-image-author" ).dialog({
            classes: {
                "ui-dialog": "led-dialog"
            },
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            show: {
                effect: 'fade',
                duration: 200
            },
            hide: {
                effect: 'fade',
                duration: 200
            },
            buttons: {
                "Confirmar": function() {
                    removeImageAuthor();
                    $( this ).dialog( "close" );
                },
                "Cancelar": function() {
                     $( this ).dialog( "close" );
                }
            }
        });
    });
    
    /**
     * Botón de clonar número de revista
     */
    $('body').on('click', '.cloneIssue', function(e){
        e.preventDefault();
        $this          = $(this);
        var idIssue    = $this.data('id');

        $( "#dialog-confirm-clone-issue" ).dialog({
            classes: {
                "ui-dialog": "led-dialog"
            },
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            show: {
                effect: 'fade',
                duration: 200
            },
            hide: {
                effect: 'fade',
                duration: 200
            },
            buttons: {
                "Confirmar": function() {
                    cloneIssue(idIssue);
                    $( this ).dialog( "close" );
                },
                "Cancelar": function() {
                     $( this ).dialog( "close" );
                }
            }
        });
    });

    /**
     * Acción para cuando se produzca el submit del formulario de creación de autor
     * en modal
     */
    $('body').on('submit', '#formCreateAuthor', function(e){

        e.preventDefault();
        var $this = $(this);

        var modalLoading = new ledModalLoading('html');
        modalLoading.setAnimationTime(500);

        var url = '/author/createAjax';
        var data = new FormData(this);

        var request = jQuery.ajax({
            url: url,
            type: 'POST',
            data: data,
            processData: false,
            contentType: false,
            beforeSend: function() {
                modalLoading.show();
            },
            success: function(dataResult) {

                dataResult = jQuery.parseJSON(dataResult);

                if (dataResult.OK == true) {

                    $('#idNew').val(dataResult.idNew);
                    $('#fullNameNew').val(dataResult.fullNameNew);
                    $('#createAuthorModal').modal('toggle');

                }else{
                    var html = '';

                    dataResult.msg.forEach(function(item) {
                        html += item;
                    });

                    $('#errorsCreateAuthorModal').html(html);
                    $('#errorsCreateAuthorModal').css('display', 'block');
                }

                modalLoading.hide();

            },
            error: function() {
                console.log('Error al cargar info');
                modalLoading.hide();
            },
            done: function() {
            }
        });


     });


    /**
     * Relación de listas dragables y sortables
     */

    //Contenidos
    $("#finishListForums, #nrefForumList").sortable({
        connectWith: ".connectedSortable"
    });
    $("#finishListTribunes, #nrefTribuneList").sortable({
        connectWith: ".connectedSortable"
    });

    $("#finishListRJU, #nrefRJUList").sortable({
        connectWith: ".connectedSortable"
    });

    $("#finishListConsulta, #nrefConsultaList").sortable({
        connectWith: ".connectedSortable"
    });

    $("#finishListLegislativas, #nrefLegislativasList").sortable({
        connectWith: ".connectedSortable"
    });

    $("#finishListRespuestaT, #nrefRespuestaTList").sortable({
        connectWith: ".connectedSortable"
    });

    $("#finishListDocAdministrativa, #nrefDocAdministrativaList").sortable({
        connectWith: ".connectedSortable"
    });

    $("#finishListOrgConsultivos, #nrefOrgConsultivosList").sortable({
        connectWith: ".connectedSortable"
    });

    //Modal Autores
    $("#finishListAuthorsTribunes, #authorTribuneList").sortable({
        connectWith: ".connectedAuthorsSortable"
    });

    //Modal Puntos de Vista
    $("#finishListAuthorPointView, #authorPointViewList").sortable({
        connectWith: ".connectedAuthorPointViewSortable"
    });

    //Convertir en ordenable la lista de puntos de vista
    $( "#pointsViewsList" ).sortable();
    $( "#pointsViewsList" ).disableSelection();

    /**
     * Acciones cuando se cierra el modal de edición del contenido seleccionado
     */
    $('#addReviewModal').on('hidden.bs.modal', function () {

        //Limpiamos los campos "requeridos"
        $('#addReviewModal .required').removeClass('required');

        //Limpiamos los campos disabled
        var elements = document.querySelectorAll('.trialForm input, .trialForm textarea');
        for (var i = 0, element; element = elements[i++];) {

            var id = element.getAttribute('id');
            $("#" + id).attr("disabled", false);
        }

        //Limpio todos los campos
        $('#reviewTitle').val('');
        $('#reviewEpigraph').val('');
        $('#reviewKeywords').val('');
        $('#reviewSignature').val('');
        $('#reviewResume').val('');
        $('#reviewText').val('');
        $('.reviewTextIframe').contents().find('body').html(''); //También hay que cambiar el iframe del WYSIWYG

        //Limpio mensajes de error
        $('#errorsModal').html('');
        $('#errorsModal').css('display', 'none');
        $('.trialForm .error').removeClass('error');

    });
    
    /**
     * Acciones cuando se cierra el modal de generación de foro
     */
    $('#generateForumModal').on('hidden.bs.modal', function () {
        
        //Limpiamos de acciones anteriores
        $('#errorsGenerateForumModal').html('');
        $('#errorsGenerateForumModal').css('display', 'none');
        $('.trialFormGenerateForum .error').removeClass('error');
        
        $('input#forumTitle').val('');
        $('input#forumKeywords').val('');
        $('textarea#approachText').val('');
        $('textarea#resultText').val('');
        $('#pointsViewsList').empty();
        $('.approachTextIframe').contents().find('body').html(''); //También hay que cambiar el iframe del WYSIWYG
        $('.resultTextIframe').contents().find('body').html(''); //También hay que cambiar el iframe del WYSIWYG
        
        $('#generateForumButton').attr("data-id", '');
    });
    
    /**
     * Acciones cuando se cierra el modal de añadir nuevo punto de vista
     */
    $('#generatePointViewModal').on('hidden.bs.modal', function () {
        
        //Limpio todos los campos
        $('#errorsGeneratePointViewModal').html('');
        $('#errorsGeneratePointViewModal').css('display', 'none');
        $('#fullNamePointView').val('');
        $('#pointViewText').val('');
        $('.pointViewTextIframe').contents().find('body').html('');
        $('#finishListAuthorPointView').empty();
        $('#authorPointViewList').empty();
        $('#action').val('new');
        $('#idpoint').val('0');

    });

    /**
     * Acciones cuando se cierra el modal de creación de autor
     */
    $('#createAuthorModal').on('hidden.bs.modal', function () {

        //Recojo el id del autor en el caso de que se haya cerrado el modal
        //porque se ha acabado el proceso de creación de autor, si el modal se
        //ha cerrado de forma manual, el valor de este será 0
        var idNew = $('#formCreateAuthor #idNew').val();
        var fullNameNew = $('#formCreateAuthor #fullNameNew').val();
        var chargeOn = $('#formCreateAuthor #chargeOn').val();
        var classItem = $('#formCreateAuthor #classItem').val();

        //Limpio todos los campos
        $('#formCreateAuthor #fullName').val('');
        $('#formCreateAuthor #job').val('');
        $('#formCreateAuthor #urlImageFile').val('');
        $('#formCreateAuthor #idNew').val('0');
        $('#formCreateAuthor #fullNameNew').val('');
        $('#formCreateAuthor #chargeOn').val('');
        $('#formCreateAuthor #classItem').val('');
        $('#formCreateAuthor #authorImageDisplay').attr('src', '/images/authors/no-img.png');
        $('.img-wrap').addClass('hidden');

        //Limpio mensajes de error
        $('#errorsCreateAuthorModal').html('');
        $('#errorsCreateAuthorModal').css('display', 'none');
        $('.trialFormCreateAuthor .error').removeClass('error');

        //Si el modal se ha cerrado porque ha finalizado el proceso de creación,
        //adjuntamos este nuevo autor a la lista de autores seleccionados para asociar
        if ( idNew != '0' ){
            var html = '<li class="list-group-item ui-sortable-handle ' + classItem + '" data-id="' + idNew + '" data-complete="false"><image src="/images/icons/icon-arrows-az.png" /><p>' + fullNameNew + '</p></li>';
            $('#' + chargeOn).append(html);
        }
    });

    /**
     * Acciones cuando se cierra el modal de asociación de autor
     */
    $('#addAuthorModal').on('hidden.bs.modal', function () {

        //Limpio todos los campos
        $('.trialFormAuthor #fullName').val('');
        $('.trialFormAuthor #finishListAuthorsTribunes').empty();
        $('.trialFormAuthor #authorTribuneList').empty();

        //Limpio mensajes de error
        $('#errorsModalAuthor').html('');
        $('#errorsModalAuthor').css('display', 'none');

    });

    /**
     * Acciones sobre la lista de tribunas definitivas
     */
    $("#finishListTribunes").sortable({
        receive: function(e, ui) {
            /**
             * Añado los link de añadir reseña y editar autor de esa tribuna
             */
            var html = '<div class="configTribune"><p><a href="javascript:void(0);" class="addReview">Añadir Reseña</a></p><p><a href="javascript:void(0);" class="addAuthor">Asociar Autor</a></p></div>';
            ui.item.append(html);
            ui.item.addClass('listables');

        }
    });

    /**
     * Acciones sobre la lista de tribunas propuestas
     */
    $("#nrefTribuneList").sortable({
        receive: function(e, ui) {
            var deleteItem = ui.item.find('.configTribune');
            deleteItem.remove();
            ui.item.attr('data-review', '');
            ui.item.attr('data-title', '');
            ui.item.attr('data-keywords', '');
            ui.item.attr('data-epigraph', '');
            ui.item.attr('data-resume', '');
            ui.item.attr('data-signature', '');
            ui.item.attr('data-authors', '');
            ui.item.attr('data-complete', 'false');
            ui.item.removeClass('listables');
        }
    });

    /**
     * Acciones sobre la lista de foros definitivos
     */
    $("#finishListForums").sortable({
        receive: function(e, ui) {
            /**
             * Añado los link de generar foro y asociar coordinador de ese foro
             */
            var html = '<div class="configForum"><p><a href="javascript:void(0);" class="generateForum">Generar Foro</a></p><p><a href="javascript:void(0);" class="addAuthor addCordinator">Asociar Coordinador</a></p></div>';
            ui.item.append(html);
            ui.item.addClass('listables');

        }
    });

    /**
     * Acciones sobre la lista de foros propuestos
     */
    $("#nrefForumList").sortable({
        receive: function(e, ui) {
            var deleteItem = ui.item.find('.configForum');
            deleteItem.remove();
            ui.item.attr('data-title', '');
            ui.item.attr('data-keywords', '');
            ui.item.attr('data-approach', '');
            ui.item.attr('data-result', '');
            ui.item.attr('data-pointviews', '');
            ui.item.attr('data-authors', '');
            ui.item.attr('data-complete', 'false');
            ui.item.removeClass('listables');
        }
    });

    /**
     * Acciones sobre la lista de reseñas jurisprudencias
     */
    $("#finishListRJU").sortable({
        receive: function(e, ui) {
            /**
             * Añado los link de añadir reseña de esa rju
             */
            var html = '<div class="configRJU"><p><a href="javascript:void(0);" class="addReview">Editar Contenido</a></p></div>';
            ui.item.append(html);
            ui.item.addClass('listables');

        }
    });

    /**
     * Acciones sobre la lista de reseñas jurisprudencias propuestas
     */
    $("#nrefRJUList").sortable({
        receive: function(e, ui) {
            var deleteItem = ui.item.find('.configRJU');
            deleteItem.remove();
            ui.item.attr('data-review', '');
            ui.item.attr('data-title', '');
            ui.item.attr('data-keywords', '');
            ui.item.attr('data-epigraph', '');
            ui.item.attr('data-resume', '');
            ui.item.attr('data-signature', '');
            ui.item.attr('data-complete', 'false');
            ui.item.removeClass('listables');
        }
    });

    /**
     * Acciones sobre la lista de respuestas de los tribunales
     */
    $("#finishListRespuestaT").sortable({
        receive: function(e, ui) {
            /**
             * Añado los link de añadir reseña de esa rju
             */
            var html = '<div class="configRespuestaT"><p><a href="javascript:void(0);" class="addReview">Editar Contenido</a></p></div>';
            ui.item.append(html);
            ui.item.addClass('listables');

        }
    });

    /**
     * Acciones sobre la lista de respuestas de los tribunales propuestas
     */
    $("#nrefRespuestaTList").sortable({
        receive: function(e, ui) {
            var deleteItem = ui.item.find('.configRespuestaT');
            deleteItem.remove();
            ui.item.attr('data-review', '');
            ui.item.attr('data-title', '');
            ui.item.attr('data-keywords', '');
            ui.item.attr('data-epigraph', '');
            ui.item.attr('data-resume', '');
            ui.item.attr('data-signature', '');
            ui.item.attr('data-complete', 'false');
            ui.item.removeClass('listables');
        }
    });

    /**
     * Acciones sobre la lista de consultas
     */
    $("#finishListConsulta").sortable({
        receive: function(e, ui) {
            /**
             * Añado los link de añadir reseña de esa consulta
             */
            var html = '<div class="configConsulta"><p><a href="javascript:void(0);" class="addReview">Editar Contenido</a></p></div>';
            ui.item.append(html);
            ui.item.addClass('listables');

        }
    });

    /**
     * Acciones sobre la lista de consultas
     */
    $("#nrefConsultaList").sortable({
        receive: function(e, ui) {
            var deleteItem = ui.item.find('.configConsulta');
            deleteItem.remove();
            ui.item.attr('data-review', '');
            ui.item.attr('data-title', '');
            ui.item.attr('data-keywords', '');
            ui.item.attr('data-epigraph', '');
            ui.item.attr('data-resume', '');
            ui.item.attr('data-signature', '');
            ui.item.attr('data-complete', 'false');
            ui.item.removeClass('listables');
        }
    });

    /**
     * Acciones sobre la lista de novedades legislativas
     */
    $("#finishListLegislativas").sortable({
        receive: function(e, ui) {
            /**
             * Añado los link de añadir reseña de esa consulta
             */
            var html = '<div class="configLegislativas"><p><a href="javascript:void(0);" class="addReview">Editar Contenido</a></p></div>';
            ui.item.append(html);
            ui.item.addClass('listables');

        }
    });

    /**
     * Acciones sobre la lista de novedades legislativas
     */
    $("#nrefLegislativasList").sortable({
        receive: function(e, ui) {
            var deleteItem = ui.item.find('.configLegislativas');
            deleteItem.remove();
            ui.item.attr('data-review', '');
            ui.item.attr('data-title', '');
            ui.item.attr('data-keywords', '');
            ui.item.attr('data-epigraph', '');
            ui.item.attr('data-resume', '');
            ui.item.attr('data-signature', '');
            ui.item.attr('data-complete', 'false');
            ui.item.removeClass('listables');
        }
    });

    /**
     * Acciones sobre la lista de doctrinas administrativas
     */
    $("#finishListDocAdministrativa").sortable({
        receive: function(e, ui) {
            /**
             * Añado los link de añadir reseña de doctrina
             */
            var html = '<div class="configDocAdministrativa"><p><a href="javascript:void(0);" class="addReview">Editar Contenido</a></p></div>';
            ui.item.append(html);
            ui.item.addClass('listables');

        }
    });

    /**
     * Acciones sobre la lista de doctrinas administrativas propuestas
     */
    $("#nrefDocAdministrativaList").sortable({
        receive: function(e, ui) {
            var deleteItem = ui.item.find('.configDocAdministrativa');
            deleteItem.remove();
            ui.item.attr('data-review', '');
            ui.item.attr('data-title', '');
            ui.item.attr('data-keywords', '');
            ui.item.attr('data-epigraph', '');
            ui.item.attr('data-resume', '');
            ui.item.attr('data-signature', '');
            ui.item.attr('data-complete', 'false');
            ui.item.removeClass('listables');
        }
    });

    /**
     * Acciones sobre la lista de organos consultivos
     */
    $("#finishListOrgConsultivos").sortable({
        receive: function(e, ui) {
            /**
             * Añado los link de añadir reseña de organos consultivos
             */
            var html = '<div class="configOrgConsultivos"><p><a href="javascript:void(0);" class="addReview">Editar Contenido</a></p></div>';
            ui.item.append(html);
            ui.item.addClass('listables');

        }
    });

    /**
     * Acciones sobre la lista de organos consultivos propuestas
     */
    $("#nrefOrgConsultivosList").sortable({
        receive: function(e, ui) {
            var deleteItem = ui.item.find('.configOrgConsultivos');
            deleteItem.remove();
            ui.item.attr('data-review', '');
            ui.item.attr('data-title', '');
            ui.item.attr('data-keywords', '');
            ui.item.attr('data-epigraph', '');
            ui.item.attr('data-resume', '');
            ui.item.attr('data-signature', '');
            ui.item.attr('data-complete', 'false');
            ui.item.removeClass('listables');
        }
    });

    /**
     * Acciones sobre la lista de autores
     */
    $("#finishListAuthorsTribunes").sortable({
        receive: function(e, ui) {
            ui.item.addClass('listablesAuthors');
        }
    });

    /**
     * Acciones sobre la lista de autores de tribunas propuestas
     */
    $("#authorTribuneList").sortable({
        receive: function(e, ui) {
            ui.item.removeClass('listablesAuthors');
        }
    });

    /**
     * Acciones sobre la lista de autores en puntos de vistas
     */
    $("#finishListAuthorPointView").sortable({
        receive: function(e, ui) {
            ui.item.addClass('listablesAuthorsPointView');
        }
    });

    /**
     * Acciones sobre la lista de autores en puntos de vista propuestas
     */
    $("#authorPointViewList").sortable({
        receive: function(e, ui) {
            ui.item.removeClass('listablesAuthorsPointView');
        }
    });

    /**
     * Botón para buscar tribunas
     */
    $('body').on('click', '#searchTribune', function() {
        $('#nrefTribuneList').empty();
        searchNref('tribuna', '24', 'Tribune');
    });

    $('body').on('keypress', '#nrefTribune', function(e) {
        if(e.which == 13) {
            $('#nrefTribuneList').empty();
            searchNref('tribuna', '24', 'Tribune');
        }
    });
    $('body').on('keypress', '#titleTribune', function(e) {
        if(e.which == 13) {
            $('#nrefTribuneList').empty();
            searchNref('tribuna', '24', 'Tribune');
        }
    });

    /**
     * Botón para buscar foros
     */
    $('body').on('click', '#searchForum', function() {
        $('#nrefForumList').empty();
        searchNref('foro', '25', 'Forum');
    });

    $('body').on('keypress', '#nrefForum', function(e) {
        if(e.which == 13) {
            $('#nrefForumList').empty();
            searchNref('foro', '25', 'Forum');
        }
    });
    $('body').on('keypress', '#titleForum', function(e) {
        if(e.which == 13) {
            $('#nrefForumList').empty();
            searchNref('foro', '25', 'Forum');
        }
    });

    /**
     * Botón para buscar reseñas de jurisprudencias
     */
    $('body').on('click', '#searchRJU', function() {
        $('#nrefRJUList').empty();
        searchNref('rju', '26', 'RJU');
    });

    $('body').on('keypress', '#nrefRJU', function(e) {
        if(e.which == 13) {
            $('#nrefRJUList').empty();
            searchNref('rju', '26', 'RJU');
        }
    });
    $('body').on('keypress', '#titleRJU', function(e) {
        if(e.which == 13) {
            $('#nrefRJUList').empty();
            searchNref('rju', '26', 'RJU');
        }
    });

    /**
     * Botón para buscar respuestas de los tribunales
     */
    $('body').on('click', '#searchRespuestaT', function() {
        $('#nrefRespuestaTList').empty();
        searchNref('respuesta', '31', 'RespuestaT');
    });
    $('body').on('keypress', '#nrefRespuestaT', function(e) {
        if(e.which == 13) {
            $('#nrefRespuestaTList').empty();
            searchNref('respuesta', '31', 'RespuestaT');
        }
    });
    $('body').on('keypress', '#titleRespuestaT', function(e) {
        if(e.which == 13) {
            $('#nrefRespuestaTList').empty();
            searchNref('respuesta', '31', 'RespuestaT');
        }
    });

    /**
     * Botón para buscar consultas
     */
    $('body').on('click', '#searchConsulta', function() {
        $('#nrefConsultaList').empty();
        searchNref('consulta', '28', 'Consulta');
    });
    $('body').on('keypress', '#nrefConsulta', function(e) {
        if(e.which == 13) {
            $('#nrefConsultaList').empty();
            searchNref('consulta', '28', 'Consulta');
        }
    });
    $('body').on('keypress', '#titleConsulta', function(e) {
        if(e.which == 13) {
            $('#nrefConsultaList').empty();
            searchNref('consulta', '28', 'Consulta');
        }
    });

    /**
     * Botón para buscar novedades legislativas
     */
    $('body').on('click', '#searchLegislativas', function() {
        $('#nrefLegislativasList').empty();
        searchNref('nlegislativas', '27', 'Legislativas');
    });
    $('body').on('keypress', '#nrefLegislativas', function(e) {
        if(e.which == 13) {
            $('#nrefLegislativasList').empty();
            searchNref('nlegislativas', '27', 'Legislativas');
        }
    });
    $('body').on('keypress', '#titleLegislativas', function(e) {
        if(e.which == 13) {
            $('#nrefLegislativasList').empty();
            searchNref('nlegislativas', '27', 'Legislativas');
        }
    });

    /**
     * Botón para buscar doctrinas administrativas
     */
    $('body').on('click', '#searchDocAdministrativa', function() {
        $('#nrefDocAdministrativaList').empty();
        searchNref('dadministrativa', '29', 'DocAdministrativa');
    });
    $('body').on('keypress', '#nrefDocAdministrativa', function(e) {
        if(e.which == 13) {
            $('#nrefDocAdministrativaList').empty();
            searchNref('dadministrativa', '29', 'DocAdministrativa');
        }
    });
    $('body').on('keypress', '#titleDocAdministrativa', function(e) {
        if(e.which == 13) {
            $('#nrefDocAdministrativaList').empty();
            searchNref('dadministrativa', '29', 'DocAdministrativa');
        }
    });

    /**
     * Botón para buscar organos consultivos
     */
    $('body').on('click', '#searchOrgConsultivos', function() {
        $('#nrefOrgConsultivosList').empty();
        searchNref('oconsultivos', '30', 'OrgConsultivos');
    });
    $('body').on('keypress', '#nrefOrgConsultivos', function(e) {
        if(e.which == 13) {
            $('#nrefOrgConsultivosList').empty();
            searchNref('oconsultivos', '30', 'OrgConsultivos');
        }
    });
    $('body').on('keypress', '#titleOrgConsultivos', function(e) {
        if(e.which == 13) {
            $('#nrefOrgConsultivosList').empty();
            searchNref('oconsultivos', '30', 'OrgConsultivos');
        }
    });

    /**
     * Botón para añadir reseñas
     */
    $('body').on('click', '.addReview', function() {

        var nref = $(this).closest("li").attr("data-nref");
        var type = $(this).closest("li").attr("data-type");

        $('#addReviewButton').attr("data-id", nref);

        /**
         * Consulto al servicio para autocompletar formulario
         */
        getDataByNrefToModal(nref, type, $(this));

    });

    /**
     * Botón para generar foros
     */
    $('body').on('click', '.generateForum', function() {

        var nref = $(this).closest("li").attr("data-nref");
        var type = $(this).closest("li").attr("data-type");

        $('#generateForumButton').attr("data-id", nref);
        
        setForumFormData($(this));
        
    });

    /**
     * Botón para asociar autor
     */
    $('body').on('click', '.addAuthor', function() {

        var nref = $(this).closest("li").attr("data-nref");
        var idsAuthors = $('li.listables[data-nref="' + nref + '"]').attr('data-authors');

        $('#addAuthorButton').attr("data-id", nref);
        openAddAuthorModal(idsAuthors);
    });

    /**
     * Botón para abrir modal de formulario de autor
     */
    $('body').on('click', '.createAuthorButton', function() {

        //Indico en el propio formulario de autor dónde deberá cargar el autor
        //recién creado dependiendo de los datos del botón donde se ha hecho
        //click y con qué clase se añadirá.

        var chargeon = $(this).data('chargeon');
        var classitem = $(this).data('classitem');
        $('#formCreateAuthor #chargeOn').val(chargeon);
        $('#formCreateAuthor #classItem').val(classitem);

        $('#createAuthorModal').modal('show');
    });

    /**
     * Botón para abrir modal de creación de punto de vista
     */
    $('body').on('click', '#addViewPoint', function() {

        $('#generatePointViewModal').modal('show');
    });

    /**
     * Intro en input para buscar autores para tribunas
     */
    $('body').on('keypress', '#fullName', function(e) {
        if(e.which == 13) {
            $('#authorTribuneList').empty();
            searchAuthor( '#fullName', '#authorTribuneList', '#errorSearchAuthorTribune');
        }
    });

    /**
     * Intro en input para buscar autores para puntos de vista
     */
    $('body').on('keypress', '#fullNamePointView', function(e) {
        if(e.which == 13) {
            $('#authorPointViewList').empty();
            searchAuthor( '#fullNamePointView', '#authorPointViewList', '#errorSearchAuthorPointView' );
        }
    });

    /**
     * Botón en modal para vincular reseña a contenido
     */
    $('body').on('click', '#addReviewButton', function() {

        var error = false;
        var itemError = new Array();

        //Limpiamos de acciones anteriores
        $('#errorsModal').html('');
        $('#errorsModal').css('display', 'none');
        $('.trialForm .error').removeClass('error');

        var title       = $('input#reviewTitle').val();
        var epigraph    = $('input#reviewEpigraph').val();
        var keywords    = $('input#reviewKeywords').val();
        var signature   = $('input#reviewSignature').val();
        var resume      = $('textarea#reviewResume').val();
        var review      = $('textarea#reviewText').val();


        /**
         * Validamos los datos obligatorios (aquellos campos que llevan la clase "required)
         */
        var elements = document.querySelectorAll('.trialForm input, .trialForm textarea');
        for (var i = 0, element; element = elements[i++];) {
            var id = element.getAttribute('id');
            if ($('#'+id).hasClass('required')){
                if (element.value === ""){
                    error = true;
                    itemError.push(id);
                }
            }
        }

        if (error){
            var html = '<p>Debes completar los campos obligatorios</p>';

            itemError.forEach(function(item) {
                $('#' + item).parent().addClass('error');
            });

            $('#errorsModal').html(html);
            $('#errorsModal').css('display', 'block');

        }else{
            var idItem = $('#addReviewButton').attr("data-id");
            $('li.listables[data-nref="' + idItem + '"]').attr('data-review', review);
            $('li.listables[data-nref="' + idItem + '"]').attr('data-title', title);
            $('li.listables[data-nref="' + idItem + '"]').attr('data-keywords', keywords);
            $('li.listables[data-nref="' + idItem + '"]').attr('data-epigraph', epigraph);
            $('li.listables[data-nref="' + idItem + '"]').attr('data-resume', resume);
            $('li.listables[data-nref="' + idItem + '"]').attr('data-signature', signature);
            $('li.listables[data-nref="' + idItem + '"]').attr('data-complete', 'true');
            
            var labelButton = $('li.listables[data-nref="' + idItem + '"] .addReview').html();
            if ( labelButton.search("Contenido") == -1 ){
                //Cambio el texto del botón "Añadir Reseña" por "Editar Reseña"
                $('li.listables[data-nref="' + idItem + '"] .addReview').html('Editar Reseña');
            }
            
            
            //Limpio todos los campos
            $('#reviewTitle').val('');
            $('#reviewEpigraph').val('');
            $('#reviewKeywords').val('');
            $('#reviewSignature').val('');
            $('#reviewResume').val('');
            $('#reviewText').val('');
            $('.reviewTextIframe').contents().find('body').html(''); //También hay que cambiar el iframe del WYSIWYG


            $('#addReviewButton').attr("data-id", '');
            $('#addReviewModal .required').removeClass('required');

            var elements = document.querySelectorAll('.trialForm input, .trialForm textarea');
            for (var i = 0, element; element = elements[i++];) {

                var id = element.getAttribute('id');
                $("#" + id).attr("disabled", false);
            }

            $('#addReviewModal').modal('toggle');
        }
    });

    /**
     * Botón en modal para vincular autor a contenido
     */
    $('body').on('click', '#addAuthorButton', function() {

        var error = false;
        var itemError = new Array();

        //Limpiamos de acciones anteriores
        $('#errorsModalAuthor').html('');
        $('#errorsModalAuthor').css('display', 'none');

        if ( $('.listablesAuthors').length == 0 ){
            error = true;
        }else{
            var listAuthors = [];
            $('.listablesAuthors').each(function(i, obj) {
                listAuthors.push($(this).attr("data-id"));
            });

            var listAuthorsString = listAuthors.join();
        }

        if (error){
            var html = '<p>Debes añadir, al menos, un autor / coordinador</p>';

            $('#errorsModalAuthor').html(html);
            $('#errorsModalAuthor').css('display', 'block');

        }else{
            var idItem = $('#addAuthorButton').attr("data-id");
            $('li.listables[data-nref="' + idItem + '"]').attr('data-authors', listAuthorsString);

            //Cambio el texto del botón "Asociar Autor" por "Editar Autor/es"
            $('li.listables[data-nref="' + idItem + '"] .addAuthor').html('Editar Autor/es');

            $('#addAuthorButton').attr("data-id", '');
            $('#addAuthorModal').modal('toggle');
        }
    });

    /**
     * Botón para eliminar un punto de vista
     */
    $('body').on('click', '.removePointView', function(e){

        e.preventDefault();
        $this = $(this);

        $( "#dialog-confirm-delete-pointview" ).dialog({
            classes: {
                "ui-dialog": "led-dialog"
            },
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            show: {
                effect: 'fade',
                duration: 200
            },
            hide: {
                effect: 'fade',
                duration: 200
            },
            open: function (event, ui) {
                $('.ui-dialog').css('z-index', 9999);
                $('.ui-widget-overlay').css('z-index',9998);
            },
            buttons: {
                "Confirmar": function() {
                    $this.parent().remove();
                    $( this ).dialog( "close" );
                },
                "Cancelar": function() {
                     $( this ).dialog( "close" );
                }
            }
        });
    });
    
    /**
     * Botón para editar un punto de vista
     */
    $('body').on('click', '.editPointView', function(e){

        e.preventDefault();
        $this = $(this);
        
        var idAuthor = $this.parent().data('author');
        var fullName = $this.parent().data('fullname');
        var textPointView = $this.parent().data('pointview');
        var idpoint = $this.parent().data('idpoint');
        var html = '';
        
        $('#action').val('edit');
        $('#idpoint').val(idpoint);
        $('#pointViewText').val(textPointView);
        $('.pointViewTextIframe').contents().find('body').html(textPointView);
        html = '<li class="list-group-item ui-sortable-handle listablesAuthorsPointView" data-id="' + idAuthor + '" data-fullname="' + fullName + '" data-complete="false"><image src="/images/icons/icon-arrows-az.png" /><p>' + fullName + '</p></li>';
        $('#finishListAuthorPointView').append(html);
        
        $('#generatePointViewModal').modal('show');
    });

    /**
     * Botón para guardar un foro
     */
    $('body').on('click', '#generateForumButton', function(e){
        
        var error = false;
        var itemError = new Array();

        //Limpiamos de acciones anteriores
        $('#errorsGenerateForumModal').html('');
        $('#errorsGenerateForumModal').css('display', 'none');
        $('.trialFormGenerateForum .error').removeClass('error');
        
        var title       = $('input#forumTitle').val();
        var keywords    = $('input#forumKeywords').val();
        var approach    = $('textarea#approachText').val();
        var result      = $('textarea#resultText').val();
        
        //Puntos de vista
        var pointsViews = [];
        $('.listablePointView').each(function(i, obj) {
            
            var authorID        = $(this).attr("data-author");
            var pointviewText   = $(this).attr("data-pointview");
            var fullName        = $(this).attr("data-fullname");
            var idPointView     = $(this).attr("data-idpoint");
            
            if ( idPointView.search("point") != -1 ){
                //Punto de vista recién creado (no viene de la bbdd, no se está editando)
                idPointView = '';
            }
            
            var pointView       = {id: idPointView, author:authorID, text:pointviewText, fullname:fullName};
            pointsViews[i]      = JSON.stringify(pointView);
            
        });
        var pointsViewsString = pointsViews.join(';');
        
        /**
         * Validamos los datos obligatorios (aquellos campos que llevan la clase "required)
         */
        var elements = document.querySelectorAll('.trialFormGenerateForum input, .trialFormGenerateForum textarea');
        for (var i = 0, element; element = elements[i++];) {
            var id = element.getAttribute('id');
            if ($('#'+id).hasClass('required')){
                if (element.value === ""){
                    error = true;
                    itemError.push(id);
                }
            }
        }
        
        if (error){
            var html = '<p>Debes completar los campos obligatorios</p>';

            itemError.forEach(function(item) {
                $('#' + item).parent().addClass('error');
            });

            $('#errorsGenerateForumModal').html(html);
            $('#errorsGenerateForumModal').css('display', 'block');

        }else{
            var idItem = $('#generateForumButton').attr("data-id");
            $('li.listables[data-nref="' + idItem + '"]').attr('data-title', title);
            $('li.listables[data-nref="' + idItem + '"]').attr('data-keywords', keywords);
            $('li.listables[data-nref="' + idItem + '"]').attr('data-approach', approach);
            $('li.listables[data-nref="' + idItem + '"]').attr('data-result', result);
            $('li.listables[data-nref="' + idItem + '"]').attr('data-pointsviews', pointsViewsString);
            $('li.listables[data-nref="' + idItem + '"]').attr('data-complete', 'true');
            
            var labelButton = $('li.listables[data-nref="' + idItem + '"] .generateForum').html();
            
            //Cambio el texto del botón "Generar Foro" por "Editar Foro"
            $('li.listables[data-nref="' + idItem + '"] .generateForum').html('Editar Foro');
            

            //Limpio todos los campos
            $('#forumTitle').val('');
            $('#forumKeywords').val('');
            $('#approachText').val('');
            $('#resultText').val('');
            $('#pointsViewsList').empty();
            $('.approachTextIframe').contents().find('body').html(''); //También hay que cambiar el iframe del WYSIWYG
            $('.resultTextIframe').contents().find('body').html(''); //También hay que cambiar el iframe del WYSIWYG


            $('#generateForumButton').attr("data-id", '');
            $('#generateForumModal').modal('toggle');
        }
        
        
    });
    
    /**
     * Botón para generar y añadir a la lista un punto de vista
     */
    $('body').on('click', '#generatePointViewButton', function(e){

        var error       = false;
        var msgError    = '';

        //Limpiamos de acciones anteriores
        $('#errorsGeneratePointViewModal').html('');
        $('#errorsGeneratePointViewModal').css('display', 'none');

        //Validamos que los campos son correctos
        if ( $('#pointViewText').val() == '' ){

            error = true;
            $('#pointViewText').parent().addClass('error');
            msgError += '<p>Es necesario introducir el texto del punto de vista</p>';
        }

        if ( $('.listablesAuthorsPointView').length > 1 || $('.listablesAuthorsPointView').length == 0 ){
            error = true;
            msgError += '<p>Es necesario asociar un sólo autor al punto de vista</p>';
        }

        if (error){
            $('#errorsGeneratePointViewModal').html(msgError);
            $('#errorsGeneratePointViewModal').css('display', 'block');
            
        }else{
            
            var textPointView   = $('#pointViewText').val();
            var idAuthor        = $('.listablesAuthorsPointView').data('id');
            var fullName        = $('.listablesAuthorsPointView').data('fullname');
            var action          = $('#action').val();
            var idpoint         = $('#idpoint').val();
            var longitud        = 50;

            if(textPointView.length > longitud){

                var resume = textPointView.substring(0,longitud);
                var indexLastSpace = resume.lastIndexOf(' ');
                resume = resume.substring(0,indexLastSpace) +'<span class="puntos">...</span>';

            }else{
                resume = textPointView;
            }

            //Limpio todo en el modal
            $('#errorsGeneratePointViewModal').html('');
            $('#errorsGeneratePointViewModal').css('display', 'none');
            $('#fullNamePointView').val('');
            $('#pointViewText').val('');
            $('.pointViewTextIframe').contents().find('body').html('');
            $('#finishListAuthorPointView').empty();
            $('#action').val('new');
            $('#idpoint').val('0');
            
            if ( action == 'new' ){
                var randomID = Math.floor((Math.random() * 100) + 100);

                var html = '<li class="list-group-item ui-sortable-handle listablePointView" data-idpoint="point' + randomID + '" data-author="' + idAuthor + '" data-fullname="' + fullName + '" data-pointview="' + textPointView + '"><a href="javascript:void(0);" class="removePointView" style="cursor: pointer;"><span class="lf-icon-close-round-full" style="color:red; float:right;"></span></a><a href="javascript:void(0);" class="editPointView" style="cursor: pointer;"><span class="lf-icon-edit" style="color: #001978;float:right;margin-right: 10px;"></span><p> <span class="lf-icon-arrow-up-down" style="color: #001978;float: left;font-size: 22px;margin-right: 12px;"></span></a>' + resume + '</p></li>';
                $('#pointsViewsList').append(html);
                
            }else{
                $('li.listablePointView[data-idpoint="' + idpoint + '"]').attr('data-author', idAuthor);
                $('li.listablePointView[data-idpoint="' + idpoint + '"]').attr('data-fullname', fullName);
                $('li.listablePointView[data-idpoint="' + idpoint + '"]').attr('data-pointview', textPointView);
            }
            
            

            $('#generatePointViewModal').modal('toggle');
        }

    });

    function searchNref(typeDocument, idTypeContent, keyword) {
        modalLoading = new ledModalLoading('html');
        modalLoading.setAnimationTime(500);

        var url         = '/online/getDocument';
        var idMagazine  = $('#idMagazine').val();
        var param       = {"nref": $('#nref' + keyword).val(), "title": $('#title' + keyword).val(), "type": typeDocument, "idMagazine": idMagazine};

        var request = jQuery.ajax({
            url: url,
            type: 'POST',
            data: param,
            beforeSend: function() {
                modalLoading.show();
            },
            success: function(dataResult) {

                dataResult = jQuery.parseJSON(dataResult);
                $('#nref' + keyword + 'List').empty();

                $('#errorSearch' + keyword).addClass('hidden');
                $('#errorSearch' + keyword).empty();

                var html = '';

                //Solo los contenido de tipo tribuna y foros necesitaran asociar uno o varios autores / coordinadores
                var dataAuthor = '';
                if(typeDocument == 'tribuna' || typeDocument == 'foro'){
                    dataAuthor = 'data-authors=""';
                }

                if (dataResult.OK == true) {
                    if (jQuery.type(dataResult.msg) == 'string') {
                        
                        if(typeDocument == 'foro'){
                            //Los datos que recolectan los foros son totalmente distintos al resto
                            html += '<li class="list-group-item ui-sortable-handle" data-type="' + idTypeContent + '" data-nref="' + $('#nref' + keyword).val() + '" data-title="" data-keywords="" data-approach="" data-result="" data-pointsviews="" ' + dataAuthor + ' data-complete="false"><image src="/images/icons/icon-arrows-az.png" /><p>' + dataResult.msg + '</p></li>';
                        }else{
                            html += '<li class="list-group-item ui-sortable-handle" data-type="' + idTypeContent + '" data-nref="' + $('#nref' + keyword).val() + '" data-review="" data-title="" data-keywords="" data-epigraph="" data-resume="" data-signature="" ' + dataAuthor + ' data-complete="false"><image src="/images/icons/icon-arrows-az.png" /><p>' + dataResult.msg + '</p></li>';
                        }
                        
                    } else {
                        dataResult.msg.forEach(function(item) {
                            
                            if(typeDocument == 'foro'){
                                //Los datos que recolectan los foros son totalmente distintos al resto
                                html += '<li class="list-group-item ui-sortable-handle" data-type="' + idTypeContent + '" data-nref="' + item['nref'] + '" data-title="" data-keywords="" data-approach="" data-result="" data-pointsviews="" ' + dataAuthor + ' data-complete="false"><image src="/images/icons/icon-arrows-az.png" /><p>' + item['title'] + '</p></li>';
                            }else{
                                html += '<li class="list-group-item ui-sortable-handle" data-type="' + idTypeContent + '" data-nref="' + item['nref'] + '" data-review="" data-title="" data-keywords="" data-epigraph="" data-resume="" data-signature="" ' + dataAuthor + ' data-complete="false"><image src="/images/icons/icon-arrows-az.png" /><p>' + item['title'] + '</p></li>';
                            }
                            
                            
                        });
                    }
                    $('#nref' + keyword + 'List').append(html);

                } else {
                    $('#errorSearch' + keyword).append(dataResult.msg);
                    $('#errorSearch' + keyword).removeClass('hidden');
                }

                modalLoading.hide();
            },
            error: function() {
                console.log('Error al cargar info');
                modalLoading.hide();
            },
            done: function() {
            }
        });
    }

    function searchAuthor(idInputSearch, idChargeList, idDivError){
        modalLoading = new ledModalLoading('html');
        modalLoading.setAnimationTime(500);

        var url = '/author/searchAjax';
        var param = {"fullName": $(idInputSearch).val()};

        var request = jQuery.ajax({
            url: url,
            type: 'POST',
            data: param,
            beforeSend: function() {
                modalLoading.show();
            },
            success: function(dataResult) {

                dataResult = jQuery.parseJSON(dataResult);
                $(idChargeList).empty();

                $(idDivError).addClass('hidden');
                $(idDivError).empty();

                var html = '';

                if (dataResult.OK == true) {

                    dataResult.authors.forEach(function(item) {
                        html += '<li class="list-group-item ui-sortable-handle" data-id="' + item['id'] + '" data-fullname="' + item['fullName'] + '" data-complete="false"><image src="/images/icons/icon-arrows-az.png" /><p>' + item['fullName'] + '</p></li>';
                    });

                    $(idChargeList).append(html);

                } else {
                    $(idDivError).append('No hay resultados');
                    $(idDivError).removeClass('hidden');
                }

                modalLoading.hide();
            },
            error: function() {
                console.log('Error al cargar info');
                modalLoading.hide();
            },
            done: function() {
            }
        });
    }
    
    function setForumFormData( item ) {
        
        var htmlListPointViews = '';
        
        var title       = item.closest("li").attr("data-title");
        var keywords    = item.closest("li").attr("data-keywords");
        var approach    = item.closest("li").attr("data-approach");
        var result      = item.closest("li").attr("data-result");
        var pointsviews = item.closest("li").attr("data-pointsviews");
        
        $('#forumTitle').val(title);
        $('#forumKeywords').val(keywords);
        $('#approachText').val(approach);
        $('.approachTextIframe').contents().find('body').html(approach);
        $('#resultText').val(result);
        $('.resultTextIframe').contents().find('body').html(result);
        
        if( pointsviews != '' ){
            
            var arrayPointsViews = pointsviews.split(";");
            
            arrayPointsViews.forEach(function(item) {
                var pointView = JSON.parse(item);
                
                if (pointView.id != ''){
                    idpoint = pointView.id;
                }else{
                    var randomID = Math.floor((Math.random() * 100) + 100);
                    idpoint = 'point' + randomID;
                }
                
                
                if(pointView.text.length > 50){

                    var resume = pointView.text.substring(0,50);
                    var indexLastSpace = resume.lastIndexOf(' ');
                    resume = resume.substring(0,indexLastSpace) +'<span class="puntos">...</span>';

                }else{
                    resume = pointView.text;
                }
                
                htmlListPointViews += '<li class="list-group-item ui-sortable-handle listablePointView" data-idpoint="' + idpoint + '" data-author="' + pointView.author + '" data-fullname="' + pointView.fullname + '" data-pointview="' + pointView.text + '"><a href="javascript:void(0);" class="removePointView" style="cursor: pointer;"><span class="lf-icon-close-round-full" style="color:red; float:right;"></span></a><a href="javascript:void(0);" class="editPointView" style="cursor: pointer;"><span class="lf-icon-edit" style="color: #001978;float:right;margin-right: 10px;"></span><p> <span class="lf-icon-arrow-up-down" style="color: #001978;float: left;font-size: 22px;margin-right: 12px;"></span></a>' + resume + '</p></li>';
            });
            
            $('#pointsViewsList').append(htmlListPointViews);
        }
        
        $('#generateForumModal').modal('show');
    }
    
    function getDataByNrefToModal(nref, type, item) {

        var url         = '/online/getDataToModal';
        var idMagazine  = $('#idMagazine').val();
        var param       = {"nref": nref, "type": type, "idMagazine": idMagazine};

        var request = jQuery.ajax({
            url: url,
            type: 'POST',
            data: param,
            beforeSend: function() {
            },
            success: function(dataResult) {

                dataResult = jQuery.parseJSON(dataResult);

                if ( dataResult.data !== undefined && dataResult.data.length != 0 ){

                    var review      = item.closest("li").attr("data-review");
                    var title       = item.closest("li").attr("data-title");
                    var keywords    = item.closest("li").attr("data-keywords");
                    var epigraph    = item.closest("li").attr("data-epigraph");
                    var signature   = item.closest("li").attr("data-signature");
                    var resume      = item.closest("li").attr("data-resume");

                    if (review != '') {
                        $('#reviewText').val(review);
                        $('.reviewTextIframe').contents().find('body').html(review); //También hay que cambiar el iframe del WYSIWYG
                    }else{
                        $('#reviewText').val(dataResult.data["review"]);
                        $('.reviewTextIframe').contents().find('body').html(dataResult.data["review"]); //También hay que cambiar el iframe del WYSIWYG
                    }
                    if (title != '') {
                        $('#reviewTitle').val(title);
                    }else{
                        $('#reviewTitle').val(dataResult.data["title"]);
                    }
                    if (keywords != '') {
                        $('#reviewKeywords').val(keywords);
                    }else{
                        $('#reviewKeywords').val(dataResult.data["keywords"]);
                    }
                    if (epigraph != '') {
                        $('#reviewEpigraph').val(epigraph);
                    }else{
                        $('#reviewEpigraph').val(dataResult.data["epigraph"]);
                    }
                    if (type != '24'){ //Las tribunas llevan los autores por otro lado
                        if (signature != '') {
                            $('#reviewSignature').val(signature);
                        }else{
                            $('#reviewSignature').val(dataResult.data["signature"]);
                        }
                    }
                    if (resume != '') {
                        $('#reviewResume').val(resume);
                    }else{
                         $('#reviewResume').val(dataResult.data["resume"]);
                    }

                    // Añado la clase required a los campos requeridos para poder
                    // identificarlos en la validación
                    var requiredField = dataResult.required;
                    var requiredSplit = requiredField.split(',');

                    requiredSplit.forEach(function(item) {
                        $('#'+item).addClass('required');
                    });

                    // Recorro tods los elementos del modal formulario e inhabilito
                    // aquellos que no tengan la clase required del paso anterior
                    var elements = document.querySelectorAll('.trialForm input, .trialForm textarea');
                    for (var i = 0, element; element = elements[i++];) {

                        var id = element.getAttribute('id');

                        if (!$("#" + id).hasClass("required")){
                            $("#" + id).attr("disabled", true);
                        }
                    }

                    $('#addReviewModal').modal('show');

                }else{
                    // Esto solo se produce si el nref introducido no corresponde con el tipo de contenido esperado
                    // por ejemplo, un nref de tribuna para un contenido de respuesta tribunales (este caso se dará en contadas ocasiones)
                    showMsgFormAction('KO', '<p>Este contenido no corresponde con el tipo de contenido esperado</p>');
                }


            },
            error: function() {
                console.log('Error al cargar info');
            },
            done: function() {
            }
        });


    }

    function preSaveContentIssue() {

        var resultList = [];
        var url = '/issue/create';
        var error = false;
        var contentError = false;
        var html = '';
        var success = 'OK';
        var msgError = '';

        //Limpio posibles errores
         $('#issueNumber').parent().removeClass('error');

        $('.listables').each(function(i, obj) {

            var complete = $(this).attr("data-complete");
            if (complete == 'true'){

                //Comprobamos si el contenido necesita de autor/es asociados y, en caso afirmativo, comprobamos si se han asociado
                if ($(this).attr("data-authors") !== 'undefined' && $(this).attr("data-authors") === '') {
                    error = true;
                    if ($(this).attr("data-type") == '24'){
                        msgError += '<p>Es necesario asociar, al menos, un autor a su tribuna</p>';
                    }
                    if ($(this).attr("data-type") == '25'){
                        msgError += '<p>Es necesario asociar, al menos, un coordinador a su foro abierto</p>';
                    }
                    
                }else{
                    
                    var dataid = '';
                    if (typeof $(this).attr("data-id") !== 'undefined') {
                        //Si existe el attributo data-id es que se trata de un contenido que se está editando
                        dataid = $(this).attr("data-id");
                    }
                    
                    if ($(this).attr("data-type") == '25'){
                        //Los datos que recoge los foros son distintos del resto
                        
                        var dataIdApproach = '';
                        if (typeof $(this).attr("data-idapproach") !== 'undefined') {
                            //Si existe el attributo data-idapproach es que se trata de un contenido que se está editando
                            dataIdApproach = $(this).attr("data-idapproach");
                        }
                        
                        var dataIdResult = '';
                        if (typeof $(this).attr("data-idresult") !== 'undefined') {
                            //Si existe el attributo data-idresult es que se trata de un contenido que se está editando
                            dataIdResult = $(this).attr("data-idresult");
                        }
                        
                        var item = {id: dataid, type: $(this).attr("data-type"), nref: $(this).attr("data-nref"), title: $(this).attr("data-title"), keywords: $(this).attr("data-keywords"), authors: $(this).attr("data-authors"), idapproach: dataIdApproach, approach: $(this).attr("data-approach"), idresult: dataIdResult, result: $(this).attr("data-result"), pointsviews: $(this).attr("data-pointsviews")};
                    }else{
                        var item = {id: dataid, type: $(this).attr("data-type"), nref: $(this).attr("data-nref"), review: $(this).attr("data-review"), title: $(this).attr("data-title"), keywords: $(this).attr("data-keywords"), epigraph: $(this).attr("data-epigraph"), resume: $(this).attr("data-resume"), signature: $(this).attr("data-signature"), authors: $(this).attr("data-authors")};
                    }
                    
                    resultList[i] = item;
                }

            }else if (!contentError){
                error = true;
                contentError = true;
                msgError += '<p>Existe contenido sin editar, por favor, revísalo</p>';
            }
        });

        var nameIssue = $('#issueNumber').val();
        if (nameIssue == '') {
            error = true;
            $('#issueNumber').parent().addClass('error');
            msgError += '<p>Debes introducir el nombre del número que estás editando</p>';
        }

        var idMagazine = $('#idMagazine').val();
        if (!error) {
            
            if ( $('#idIssue').length == 1 ){
                var idIssue = $('#idIssue').val();
                var param = {"data": resultList, "idMagazine": idMagazine, "nameIssue": nameIssue, "idIssue": idIssue};
            }else{
                var param = {"data": resultList, "idMagazine": idMagazine, "nameIssue": nameIssue};
            }
            
            var request = jQuery.ajax({
                url: url,
                type: 'POST',
                data: param,
                beforeSend: function() {
                    modalLoading.show();
                },
                success: function(dataResult) {

                    dataResult = jQuery.parseJSON(dataResult);

                    if (dataResult.OK == false) {
                        success = 'KO';

                    }

                    dataResult.msg.forEach(function(item) {
                        html += '<p>' + item + '</p>';
                    });

                    showMsgFormAction(success, html);
                    
                    if (dataResult.OK == true) {
                        setTimeout(function() { window.location.replace("/issue/index/" + idMagazine); }, 3000);
                    }

                    modalLoading.hide();

                },
                error: function() {
                    console.log('Error al cargar info');
                    modalLoading.hide();
                },
                done: function() {
                }
            });
        }else{
            showMsgFormAction('KO', msgError);
        }

    }

    /**
     * Valida los campos obligatorios del formulario de creación de autor y, si no hay error,
     * fuerza el submit del formulario.
     * @returns {undefined}
     */
    function createNewAuthor(){

        var form        = $( "#formNewAuthor" );
        var error       = false;
        var itemError   = new Array();

        //Limpio acciones anteriores
        $('#formNewAuthor .error').removeClass('error');

        /**
         * Validamos los datos obligatorios (aquellos campos que llevan la clase "required)
         */
        var elements = document.querySelectorAll('#formNewAuthor input, #formNewAuthor select');
        for (var i = 0, element; element = elements[i++];) {
            var id = element.getAttribute('id');
            if ($('#'+id).hasClass('required')){
                if (element.value === ""){
                    error = true;
                    itemError.push(id);
                }
            }
        }

        if (error){
            var html = '<p>Debes completar los campos obligatorios</p>';

            itemError.forEach(function(item) {
                $('#' + item).parent().addClass('error');
            });

            showMsgFormAction('KO', html);

        }else{
            form.submit();
        }
    }

    /**
     * Valida los campos obligatorios del formulario de actualización y creación de autor y, si no hay error,
     * fuerza el submit del formulario.
     * @returns {undefined}
     */
    function validateFormAuthor(formName, inModal = false){

        var form        = $( "#" + formName );
        var error       = false;
        var itemError   = new Array();

        //Limpio acciones anteriores
        $('#' + formName + ' .error').removeClass('error');
        if (inModal){
            $('#errorsCreateAuthorModal').html('');
            $('#errorsCreateAuthorModal').css('display', 'none');
        }

        /**
         * Validamos los datos obligatorios (aquellos campos que llevan la clase "required)
         */
        var elements = document.querySelectorAll('#' + formName + ' input, #' + formName + ' select');
        for (var i = 0, element; element = elements[i++];) {
            var id = element.getAttribute('id');
            if ($('#' + formName + ' #'+id).hasClass('required')){
                if (element.value === ""){
                    error = true;
                    itemError.push('#' + formName + ' #'+id);
                }
            }
        }

        if (error){
            var html = '<p>Debes completar los campos obligatorios</p>';

            itemError.forEach(function(item) {
                $(item).parent().addClass('error');
            });

            if (!inModal){
                showMsgFormAction('KO', html);
            }else{
                $('#errorsCreateAuthorModal').html(html);
                $('#errorsCreateAuthorModal').css('display', 'block');
            }


        }else{
            form.submit();
        }
    }


    function removeAuthor( id ) {

        var url = '/author/delete';
        var html = '';
        var success = 'OK';


        var param = {"id": id};
        var request = jQuery.ajax({
            url: url,
            type: 'POST',
            data: param,
            beforeSend: function() {
                modalLoading.show();
            },
            success: function(dataResult) {

                dataResult = jQuery.parseJSON(dataResult);

                if (dataResult.OK == false) {
                    success = 'KO';
                }

                dataResult.msg.forEach(function(item) {
                    html += '<p>' + item + '</p>';
                });

                showMsgFormAction(success, html);
                setTimeout(function() { location.reload(); }, 1000);

                modalLoading.hide();

            },
            error: function() {
                console.log('Error al cargar info');
                modalLoading.hide();
            },
            done: function() {
            }
        });


    }
    
    function removeIssue( id ) {

        var url = '/issue/delete';
        var html = '';
        var success = 'OK';


        var param = {"id": id};
        var request = jQuery.ajax({
            url: url,
            type: 'POST',
            data: param,
            beforeSend: function() {
                modalLoading.show();
            },
            success: function(dataResult) {

                dataResult = jQuery.parseJSON(dataResult);

                if (dataResult.OK == false) {
                    success = 'KO';
                }

                dataResult.msg.forEach(function(item) {
                    html += '<p>' + item + '</p>';
                });

                showMsgFormAction(success, html);
                setTimeout(function() { location.reload(); }, 1000);

                modalLoading.hide();

            },
            error: function() {
                console.log('Error al cargar info');
                modalLoading.hide();
            },
            done: function() {
            }
        });


    }

    function removeImageAuthor(){
        $('#changeImage').val('1');
        $('.img-wrap').addClass('hidden');
        $('#groupFile').removeClass('hidden');

        //Reseteo el input type file por si acaso
         $('#urlImageFile').val('');

    }
    
    function cloneIssue(id){
        
        var url = '/issue/clone';
        var html = '';
        var success = 'OK';


        var param = {"id": id};
        var request = jQuery.ajax({
            url: url,
            type: 'POST',
            data: param,
            beforeSend: function() {
                modalLoading.show();
            },
            success: function(dataResult) {

                dataResult = jQuery.parseJSON(dataResult);

                if (dataResult.OK == false) {
                    success = 'KO';
                }

                dataResult.msg.forEach(function(item) {
                    html += '<p>' + item + '</p>';
                });

                showMsgFormAction(success, html);
                setTimeout(function() { location.reload(); }, 1000);

                modalLoading.hide();

            },
            error: function() {
                console.log('Error al cargar info');
                modalLoading.hide();
            },
            done: function() {
            }
        });

    }

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#authorImageDisplay').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function openAddAuthorModal( idsAuthors ){

        $('.affiliateAuthors').empty();

        if ( idsAuthors != '' ){
            //Saco la lista de los autores que ya tenía asociados previamente
            modalLoading = new ledModalLoading('html');
            modalLoading.setAnimationTime(500);

            var url = '/author/searchAjax';
            var param = {"onlyID": 'true', "ids": idsAuthors};
            var request = jQuery.ajax({
                url: url,
                type: 'POST',
                data: param,
                beforeSend: function() {
                    modalLoading.show();
                },
                success: function(dataResult) {

                    dataResult = jQuery.parseJSON(dataResult);

                    var html = '';

                    if (dataResult.OK == true) {
                        html += '<p class="h3">Autores ya asociados</p><ul style="margin-bottom:1em;">';
                        dataResult.authors.forEach(function(item) {
                            html += '<li>' + item.fullName + '</li>';
                        });
                        html += '</ul><small style="color: red;">Recuerda que, al guardar, se cambiarán los autores ya asociados por los autores seleccionados</small>';

                        $('.affiliateAuthors').html(html);

                    }

                    modalLoading.hide();
                },
                error: function() {
                    console.log('Error al cargar info');
                    modalLoading.hide();
                },
                done: function() {
                }
            });
        }
        $('#addAuthorModal').modal('show');
    }

    function showMsgFormAction(result, msg) {

        $ledInfoActions = $('#ledInfoActions');
        $ledInfoActions.html(msg);
        $ledInfoActions.removeClass();

        switch (result) {
            case 'KO':
                $ledInfoActions.addClass('alert-danger');
                break;

            case 'OK':
                $ledInfoActions.addClass('alert-success');
                break;

            default:
                break;
        }

        $ledInfoActions.fadeIn('200');
        clearTimeout(timeOutMsg);
        timeOutMsg = window.setTimeout(function() {
            $ledInfoActions.fadeOut('200', function() {
                $ledInfoActions.removeAttr('class');
                $ledInfoActions.html('');
                $ledInfoActions.css('display', 'none');
            });
        }, 4000);
    }
});