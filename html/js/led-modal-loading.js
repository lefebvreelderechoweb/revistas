/**
 * Clase utilizada para modal cargando en el backend
 * @param {string} selectorParent	Selector que contendra el modal 
 */
var ledModalLoading = function( selectorParent, template ) {
	this.template           = template,
	this.selectorParent		= selectorParent,
	this.$selectorParent	= jQuery( selectorParent ),
	this.id					= 'ledModalLoading',
	this.selector			= '#' + this.id,
	this.$selector			= null,
	this.animationTime		= 0,
	this.html				= '',
	this.htmlLoading		= '',
	this.init  = function( ) {
        this.setTemplate();
		this.render();
		this.refreshSelector();
	},
	this.setTemplate = function() {
        switch( this.template ) {
            case 'circle':
                this.htmlLoading = '<i class="fa fa-spin fa-spinner"></i>';
                break;
            case 'lefebvre':
            default:
                this.htmlLoading = '<div class="preloader-holder"></div>';
        }
		this.refreshRender();
	},
	this.setId = function( id ){
		if( id ){
			this.id = id;
		}
		this.selector = '#' + this.id,
		this.refreshSelector();
	},
	this.setAnimationTime = function( time ){
		if( time ){
			this.animationTime = time;
		}
	},
	this.getStyleModal = function () {
		var style = '';
		style+= 'display: none;';
		style+= 'width: 100%;';
		style+= 'height: 100%;';
		style+= 'z-index: 9999;';
		style+= 'position: fixed;';
		style+= 'top: 0px;';
		style+= 'background-color: rgba(23,28,50, 0.8);';
		style+= 'color: rgba(0, 0, 0, 0.4);';
		return style;
	},	
	this.getStyleLoading = function () {
		var style = '';
		style+= 'position: relative;';
		style+= 'top: 50%;';
		style+= 'transform: translateY(-50%);';
		style+= 'text-align: center;';
		style+= 'width: 100px;';
		style+= 'left: calc( 50% - 50px );';
		style+= 'color: white;';
		style+= 'font-size: 80px;';
		return style;
	},	
	this.loadHtml = function( ) {
		this.html = '';
		this.html += '<div id="'+ this.id +'" style="' + this.getStyleModal() + '">';
			this.html += '<div style="' + this.getStyleLoading() + '" >' + this.htmlLoading + '</div>';
		this.html += '</div>';
	},
	this.setHtml = function( html ) {
		this.html = html;
	},	
	this.setHtmlLoading = function( html ) {
		this.htmlLoading = html;
		this.refreshRender();
	},		
	this.isLoaded = function( ) {
		if( jQuery( this.selectorParent + ' ' + this.selector ).length > 0 ){
			return true;
		}
		return false;
	},	
	this.render = function( ) {
		if( ! this.isLoaded() ){
			this.loadHtml();
			this.$selectorParent.append( this.html );
		}
	},
	this.refreshRender = function( ) {
		jQuery( this.selectorParent ).find( this.selector ).remove();
		this.loadHtml();
		jQuery( this.selectorParent ).append( this.html );
		this.refreshSelector();
	},
	this.refreshSelector = function( ) {
		this.$selector = jQuery( selectorParent + ' ' + this.selector );
	},
			
	this.show = function( duration, complete ) {
		if(typeof(duration)==='undefined') duration = this.animationTime;
		
		this.$selector.fadeIn( duration, complete );
	},
	this.hide = function( duration, complete ) {	
		if(typeof(duration)==='undefined') duration = this.animationTime;
		this.$selector.fadeOut(duration, complete );
	},
	this.remove = function( ) {
		this.$selector.remove();
	},
	this.css = function( propertyName, value ) {
		this.$selector.css( propertyName, value );
	},
	this.init();
};
