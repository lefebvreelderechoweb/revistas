$(document).ready(function() {
    
    
    datePicker();
    
    /**
     * Datepicker
     * @param {type} format
     * @returns {undefined}
     */
    function datePicker (format) {
            var minDate = '';
            if ($('.with-min-date').length > 0){
                minDate = 0;
            }
            format = format || 'dd/mm/yy';
            $.datepicker.regional['es'] = {
            closeText: 'Cerrar',
            prevText: '< Ant',
            nextText: 'Sig >',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            weekHeader: 'Sm',
            dateFormat: format,
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: '',
            minDate: minDate,
            };
            $.datepicker.setDefaults($.datepicker.regional['es']);
            
            $( "#dateFrom.led-datepicker, #dateTo.led-datepicker" ).datepicker({
                onSelect: function( selectedDate ) {
                    if(this.id == 'dateFrom'){
                        var dateValue = $('#dateFrom').datepicker().val();
                        
                        var dateFrom = $('#dateFrom').datepicker("getDate");
                        var dateMin = new Date(dateFrom.getFullYear(), dateFrom.getMonth(), dateFrom.getDate());
                        
                        if($('.frequency-form').length > 0){
                            var days = $('.frequency-form input[type=radio]:checked').data('limit-days');
                            dateMin = new Date (dateMin.setDate(dateMin.getDate() + parseInt(days)));
                        }
                        
                        var dateTo = $('#dateTo').datepicker("getDate");
                        var dateToCompare = "";
                        if(dateTo){
                            dateToCompare = new Date(dateTo.getFullYear(), dateTo.getMonth(),dateTo.getDate());
                        }
                        
                        var changeEmptyDateTo = $('#dateFrom').data('change-empty-date');
                        $('#dateTo').datepicker("option","minDate",dateMin); 
                        if((dateFrom > dateToCompare && dateToCompare.lenght > 0) || (!dateToCompare && changeEmptyDateTo)){
                            if($('.frequency-form').length > 0){
                                $('#dateTo').val($.format.date(dateMin, 'dd/MM/yyyy'));
                            }else{
                                $('#dateTo').val(dateValue);
                            }
                        }
                    }
                    
                    if($(this).hasClass('applyDateFilter')){
                        $(this).change();
                    }
                }
            });
            
            $( "#singleDate.led-datepicker" ).datepicker({
                minDate: 0
            });
            
            $( "#defaultDate" ).datepicker();
        }
    
});