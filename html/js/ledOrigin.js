(function ($) {

    $.fn.ledOrigin = function () {
        
        var ledOrigin           = this;
        var ledHelper           = $(window).ledHelper();
        var BASE_URL            = window.location.origin;
        window.magazineStatus   = false;
        window.componentStatus  = false;
        window.fieldStatus      = false;
        window.filterStatus     = false;
        
        /**
         * Iniciamos la funcionalidad
         */
        ledOrigin.init = function () {
            ledHelper.noErrorField();
            ledOrigin.fields();
            ledOrigin.filters();
            ledOrigin.filterOptions();
            ledOrigin.filterAttributes();
            ledOrigin.selectMagazines();
            ledOrigin.selectComponents();
            ledOrigin.origin();
        };
        
        /******************************************************
         ******************** Campos **************************
         ******************************************************/
        
        /**
         * Funcionalidad referente a los campos asociados 
         * a los origenes
         */
        ledOrigin.fields = function (){
            ledOrigin.showFieldModal();
            ledOrigin.addField();
            ledOrigin.removeField();
        };
        
        /**
         * Mostramos el modal de los campos con el formulario
         */
        ledOrigin.showFieldModal= function (){
            $('#addFieldOrigin').click(function(){
                var $form = $('#fields-form');
                ledHelper.clearForm($form);
                $('#fieldModal').modal();
            });
        };
        
        /**
         * Ocultar el modal de campos
         */
        ledOrigin.closeFieldModal= function (){
            $('#fieldModal').modal('toggle');
        };
        
        /**
         * Insertamos el nuevo campo al campo hidden del
         * formulario del origen
         */
        ledOrigin.addField = function(){
            $('#insertField').click(function(e){
                e.preventDefault();
                e.stopPropagation();
                
                var $form = $('#fields-form');
                
                //Pasamos el objeto serialize a un JSON para trabajar con la informacion
                data = ledHelper.formDataToJson($form);
                
                //Validamos que no tenga campos vacios
                if(ledHelper.validateRequiredFields($form, data)){
                    //Agregamos el nuevo registro
                    ledOrigin.addFieldToJSON(data);
                    ledOrigin.addFieldToList(data);

                    //Mostramos el listado o un mensaje
                    ledOrigin.statusFieldsList();
                    
                    //Limpiamos y cerramos el formulario
                    ledHelper.clearForm($form);
                    ledOrigin.closeFieldModal();
                }
            });
        }
        
        /**
         * Eliminamos un campo
         */
        ledOrigin.removeField = function(){
            $(document).on('click', '.remove-field', function(){
                var $this    = $(this);
                var $field   = $this.closest('li');
                var position = $field.index();
                
                //Eliminamos el campo
                ledOrigin.removeFieldToJSON(position);
                $field.remove();
                ledOrigin.statusFieldsList();
            });
        }
        
        /**
         * Agregamos el nuevo registro del campo a la lista
         * 
         * @param JSON data
         */
        ledOrigin.addFieldToList = function(data){
            var $list         = $('#fields-list');
            var $listItem     = '<li title="'+data.tag+'">'+
                                data.field+
                                '<div class="pull-right remove-field"><i class="icon-bin"></i></div>'+
                                '</li>';
            $list.append($listItem);
        }
        
        /**
         * Agregamos el nuevo registro del campo al JSON del input hidden del origen
         * 
         * @param JSON data
         */
        ledOrigin.addFieldToJSON = function(data){
            var fields = $('#fields').val() 
                         ? jQuery.parseJSON($('#fields').val())
                         : [];
                            
            //Agregamos el nuevo campo                
            fields.push(data);
            
            //Pasamos el objeto a un JSON y actualizamos el input hidden del origen
            var fieldsJSON = JSON.stringify(fields);
            $('#fields').val(fieldsJSON);
        }
        
        /**
         * Eliminamos un registro del JSON
         * 
         * @param int position
         */
        ledOrigin.removeFieldToJSON = function(position){
            var fields = jQuery.parseJSON($('#fields').val());
                            
            //Eliminamos el campo               
            fields.splice(position,1);
            
            //Pasamos el objeto a un JSON y actualizamos el input hidden del origen
            var fieldsJSON = JSON.stringify(fields);
            $('#fields').val(fieldsJSON);
        }
        
        /**
         * Mostramos el estado de la lista de campos
         */
        ledOrigin.statusFieldsList = function(){
            var $list         = $('#fields-list');
            var $noField      = $('#no-field'); 
            var countItemList = $list.find('li').length;
            
            if(countItemList > 0){
                $list.removeClass('hidden');
                $noField.addClass('hidden');
            }else{
                $noField.removeClass('hidden');
                $list.addClass('hidden');
            }
            window.fieldStatus = true;
        }
        
        /******************************************************
         ******************** Filtros *************************
         ******************************************************/
        
        /**
         * Funcionalidad referente a los filtros asociados 
         * a los origenes
         */
        ledOrigin.filters = function (){
            ledOrigin.showFilterModal();
            ledOrigin.addFilter();
            ledOrigin.removeFilter();
            ledOrigin.makeFiltersSortable();
        };
        
        /**
         * Mostramos el modal de los filtros con el formulario
         */
        ledOrigin.showFilterModal = function (){
            $('#addFilterOrigin').click(function(){
                var $form = $('#filters-form');
                ledHelper.clearForm($form);
                $('#filterModal').modal();
            });
        };
        
        /**
         * Ocultar el modal de filtros
         */
        ledOrigin.closeFilterModal = function (){
            $('#filterModal').modal('toggle');
        };
        
        /**
         * Agregamos un nuevo filtro
         */
        ledOrigin.addFilter = function(){
            $('#insertFilter').click(function(e){
                e.preventDefault();
                e.stopPropagation();
                
                var $form = $('#filters-form');
                
                //Pasamos el objeto serialize a un JSON para trabajar con la informacion
                data = ledHelper.formDataToJson($form);
                
                //Validamos que no tenga campos vacios
                if(ledHelper.validateRequiredFields($form, data)){
                    //Agregamos el nuevo registro
                    ledOrigin.addFilterToJSON(data);
                    ledOrigin.addFilterToList(data);

                    //Mostramos el listado o un mensaje
                    ledOrigin.statusFiltersList();
                    
                    //Limpiamos y cerramos el formulario
                    ledHelper.clearForm($form);
                    ledOrigin.closeFilterModal();
                }
            });
        };
        
        /**
         * Eliminamos un filtro asociado al origen
         */
        ledOrigin.removeFilter = function(){
            $(document).on('click', '.remove-filter', function () {
                var $this    = $(this);
                var $filter  = $this.closest('li');
                var position = $filter.index();

                //Eliminamos el campo
                ledOrigin.removeFilterToJSON(position);
                $filter.remove();
                ledOrigin.statusFiltersList();
            });
        };
        
        /**
         * Agregamos el nuevo registro del filtro a la lista
         * 
         * @param JSON data
         */
        ledOrigin.addFilterToList = function(data){
            var $list         = $('#filters-list');
            var $listItem     = '<li title="'+data.param+'" data-options="" data-attributes="">'+
                                data.title+
                                '<div class="pull-right remove-filter"><i class="icon-bin"></i></div>'+
                                '<div class="pull-right options-filter"><a href="javascript:void(0)" class="options" title="Opciones"> Opc. </a></div>'+
                                '<div class="pull-right attributes-filter"><a href="javascript:void(0)" class="attributes" title="Atributos"> Atr. </a></div>'+
                                '</li>';
            $list.append($listItem);
        }
        
        /**
         * Agregamos el nuevo registro del filtro al JSON del input hidden del origen
         * 
         * @param JSON data
         */
        ledOrigin.addFilterToJSON = function(data){
            var filters = $('#filters').val() 
                         ? jQuery.parseJSON($('#filters').val())
                         : [];
                            
            //Agregamos el nuevo campo                
            filters.push(data);
            
            //Pasamos el objeto a un JSON y actualizamos el input hidden del origen
            var filtersJSON = JSON.stringify(filters);
            $('#filters').val(filtersJSON);
        }
        
        /**
         * Eliminamos un registro del JSON
         * 
         * @param int position
         */
        ledOrigin.removeFilterToJSON = function(position){
            var filters = jQuery.parseJSON($('#filters').val());
                            
            //Eliminamos el filtro               
            filters.splice(position,1);
            
            //Pasamos el objeto a un JSON y actualizamos el input hidden del origen
            var filtersJSON = JSON.stringify(filters);
            $('#filters').val(filtersJSON);
        }
        
        /**
         * Mostramos el estado de la lista de filtros
         */
        ledOrigin.statusFiltersList = function(){
            var $list          = $('#filters-list');
            var $noFilter      = $('#no-filters'); 
            var countItemList  = $list.find('li').length;
            
            if(countItemList > 0){
                $list.removeClass('hidden');
                $noFilter.addClass('hidden');
            }else{
                $noFilter.removeClass('hidden');
                $list.addClass('hidden');
            }
            window.filterStatus = true;
        }
        
        /**
         * Damos la posibilidad de reordenar los filtros
         */
        ledOrigin.makeFiltersSortable = function(){
            $('#filters-list').sortable({
                items: '> li',
                helper: 'clone', 
                start: function(event, ui) {
                    ui.item.startPos = ui.item.index();
                },
                stop: function (event, ui) {
                    var startItemPosition = ui.item.startPos;
                    var stopItemPosition  = ui.item.index();
                    
                    ledOrigin.orderFilters(startItemPosition, stopItemPosition);
                }
            });
        }
        
        /**
         * Reordenamos los filtros
         * 
         * @param int startItemPosition
         * @param int stopItemPosition{undefined}
         */
        ledOrigin.orderFilters = function(startItemPosition, stopItemPosition){
            var filters = JSON.parse($('#filters').val());
            var newOrdersFilters = ledHelper.arrayMove(filters, startItemPosition, stopItemPosition);
            
            //Pasamos el objeto a un JSON y actualizamos el input hidden del origen
            var filtersJSON = JSON.stringify(newOrdersFilters);
            $('#filters').val(filtersJSON);
            
            //Activamos el estado
            window.filterStatus = true;
        }
        
        /******************************************************
         ******************** Opciones ************************
         ******************************************************/
        
        /**
         * Funcionalidad referente a los opciones de los 
         * filtros asociados a los origenes
         */
        ledOrigin.filterOptions = function (){
            ledOrigin.showFilterOptionsModal();
            ledOrigin.addFilterOption();
            ledOrigin.removeFilterOption();
        };
        
        /**
         * Mostramos el modal de los filtros con el formulario
         */
        ledOrigin.showFilterOptionsModal = function (){
            $(document).on('click', '.options-filter', function(){
                //Insertamos el indice del item para identificarlo
                var $this = $(this);
                $('#indexFilterOptions').val($this.closest('li').index());
                
                //Actualizamos el listado
                ledOrigin.updateFilterOptionsList($this.closest('li')); 
                
                //Limpiamos el formulario y mostramos el modal
                var $form = $('#filter-options-form');
                ledHelper.clearForm($form);
                $('#filterOptionsModal').modal();
                
                //Lo hacemos ordenable
                ledOrigin.makeFiltersOptionsSortable();
            });
        };
        
        /**
         * Actualizamos el contenido del listado de opciones
         */
        ledOrigin.updateFilterOptionsList = function($filter){
            var options = $filter.attr('data-options') 
                          ? jQuery.parseJSON($filter.attr('data-options'))
                          : [];
                          
            //Generamos el HTML con todas las opciones
            var optionsHTML = '';
            $.each(options, function(key, item){
                optionsHTML += '<li title="'+item.value+'">'+
                                item.name+
                                '<div class="pull-right remove-option"><i class="icon-bin"></i></div>'+
                                '</li>';
            });
            
            //Actualizamos el HTML del listado de opciones
            $('#filter-options-list').html(optionsHTML);
            
            //Actualizamos el estado de las opciones
            ledOrigin.statusFilterOptionsList();
        }
        
        /**
         * Agregamos un nuevo filtro
         */
        ledOrigin.addFilterOption = function(){
            $('#insertOption').click(function(e){
                e.preventDefault();
                e.stopPropagation();
                
                var $form = $('#filter-options-form');
                
                //Pasamos el objeto serialize a un JSON para trabajar con la informacion
                data = ledHelper.formDataToJson($form);
                
                //Validamos que no tenga campos vacios
                if(ledHelper.validateRequiredFields($form, data)){
                    //Agregamos el nuevo registro
                    ledOrigin.addFilterOptionsToJSON(data);
                    ledOrigin.addFilterOptionsToList(data);

                    //Mostramos el listado o un mensaje
                    ledOrigin.statusFilterOptionsList();
                    
                    //Limpiamos y cerramos el formulario
                    ledHelper.clearForm($form);
                }
            });
        };
        
        /**
         * Eliminamos un filtro asociado al origen
         */
        ledOrigin.removeFilterOption = function(){
            $(document).on('click', '.remove-option', function () {
                var $this    = $(this);
                var $option  = $this.closest('li');
                var position = $option.index();

                //Eliminamos el campo
                ledOrigin.removeFilterOptionsToJSON(position);
                $option.remove();
                ledOrigin.statusFilterOptionsList();
            });
        };
        
        /**
         * Agregamos el nuevo registro del filtro a la lista
         * 
         * @param JSON data
         */
        ledOrigin.addFilterOptionsToList = function(data){
            var $list         = $('#filter-options-list');
            var $listItem     = '<li title="'+data.value+'">'+
                                data.name+
                                '<div class="pull-right remove-option"><i class="icon-bin"></i></div>'+
                                '</li>';
            $list.append($listItem);
        }
        
        /**
         * Agregamos el nuevo registro a los JSON necesarios
         * 
         * @param JSON data
         */
        ledOrigin.addFilterOptionsToJSON = function(data){
            var index   = parseInt($('#indexFilterOptions').val());
            var $filter = $('#filters-list > li:nth-child('+(index+1)+')');
            var options = $filter.attr('data-options') 
                          ? jQuery.parseJSON($filter.attr('data-options'))
                          : [];
                            
            //Agregamos el nuevo campo                
            options.push(data);
            
            //Pasamos el objeto a un JSON y actualizamos el input hidden del origen
            var optionsJSON = JSON.stringify(options);
            $filter.attr('data-options', optionsJSON);
            
            //Actualizamos el JSON de #filtros              
            var filters = $('#filters').val() 
                          ? jQuery.parseJSON($('#filters').val())
                          : [];
            filters[index]['options'] = options;
            var filtersJSON = JSON.stringify(filters);
            $('#filters').val(filtersJSON);
        }
        
        /**
         * Eliminamos un registro del JSON
         * 
         * @param int position
         */
        ledOrigin.removeFilterOptionsToJSON = function(position){
            var index   = parseInt($('#indexFilterOptions').val());
            var $filter = $('#filters-list > li:nth-child('+(index+1)+')');
            var options = jQuery.parseJSON($filter.attr('data-options'));
                            
            //Eliminamos la opcion               
            options.splice(position,1);
            
            //Pasamos el objeto a un JSON y actualizamos las opciones
            var optionsJSON = JSON.stringify(options);
            $filter.attr('data-options', optionsJSON);
            
            //Actualizamos el JSON de #filtros              
            var filters = jQuery.parseJSON($('#filters').val());
            filters[index]['options'] = options;
            var filtersJSON = JSON.stringify(filters);
            $('#filters').val(filtersJSON);
        }
        
        /**
         * Mostramos el estado de la lista de filtros
         */
        ledOrigin.statusFilterOptionsList = function(){
            var $list          = $('#filter-options-list');
            var $noFilter      = $('#no-filter-options'); 
            var countItemList  = $list.find('li').length;
            
            if(countItemList > 0){
                $list.removeClass('hidden');
                $noFilter.addClass('hidden');
            }else{
                $noFilter.removeClass('hidden');
                $list.addClass('hidden');
            }
            window.filterStatus = true;
        }
        
        /**
         * Damos la posibilidad de reordenar las opciones
         */
        ledOrigin.makeFiltersOptionsSortable = function(){
            $('#filter-options-list').sortable({
                items: '> li',
                helper: 'clone', 
                start: function(event, ui) {
                    ui.item.startPos = ui.item.index();
                },
                stop: function (event, ui) {
                    var startItemPosition = ui.item.startPos;
                    var stopItemPosition  = ui.item.index();
                    
                    ledOrigin.orderOptions(startItemPosition, stopItemPosition);
                }
            });
        }
        
        /**
         * Reordenamos las opciones
         * 
         * @param int startItemPosition
         * @param int stopItemPosition{undefined}
         */
        ledOrigin.orderOptions = function(startItemPosition, stopItemPosition){
            var index   = parseInt($('#indexFilterOptions').val());
            var $filter = $('#filters-list > li:nth-child('+(index+1)+')');
            
            var options        = JSON.parse($filter.attr('data-options'));
            var newOptions     = ledHelper.arrayMove(options, startItemPosition, stopItemPosition);
            var newOptionsJSON = JSON.stringify(newOptions);
            $filter.attr('data-options', newOptionsJSON);
            
            //Actualizamos el JSON de #filtros              
            var filters = JSON.parse($('#filters').val());
            filters[index]['options'] = newOptions;
            
            var filtersJSON = JSON.stringify(filters);
            $('#filters').val(filtersJSON);
            
            //Activamos el estado
            window.filterStatus = true;
        }
        
        /******************************************************
         ******************** Atributos ***********************
         ******************************************************/
        
        /**
         * Funcionalidad referente a los atributos de los 
         * filtros asociados a los origenes
         */
        ledOrigin.filterAttributes = function (){
            ledOrigin.showFilterAttributesModal();
            ledOrigin.addFilterAttribute();
            ledOrigin.removeFilterAttribute();
        };
        
        /**
         * Mostramos el modal de los atributos con el formulario
         */
        ledOrigin.showFilterAttributesModal = function (){
            $(document).on('click', '.attributes-filter', function(){
                //Insertamos el indice del item para identificarlo
                var $this = $(this);
                $('#indexFilterAttributes').val($this.closest('li').index());
                
                //Actualizamos el listado
                ledOrigin.updateFilterAttributesList($this.closest('li')); 
                
                //Limpiamos el formulario y mostramos el modal
                var $form = $('#filter-attributes-form');
                ledHelper.clearForm($form);
                $('#filterAttributesModal').modal();
            });
        };
        
        /**
         * Actualizamos el contenido del listado de opciones
         */
        ledOrigin.updateFilterAttributesList = function($filter){
            var attributes = $filter.attr('data-attributes') 
                          ? jQuery.parseJSON($filter.attr('data-attributes'))
                          : [];
                          
            //Generamos el HTML con todas los atributos
            var attributesHTML = '';
            $.each(attributes, function(key, item){
                attributesHTML += '<li title="'+item.value+'">'+
                                item.name+
                                '<div class="pull-right remove-attribute"><i class="icon-bin"></i></div>'+
                                '</li>';
            });
            
            //Actualizamos el HTML del listado de atributos
            $('#filter-attributes-list').html(attributesHTML);
            
            //Actualizamos el estado de los atributos
            ledOrigin.statusFilterAttributesList();
        }
        
        /**
         * Agregamos un nuevo atributo
         */
        ledOrigin.addFilterAttribute = function(){
            $('#insertAttribute').click(function(e){
                e.preventDefault();
                e.stopPropagation();
                
                var $form = $('#filter-attributes-form');
                
                //Pasamos el objeto serialize a un JSON para trabajar con la informacion
                data = ledHelper.formDataToJson($form);
                
                //Validamos que no tenga campos vacios
                if(ledHelper.validateRequiredFields($form, data)){
                    //Agregamos el nuevo registro
                    ledOrigin.addFilterAttributesToJSON(data);
                    ledOrigin.addFilterAttributesToList(data);

                    //Mostramos el listado o un mensaje
                    ledOrigin.statusFilterAttributesList();
                    
                    //Limpiamos y cerramos el formulario
                    ledHelper.clearForm($form);
                }
            });
        };
        
        /**
         * Eliminamos un atributo asociado al filtro
         */
        ledOrigin.removeFilterAttribute = function(){
            $(document).on('click', '.remove-attribute', function () {
                var $this       = $(this);
                var $attribute  = $this.closest('li');
                var position    = $attribute.index();

                //Eliminamos el campo
                ledOrigin.removeFilterAttributesToJSON(position);
                $attribute.remove();
                ledOrigin.statusFilterAttributesList();
            });
        };
        
        /**
         * Agregamos el nuevo registro del atributo a la lista
         * 
         * @param JSON data
         */
        ledOrigin.addFilterAttributesToList = function(data){
            var $list         = $('#filter-attributes-list');
            var $listItem     = '<li title="'+data.value+'">'+
                                data.name+
                                '<div class="pull-right remove-attribute"><i class="icon-bin"></i></div>'+
                                '</li>';
            $list.append($listItem);
        }
        
        /**
         * Agregamos el nuevo registro a los JSON necesarios
         * 
         * @param JSON data
         */
        ledOrigin.addFilterAttributesToJSON = function(data){
            var index      = parseInt($('#indexFilterAttributes').val());
            var $filter    = $('#filters-list > li:nth-child('+(index+1)+')');
            var attributes = $filter.attr('data-attributes') 
                          ? jQuery.parseJSON($filter.attr('data-attributes'))
                          : [];
                            
            //Agregamos el nuevo atributo                
            attributes.push(data);
            
            //Pasamos el objeto a un JSON y actualizamos el input hidden del origen
            var attributesJSON = JSON.stringify(attributes);
            $filter.attr('data-attributes', attributesJSON);
            
            //Actualizamos el JSON de #filtros              
            var filters = $('#filters').val() 
                          ? jQuery.parseJSON($('#filters').val())
                          : [];
            filters[index]['attributes'] = attributes;
            var filtersJSON = JSON.stringify(filters);
            $('#filters').val(filtersJSON);
        }
        
        /**
         * Eliminamos un registro del JSON
         * 
         * @param int position
         */
        ledOrigin.removeFilterAttributesToJSON = function(position){
            var index      = parseInt($('#indexFilterAttributes').val());
            var $filter    = $('#filters-list > li:nth-child('+(index+1)+')');
            var attributes = jQuery.parseJSON($filter.attr('data-attributes'));
                            
            //Eliminamos la opcion               
            attributes.splice(position,1);
            
            //Pasamos el objeto a un JSON y actualizamos los atributos
            var attributesJSON = JSON.stringify(attributes);
            $filter.attr('data-attributes', attributesJSON);
            
            //Actualizamos el JSON de #filtros              
            var filters = jQuery.parseJSON($('#filters').val());
            filters[index]['attributes'] = attributes;
            var filtersJSON = JSON.stringify(filters);
            $('#filters').val(filtersJSON);
        }
        
        /**
         * Mostramos el estado de la lista de atributos
         */
        ledOrigin.statusFilterAttributesList = function(){
            var $list          = $('#filter-attributes-list');
            var $noFilter      = $('#no-filter-attributes'); 
            var countItemList  = $list.find('li').length;
            
            if(countItemList > 0){
                $list.removeClass('hidden');
                $noFilter.addClass('hidden');
            }else{
                $noFilter.removeClass('hidden');
                $list.addClass('hidden');
            }
            window.filterStatus = true;
        }
        
        /******************************************************
         ******************** Revistas ************************
         ******************************************************/
        
        /**
         * Actualizamos el input hidden de asociacion de las revistas 
         */
        ledOrigin.selectMagazines = function () {
            $(document).on('change click', '#magazines_id', function () {
                var $this = $(this);
                $('#magazines').val($this.val());
                window.magazineStatus = true;
            });
        }
        
        /******************************************************
         ******************** Componentes *********************
         ******************************************************/
        
        /**
         * Actualizamos el input hidden de asociacion de las revistas 
         */
        ledOrigin.selectComponents = function () {
            $(document).on('change click', '#components_id', function () {
                var $this = $(this);
                $('#components').val($this.val());
                window.componentStatus = true;
            });
        }
        
        /******************************************************
         ******************** Origenes ************************
         ******************************************************/
        
        /**
         * Funcionalidades de los origenes
         */
        ledOrigin.origin = function(){
            ledOrigin.addEventOrigin();
            ledOrigin.updateEventOrigin();
            ledOrigin.removeEventOrigin();
        }
        
        /**
         * Insertamos un origen
         */
        ledOrigin.addEventOrigin = function(){
            $('#saveOriginContent').click(function(e){
                e.preventDefault();
                e.stopPropagation();
                
                ledOrigin.addOrigin();
            });
        }
        
        /**
         * Insertamos por ajax un origen
         */
        ledOrigin.addOrigin = function(){
            var action   = BASE_URL+'/origin/create/';
            var redirect = BASE_URL+'/origin/edit/';
            var $form    = $('#origin-form');
            var data     = ledHelper.formDataToJson($form);

            if (ledHelper.validateRequiredFields($form, data)) {
                var request = ledHelper.ajaxCall({
                    url: action,
                    data: data
                });

                request.done(function (response) {
                    if (response.status === 'OK') {
                        window.location.href = redirect + response.id;
                    } else {
                        ledHelper.showMsgFormAction(response.status, response.msg);
                    }
                }).fail(function (xhr, ajaxOptions, thrownError) {
                    ledHelper.showMsgFormAction('KO', thrownError);
                });
            } 
        }
        
        /**
         * Actualizamos un origen
         */
        ledOrigin.updateEventOrigin = function(){
            $('#updateOriginContent').click(function(e){
                e.preventDefault();
                e.stopPropagation();
                
                ledOrigin.updateOrigin();
            });
        }
        
        /**
         * Actualizamos por ajax un origen
         */
        ledOrigin.updateOrigin = function(){
            var action   = BASE_URL+'/origin/update/';
            var $form    = $('#origin-form');
            var data     = ledHelper.formDataToJson($form);
            
            //Eliminamos las propiedades que no han sido modificadas
            if(!window.magazineStatus){
                delete data.magazines;
            }
            
            if(!window.componentStatus){
                delete data.components;
            }
            
            if(!window.fieldStatus){
                delete data.fields;
            }
            
            if(!window.filterStatus){
                delete data.filters;
            }
            
            if (ledHelper.validateRequiredFields($form, data)) {
                var request = ledHelper.ajaxCall({
                    url: action,
                    data: data
                });

                request.done(function (response) {
                    ledHelper.showMsgFormAction(response.status, response.msg);
                }).fail(function (xhr, ajaxOptions, thrownError) {
                    ledHelper.showMsgFormAction('KO', thrownError);
                });
            }
        }
        
        /**
         * Eliminamos un origen
         */
        ledOrigin.removeEventOrigin = function (){
            $(document).on('click', '.deleteOrigin', function(){
                var $this   = $(this);
                var options = {
                    buttons: {
                        Aceptar: function () {
                            ledOrigin.removeOrigin($this);
                            $('#led-dialog').dialog("close");
                        },
                        Cancelar: function () {
                            $('#led-dialog').dialog("close");
                        }
                    }
                };
                ledHelper.addDialog('Eliminar origen', '&iquest;Est&aacute; seguro?', 'alert', options);
            });
        }
        
        /**
         * Eliminamos por ajax un origen
         */
        ledOrigin.removeOrigin = function($this){
            var action     = $this.data('action');
            var data       = {id : $this.data('id')};
            var currentUrl = window.location.href;
            
            var request = ledHelper.ajaxCall({
                url : action,
                data: data
            });

            request.done(function (response) {
                if (response.status === 'OK') {
                    $('.magazines-table').load(currentUrl + ' .magazines-table');
                } 
                ledHelper.showMsgFormAction(response.status, response.msg);
            }).fail(function (xhr, ajaxOptions, thrownError) {
                ledHelper.showMsgFormAction('KO', thrownError);
            });
        }
        
        return ledOrigin.init();
    };

    $(window).ledOrigin();
})(jQuery);