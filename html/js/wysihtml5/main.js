$(document).ready(function() {
    var editor = new wysihtml5.Editor("reviewText", { // id of textarea element
      //toolbar:      "wysihtml5-toolbar", // id of toolbar element
      parserRules:  wysihtml5ParserRules, // defined in parser rules set
      name: 'reviewTextIframe' //clase del iframe que genera el WYSIWYG
    });
    
    var editorApproach = new wysihtml5.Editor("approachText", { // id of textarea element
      //toolbar:      "wysihtml5-toolbar", // id of toolbar element
      parserRules:  wysihtml5ParserRules, // defined in parser rules set
      name: 'approachTextIframe' //clase del iframe que genera el WYSIWYG
    });
    
    var editorResult = new wysihtml5.Editor("resultText", { // id of textarea element
      //toolbar:      "wysihtml5-toolbar", // id of toolbar element
      parserRules:  wysihtml5ParserRules, // defined in parser rules set
      name: 'resultTextIframe' //clase del iframe que genera el WYSIWYG
    });
    var editorPointView = new wysihtml5.Editor("pointViewText", { // id of textarea element
      //toolbar:      "wysihtml5-toolbar", // id of toolbar element
      parserRules:  wysihtml5ParserRules, // defined in parser rules set
      name: 'pointViewTextIframe' //clase del iframe que genera el WYSIWYG
    });
});