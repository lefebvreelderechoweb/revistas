$(document).ready(function() {
    
    var timeOutMsg;
    var modalLoading = new ledModalLoading('html');
    modalLoading.setAnimationTime(500);
    
    /**
    * Mostramos modal para la prueba de envío
    */
    $('body').on('click', '.trial-btn', function(e){
        $("#trial input:not([type*=hidden])").val('');
        $("#trial").modal();
    });
    
    /**
     * Click en cualquier item de la listas de envío
     */
    
    $('body').on('change', '.edit-list input[type=checkbox]', function(e){
        var $this = $(this);

        //Cambiamos las clases para definir las listas seleccionadas
        if($this.is(':checked')){
            $this.parent().parent().addClass('active-list').addClass('id-list');
            
            if($this.attr('id') == 'browserAll'){
                $('input[type=checkbox]').each(function(){
                    
                    if($(this).attr('name') == 'browser' && $(this).attr('id') != 'browserAll'){
                        
                        if ($(this).is(':checked'))
                            $(this).parent().parent().removeClass('active-list').removeClass('id-list');
                        
                         $(this).prop('checked', true);
                         $(this).attr("disabled", true);
                         var $label = $("label[for='"+$(this).attr('id')+"']");
                         $label.addClass( "desactiveCheck" );
                    }
                });
            }
        }else{
            $this.parent().parent().removeClass('active-list').removeClass('id-list');
            
            // Cambio el hidden para los usuarios identificados si está marcada la casilla
            // correspondiente
            if($(this).attr('id') == 'indentify'){
                $('#noIndentify').val('');
            }
            
            if($this.attr('id') == 'browserAll'){
                $('input[type=checkbox]').each(function(){
                    
                    if($(this).attr('name') == 'browser' && $(this).attr('id') != 'browserAll'){
                        
                         $(this).parent().parent().removeClass('active-list').removeClass('id-list');
                         $(this).prop('checked', false);
                         $(this).attr("disabled", false);
                         var $label = $("label[for='"+$(this).attr('id')+"']");
                         $label.removeClass( "desactiveCheck" );
                    }
                });
            }
        }

        //Pasamos un string con todos los ids y nombres seleccionados
        var infoLists = getIdsNamesList();
        $('#list-id').val(infoLists['ids']);
        $('#list-name').text(infoLists['names']);
        $('#notifications-list-ids').val(infoLists['names']);

        //El tipo de lista del ultimo seleccionado sera necesario si cambia la logica
        var typeList = $this.data('type');
        $('#list-type').val(typeList);
        
        // Cambio el hidden para los usuarios identificados si está marcada la casilla
        // correspondiente
        /*$('#noIndentify').val('1');
        $('.edit-list.active-list.id-list').each(function(){
            if ( $(this).find('input[type=checkbox]').attr("id") == 'indentify' ){
                $('#noIndentify').val('0');
            }
        });*/
    });
    
    $('body').on('click', '#ledCloneEventAction', function(){
        var url = $(this).data('url');
        location.href = url;
    });
    
    /**
     * Procesamos el formulario de planificación
     */
    $('body').on('submit', '#ledFormAddEventAction', function(e){
        /**
         * Añadimos un registro desde un formulario
         */
        e.preventDefault();

        modalAux = new ledModalLoading('html');
        modalAux.setAnimationTime(500);
        modalAux.show();


        var group = $(this).data('param-group');
        var data = getDataParams('.add-btn-event-item', group);
        var url = this.action;
        var urlLoad = $(this).data("url-load").length > 0 ? $(this).data("url-load") : "";
        var idOrClassLoad = $(this).data("class-id").length > 0 ? $(this).data("class-id") : "";
        var classMessage = $(this).data("class-message").length > 0 ? $(this).data("class-message") : "";

        if(data){
            var event = callAjax(url, data, urlLoad, idOrClassLoad, classMessage);
            if($('.frequency-form').length>0){
                $('.frequency-form input[type=text]').val("");
                $(".frequency-form select").val($(".frequency-form select option:first").val());
            }
        }
        window.clickMenuItem = false;
    });
    
    
    
    $('body').on('click', '#ledAddEventAction, #ledSaveEventAction', function(){

        modalAux = new ledModalLoading('html');
        modalAux.setAnimationTime(500);
        modalAux.show();

        var group = $(this).data('param-group');
        var url = $(this).data('url');
        var urlLoad = $(this).data("url-load").length > 0 ? $(this).data("url-load") : "";
        var idOrClassLoad = $(this).data("class-id").length > 0 ? $(this).data("class-id") : "";
        var data = getDataParams('.add-btn-event-item', group);
        var classMessage = $(this).data("class-message").length > 0 ? $(this).data("class-message") : "";

        if(data){
            callAjax(url, data, urlLoad, idOrClassLoad, classMessage);
            if($(this).attr('id') == 'ledAddEventAction'){
                reloadMenu($(this).data('refresh-menu'));
            }
        }
        window.clickMenuItem = false;
        
        /**
         * En el caso de listado auxiliar
         * actualizamos el número destinatarios
         */
        if ($(this).hasClass('add-button-list-auxiliary')){
            var $elementAddreessess = $(this).parents('.active-list').find('.addresseess-list-auxiliary');
            var addresseess = $elementAddreessess.text();
            if(!Number.isInteger(parseInt(addresseess))){
                $elementAddreessess.removeClass('no-results');
                $elementAddreessess.text('1');
            }else{
                $elementAddreessess.text(parseInt(addresseess)+1);
            }
        }
    });
    
    /**
     * Elimina una fila de planificación
     */
    $('body').on('click', '.deleteRow', function(){
        var type = $(this).data('type');

        if(type === 'delete'){

            $(this).hide();
            $(this).after("<button class=\"delete deleteRow deleteRowAfter\" data-type=\"delete-ok\"><span class=\"icon-ok color-icon-green\" title=\"Eliminar\"></span></button><button class=\"delete deleteRow deleteRowAfter\" data-type=\"delete-ko\"><span class=\"icon-cancel color-icon-red\" title=\"Cancelar\"></span></button>");

        }else if(type === 'delete-ko'){

            $(this).siblings('.deleteRow').show();
            $(this).siblings('.deleteRowAfter').remove();
            $(this).remove();

        }else if(type === 'delete-ok'){

            var $btnDelete = $(this).siblings('button[data-type=delete]');
            var url = $btnDelete.data('url');
            var data = {
                'id' : $btnDelete.data('id')
            };
            var urlLoad = $btnDelete.data('url-load').length > 0 ? $btnDelete.data('url-load') : "";
            var idOrClassLoad = $btnDelete.data('class-id').length > 0 ? $btnDelete.data('class-id') : "";
            if(data.id){
                callAjax(url, data, urlLoad, idOrClassLoad);
                /**
                * En el caso de listado auxiliar
                * actualizamos el número destinatarios
                */
                var $elementTdParent = $(this).parents('.delete-button-list-auxiliary').length;
                if ($elementTdParent > 0){
                    var $elementAddreessess = $(this).parents('.active-list').find('.addresseess-list-auxiliary');
                    var addresseess = $elementAddreessess.text();
                    if(parseInt(addresseess) == 1){
                        $elementAddreessess.addClass('no-results');    
                        $elementAddreessess.text('no hay resultados');
                    }else{
                        $elementAddreessess.text(parseInt(addresseess)-1);
                    }
                }
            }
            reloadMenu($btnDelete.data('refresh-menu'));
        }
    });
    
    
    
    
    
    //******************************** FUNCIONES *************************************//
    
    
   
   var $request = null;
   
   /**
    * Llamamos al ajax para enviar los datos y devolvemos mensaje
    * @param {type} url
    * @param {type} data
    * @param {type} urlLoad
    * @param {type} idOrClassLoad
    * @param {type} classMessage
    * @returns {undefined}
    */
   function callAjax ( url, data, urlLoad, idOrClassLoad, classMessage ){
       urlLoad = urlLoad || "";
       idOrClassLoad = idOrClassLoad || "";
       classMessage = classMessage || "";
       
       if ($request != null) {
           $request.abort();
           $request = null;
       }
       $request = $.ajax({
           type: "POST",
           url: url,
           dataType: "json",
           data: data,
           success: function (response) {
               if (response) {
                   console.log(response);
                   if(classMessage){
                       
                       var statusOK = 'KO';
                       if(response.ok != false){
                           statusOK = 'OK';
                           window.isModify = false;
                       }

                       showMsgActionEvent( statusOK , response.message );
                       if($("#trial").length > 0 && statusOK === 'OK'){
                           $("#trial").modal('hide');
                       }
                   }
                   if(response.ok && urlLoad !== "" && idOrClassLoad !== ""){
                       $("."+idOrClassLoad).load(urlLoad + ' #'+idOrClassLoad);
                   }else if(response.ok && response.html){
                       $("."+idOrClassLoad).html(response.html);
                   }
                   
                    if (typeof(modalAux) !== 'undefined') {
                        modalAux.hide();
                    }
                    
                    if (typeof(modalLoading) !== 'undefined') {
                        modalLoading.hide();
                    }
                   
               }
           },
           error: function (xhr, ajaxOptions, thrownError) {
               console.log(xhr.status);
               console.log(thrownError);
               if(typeof modalAux !== 'undefined'){
                   modalAux.hide();
               }
               showMsgActionEvent( 'KO' , 'Se ha producido un error. Pruebe más tarde.' );
           }
       }).done(function(){
           if($('#ledFormAddEventAction').length > 0){
               reloadMenu($('#ledFormAddEventAction').data('refresh-menu'));
           }
           if($('#ledSaveEventAction').length> 0){
               reloadMenu($('#ledSaveEventAction').data('refresh-menu'));
           }
           try {
               modalAux.hide();
           }
           catch(error) {
           }
       });
   }
   
   
   /**
    * Refrescar menu inferior para actualizar el estado de los botones
    * @param {type} refresh
    * @returns {undefined}
    */
   function reloadMenu (refresh){
       if(refresh){
          $( '#walkthrough-bar').load($('#walkthrough-bar').data( 'url' ) + ' .walkthrough-bar');
       }
   }
   
   
   /**
    * Función donde creamos un objeto con los datos necesarios para hacer el ajax
    * @param {type} paramClass
    * @param {type} group
    * @returns {valueParam}
    */
   function getDataParams (paramClass, group){
       group = group || "";

       var classForEach = group ? paramClass + '[data-param-group='+group+']' : paramClass;
       var data = {};

       $( classForEach ).each(function() {
           var param = $(this).data('param');
           var valueParam = "";

           if(($(this).is('input:checkbox') && $(this).is(":checked")) || (($(this).is('input:radio') && $(this).is(":checked")) || $(this).is('input:text') || $(this).is('textarea') || $(this).is('input:hidden') || ($(this).attr('type')==="date") || ($(this).attr('type')==="email")) && ($(this).attr("disabled") === undefined)){
               valueParam = $(this).val();
           }else if($(this).is('select') && $(this).attr("disabled") === undefined){
               valueParam = $(this).find('option:selected').val();
           }

           if(param !== "" && valueParam !== ""){
               data[param] = valueParam;
           }
       });
       return data;
   }
   
    /**
     * Devuelve un string con todos los ids seleccionados
     * @returns {_L1.getIdsNamesList.Anonym$1}
     */
    function getIdsNamesList(){
        var idsLists = '';
        var namesLists = '';
        $('.edit-list.active-list.id-list').each(function(){
            
            // Excepción para el tipo de radio button de selección de usuarios
            // identificados en las listas
            if ( $(this).find('input[type=checkbox]').attr( "id") == 'indentify' ){
                namesLists += $(this).find('input[type=checkbox]').data('name')+', ';
                $('#noIndentify').val($(this).find('input[type=checkbox]').val());
            }else{
                idsLists += $(this).find('input[type=checkbox]').val()+',';
                namesLists += $(this).find('input[type=checkbox]').data('name')+', ';
            } 
            
        }); 
        return {
            ids   : idsLists.substring(0, idsLists.length - 1),
            names : namesLists.substring(0, namesLists.length - 2)
        };
    };
   
   /**
    * Mensaje de la cabecera al termina la llamada a callAjax
    * @param {type} result
    * @param {type} msg
    * @returns {undefined}
    */
   function showMsgActionEvent ( result, msg ) {

       $ledInfoActions = $('#ledInfoActions');
       $ledInfoActions.removeClass('alert-danger').removeClass('alert-success');
       $ledInfoActions.html( msg );
       switch ( result ){
           case 'KO':
               $ledInfoActions.addClass('alert-danger');
               break;

           case 'OK':
               $ledInfoActions.addClass('alert-success');
               break;
           default:
               break;
       }

       $ledInfoActions.fadeIn('200');
       clearTimeout(timeOutMsg);

       timeOutMsg = window.setTimeout(function(){
           $ledInfoActions.fadeOut('200', function(){});
       }, 4000);
   }
    
    
    //****************************** END FUNCIONES ***********************************//
    
    
});