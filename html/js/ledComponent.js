(function ($) {

    $.fn.ledComponent = function () {
        
        var ledComponent        = this;
        var ledHelper           = $(window).ledHelper();
        var BASE_URL            = window.location.origin;
        window.magazineStatus   = false;
        window.zoneStatus       = false;
        window.originStatus     = false;
        
        /**
         * Iniciamos la funcionalidad
         */
        ledComponent.init = function () {
            ledHelper.noErrorField();
            ledComponent.selectMagazines();
            ledComponent.selectZones();
            ledComponent.selectOrigins();
            ledComponent.component();
        };
        
        /******************************************************
         ******************** Revistas ************************
         ******************************************************/
        
        /**
         * Actualizamos el input hidden de asociacion de las revistas 
         */
        ledComponent.selectMagazines = function () {
            $(document).on('change click', '#magazines_id', function () {
                var $this = $(this);
                $('#magazines').val($this.val());
                window.magazineStatus = true;
            });
        }
        
        /******************************************************
         ******************** Zonas ***************************
         ******************************************************/
        
        /**
         * Actualizamos el input hidden de asociacion de las zonas 
         */
        ledComponent.selectZones = function () {
            $(document).on('change click', '#zones_id', function () {
                var $this = $(this);
                $('#zones').val($this.val());
                window.zoneStatus = true;
            });
        }
        
        /******************************************************
         ******************** Origenes ************************
         ******************************************************/
        
        /**
         * Actualizamos el input hidden de asociacion de los origenes 
         */
        ledComponent.selectOrigins = function () {
            $(document).on('change click', '#origins_id', function () {
                var $this = $(this);
                $('#origins').val($this.val());
                window.originStatus = true;
            });
        }
        
        /******************************************************
         ******************** Origenes ************************
         ******************************************************/
        
        /**
         * Funcionalidades de componentes
         */
        ledComponent.component = function(){
            ledComponent.updateEventComponent();
        }
        
        /**
         * Actualizamos un componente
         */
        ledComponent.updateEventComponent = function(){
            $('#updateComponent').click(function(e){
                e.preventDefault();
                e.stopPropagation();
                
                ledComponent.updateComponent();
            });
        }
        
        /**
         * Actualizamos por ajax un componente
         */
        ledComponent.updateComponent = function(){
            var action   = BASE_URL+'/component/update/';
            var $form    = $('#component-form');
            var data     = ledHelper.formDataToJson($form);
            
            //Eliminamos las propiedades que no han sido modificadas
            if(!window.magazineStatus){
                delete data.magazines;
            }
            
            if(!window.zoneStatus){
                delete data.zones;
            }
            
            if(!window.originStatus){
                delete data.origins;
            }
            
            if (ledHelper.validateRequiredFields($form, data)) {
                var request = ledHelper.ajaxCall({
                    url: action,
                    data: data
                });

                request.done(function (response) {
                    ledHelper.showMsgFormAction(response.status, response.msg);
                }).fail(function (xhr, ajaxOptions, thrownError) {
                    ledHelper.showMsgFormAction('KO', thrownError);
                });
            }
        }
        
        return ledComponent.init();
    };

    $(window).ledComponent();
})(jQuery);