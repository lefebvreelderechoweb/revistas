$(document).ready(function() {
    
    
    var timeOutMsg;
    var modalLoading = new ledModalLoading('html');
    modalLoading.setAnimationTime(500);
    window.isModify = false;
    
    w = $('#nl-edit').width();
    wSaveChanges = parseInt($('#containerComponents').width()) - 16;
    $('#handleZonesComponents').css('width', w + 'px');
    $('.save-changes').css('width', wSaveChanges + 'px');
    
    $(window).resize(function() {
        w = $('#nl-edit').width();
        wSaveChanges = parseInt($('#containerComponents').width()) - 16;
        $('#handleZonesComponents').css('width', w + 'px');
        $('.save-changes').css('width', wSaveChanges + 'px');
     });
     
     
     
    /**
     * Al cargar la sección de nueva notificación, cargamos también los origenes
     * de RSS
     */ 
    if ( $('#ledOriginSettings').length > 0 ){
        
        //Sacar los origenes disponibles dependiendo de la web que estemos 
        //notificando
        var origins = $('#formNotifications').data('origins');
        origins     = origins.toString(); //Para que no falle la siguiente linea, por si viene un solo origen, forzamos a que sea string
        var aOrigins = origins.split(",");
        var defaultOrigin = "";
        
        if (origins){
            defaultOrigin = aOrigins[0] ;
        }
        
        var dataUrl = $('.ledOrigin').data('origins-url');
        $('.ledOrigin').load(dataUrl+'/'+origins);
        getOriginList( defaultOrigin, '', '', '' );
        
        $( '#ledOriginSettings, button[data-target*="#ledOriginSettings"]' ).removeClass('hidden');
        $( '.itemOrigin > span' ).removeClass( 'icon-ok' ).addClass('icon-cancel');
        
    }
    
    //******************* GESTIÓN DE TABS (MANUAL E IMPORTACIÓN)**************************//
    /**
     * Gestión de cambios y click en las Tabs de Edición Manual e Importanción de
     * Contenido
     */
    $('body').on('click', '.ledSettingsTab', function(){
        var $elementId = $(this).parents('.horizontal-list').attr('id');
        $('#'+$elementId).find('li button.active').removeClass('active');
        var $this = $(this);
        $this.addClass('active');
    });
    
    $('body').on('click', '#ledSettingsMenu button, #ledSettingsOrigin button', function(){
        var $elementId = $(this).parents('.horizontal-list').attr('id');
        $('.ledSettingsTabContent[data-ul-id*='+$elementId+']').hide();
        $('.ledSettingsTab[data-ul-id*='+$elementId+']').removeClass('active'); 
        $('.noOriginAuto .noGetValue').val('');
        
        var $this = $(this);
        $($this.data('target')).show();
    });
    
    $('body').on('click', '.ledSettingsTab[data-target*=ledManualOrigin]', function(){
        $('.noOriginAuto').removeClass('hidden');
        $('.noOriginAuto').css('display', 'block');
        $('.noOriginAutoInput').removeClass('noGetValue');
        $(".ledSettingsOriginRadio").removeAttr("checked");
        $("#ledManualOriginRadio").prop("checked", true);
    });
    $('body').on('click', '.ledSettingsTab[data-target*=ledAutoOrigin]', function(){
        $('.noOriginAuto').addClass('hidden');
        $('.noOriginAutoInput').addClass('noGetValue');
        $(".ledSettingsOriginRadio").removeAttr("checked");
        $("#ledAutoOriginRadio").prop("checked", true);
    });
    
    //******************* END GESTIÓN DE TABS (MANUAL E IMPORTACIÓN)**********************//
    
    
    //************************* GESTIÓN DE INTERACCIONES SOBRE FORMULARIO DE FILTROS DE ORÍGENES **************************//
    
    /**
     * Cambio de origen
     */
    $('body').on('change', '#dropdownOrigin', function(){
        var originId = $('#dropdownOrigin :selected').val();
        $('.originsFilterList').empty();
        getOriginList(originId, '', '', '');

    });
    
    /**
     * Intro en cualquier input de filtrado
     */
    $('body').on('keypress', '.filterOrigin', function(){
        if(event.keyCode == 13){
            filterOrigin( true );
        }
    });
    
    /**
     * Click en cualquier select multiple
     */
    $('body').on('mousedown', 'select[multiple]', function(e){
        e.preventDefault();
        e.stopPropagation();
        var select = this;
        var scroll = select .scrollTop;
        e.target.selected = !e.target.selected;
        setTimeout(function(){select.scrollTop = scroll;}, 0);
        $(select ).focus();
        return false;
    });
    
    $('body').on('mousemove', 'select[multiple] option', function(e){
        e.preventDefault();
        e.stopPropagation();
        return false;
    });
    
    /**
     * Click en "BUSCAR"
     */
    $('body').on('click', '#submitFilterOrigin', function(){				
        filterOrigin( true );  
    });
    
    /**
     * Click en "LIMPIAR"
     */
    $('body').on('click', '#resetFilterOrigin', function(){
        $('.filterOrigin').val('');
        $('select.filterOrigin option').prop('selected', false);
        filterOrigin( true );  
    });
    
    
    /**
     * Click en un item de la lista de RSS/WS
     * Añadimos la información de este item al formulario de la notificación
     */
    $('body').on('click', '.itemOrigin', function(){
        
        //removeDatasOriginInElement();
        var $this = $(this);
        var content = $this.data('origin-item');
        for (var key in content) {
            if (content.hasOwnProperty(key)) {
                $( '[data-tag="'+key+'"]' ).each(function(){
                    var $element = $(this);
                    
                    if( key == 'image' && content[key] != '' ){
                        $("#typeImage").val('urlImage');
                        $("#typeImage").change();
                        
                    }
                    
                    if($element.is('textarea')){
                        $element.text(content[key]);
                        $element.val(content[key]);
                    }else{
                        $element.val(content[key]);
                    }
                    $element.keyup();
                    $this.find('.radioItemOrigin').prop('checked', true);
                    window.isModify = true;
                });
            }
        }
    })
    
    //************************* END GESTIÓN DE INTERACCIONES SOBRE FORMULARIO DE FILTROS DE ORÍGENES **********************//
    
    
    
    
    //************************* BOTONERA DESPLEGABLE EN CADA NOTIFICACIÓN **********************//
    
    /**
     * Botón de desplegable de acciones sobre las notificaciones
     */
    $('body').on('click', '.magazine-item-actions-show', function(e){
        e.preventDefault();
        $this = $(this);
        $('.magazine-item-actions-list').addClass('notificationNoViewActions');
        $this.parent().find('.magazine-item-actions-list').removeClass('notificationNoViewActions');
        $('.notificationNoViewActions').addClass('hide');

        $this.parent().find('.magazine-item-actions-list').toggleClass('hide');
     });
     
     $('body').on('click', '.applyStatusFilter, .clearFilter, .falseSearchButton, .pagination a', function(e){
        e.preventDefault();
        var $this = $(this);
        
        if ( $this.data('page') != undefined ){
            $('#actualPage').val($this.data('page'));
        }
        handleFilter($this);
     });
     
     $('.applyDateFilter').on('change', function() {
         handleFilter($(this));
     });
    
    /**
     * Botón de eliminar Notificación
     */
    $('body').on('click', '.deleteNotification', function(e){
        e.preventDefault();
        $this		= $(this);
        action		= $this.data('action');
        postAction	= $this.data('post-action') ?  $this.data('post-action') : '';

        $( "#dialog-confirm-delete-notification" ).dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            show: {
                effect: 'fade',
                duration: 200
            },
            hide: {
                effect: 'fade',
                duration: 200
            },
            buttons: {
                "Confirmar": function() {
                    $( this ).dialog( "close" );
                    $('.notification-item-actions-list').addClass('hide');
                    removeNotification( action, postAction );
                },
                "Cancelar": function() {
                     $( this ).dialog( "close" );
                }
            }
        });
    });
    
    /**
     * Botón de enviar Notificación
     */
    $('body').on('click', '.sendNotification', function(e){
        e.preventDefault();
        $this		= $(this);
        action		= $this.data('action');
        //postAction	= $this.data('post-action') ?  $this.data('post-action') : '';

        $( "#dialog-confirm-send-notification" ).dialog({
            resizable: false,
            height: "auto",
            width: 400,
            modal: true,
            show: {
                effect: 'fade',
                duration: 200
            },
            hide: {
                effect: 'fade',
                duration: 200
            },
            buttons: {
                "Confirmar": function() {
                    $( this ).dialog( "close" );
                    $('.notification-item-actions-list').addClass('hide');
                    sendNotification( action );
                },
                "Cancelar": function() {
                     $( this ).dialog( "close" );
                }
            }
        });
    });
    
    //*********************** END BOTONERA DESPLEGABLE EN CADA NOTIFICACIÓN ********************//
    
     
    /**
     * Detecta cuando cualquier input es modificado
     */
    $('body').on('change', 'input', function(e){
         window.isModify = true;
    });
    
    $('body').on('keyup', '#searchFromText', function(event){
        if( $(this).val() === ''){
            $(this).data( 'id', null );
            handleFilter( $(this) );							
        }else{
            if(event.which == 13) {
                /*$firstElementFromSearch = $('#containerSearchFromText .searchFromTextItem:not(.hidden)').first();
                var name = $firstElementFromSearch.data('name');
                $(this).val(name);
                $firstElementFromSearch.click();*/
                $('.falseSearchButton').click();
            }
        }
    });
    
    /*$('body').on('keyup click', '#searchFromText', function(e){
        var $this = $(this);
        var textSearch = clearString($this.val().toLowerCase());
        if(textSearch.length >= 3){
            $this.siblings().find("li").addClass("hidden").removeClass('border-blue');
            if(textSearch.substr(0,1) == "'" || textSearch.charAt(textSearch.length-1) == "'"){
                $('#containerSearchFromText li[data-name*="'+textSearch+'"]').removeClass("hidden");
            }else{
                $("#containerSearchFromText li[data-name*='"+textSearch+"']").removeClass("hidden");
            }
            $('#containerSearchFromText li:not(.hidden)').last().addClass('border-blue');
            if(!$this.siblings().find("li.searchFromTextItem:not(.hidden)").length){
                $this.siblings().find("li.noResults").removeClass("hidden");
            }
            if(e.which == 13) {

            }
        }else{
            $this.siblings().find("li").addClass("hidden");
        }
    });*/
    
    /**
     * Guardamos al pasar de nivel en el menu inferior
     */
    $('body').on('click', '.walkthrough-save', function(e){
        var $this = $(this); 
        var isEnabled = $this.parents('li.enabled').length;
        if(isEnabled > 0){
            walkthroughSave($this, e);
        }else{
            showMsgAction( 'KO' , "Debe guardar para pasar al siguiente paso" );
        }
    });
     
     /**
      * Acción del botón guardar notificación
      */
     $('body').on('click', '#ledBuildAction', function(){
             $('#addNotification').submit();
     });
     
    $('body').on('submit', '#addNotification', function(e){
         
        e.preventDefault();
        var $this = $(this);
        
        if (window.isModify){
            
            var url = '/notifications/saveNotification';
        
            if ( $('#ledBuildAction').attr('data-notificationid') != 0 )
                var url = '/notifications/saveNotification/' + $('#ledBuildAction').attr('data-notificationid');

            var data = new FormData(this);

            var request = jQuery.ajax({
                url: url,
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                beforeSend: function() {
                    modalLoading.show();
                },
                success: function(dataResult) {

                    dataResult = jQuery.parseJSON(dataResult);

                    if (dataResult.result == 'KO'){
                        var  msgOk = "Completa los campos obligatorios";
                        showMsgAction('KO', msgOk);
                    }else{
                        var  msgOk = "Notificación guardada correctamente";
                        showMsgAction('OK', msgOk);
                        $('#ledBuildAction').attr('data-notificationid', dataResult.idRow );
                        window.isModify = false;

                        if($( '#walkthrough-bar' ).length > 0){
                            $( '#walkthrough-bar').load($('#walkthrough-bar').data( 'url' ) + ' .walkthrough-bar', function() {
                                var actualDataUrl = $('#walkthrough-bar').attr('data-url');
                                $('#walkthrough-bar').attr('data-url', actualDataUrl + dataResult.idRow );
                            });
                            $( '#led-mainmenu').load($('#walkthrough-bar').data( 'url' ) + ' .led-mainmenu',  function() {
                                $('#ledBuildAction').attr('data-notificationid', dataResult.idRow );
                            });
                        }
                        $('#idNotificationTrial').val(dataResult.idRow);
                    }
                    modalLoading.hide();

                },
                error: function() {
                    console.log('Error al cargar info');
                    modalLoading.hide();
                },
                done: function() {
                }
            });
            
        }else{
            var  msgOk = "No existen modificaciones";
            showMsgAction('KO', msgOk);
        }
     });
     
     /**
      * Controlamos que al movernos en el menú, la notificación se haya guardado
      */
     $('body').on( 'click', '#led-mainmenu a.link-menu', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var $this = $(this);
        var url   = $this.attr('href');

        if(window.isModify){
            $( "#dialog-confirm-change-view" ).dialog({
               resizable: false,
               height: "auto",
               width: 400,
               modal: true,
               show: {
                   effect: 'fade',
                   duration: 200
               },
               hide: {
                   effect: 'fade',
                   duration: 200
               },
               buttons: {
                   "Confirmar": function() {
                       modalAux = new ledModalLoading('html');
                       modalAux.setAnimationTime(500);
                       modalAux.show();
                       location.href = url;
                   },
                   "Cancelar": function() {
                        $( this ).dialog( "close" );
                   }
               }
           });
        }else{

            modalAux = new ledModalLoading('html');
            modalAux.setAnimationTime(500);
            modalAux.show();
            location.href = url;
        }
    });
    
    
    
    //******************************************************************** FUNCIONES ************************************************************//
     
     
     /**
      * Manejador del filtrado de notificaciones
      * @returns {undefined}
      */
     function handleFilter( $ele ) {
         
        modalAux = new ledModalLoading('html');
        modalAux.setAnimationTime(500);
        modalAux.show();
        
        var url                 = APP.base_url + APP.uri + '/';
        var paramsStatus        = '';
        var paramsDate			= '';
        var paramsText			= '';
        var paramsPage			= '';
        var params				= '';
        
        // Limpiamos filtro por fecha
        if( $ele.hasClass( 'clearDateFilter' ) ){				
            $( '.applyDateFilter' ).val( '' );
            $('#actualPage').val('1'); //Limpiamos también el número de página
        }
        
        // Limpiamos filtro por texto
        if( $ele.hasClass( 'clearTxtFilter' ) &&  $('#searchFromText').length ){
            $('#searchFromText').data( 'id', null );
            $('#searchFromText' ).val("");	
            $('#actualPage').val('1'); //Limpiamos también el número de página
        }
            
        // Limpiamos filtro por estado
        if( $ele.hasClass( 'clearStatusFilter' ) ){
            $( '.applyStatusFilter' ).removeClass( 'active' );
            $( '.applyStatusFilter' ).data( 'active', 0 );

            if( $('#searchFromText').length ){
                $('#searchFromText').data( 'id', null );
                $( '#searchFromText' ).val("");
            }
            $('#actualPage').val('1'); //Limpiamos también el número de página
        }
        
        // Filtrado por 'estado'
        if( $ele.hasClass( 'applyStatusFilter' ) ){
            $( '.applyStatusFilter' ).data( 'active', 0 );
            $( '.applyStatusFilter' ).attr( 'data-active', 0 );
            $( '.applyStatusFilter' ).removeClass( 'active' );
            $ele.data( 'active', 1 );
            $ele.attr('data-active',1);
            $ele.addClass( 'active' );
            $('#actualPage').val('1'); //Limpiamos también el número de página
        }			

        $( '.applyStatusFilter' ).each(function () {
            paramsStatus += ( $( this ).data( 'active' ) == 1 ) ? '/true' : '/null';
        });
        
        
        // Filtrado por 'fecha'
        paramsDate = ( $( '#dateFrom' ).val() != '' ) ? '/' + defaultDateFormat($( '#dateFrom' ).val()) : '/0';
        paramsDate += ( $( '#dateTo' ).val() != '' ) ? '/' + defaultDateFormat($( '#dateTo' ).val()): '/0';	
        
        // Filtrado por 'texto'
        paramsText = ($('#searchFromText').val() != '') ? '/' + $('#searchFromText').val() : '/null';
        
        //Filtrado por página
        paramsPage = "/" + $('#actualPage').val();
        
        
        //var params	= 'index' + paramsStatus + '/';
        
        var params	= 'index' + paramsStatus + paramsDate + paramsText + paramsPage + '/';
        
        console.log(url + params);
        
        // Mostramos/ocultamos botón 'Eliminar filtro'
        if( $( '.applyDateFilter' ).val() == '' && $( '#searchFromText' ).val() == '' ){
            $('#btnDeleteFilters').addClass('hide');
        }else{
            $('#btnDeleteFilters').removeClass('hide');
        }
        
        $( '#list_' + APP.uri ).load( url + params + ' #list_' + APP.uri, function(){
				modalAux.hide();
			});
         
     }
     
    /**
     * Listado de Origenes
     * @param {type} id
     * @param {type} params
     * @param {type} isSubmit
     * @param {type} autoButton
     * @returns {undefined}
     */ 
    function getOriginList ( id, params, isSubmit , autoButton ){
        
        params = params || "";
        isSubmit = isSubmit || "";
        autoButton = autoButton || "";
        
        var $request = null;
        modalAux = new ledModalLoading('html');
        modalAux.setAnimationTime(500);
        modalAux.show();

        var ledAutoOriginActive = $('.ledSettingsTab[data-target*=ledAutoOrigin]').hasClass('active');

        var dataUrl = $('.originsFilterList').data('url');
        var url = dataUrl + '/' + id;
        var classLoad = ".originsFilterList";
        
        if(params){
            //url += '/' + btoa(encodeURI(params.replace(new RegExp(' ', 'g'), '%20')));
            url += '/' + btoa(encodeURI(params));
            classLoad = '.originsList';
        }

        if ($request != null){ 
            $request.abort();
            $request = null;
        }
        $request = $.ajax({
            url: url,
            dataType: "HTML",
            success: function (html) {
                if (html) {
                    $(classLoad).html(html);
                    //ledFilters.datePicker();
                    $('.ledOriginLoading').addClass('hidden');

                    if( isSubmit ){
                        if($('#ledSettingsOrigin').length > 0){
                            $('#containerComponents').animate({
                                scrollTop: $('#ledSettingsOrigin').offset().top + 'px'
                            }, 'fast');
                        }
                    }
                    var hasLedHalfContainer = $('.ledComponent.activeComponent').parents('.ledHalfContainer').length;
                    var limitListOrigin = hasLedHalfContainer > 0 ? Math.floor($('#originsList .itemOrigin').length/2) : $('#originsList .itemOrigin').length;
                    var autoOriginValue = $('.autoOrigin').val() > limitListOrigin ? limitListOrigin : $('.autoOrigin').val(); 
                    //$('.autoOrigin').val(autoOriginValue).attr('max', limitListOrigin);
                    $('#ledSettingsOrigin button[data-target*=ledAutoOrigin]').removeClass('hidden');
                    $('#ledSettingsOrigin .ledSettingsOriginRadio').removeClass('hidden');

                    if($('#originsList .itemOrigin').length > 0){
                        if(ledAutoOriginActive || autoButton){
                            $('#ledSettingsOrigin button[data-target*=ledManualOrigin]').removeClass('active');
                            $('#ledSettingsOrigin button[data-target*=ledAutoOrigin]').addClass('active');
                            $('#ledManualOrigin').css('display', 'none');
                            $('#ledAutoOrigin').css('display', 'block');
                            $('.noOriginAuto').css('display', 'none');
                            return false;
                        }
                    }else{
                        $('#ledSettingsOrigin button[data-target*=ledAutoOrigin]').addClass('hidden');
                        $('#ledSettingsOrigin .ledSettingsOriginRadio').addClass('hidden');
                        $('#ledSettingsOrigin button[data-target*=ledManualOrigin]').addClass('active');
                        $('#ledSettingsOrigin button[data-target*=ledAutoOrigin]').removeClass('active');
                        $('#ledManualOrigin').css('display', 'block');
                        $('#ledAutoOrigin').css('display', 'none');
                        $('.noOriginAuto').css('display', 'block');
                    }
                }
                modalAux.hide();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
                modalAux.hide();
            }
        });
    }
    
   /**
    * Recogemos los valores del filtro de origenes y hacemos la busqueda
    * @param {type} isSubmit
    * @param {type} autoButton
    * @returns {undefined}
    */
   function filterOrigin ( isSubmit , autoButton ){
       autoButton = autoButton || '';  
       var params="";
       var originId = $('#dropdownOrigin :selected').val();
       $('.noOriginAuto .noGetValue').val('');
       $('.filterOrigin').each(function(){
           var $element = $(this);
           if($element.hasClass('date') && $('#defaultDate').datepicker('getDate')){
               params += $('#defaultDate').datepicker('getDate') ? $element.data('param') + ':' + $.format.date($('#defaultDate').datepicker('getDate'), $element.data('format')) + ';' : '';
           }else if($element.is('select')){
               params += $element.val() ? $element.data('param') + ':' + $element.val() + ';' : '';
           }else{
               params += $element.val() ? $element.data('param') + ':' + encodeURIComponent($element.val()).replace(/%2F/g,'2FSLASH') + ';' : '';
           }
       });
       params = params.substring(0,params.length-1);
       $('#ledManualOrigin').empty();					
       getOriginList(originId, params, isSubmit , autoButton);
   }
   
   /**
    * Guardar desde menú inferior (PASOS)
    * @param {type} $this
    * @param {type} e
    * @returns {Boolean}
    */
   function walkthroughSave($this, e){
       
       e.stopPropagation();
       e.preventDefault();
       var message = '';
       var classHidden = '';

       if ($('.required-save').length > 0) {
           $('.required-save').each(function (e) {
               var value = $(this).val();
               if (value === '') {
                   message += $(this).data('required-save-message');
               }
           });
       } 

       if(message !== ''){ 
           classHidden = 'hidden';
       }

       if($('.navbar .save-btn').length > 0){
            if(window.isModify === true){

                var titleDialog = '<span class="ui-icon ui-icon-alert"></span><b>'+ message+'</b>'+$('.secure-confirmation-walkthrough').data('title');
                $('.secure-confirmation-walkthrough').empty();
                $('.secure-confirmation-walkthrough').append(titleDialog);

                $( "#dialog-walkthrough" ).dialog({
                    resizable: false,
                    height: "auto",
                    width: 400,
                    modal: true,
                    show: {
                        effect: 'fade',
                        duration: 200
                    },
                    hide: {
                        effect: 'fade',
                        duration: 200
                    },
                    buttons: [
                        {
                            text: "Guardar e ir al paso seleccionado",
                            "class": classHidden,
                            click: function() {
                                $('.navbar .save-btn').click();
                                location.href = $this.attr('href');
                            }
                        },
                        {
                            text: "Ir al paso seleccionado sin guardar",
                            click: function() {
                                location.href = $this.attr('href');
                            }
                        },
                        {
                            text: "Seguir en el paso actual",
                            click: function() {
                                $( this ).dialog( "close" );
                                $('.secure-confirmation-walkthrough').empty();
                                return false;
                            }
                        }
                    ]
                });
               return false;
            }else{
                modalAux = new ledModalLoading('html');
                modalAux.setAnimationTime(500);
                modalAux.show();
                location.href = $this.attr('href');
            }     
               
        }else{
            modalAux = new ledModalLoading('html');
            modalAux.setAnimationTime(500);
            modalAux.show();
            location.href = $this.attr('href');
        }
    };
    
    function removeNotification ( urlAction, urlPostAction ) { 
        
        $request = $.ajax({
            url: urlAction,
            dataType: "HTML",
            success: function (result) {
                response = JSON.parse(result);
                showMsgAction( 'OK' , "Notificación borrada correctamente." );

                if( urlPostAction ){
                    setTimeout(function(){ 
                        modalAux = new ledModalLoading('html');
                        modalAux.setAnimationTime(500);
                        modalAux.show();
                        window.location.replace(urlPostAction);
                    }, 1500);
                }else{
                    handleFilter( $(this) );
                }
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });        
    };
    
    function sendNotification ( urlAction ) { 
        
        $request = $.ajax({
            url: urlAction,
            dataType: "HTML",
            success: function (result) {
                result = JSON.parse(result);
                if (result){
                    
                    var statusOK = 'KO';
                    if(result.ok != false){
                        statusOK = 'OK';
                    }

                    showMsgAction( statusOK , result.message );
                    
                    //Ocultar botón de envíar
                    $('.sendNotification').hide();
                }
                
            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
            }
        });        
    };
   
     
    /**
     * Limpiador de strings
     * @param {type} str
     * @returns {String}
     */
    function clearString(str){
        var from = "ÃÀÁÄÂÈÉËÊÌÍÏÎÒÓÖÔÙÚÜÛãàáäâèéëêìíïîòóöôùúüûÑñÇç", 
            to   = "AAAAAEEEEIIIIOOOOUUUUaaaaaeeeeiiiioooouuuunncc",
            mapping = {};

        for(var i = 0, j = from.length; i < j; i++ )
            mapping[ from.charAt( i ) ] = to.charAt( i );

        var ret = [];
        for( var i = 0, j = str.length; i < j; i++ ) {
            var c = str.charAt( i );
            if( mapping.hasOwnProperty( str.charAt( i ) ) )
                ret.push( mapping[ c ] );
            else
                ret.push( c );
        }      
        return ret.join( '' );
    }
    
    /**
     * Formateo de fecha
     * @param {type} string
     * @returns {unresolved}
     */
    function defaultDateFormat(string) {
        var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
        return dateSearchFormat = string.replace(pattern,'$3-$2-$1');
    };
});

function showMsgAction( result, msg ) {
    
    var timeOutMsg;
			
    $ledInfoActions = $('#ledInfoActions');
    $ledInfoActions.html( msg );
    $ledInfoActions.removeClass();

    switch ( result ){
        case 'KO':
            $ledInfoActions.addClass('alert-danger');
            break;

        case 'OK':
            $ledInfoActions.addClass('alert-success');
            break;

        default:
            break;
    }

    $ledInfoActions.fadeIn('200');
    clearTimeout(timeOutMsg);
    timeOutMsg = window.setTimeout(function(){
        $ledInfoActions.fadeOut('200', function(){
            $ledInfoActions.removeAttr('class');
            $ledInfoActions.html('');
            $ledInfoActions.css('display', 'none');
        });
    }, 4000);
}