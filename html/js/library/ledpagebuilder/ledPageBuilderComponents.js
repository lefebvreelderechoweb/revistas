(function ($) {

    $.fn.ledPageBuilderComponents = function (params) {
        var options		              = $.extend({}, $.fn.ledPageBuilderComponents.defaults, params);
        var ledPageBuilderComponents  = this;
        var ledPageBuilderFields      = options.ledPageBuilderFields;
        var ledHelper                 = options.ledHelper;

        ledPageBuilderComponents.init = function () {
            ledPageBuilderComponents.draggableInit();
            ledPageBuilderComponents.droppableInit();
            ledPageBuilderComponents.sortableInit(); 
            ledPageBuilderComponents.controlsComponentsEvents();
            ledPageBuilderComponents.formComponentEvents();
            window.isModify = false;
            return this;
        };

        /**
         * jQuery convierte los elementos
         * que devuelve html mediante metodos
         * como append, html, preprend, etc.
         *
         * Tras utilizas estos métodos tenemos
         * que volver a poner la estructura
         * de un XHTML válido.
         *
         * @param html
         * @returns {*}
         */
        ledPageBuilderComponents.htmlToXhtml = function (html) {
            var output;

            if (!html) {
                ledCore.log('Html has been not received');
                return false;
            }

            output = html
                .replace(/<(.*)tbody>/g, '')
                .replace(/<img(.*?)>/g, '<img$1 />')
                .replace(/ \/>/g, '/>');


            return output;
        };

        /**
         * Inicializamos el plugin draggable
         * de la librería jQuery UI.
         */
        ledPageBuilderComponents.draggableInit = function () {
            $('.' + options.draggable).draggable({
                helper: 'clone',
                appendTo: 'body',
                revert: "invalid",
                scroll: false,
                start: function (e, ui) {
                    ui.helper.width($(this).width());

                    $(this).draggable("option", "cursorAt", {
                        left: Math.floor(this.clientWidth / 2),
                        top: Math.floor(this.clientHeight / 2)
                    });

                    $(this).css('visibility', 'hidden');
                },
                stop: function (e, ui) {
                    $(this).css('visibility', '');
                }
            })
        };

        /**
         * Permitimos que los conmponentes añadidos se pueda ordenar
         * independientemente de en que columna se encuentre
         */
        ledPageBuilderComponents.sortableInit = function () {
            $('.ledColumnContent').sortable({
                items: '.ledComponent',
                opacity: 0.7,
                //scroll: true,
                connectWith: ['.ledColumnContent'],
                cursor: 'move',
                start: function (event, ui) {
                    ui.placeholder.height(ui.item.height());
                },
                change: function (event, ui) {
                    //Insertar código si fuera o fuese necesario
                },
                update: function (event, ui) {
                    var $contentSortable = $(this);
                    ledPageBuilderComponents.statusComponent($contentSortable);
                }
            });

        };
        
        /**
         * Desactivamos la posibilidad de ordenar los compontes
         */
        ledPageBuilderComponents.sortableDisable = function(){
            $('.ledColumnContent').sortable('disable');
        }
        
        /**
         * Activamos la posibilidad de ordenar los compontes
         */
        ledPageBuilderComponents.sortableEnable = function(){
            $('.ledColumnContent').sortable('enable');
        }


        /**
         * Inicializamos el plugin Droppable que
         * aceptará los elementos droppable dentro
         * del mismo.
         */
        ledPageBuilderComponents.droppableInit = function () {
            $(options.droppable).droppable({

                drop: function (event, ui) {
					
                    var $zone = $(this);
                    var $component = $(ui.draggable);

                    var history = {
                        idComponent: $component.data('id'),
                        version: $component.data('ver')
                    };

					/**
					 * Realizamos scroll hacia arriba para mostrar todos los componentes
					 */
					$('#containerComponents').animate({
						scrollTop: '0px'
					}, 'fast');

                    /**
                     * Aceptamos unicamente los componentes
                     * que son compatibles con la zona
                     */
                    if ($component.hasClass('ledZoneType-' + $zone.data('zone-id'))) {

                        /**
                         * Escondemos la vista previa.
                         * No la podemos borrar porque
                         * si se elimina el componente de
                         * la zona nos quedaríamos sin
                         * vista previa
                         */
                        $zone.removeClass('noContent');


                        var html = $component.data('html');
                        var $html = $(html);
                        $html
                        //.attr('id', 'ledComponent-id-' + $component.data('id'))
                            .attr('data-fields', JSON.stringify($component.data('fields')));


                        /**
                         * Inicialmente solo dejamos agregar
                         * un componente por zona
                         */
                        $zone.append($html);
                        ledPageBuilderComponents.removeZoneStatus();
                        
                        /**
                         * Hacemos el componente nuevo sortable
                         */
                        ledPageBuilderComponents.sortableInit();


                        /**
                         * Agregamos una clase a la zona
                         * para indicar que tiene un elemento
                         * dentro y aplicar los estilos
                         * correspondientes
                         */
                        $zone.addClass('hasElement');

                        /**
                         * Agregamos la misma clase que arriba
                         * al contenedor de la clase doble donde
                         * la estructura de zonas cambia.
                         */
                        $zone
                            .closest('.lpb-content')
                            .addClass('hasElement');
                    }else{
                        ledPageBuilderComponents.removeZoneStatus();
                    }
                },


                /**
                 * Marcamos la zona al arrastrar un
                 * componente por encima de la zona.
                 * @param event
                 * @param ui
                 */
                over: function (event, ui) {

                    var $zone = $(this);
                    var $component = $(ui.draggable);


                    /**
                     * Evitamos a que la zona se marque cuando
                     * se se cambia el order de la misma y se
                     * arrastre por encima de otra zona
                     */
                    if ($component.hasClass(options.draggable)) {

                        /**
                         * Definimos el state por defecto y
                         * lo remplazamos más abajo para mostrar
                         * si un componente está siendo aceptado
                         * o no en una zona.
                         * @type {string}
                         */
                        var stateClass = options.ledDropForbidden;

                        if ($component.hasClass('ledZoneType-' + $zone.data('zone-id'))) {
                            stateClass = options.ledDropPermitted;
                        }
                        
                        $zone.closest('.lpb-content').addClass(stateClass);
                    }
                },

                /**
                 * Eliminamos la stateClass que marca la zona
                 * al arrastrar el elemento fuera de la misma
                 * @param event
                 * @param ui
                 */
                out: function (event, ui) {
                    ledPageBuilderComponents.removeZoneStatus();
                    ledPageBuilderComponents.sortableInit(); 
                },
                tolerance: 'pointer'
            });
        };

        /**
         * Elminamos el estado de la zona
         */
        ledPageBuilderComponents.removeZoneStatus = function(){
            $(options.droppable).closest('.lpb-content').removeClass(options.ledDropPermitted);
            $(options.droppable).closest('.lpb-content').removeClass(options.ledDropForbidden);
        }
        
        /**
         * Dejamos la columna con el estado de que no
         * contiene ningun componente
         * 
         * @param object $column
         */
        ledPageBuilderComponents.columnNoHasComponents = function($column){
            $column.find('.lpb-content').removeClass('hasElement');
            $column.find(options.droppable).removeClass('hasElement');
            $column.find(options.droppable).addClass('noContent');
            window.isModify = true;
        }
        
        /**
         * Dejamos la columna con el estado de que
         * contiene algún componente
         * 
         * @param object $column
         */
        ledPageBuilderComponents.columnHasComponents = function($column){
            $column.find('.lpb-content').addClass('hasElement');
            $column.find(options.droppable).addClass('hasElement');
            $column.find(options.droppable).removeClass('noContent');
            window.isModify = true;
        }
        
        
        /**
         * Mostramos el estado de la columna segun si tiene contenido o no
         * 
         * @param string $element Cualquier elemento hijo de .column 
         */
        ledPageBuilderComponents.statusComponent = function($element){
            var $column = $element.closest('.column');
            var countComponents = $column.find(options.component).length;
            if (countComponents < 1) {
                ledPageBuilderComponents.columnNoHasComponents($column);
            } else {
                ledPageBuilderComponents.columnHasComponents($column);
            }
        } 
        
        /**
         * Mostramos los botones de edicion o eliminado del componente
         * 
         * @param object $ledComponent
         */
        ledPageBuilderComponents.addControlsComponents = function ($ledComponent) {
            var html          = '';
            var editButton    = '';
            var dataFields    = $ledComponent.data('fields');
            //Comprobamos que tiene campos a modificar
            if (dataFields !== undefined) {
                editButton = '<li><a href="#" class="ledEditComponent">' +
                             '<i class="glyphicon glyphicon-cog"></i> </a>' +
                             '</li>';
            }

            var html = '' +
                '<div class="ledControls">' +
                '<ul>' +
                editButton +
                '<li><a href="#" class="ledDeleteComponent">' +
                '    <i class="icon icon-bin"></i> </a>' +
                '</li>' +
                '</ul>' +
                '</div>';

            $ledComponent.append(html);
        };
        
        /**
         * Eliminamos los botones de edicion del componente
         * 
         * @param object $ledComponent 
         */
        ledPageBuilderComponents.removeControlsComponents = function($ledComponent){
            $ledComponent.find('.ledControls').remove();
        }
        
        /**
         * Eliminamos un componente
         * 
         * @param object $ledComponent
         */
        ledPageBuilderComponents.removeComponent = function($ledComponent){
            var $contentComponent = $ledComponent.closest(options.droppable); 
            $ledComponent.remove();
            ledPageBuilderComponents.statusComponent($contentComponent);
            ledPageBuilderComponents.disableComponents();
        }
        
        /**
         * Dejamos como activo el componente a editar
         * 
         * @param object $ledComponent
         */
        ledPageBuilderComponents.activateComponent = function($ledComponent){
            //Nos aseguramos de que no haya ningun componente activo
            ledPageBuilderComponents.disableComponents();
            //Activamos el componente
            $ledComponent.addClass('activeComponent');
            $(options.contentOverlay).append('<div class="overlayComponent"></div>');
        }
        
        /**
         * Inactivamos el estado de los componentes
         */
        ledPageBuilderComponents.disableComponents = function(){
            $(options.component).removeClass('activeComponent');
            ledPageBuilderFields.disableFields();
            $('.overlayComponent').remove();
            ledPageBuilderComponents.sortableEnable();
        }
        
        /**
         * Mostramos el listado de componentes
         */
        ledPageBuilderComponents.showListComponents = function(){
            //Dejamos el estado inactivo de todos los componentes
            ledPageBuilderComponents.disableComponents();
            //Limpiamos y ocultamos el formulario de configuracion de los compoentes
            $('#componentSettings').addClass('hidden');
            $('#formComponentSettings').empty();
            //Mostramos la lista de componentes
            $('#containerComponents').removeClass('hidden');
        }
        
        /**
         * Mostramos los campos del componente
         */
        ledPageBuilderComponents.showFormComponent = function(){
            //Limpiamos y ocultamos el formulario de configuracion de los compoentes
            $('#componentSettings').removeClass('hidden');
            //Mostramos la lista de componentes
            $('#containerComponents').addClass('hidden');
        }
        
        /**
         * Eventos para añadir los botones para editar o eliminar
         * el componente
         */
        ledPageBuilderComponents.controlsComponentsEvents = function () {
            //Añadimos los botones de edicion
            $(document).on('mouseenter', options.builder + ' ' + options.component, function(){
                var $ledComponent = $(this);  
                ledPageBuilderComponents.addControlsComponents($ledComponent);
            })
            //Eliminamos los botones de edicion  
            .on('mouseleave', options.builder + ' ' + options.component, function(){
                var $ledComponent = $(this);
                ledPageBuilderComponents.removeControlsComponents($ledComponent);
            })
            //Eliminamos el componente
            .on('click', '.ledDeleteComponent', function(e){
                e.preventDefault();
                e.stopPropagation();
                
                var $this         = $(this);
                var $ledComponent = $this.closest(options.component);
                ledPageBuilderComponents.removeComponent($ledComponent);
                ledPageBuilderComponents.showListComponents();
            })
            //Mostramos la edicion del componente
            .on('click', '.ledEditComponent', function (e) {
                e.preventDefault();
                e.stopPropagation();
                
                var $this = $(this);
                var $component = $this.closest(options.component);
                var fieldsJSON = JSON.stringify($component.data('fields'));
                if (ledHelper.isValidJson(fieldsJSON)) {
                    //Generamos los input fields
                    var fields = ledPageBuilderFields.buildFields(fieldsJSON);
                    // Insertamos el contenido que nos devuelve
                    $('#formComponentSettings').html(fields);
                    ledPageBuilderFields.addAllEditors();
                    // Activamos el componente
                    ledPageBuilderComponents.activateComponent($component);
                    ledPageBuilderComponents.showFormComponent();
                    ledPageBuilderComponents.sortableDisable();
                } else {
                    contenido.log('Ha ocurrido un error: ' + params);
                }
            });
        };
        
        /**
         * Eventos relacionado con el formulario de edicion
         * de los componentes
         */
        ledPageBuilderComponents.formComponentEvents = function(){
            $(document)
            //Modificacion de los textos 
            .on('keyup', '.fieldFormComponent', function () {
                var $input = $(this);
                var $edit = null;
                
                var name     = $input.attr('name');
                var newValue = '';

                if ($input.data('target')) {
                    $edit = $('.activeComponent').find('.ledLive' + $input.data('group'));
                    if ($input.data('attr')) {
                        var value = !$input.val() && $input.attr('data-empty-value') ?  $input.attr('data-empty-value') : $input.val();
                        $edit.find($input.data('target')).attr($input.data('attr'), value);
                        newValue = value;
                    } else {
                        if($input.is('textarea')){
                            var $htmlInput = $input.val();
                            $htmlInput = $htmlInput.replace(new RegExp("\n","g"), "<br>");
                            $edit.find($input.data('target')).html($htmlInput.trim());
                            newValue = $htmlInput.trim();
                        }else{
                            $edit.find($input.data('target')).text($input.val());
                            newValue = $input.val();
                        }
                    }
                } else {
                    $edit = $('.activeComponent').find('.ledLive' + $input.data('live'));
                    var $htmlInput = $input.val(); 
                    if($input.is('textarea')){
                        $htmlInput = $htmlInput.replace(new RegExp("\n","g"), "<br>");
                    }
                    $edit.html($htmlInput);
                    newValue = $htmlInput;
                }
                
                //Actualizamos data-fields del componente activo
                ledPageBuilderFields.changeInputFieldsValue(name, newValue);
            })
            //Damos un estado al campo a editar al tener el focus en el input
            .on('focus', '.fieldFormComponent', function(){
                var $activeElement = $('.activeComponent').find('.ledLive' + ledHelper.ucFirst($(this).data('group')));
                //Nos aseguramos que no haya campos activos
                ledPageBuilderFields.disableFields();
                //Activamos el campo seleccionado
                $activeElement.addClass('activeField');
            })
            //Mostramos el listado de componentes
            .on('click', '.overlayComponent, .closeFormSettings', function(e){
                e.stopPropagation();
                e.preventDefault();
        
                ledPageBuilderComponents.showListComponents();
            });
        };
        
        return ledPageBuilderComponents.init();
    };
    
    $.fn.ledPageBuilderComponents.defaults = {

        /**
         * Selectores
         */
        draggable:			'ledDraggable',				/** draggable selector **/
        droppable:			'.ledColumnContent',	    /** droppable selector **/
        columnContent:	    '.ledColumnContent',	    /** column content class **/
        temporal:			'.ledColumnContent:after',	/** temporal <td> if zone container is a <tr> **/
        zone:				'.ledZone',					/** dropper zone container **/
        builder:			'ledBuilder',				/** builder container **/
        cContainer:			'#ledComponentsContainer',
        body:				'#ledBody',					/** body layout hidden <textarea> **/
        component:			'.ledComponent',            /** component class **/ 
        build:				'#ledBuildAction',			/** build button selector **/
        contentOverlay:     '#nl-edit',                 /** content overlay div**/
        
        /**
         * Nombres de Clases
         */
        ledDropPermitted:	'ledDropPermitted',
        ledDropForbidden:	'ledDropForbidden',
    };


})(jQuery);