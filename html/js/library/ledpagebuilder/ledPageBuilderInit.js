(function ($) {

    $.fn.ledPageBuilderInit = function () {
        
        var ledPageBuilderInit = this;
        var ledPageBuilderId   = '#myLedPageBuilder'; 
        var sidebarId          = '#sidebar'; 
        var $ledPageBuilder    = '';
        var $fields            = '';
        var $helper            = '';
        var $components        = ''; 
        
        /**
         * Inicializamos la clase.
         * 
         * La utilizaremos para inicializar lo necesario
         * para el page builder y para los eventos necesarios
         * para el correcto funcianamiento de todos sus elementos
         * incluso los anadidos desde su parametrizacion
         */
        ledPageBuilderInit.init = function () {
            ledPageBuilderInit.initPageBuilder();
            ledPageBuilderInit.showContentOrSidebar();
        };
        
        /**
         * Inicializamos el builder
         */
        ledPageBuilderInit.initPageBuilder = function(){
            //Contenido especifico para el builder
            var elementsBuilderMainControlsLeft = ['<button id="show-sidebar" class="btn btn-sm btn-primary">Ver sidebar</button>'];
            var elementsSidebarMainControlsLeft = ['<button id="show-content" class="btn btn-primary">Ver contenido</button>'];
            
            var elementsMainControlsRight       = [
                                                   '<button id="show-preview" class="btn blue-btn">Previsualizar</button>',
                                                   '<button id="show-sidebar" class="btn blue-btn">Gen. PDF</button>',
                                                   '<button id="show-sidebar" class="btn blue-btn">Desc. PDF</button>',
                                                   '<button id="show-sidebar" class="btn blue-btn">Ver HTML</button>'
                                                  ];
            //Funciones comunes
            $helper     = $(window).ledHelper();
            
            //Funcionalidad para los campos
            $fields     = $(window).ledPageBuilderFields({
                ledHelper : $helper
            });
            
            //Funcionalidad para los componentes
            $components = $(ledPageBuilderId).ledPageBuilderComponents({
                ajax_url             : window.location.origin,
                builder              : ledPageBuilderId,
                ledPageBuilderFields : $fields,
                ledHelper            : $helper 
            });
            
            //Builder para filas y columnas
            $ledPageBuilder = $(ledPageBuilderId).ledPageBuilder({
                new_row_layouts            : [[12]],
                valid_col_sizes            : [3, 4, 5, 6, 7, 8, 9, 12],
                plugin_components          : $components,
                helper                     : $helper,
                version                    : 4,
                elements_maincontrol_left  : elementsBuilderMainControlsLeft,
                elements_maincontrol_right : elementsMainControlsRight
            });
            
            //Sidebar
            $(sidebarId).ledPageBuilder({
                new_row_layouts            : [[12]],
                valid_col_sizes            : [12],
                plugin_components          : $components,
                zone_component             : 'sidebar',
                helper                     : $helper,
                version                    : 4,
                auto_rows                  : false,
                unique_size                : 3,
                elements_maincontrol_left  : elementsSidebarMainControlsLeft,
                elements_maincontrol_right : elementsMainControlsRight
            });
        }
        
        /**
         * Evento para los botones que muestran 
         * el sidebar o el contenido
         */
        ledPageBuilderInit.showContentOrSidebar = function(){
            $(document)
                .on('click', '#show-content', function(){
                    $('#nl-edit-sidebar').addClass('hidden');    
                    $('#nl-edit').removeClass('hidden');
                    $('.widget-list').load(window.location.origin + '/component/list/content', function(){
                        $components.draggableInit();
                    });
                    
                })
                .on('click', '#show-sidebar', function(){
                    $('#nl-edit').addClass('hidden');    
                    $('#nl-edit-sidebar').removeClass('hidden');    
                    $('.widget-list').load(window.location.origin + '/component/list/sidebar', function(){
                        $components.draggableInit();
                    });
                })
            
        }
        
        return ledPageBuilderInit.init();
    };
     
    $(window).ledPageBuilderInit();
        
})(jQuery);