/**
 * Plugin page builder bootstrap
 * 
 * @options
 *  'new_row_layouts'            : [] Filas por defecto, Ejemplo: [3,3,3,3]
 *  'row_classes'                : [{label: 'Clase de ejemplo', cssClass: 'example-class'}]
 *  'col_classes'                : [{label: 'Clase de ejemplo', cssClass: 'example-class'}]
 *  'col_tools'                  : [] 
 *  'row_tools'                  : []
 *  'custom_filter'              : ''
 *  'content_types'              : []  Posibilidad de utilizar editor de texto : Por defecto 'noEditor' 
 *  'valid_col_sizes'            : []  Tamanos de columnas, Ejemplo: 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
 *  'source_textarea'            : ''
 *  'version'                    : 3 Superior a 3 utiliza flex, anadir en la cabecera bootstrap 4
 *  'zone_component'             : 'content' Filtra el tipo de contenido al que puede anadir los componentes
 *  'plugin_components'          : false Pasamos el plugin de componentes para utilizar determinadas funciones
 *  'auto_rows'                  : true Damos la posibilidad de utilizar la funcion de columnas con solo 'col'
 *  'unique_size'                : 0 Damos un tamano unico como por ejemplo cuando es para un sidebar
 *  'elements_maincontrol_left'  : [] Elementos a anadir en la zona izquierda en el menu principal del builder
 *  'elements_maincontrol_right' : [] Elementos a anadir en la zona derecha en el menu principal del builder
 *  'limit_childs_row'           : 3 Limitamos las filas anidadas
 *  'limit_add_columns'          : 10 Limitamos el numero de columnas dentro de una fila
 *  
 * Ejemplo para inicializar el plugin:
 *  $('#myLedPageBuilder').ledPageBuilder({
 *      new_row_layouts: [[12], [6,6], [3,9], [9,3], [4,4,4], [3,3,3,3]],
 *      valid_col_sizes: [3, 4, 6, 7, 8, 9, 12]
 *  }); 
 */

(function ($) {

    $.fn.ledPageBuilder = function (params) {
        var options = $.extend({}, $.fn.ledPageBuilder.defaults, params);
        var builder = this;
        
        //Control de version de bootstrap
        var colMdByVersion       = 'md-';
		var laterVersionClass    = '';
        var dialogId             = '#lpb-dialog';

        // Elementos
        var canvas,
            mainControls,
            wrapper,
            addRowGroup,
            htmlTextArea
        ;
        
        //Controla las clases para los tamanos de las columnas
        var colClasses       = [];
        var curColClassIndex = 0;
        var MAX_COL_SIZE     = 12;
        
        //Variables para hacer las columnas automaticas con versiones superiores a la 3
        var regex        = /(col-[a-z-1-9]*)/g;
        var dataColSizes = 'data-col-sizes';
        var defaultCols  = 'col-12 col-sm-12 col-xs-12';

        /**
         * Inicializamos el plugin
         */
        builder.initLedPageBuilder = function () {
            builder.initBuilderSettings();
            builder.setup();
            builder.init();
            builder.addDialogHTML();
            return this;
        };
        
        /**
         * Inicializamos las variables
         */
        builder.initBuilderSettings = function (){
            if (options.version > 3) {
                colMdByVersion = 'lg-';
                laterVersionClass = 'later-version';
            }
            
            //Tener en cuenta si queremos utilizar para desktop lg-
            //colClasses = ['col-lg-', 'col-sm-', 'col-' + colMdByVersion];
            colClasses = ['col-' + colMdByVersion, 'col-sm-', 'col-xs-'];
            
            // Copia el html de un textarea
            if (options.source_textarea) {
                builder.html($(options.source_textarea).val());
            }

            // Anade contenido si no es bootstrap
            if (builder.children().length && !builder.find('div.row').length) {
                var children = builder.children();
                var newRow = $('<div class="row"><div class="col-lg-12"/></div>').appendTo(baseElem);
                newRow.find('.col-lg-12').append(children);
            }
        } 
        
        /**
         * Setup
         */
        builder.setup = function(e) {
            // Configuracion inicial
            canvas = builder.addClass('lpb-canvas');
            
            htmlTextArea = $('<textarea class="lpb-html-output"/>').insertBefore(canvas);

            //Menu prinicipal
            mainControls = $('<div class="lpb-mainControls" />').insertBefore(htmlTextArea);
            wrapper      = $('<div class="lpb-wrapper lpb-top" />').appendTo(mainControls);

            // Anadir fila
            addRowGroup = $('<div class="lpb-addRowGroup pull-left btn-group" />').appendTo(wrapper);
            $.each(options.new_row_layouts, function(j, layout) {
                var btn = $('<a class="btn btn-sm btn-primary" />')
                    .on('click', function() {
                        var row = builder.createRow().appendTo(canvas);
                        layout.forEach(function(i) {
                            if(options.auto_rows){
                                i = 0;
                            }
                            builder.createColumn(i).appendTo(row);
                        });
                        builder.limitColumnsInRow(row);
                        builder.init();
                    })
                    .appendTo(addRowGroup)
                ;
                
                btn = options.new_row_layouts.length > 1 
                      ? btn.attr('title', 'Inserta fila ' + layout.join('-'))
                      : btn;
            

                btn.append('<span class="glyphicon glyphicon-plus-sign"/>');

                var layoutName     = layout.join(' - ');
                var lpbRowIcon     = 'lpb-row-text';
                var textLpbRowIcon = 'Nueva fila';
                var buttons        = '<div class="row lpb-row-text">';
                
                if(options.new_row_layouts.length > 1){
                    buttons       = '<div class="row lpb-row-icon">';
                    textLpbRowIcon = '';
                   
                    layout.forEach(function (i) {
                        buttons += '<div class="column col-' + colMdByVersion + i + '"/>';
                    });
                }
                
                buttons += textLpbRowIcon + '</div>';
                btn.append(buttons);
            });
            
            //Selector de tamano del page builder
            if(options.unique_size === 0){
                var layoutDropdown = $('<div class="dropdown pull-left lpb-layout-mode">' +
                    '<button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-toggle="dropdown"><span>Desktop</span></button>' +
                    '<ul class="dropdown-menu" role="menu">' +
                        '<li><a data-width="auto" title="Desktop"><span>Desktop</span></a></li>' +
                        '<li><a title="Tablet"><span>Tablet</span></li>' +
                        '<li><a title="Phone"><span>Phone</span></a></li>' +
                        '</ul>' +
                    '</div>')
                    .on('click', 'a', function() {
                        var a = $(this);
                        builder.switchLayout(a.closest('li').index());
                        var btn = layoutDropdown.find('button');
                        btn.find('span').remove();
                        btn.append(a.find('span').clone());
                    })
                    .appendTo(wrapper);
            }

            //Botones de la derecha
            //Dejamos comentado ya que no es necesario
            var btnGroup = $('<div class="dev-buttons btn-group pull-left"/>')
                .appendTo(wrapper)
            ;
            var htmlButton = $('<button title="Edit Source Code" type="button" class="btn btn-sm btn-primary gm-edit-mode"><span class="glyphicon glyphicon-chevron-left"></span><span class="glyphicon glyphicon-chevron-right"></span></button>')
                .on('click', function() {
                    if (htmlButton.hasClass('active')) {
                        canvas.empty().html(htmlTextArea.val()).show();
                        builder.init();
                        htmlTextArea.hide();
                    } else {
                        builder.deinit();
                        htmlTextArea
                            .height(0.8 * $(window).height())
                            .val(canvas.html())
                            .show()
                        ;
                        canvas.hide();
                    }

                    htmlButton.toggleClass('active btn-danger');
                })
                .appendTo(btnGroup)
            ;
            var previewButton = $('<button title="Preview" type="button" class="btn btn-sm btn-primary gm-preview"><span class="glyphicon glyphicon-eye-open"></span></button>')
                .on('mouseenter', function() {
                    canvas.removeClass('lpb-editing');
                })
                .on('click', function() {
                    previewButton.toggleClass('active btn-danger').trigger('mouseleave');
                })
                .on('mouseleave', function() {
                    if (!previewButton.hasClass('active')) {
                        canvas.addClass('lpb-editing');
                    }
                })
                .appendTo(btnGroup)
            ;
            
            //Botones especificos de la izquierda
            if(options.elements_maincontrol_left.length > 0){
                var btnCustomGroupLeft = $('<div class="custom-left-buttons btn-group pull-left"/>')
                    .appendTo(wrapper)
                ;
                $.each(options.elements_maincontrol_left, function(key, value){
                    $(value).appendTo(btnCustomGroupLeft);
                });
            }
            
            //Botones especificos de la derecha
            if(options.elements_maincontrol_left.length > 0){
                var btnCustomGroupRight = $('<div class="custom-right-buttons pull-right"/>')
                    .appendTo(wrapper)
                ;
                $.each(options.elements_maincontrol_right, function(key, value){
                    $(value).appendTo(btnCustomGroupRight);
                });
            }
            
            //Anadir div clearfix para que no se descaraje nada
            $('<div class="clearfix"></div>').appendTo(wrapper);

            //Menu fixed
            $(window).on('scroll', builder.onScroll);

            //Inicia RTE
            canvas.on('click', '.lpb-content', builder.initRTE);
        }
        
        /**
         * Scrolling page builder
         */
        builder.onScroll = function(e) {
            var $window = $(window);
            
            if (
                $window.scrollTop() > mainControls.offset().top &&
                $window.scrollTop() < canvas.offset().top + canvas.height()
            ) {
                if (wrapper.hasClass('lpb-top')) {
                    wrapper
                        .css({
                            left: wrapper.offset().left,
                            width: wrapper.outerWidth(),
                        })
                        .removeClass('lpb-top')
                        .addClass('lpb-fixed')
                    ;
                }
            } else {
                if (wrapper.hasClass('lpb-fixed')) {
                    wrapper
                        .css({ left: '', width: '' })
                        .removeClass('lpb-fixed')
                        .addClass('lpb-top')
                    ;
                }
            }
        }
        
        /**
         * Inicializa RTE
         */
        builder.initRTE = function(e) {
            if ($(this).hasClass('lpb-rte-active')) { return; }
            
            var rte = builder.getRTE($(this).data('lpb-content-type'));
            if (rte) {
                $(this).addClass('lpb-rte-active', true);
                rte.init(options, $(this));
            }
        }
        
        /**
         * Obtiene RTE
         */
        builder.getRTE = function(type) {
            return $.fn.ledPageBuilder.RTEs[type];
        }
        
        /**
         * Reiniciamos el builder
         */
        builder.reset = function() {
            builder.deinit();
            builder.init();
        }

        /**
         * Inicializamos la funcionalidad del builder
         */
        builder.init = function() {
            builder.runFilter(true);
            canvas.addClass('lpb-editing');
			canvas.addClass(laterVersionClass);
            builder.addAllColClasses();
            builder.wrapContent();
            builder.createRowControls();
            builder.createColControls();
            builder.makeSortable();
            //Insertamos el ancho del builder
            var builderSize = options.unique_size === 0
                              ? curColClassIndex
                              : options.unique_size -1;
            builder.switchLayout(builderSize);
            //Funcionalidades para los componentes
            builder.droppableComponentsInit();
            builder.sortableComponentsInit();
        }

        /**
         * Limpiamos el builder
         */
        builder.deinit = function() {
            canvas.removeClass('lpb-editing');
            var contents = canvas.find('.lpb-content').removeClass('lpb-rte-active').each(function() {
                var content = $(this);
                builder.getRTE(content.data('lpb-content-type')).deinit(options, content);
            });
            canvas.find('.lpb-tools-drawer').remove();
            builder.removeSortable();
            builder.runFilter(false);
        }
        
        /**
         * Eliminar
         */
        builder.remove = function() {
            builder.deinit();
            mainControls.remove();
            htmlTextArea.remove();
            $(window).off('scroll', onScroll);
            canvas.off('click', '.lpb-content', builder.initRTE);
            canvas.removeData('ledPageBuilder');
        }

        /**
         * Crear barra de herramientas de la fila
         */
        builder.createRowControls = function() {
            canvas.find('.row').each(function() {
                var row = $(this);
                if (row.find('> .lpb-tools-drawer').length) { return; }

                var drawer = $('<div class="lpb-tools-drawer" />').prependTo(row);
                builder.createTool(drawer, 'Mover', 'lpb-move', 'glyphicon-move');
                options.row_tools.forEach(function(t) {
                    builder.createTool(drawer, t.title || '', t.className || '', t.iconClass || 'glyphicon-wrench', t.on);
                });
                
                builder.createTool(drawer, 'A&ntilde;adir columna', 'lpb-color-add lpb-add-column', 'glyphicon-plus-sign', function() {
                    var columns = 0;
                    if(options.version > 3 && !row.hasClass('auto-on')){
                        columns = 12;
                    }
                    if(!row.find('> .lpb-tools-drawer > .lpb-add-column').hasClass('no-add-column')){
                        row.append(builder.createColumn(columns));
                    }
                    builder.limitColumnsInRow(row);
                    builder.init();
                });
                
                if(options.version > 3 && options.auto_rows === true){
                    builder.createTool(drawer, 'Columnas autom&aacute;ticas', 'lpb-color-auto btn-auto-col', 'glyphicon-resize-on', function () {
                        builder.autoCol(drawer);
                    });
                }
                
                /*builder.createTool(drawer, 'ConfiguraciÃ³n', 'pull-right lpb-color-config', 'glyphicon-cog', function() {
                    details.toggle();
                });*/
                
                builder.createTool(drawer, 'Eliminar fila', 'pull-right lpb-color-remove', 'glyphicon-trash', function() {
                    var options = {
                        buttons: {
                            Aceptar: function () {
                                $(dialogId).dialog("close");
                                row.slideUp(function () {
                                    row.remove();
                                });
                            },
                            Cancelar: function () {
                                $(dialogId).dialog("close");
                            }
                        }
                    };
                    builder.addDialog('Eliminar fila', '&iquest;Est&aacute; seguro?', 'alert', options);
                });
                
                builder.createTool(drawer, 'Clonar fila', 'pull-right', 'glyphicon-clone', function() {
                    builder.cloneRow(row);
                });

                var details = builder.createDetails(row, options.row_classes).appendTo(drawer);
            });
        }

        /**
         * Crear la barra de herramientas de la columna
         */
        builder.createColControls = function() {
            canvas.find('.column').each(function() {
                var col = $(this);
                if (col.find('> .lpb-tools-drawer').length) { return; }

                var drawer = $('<div class="lpb-tools-drawer" />').prependTo(col);

                builder.createTool(drawer, 'Mover', 'lpb-move', 'glyphicon-move');

                if (options.valid_col_sizes.length > 1) {
                    builder.createTool(drawer, 'Disminuir columna', 'lpb-decrease-col-width', 'glyphicon-minus', function (e) {
                        var colSizes = options.valid_col_sizes;
                        var curColClass = colClasses[curColClassIndex];
                        var curColSizeIndex = colSizes.indexOf(builder.getColSize(col, curColClass));
                        var newSize = colSizes[builder.clamp(curColSizeIndex - 1, 0, colSizes.length - 1)];
                        if (e.shiftKey) {
                            newSize = colSizes[0];
                        }
                        builder.setColSize(col, curColClass, Math.max(newSize, 1));
                    });

                    builder.createTool(drawer, 'Aumentar columna', 'lpb-increase-col-width', 'glyphicon-plus', function (e) {
                        var colSizes = options.valid_col_sizes;
                        var curColClass = colClasses[curColClassIndex];
                        var curColSizeIndex = colSizes.indexOf(builder.getColSize(col, curColClass));
                        var newColSizeIndex = builder.clamp(curColSizeIndex + 1, 0, colSizes.length - 1);
                        var newSize = colSizes[newColSizeIndex];
                        if (e.shiftKey) {
                            newSize = colSizes[colSizes.length - 1];
                        }
                        builder.setColSize(col, curColClass, Math.min(newSize, MAX_COL_SIZE));
                    });
                }
                
                options.col_tools.forEach(function(t) {
                    builder.createTool(drawer, t.title || '', t.className || '', t.iconClass || 'glyphicon-wrench', t.on);
                });
				
                //Limitamos la posibilidad de anidar filas
                builder.buttonAddRow(drawer, col);
                
                /*builder.createTool(drawer, 'Configuracion', 'pull-right lpb-color-config', 'glyphicon-cog', function() {
                    details.toggle();
                });*/
                
                builder.createTool(drawer, 'Eliminar columna', 'pull-right lpb-color-remove', 'glyphicon-trash', function() {
                    var options = {
                        buttons: {
                            Aceptar: function () {
                                $(dialogId).dialog("close");
                                var $row = col.closest('.row');
                                col.remove();
                                builder.limitColumnsInRow($row);
                            },
                            Cancelar: function () {
                                $(dialogId).dialog("close");
                            }
                        }
                    };
                    builder.addDialog('Eliminar columna', '&iquest;Est&aacute; seguro?', 'alert', options);
                });
                
                builder.createTool(drawer, 'Clonar columna', 'pull-right lpb-clone', 'glyphicon-clone', function() {
                    builder.cloneColumn(col);
                });

                var details = builder.createDetails(col, options.col_classes).appendTo(drawer);
            });
        }

        /**
         * Crear herramienta
         * 
         * @param object drawer
         * @param string title
         * @param string className
         * @param string iconClass
         * @param object eventHandlers
         */
        builder.createTool = function(drawer, title, className, iconClass, eventHandlers) {
            var tool = $('<a title="' + title + '" class="' + className + '"><span class="glyphicon ' + iconClass + '"></span></a>')
                .appendTo(drawer)
            ;
            if (typeof eventHandlers == 'function') {
                tool.on('click', eventHandlers);
            }
            if (typeof eventHandlers == 'object') {
                $.each(eventHandlers, function(name, func) {
                    tool.on(name, func);
                });
            }
        }

        /**
         * Crear detalles 
         * 
         * @param object container
         * @param  string cssClasses
         * @returns {@this;.createDetails.detailsDiv|$}
         */
        builder.createDetails = function(container, cssClasses) {
            var detailsDiv = $('<div class="lpb-details" />');

            //Damos la posibilidad de modificar el id
            $('<label for="lpb-id">Id:</label> <input id="lpb-id" class="lpb-input" />')
                .attr('placeholder', 'id')
                .val(container.attr('id'))
                .attr('title', 'Id Ãºnico')
                .appendTo(detailsDiv)
                .keyup(function() {
                    container.attr('id', this.value);
                })
            ;
            
            //Damos la posibilidad de modificar las clases
            $(' <label for="lpb-class">Clases:</label> <input id="lpb-class" class="lpb-input" />')
                .attr('placeholder', 'Clases')
                .val(container.attr('class'))
                .attr('title', 'Clases')
                .appendTo(detailsDiv)
                .keyup(function() {
                    container.attr('class', this.value);
                })
            ;
            
            //Damos la posibilidad de modificar los estilos
            $(' <label for="lpb-styles">Estilos:</label> <input id="lpb-styles" class="lpb-input" />')
                .attr('placeholder', 'Estilos')
                .val(container.attr('styles'))
                .attr('title', 'Estilos')
                .appendTo(detailsDiv)
                .keyup(function() {
                    container.attr('styles', this.value);
                })
            ;

            return detailsDiv;
        }

        /**
         * Anade las clases de la columna
         */
        builder.addAllColClasses = function() {
            canvas.find('.column:not(.col), div[class*="col-"]').each(function() {
                var col = $(this);

                var size = 2;
                var sizes = builder.getColSizes(col);
                if (sizes.length) {
                    size = sizes[0].size;
                }

                var elemClass = col.attr('class');
                colClasses.forEach(function(colClass) {
                    if (elemClass.indexOf(colClass) == -1) {
                        col.addClass(colClass + size);
                    }
                });

                col.addClass('column');
            });
        }

        /**
         * Devuelve el tamano de la columna
         * 
         * @param string col
         * @param string colClass
         * @returns {size|null}
         */
        builder.getColSize = function(col, colClass) {
            var sizes = builder.getColSizes(col);
            for (var i = 0; i < sizes.length; i++) {
                if (sizes[i].colClass == colClass) {
                    return sizes[i].size;
                }
            }
            if (sizes.length) {
                return sizes[0].size;
            }
            return null;
        }
        
        /**
         *  @param string col
         */
        builder.getColSizes = function(col) {
            var result = [];
            colClasses.forEach(function(colClass) {
                var re = new RegExp(colClass + '(\\d+)', 'i');
                if (re.test(col.attr('class'))) {
                    result.push({
                        colClass: colClass,
                        size: parseInt(re.exec(col.attr('class'))[1])
                    });
                }
            });
            return result;
        }

        /**
         * AÃ±ade el tamano de la columna
         * 
         * @param object col
         * @param string colClass
         * @param string size
         */
        builder.setColSize = function(col, colClass, size) {
            var re = new RegExp('(' + colClass + '(\\d+))', 'i');
            var reResult = re.exec(col.attr('class'));
            if (reResult && parseInt(reResult[2]) !== size) {
                col.switchClass(reResult[1], colClass + size, 50);
            } else {
                col.addClass(colClass + size);
            }
        }

        /**
         * Hace las filas ordenables
         */
        builder.makeSortable = function() {
            canvas.find('.row').sortable({
                items: '> .column',
                connectWith: '.lpb-canvas .row',
                handle: '> .lpb-tools-drawer .lpb-move',
                start: sortStart,
                helper: 'clone',
                receive: function(event, ui) {
                    //Limitamos el numero de items por fila
                    if (($(this).children().length - 2) >= options.limit_add_columns) {
                        $(ui.sender).sortable('cancel');
                    }
                },
                start: function (event, ui) {
                    //Calculamos el alto del contenedor
                    ui.placeholder.height(ui.item.height());
                },
                over: function(event, ui){
                    var $column       = ui.item;
                    var $columnHelper = ui.helper;
                    var $startRow     = ui.sender ? ui.sender : $column.closest('.row');
                    var $overRow      = $(event.target);
                    var idStartRow    = $startRow.attr('id');
                    var idOverRow     = $overRow.attr('id');
                    var noAddColumn   = $overRow.hasClass('add-column-disabled');
                    
                    if(idStartRow !== idOverRow && noAddColumn){
                        $columnHelper.addClass('no-add-in-row');
                    }
                },
                out: function(event, ui){
                    var $columnHelper = ui.helper;
                    if($columnHelper){
                        $columnHelper.removeClass('no-add-in-row');
                    }
                },
                change: function (event, ui) {
                    //Insertar codigo si fuera o fuese necesario
                },
                stop: function (event, ui) {
                    var $startRow    = $(this);
                    var $column      = ui.item;
                    var $row         = $column.closest('.row');
                    //Actualizamos el estado de la columna
                    if($row.hasClass('auto-on')){
                        builder.columnAutoOn($column);
                    }else{
                        builder.columnAutoOff($column);
                    }
                    //Comprobamos la posibilidad de poder añadir mas filas
                    builder.buttonAddRow($column.find('.lpb-tools-drawer'), $column);
                    builder.limitColumnsInRow($startRow);
                    builder.limitColumnsInRow($row);
                }
            });
            canvas.add(canvas.find('.column')).sortable({
                items: '> .row, > .lpb-content',
                connectsWith: '.lpb-canvas, .lpb-canvas .column',
                handle: '> .lpb-tools-drawer .lpb-move',
                start: sortStart,
                helper: 'clone',
                start: function (event, ui) {
                    //Insertar codigo si fuera o fuese necesario
                    ui.placeholder.height(ui.item.height());
                },
                change: function (event, ui) {
                    //Insertar codigo si fuera o fuese necesario
                },
                update: function (event, ui) {
                    //Insertar codigo si fuera o fuese necesario
                }
            });

            function sortStart(e, ui) {
                ui.placeholder.css({ height: ui.item.outerHeight()});
            }
        }
        
        builder.removeSortable = function() {
            canvas.add(canvas.find('.column')).add(canvas.find('.row')).sortable('destroy');
        }

        /**
         * Crear fila
         */
        builder.createRow = function() {
            var $idRow   = ' id="lpb-row-'.getRandomInt()+'" ';
            var $autoCol = options.version > 3 || options.auto_rows === 3 ? 'auto-on' : '';
            return $('<div class="row ' + $autoCol + '" '+$idRow+'/>');
        }
        
        /**
         * Crear columna
         */
        builder.createColumn = function(size) {
            var $idColumn = ' id="lpb-column-'.getRandomInt()+'" ';
            var $col      = size > 0 
                        ? 'column ' + colClasses.map(function(c) { return c + size; }).join(' ')
                        : 'col column';
            return $('<div ' + $idColumn + '/>')
                .addClass($col)
                .append(builder.createDefaultContentWrapper().html(
                    builder.getRTE(options.content_types[0]).initialContent(options.zone_component))
                )
            ;
        }

        /**
         * Filtra el contenido
         */
        builder.runFilter = function(isInit) {
            if (options.custom_filter.length) {
                $.each(options.custom_filter, function(key, func) {
                    if (typeof func == 'string') {
                        func = window[func];
                    }

                    func(canvas, isInit);
                });
            }
        }

        /**
         * Envuelve el contenido en <div class="lpb-content"> donde se necesite
         */
        builder.wrapContent = function() {
            canvas.find('.column').each(function() {
                var col = $(this);
                var contents = $();
                col.children().each(function() {
                    var child = $(this);
                    if (child.is('.row, .lpb-tools-drawer, .lpb-content')) {
                        builder.doWrap(contents);
                    } else {
                        contents = contents.add(child);
                    }
                });
                builder.doWrap(contents);
            });
        }
        
        builder.doWrap = function(contents) {
            if (contents.length) {
                var container = builder.createDefaultContentWrapper().insertAfter(contents.last());
                contents.appendTo(container);
                contents = $();
            }
        }

        builder.createDefaultContentWrapper = function() {
            return $('<div/>')
                .addClass('lpb-content lpb-content-type-' + options.content_types[0])
                .attr('data-lpb-content-type', options.content_types[0])
            ;
        }

        builder.switchLayout = function(colClassIndex) {
            curColClassIndex = colClassIndex;

            var layoutClasses = ['lpb-layout-desktop', 'lpb-layout-tablet', 'lpb-layout-phone'];
            layoutClasses.forEach(function(cssClass, i) {
                canvas.toggleClass(cssClass, i == colClassIndex);
            });
        }
        
        builder.clamp = function(input, min, max) {
            return Math.min(max, Math.max(min, input));
        }
        
        /**
         * Obtenemos el HTML limpio del editor
         */
        builder.getHTML = function(){
            builder.deinit();
            var html = builder.html();
            builder.init();
            return html;
        }
        
        /**
         * Eliminamos el Builder
         */
        builder.removeBuilder = function(){
            builder.remove();
        }
        
        /**
         * Anadimos el html para los dialogs
         */
        builder.addDialogHTML = function () {
            if ($(dialogId).length === 0) {
                var dialog = '<div id="lpb-dialog" class="lpb-dialog-title" title="T&iacute;tulo">' +
                        '<p>' +
                        '<span class="lpb-dialog-icon ui-icon"></span>' +
                        '<span class=lpb-dialog-content>Content</span>' +
                        '</p>' +
                        '</div>';
                builder.after(dialog);
            }
        }
        
        /**
         * Configuramos el dialog a necesidad
         * 
         * @param string title
         * @param string content
         * @param string icon, Ejemplo: alert 
         * @param array options
         * 
         * Ejemplo para options:
         * var options = {
         *      buttons: {
         *          "Delete all items": function () {
         *              $(this).dialog("close");
         *          },
         *          Cancel: function () {
         *              $(this).dialog("close");
         *          }
         *      }
         *  };
         */
        builder.addDialog = function(title, content, icon, options){
            icon    = icon || false;
            options = options || false;
            
            var $dialog = $('#lpb-dialog');
            $dialog.attr('title', title);
            $dialog.find('.lpb-dialog-content').html(content);
            if(icon){
                $dialog.find('.lpb-dialog-icon').addClass('ui-icon-'+icon);
            }
            
            var defaultOptions = {
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                dialogClass: 'lpb-dialog',
            };
            
            var allOptions = options ? $.extend({}, defaultOptions, options) : defaultOptions;
            $( "#lpb-dialog" ).dialog(allOptions);
        }
        
        /**
         * Iniciamos la funcionalidad de droppable
         * ya que se insertan las columnas de forma dinamica
         */
        builder.droppableComponentsInit = function(){
            if(options.plugin_components !== false){
                options.plugin_components.droppableInit();
            }
        }
        
        /**
         * Iniciamos la funcionalidad de droppable
         * ya que se insertan las columnas de forma dinamica
         */
        builder.sortableComponentsInit = function(){
            if(options.plugin_components !== false){
                options.plugin_components.sortableInit();
            }
        }
        
        /**
         * Cambiamos el estado de la fila para permitir
         * cambiar de tamano las columnas hijas y cambiamos 
         * las clases de estas
         * 
         * @param object $this 
         */
        builder.autoCol = function($this, noChangeStatus){
            noChangeStatus = noChangeStatus || false; 
            
            var $row         = $this.hasClass('row') ? $this : $this.closest('.row');
            var $iconAutoCol = $row.find('> .lpb-tools-drawer .btn-auto-col .glyphicon');
            var $columns     = $row.find('> .column');
            
            //Si onAutoCol es true pasa a activo
            var onAutoCol = noChangeStatus ? $row.hasClass('auto-on') : !$row.hasClass('auto-on');
            
            //Cambiamos el estado de los botones
            if (onAutoCol){
                $iconAutoCol.removeClass('glyphicon-resize-off')
                            .addClass('glyphicon-resize-on');
                $row.removeClass('auto-off')
                            .addClass('auto-on');
                    
                //Cambiamos las clases de las columnas
                $columns.each(function(){
                    var $column     = $(this);
                    builder.columnAutoOn($column);
                });
            } else {
                $iconAutoCol.removeClass('glyphicon-resize-on')
                            .addClass('glyphicon-resize-off')
                $row.removeClass('auto-on')
                            .addClass('auto-off');
                    
                //Cambiamos las clases de las columnas
                $columns.each(function () {
                    var $column = $(this);
                    builder.columnAutoOff($column);
                });
            }
        }
        
        /**
         * Cambiamos el estado de la columna a automatica 'col'
         * 
         * @param object $column
         */
        builder.columnAutoOn = function($column){
            var columnClass = $column.attr('class');
            
            //Obtenemos las clases col-* actuales y la aÃ±adimos a un data
            var colColumnClass = columnClass.match(regex);
            colColumnClass = colColumnClass !== null ? colColumnClass.join(' ') : '';

            //Eliminamos las clases col-*
            columnClass = columnClass.replace(regex, '');
            $column.attr('class', columnClass)
                   .addClass('col')
                   .attr(dataColSizes, colColumnClass);
        }
        
        /**
         * Cambiamos el estado de la columna a redimensionble 'col-*'
         * 
         * @param object $column
         */
        builder.columnAutoOff = function($column){
            
            var colClass = $column.attr(dataColSizes) !== undefined 
                           && $column.attr(dataColSizes) !== '' 
                           ? $column.attr(dataColSizes)
                           : defaultCols;
            $column.removeClass('col')
                   .addClass(colClass);
        }
        
        /**
         * Agregamos o eliminamos el boton de insertar
         * fila en la columna segun el limite que configuremos
         * 
         * @param object $drawer Barra de controles de la columna
         * @param object $col Columna en la que mostraremos el boton
         */
        builder.buttonAddRow = function($drawer, $col){
            if ($col.parents('.row').length < options.limit_childs_row) {
                var issetAddRow = $col.find('.lpb-add-new-row').length > 0; 
                if(!issetAddRow){
                    builder.createTool($drawer, 'A&ntilde;adir fila', 'lpb-color-add lpb-add-new-row', 'glyphicon-plus-sign', function () {
                        var $row = builder.createRow();
                        $col.append($row);
                        //row.append(builder.createColumn(6)).append(builder.createColumn(6));
                        $row.append(builder.createColumn(0));
                        builder.init();
                    });
                }
            }else{
                $drawer.find('.lpb-add-new-row').remove();
            }
        }
        
        /**
         * Mostraremos el boton de insertar columna segun el 
         * limite que configuremos
         * 
         * @param object $row
         */
        builder.limitColumnsInRow = function($row, isRemovedColumn){
            isRemovedColumn      = isRemovedColumn || false; 
            var countColumns     = !isRemovedColumn 
                                   ? $row.find('> .column').length
                                   : $row.find('> .column').length - 1;                     
            var $buttonAddColumn = $row.find('> .lpb-tools-drawer > .lpb-add-column');
            var $columns         = $row.find('> .column');
            if(countColumns >= options.limit_add_columns){
                $buttonAddColumn.addClass('no-add-column');
                $row.addClass('add-column-disabled');
            }else{
                $buttonAddColumn.removeClass('no-add-column');
                $row.removeClass('add-column-disabled');
            }
            builder.statusRowByColumns($row);
        }
        
        /**
         * Comprobamos el numero de columnas que contiene
         * una fila para mostrar un mensaje
         * 
         * @param object $row
         */
        builder.statusRowByColumns = function($row){
            var classNoColumns = 'no-columns';
            var countColumns   = $row.find('> .column').length;
            
            $row.removeClass(classNoColumns);
            if(countColumns <= 0){
                $row.addClass(classNoColumns);
            }
        }
        
        /**
         * Clonar fila
         * @param object $row
         */
        builder.cloneRow = function($row){
            var $cloneRow = $row.clone();
            $cloneRow.find('.lpb-tools-drawer')
                     .remove();
            
            //Anadimos los nuevos ids
            $cloneRow = builder.refreshAllIds($cloneRow);
            $row.after($cloneRow);
            
            //Anadimos los eventos
            builder.init();
        }
        
        /**
         * Clonar columna 
         * @param object $column
         * @param boolean afterColumn Por defecto true, falso inserta la
         *                            columna a final de la fila
         */
        builder.cloneColumn =  function ($column){
            var $cloneColumn = $column.clone();
            $cloneColumn.find('.lpb-tools-drawer')
                        .remove();
            
            //Anadimos los nuevos ids
            $cloneColumn = builder.refreshAllIds($cloneColumn);
            $column.after($cloneColumn);
            
            //Anadimos los eventos
            builder.init();
            
            //Mostramos el estado de la fila segun columnas
            var $row = $column.closest('.row');
            builder.limitColumnsInRow($row);
        }
        
        builder.refreshAllIds = function($element){
            //Cambiamos el id del elemento
            var newIdElement   = '';
            var issetIdElement = false;
            do {
                newIdElement   = $element.hasClass('row')
                                 ? 'lpb-row-'.getRandomInt()
                                 : 'lpb-column-'.getRandomInt();
                issetIdElement = $('#' + newIdElement).length > 0
                                 ? true
                                 : false;
            } while (issetIdElement);
            $element.attr('id', newIdElement);
            
            //Cambiamos los ids de las filas
            $rows = $element.find('.row');
            $rows.each(function(){
                $row = $(this);
                //Comprobamos que el is no existe para no crear conflictos
                var newIdRow   = '';
                var issetIdRow = false;
                do {
                    newIdRow   = 'lpb-row-'.getRandomInt();
                    issetIdRow = $('#' + newIdRow).length > 0 
                                 ? true 
                                 : false;
                } while (issetIdRow);
                $row.attr('id', newIdRow);
            });
            
            //Cambiamos los ids de las columnas
            $columns = $element.find('.column');
            $columns.each(function(){
                $column = $(this);
                //Comprobamos que el is no existe para no crear conflictos
                var newIdColumn   = '';
                var issetIdcolumn = false;
                do {
                    newIdColumn   = 'lpb-column-'.getRandomInt();
                    issetIdColumn = $('#' + newIdColumn).length > 0 
                                    ? true 
                                    : false;
                } while (issetIdColumn);
                $column.attr('id', newIdColumn);
            });
            return $element;
        }
        
        return builder.initLedPageBuilder();
    };
    
    /** METODOS **/
    /**
     * Elegimos si tiene editor e insertamos el contenido por defecto
     * En principio solo dejaremos sin editor
     */
    $.fn.ledPageBuilder.RTEs = {};
    $.fn.ledPageBuilder.RTEs.noEditor = {
        init: function (settings, contentAreas) {

            var self = this;
            contentAreas.each(function () {
                var contentArea = $(this);
                if (!contentArea.hasClass('active')) {
                    if (contentArea.html() == self.initialContent) {
                        contentArea.html('');
                    }
                    contentArea.addClass('active');
                }
            });
        },

        deinit: function (settings, contentAreas) {
            contentAreas.filter('.active').each(function () {
                var contentArea = $(this);
                contentArea
                        .removeClass('active')
                        .removeAttr('id')
                        .removeAttr('style')
                        .removeAttr('spellcheck')
                        ;
            });
        },

        initialContent: function (contentType) {
            var defaultContent = '<div class="ledColumnContent ledZoneType-' + contentType + ' noContent" data-zone-id="' + contentType + '"></div>';
            return defaultContent;
        },
    };
    
    /** /FIN METODOS **/
    
    /** VALORES POR DEFECTO **/
    
    $.fn.ledPageBuilder.defaults = {
        'new_row_layouts'            : [// Filas por defecto
                                       [12],
                                       [6, 6],
                                       [4, 4, 4],
                                       [3, 3, 3, 3],
                                       [2, 2, 2, 2, 2, 2],
                                       [2, 8, 2],
                                       [4, 8],
                                       [8, 4]
        ],
        'row_classes'                : [{label: 'Clase de ejemplo', cssClass: 'example-class'}],
        'col_classes'                : [{label: 'Clase de ejemplo', cssClass: 'example-class'}],
        'col_tools'                  : [], /* Ejemplo:
                                                [ {
                                                title: 'Fondo imagen',
                                                iconClass: 'glyphicon-picture',
                                                on: { click: function() {} }
                                                } ]
                                            */
        'row_tools'                  : [],
        'custom_filter'              : '',
        'content_types'              : ['noEditor'],
        'valid_col_sizes'            : [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
        'source_textarea'            : '',
        'version'                    : 3,
        'zone_component'             : 'content',
        'plugin_components'          : false,
        'auto_rows'                  : true,
        'unique_size'                : 0,
        'elements_maincontrol_left'  : [],
        'elements_maincontrol_right' : [],
        'limit_childs_row'           : 3,
        'limit_add_columns'          : 10
    };
    
    /** /FIN VALORES POR DEFECTO **/
    
    /**
     * Funcion para recoger el HTML limpio
     * desde cualquier sitio
     * 
     * @methods
     *  $('#myLedPageBuilder').ledGetHTMLPageBuilder('editor'); HTML completo
     *  $('#myLedPageBuilder').ledGetHTMLPageBuilder('clean');  HTML limpio
     */
    $.fn.ledGetHTMLPageBuilder = function(method){
        var builder = this;
        
        /**
         * Metodos publicos
         */
        var methods = {
            //HTML con las clases del editor
            editor : function() {
                var $builder = this.clone(true);
                
                //Eliminamos el contenido necesario
                $builder.removeClass('lpb-editing');
                $builder.find('.ui-sortable').removeClass('ui-sortable');
                $builder.find('.lpb-tools-drawer').remove();
                
                var HTML = builder.minifyHtml($builder.html());
                return HTML;
            },
            //HTML limpio sin las clases del editor
            clean: function() {
                var $builder = this.clone(true);
                
                //Eliminamos las clases necesarios
                $builder.removeClass('lpb-editing');
                $builder.find('.ui-sortable').removeClass('ui-sortable');
                $builder.find('.row').removeClass('auto-on auto-off no-columns');
                
                //Eliminamos elementos no necesarios
                $builder.find('.lpb-content').removeClass('lpb-rte-active').each(function(){
                    var $element = $(this);
                    var $column  = $element.parents('.column');
                    var $content = builder.minifyHtml($element.find('.ledColumnContent').html());
                    $element.remove();
                    $column.html($content);
                });
                $builder.find('.lpb-tools-drawer').remove();
                $builder.find('.ledLive').each(function(){
                    $ledLive = $(this);
                    $content = builder.minifyHtml($ledLive.html());
                    $ledLive.after($content)
                            .remove();
                });
                
                //Eliminamos los attr necesarios
                $builder.find('.column').removeAttr('data-col-sizes');
                $builder.find('.ledComponent').removeAttr('id data-component-name data-id data-ver data-fields ui-sortable-handle');

                var HTML = builder.minifyHtml($builder.html());
                return HTML;
            }
        }
        
        /**
         * Minificamos el html
         */
        builder.minifyHtml = function($html){
            $html = $html.ltrim();
            $html = $html.rtrim();
            return $html.replace(/\n\s+|\n/g, "");
        }
        
        //Gestiona los metodos
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error( 'Method "' +  method + '" does not exist in pluginName plugin!');
        }
    }
    
    /** FUNCIONES COMUNES **/
    
    /**
     * Quita los espacios vacios de la derecha de un string
     */
    String.prototype.rtrim = function (s) {
        if (s == undefined)
            s = '\\s';
        return this.replace(new RegExp("[" + s + "]*$"), '');
    };
    
    /**
     * Quita los espacios vacios de la izquierda de un string
     */
    String.prototype.ltrim = function (s) {
        if (s == undefined)
            s = '\\s';
        return this.replace(new RegExp("^[" + s + "]*"), '');
    };
    
    /**
     * Obtenemos un numero aleatorio mas el timestamp
     * para generar un 'id' unico
     */
    String.prototype.getRandomInt = function (s) {
        return this + (Math.floor((Math.random() * 9999) + 1).toString() + Date.now().toString());
    }
    
    /** /FIN FUNCIONES COMUNES **/
})(jQuery);