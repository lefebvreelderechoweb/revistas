(function ($) {

    $.fn.ledPageBuilderFields = function (params) {

        var options = $.extend({}, $.fn.ledPageBuilderFields.defaults, params);
        var ledPageBuilderFields = this;
        var ledHelper = options.ledHelper;


        /**
         * @param config
         * @param data
         * @returns {object}
         */
        ledPageBuilderFields.getAttributes = function(config, data){
            /**
             *
             * @type {string}
             * Nos siver para hacer las
             * comprobaciones sin escibir
             * cada vez undefined
             */
            var ud = 'undefined';

            /**
             *
             * @type {string}
             * Guarda el atributo data-type
             * que nos servirá luego para
             * hacer el parse y recomponer el
             * json para enviarlo a la WS
             */
            var dataType = '';
            if(typeof typeof config['data-type'] != ud){
                dataType = ' data-type="'+config['data-type']+'"';
            }

            /**
             *
             * @type {string}
             * La clase por defecto de los
             * inputs. Para agregar una clase
             * personalizada tienes que enviarla
             * como parámetro en el objeto config
             */
            var inputClass = ' class="form-control fieldFormComponent"';
            if(typeof config.class != ud){
                inputClass = ' class="form-control fieldFormComponent '+config.class+'"';
            }

            /**
             *
             * @type {string}
             * genera el atributo data-required
             * y se utiliza para hacer la validación
             */
            var required = '';
            if(typeof config.required != ud){
                required = ' data-required="'+config.required+'"';
            }

            /**
             *
             * @type {string}
             * genera el atributo max-length y por
             * ahora no la estamos utilizanod pero en
             * el futuro puede resultar útil
             */
            var maxLength = '';
            if(typeof config.max_length != ud) {
                maxLength = ' maxlength="' + config.max_length + '"';
            }

            /**
             *
             * @type {string}
             * Lo mismo que el parámetro que hay
             * más arriba
             */
            var minLength = '';
            if(typeof config.min_length != ud) {
                minLength = ' minlength="' + config.min_length + '"';
            }

            /**
             *
             * @type {string}
             * genera el atributo multiple que se
             * aplica en selects
             */
            var multiple = '';
            if(typeof config.multiple != ud) {
                multiple = ' multiple="' + config.multiple + '"';
            }

            /**
             *
             * @type {string}
             * es el atributo name del input
             */
            var inputName = '';
            if(typeof data.name != ud) {
                inputName = ' name="' + data.name + '"';
            }

            /**
             *
             * @type {string}
             * es el valor del input que puede generarse
             * con los datos por defecto o los que rellenó
             * el usuario.
             */
            var inputVal = '';
            if(typeof data.name != ud) {
                inputVal = ' value="' + data.value + '"';
            }


            var dataGroup = '';
            if(typeof data.group != ud){
                dataGroup = ' data-group="'+data.group+'"';
            }


            var dataLive = '';
            if(typeof data.live != ud){
                dataLive = ' data-live="'+data.live+'"';
            }
            
            var dataEmptyValue = '';
            if(typeof data.emptyValue != ud){
                dataEmptyValue = ' data-empty-value="'+data.emptyValue+'"';
            }


            /**
             * Sirve para seleccionar el mismo elemento
             * del DOM pero en atributos distintos
             * por ejemplo un anchor.
             *
             * Funciona en combinación con el dataAttr
             * @type {string}
             */
            var dataTarget = '';
            if(typeof data.target != ud){
                dataTarget = ' data-target="'+data.target+'"';
            }
            
            /**
             * Lo utilizaremos para que coincida el
             * campo del origen con el elemento del DOM
             */
            var dataTag = '';
            if(typeof data.tag != ud){
                dataTag = ' data-tag="'+data.tag+'"';
            }

            /**
             * Sirve para seleccionar un atributo
             * del elemento que se define mediante
             * target
             *
             * Funciona en combinación con el dataTarget
             * @type {string}
             */
            var dataAttr = '';
            if(typeof data.attr != ud){
                dataAttr = ' data-attr="'+data.attr+'"';
            }


            /**
             * Es el ancho que soporta la imagen de un
             * componente
             * @type {string}
             */
            var dataWidth = '';
            if(typeof data.width != ud){
                dataWidth = ' data-width="'+data.width+'"';
            }


            /**
             * Es el alto que soporta la imagen de un
             * component
             * @type {string}
             */
            var dataHeight = '';
            if(typeof data.height != ud){
                dataHeight = ' data-height="'+data.height+'"';
            }



            /**
             *
             * @type {string}
             * genera el atributo data-parse que indica
             * si un input se tiene que parsear tras guardar
             * los datos del componente
             */
            var dataParse = '';
            if(typeof config.parse != ud) {
                dataParse = ' data-led-parse="true"';
            }
            
            /**
             *
             * @type {string}
             * genera el atributo data-editor
             * con el que damos la posibilidad de que el campo sea un 
             * editor de texto
             */
            var dataEditor = '';
            if(typeof data.editor != ud) {
                dataEditor = ' data-editor="'+ data.editor+ '"';
            }

            /**
             *
             * @type {string}
             * genera el atributo data-label que lo
             * necesitamos para volver a generar la colección
             * de ajustes (el json) al guardar los ajustes
             * del componente
             */
            var dataLabel = '';
            if(typeof data.label != ud) {
                dataLabel = ' data-led-label="'+data.label+'"';
            }


            /**
             *
             * @type {string}
             * genera el atributo data-title que se
             * utiliza al parsear los campos de tipo
             * imagen y generar el Json que se envia
             * a la API
             */
            var dataTitle = '';
            if(typeof config['data-title'] != ud){
                dataTitle = ' data-title="'+config['data-title']+'"';
            }


            /**
             *
             * @type {string}
             * genera el atributo alt que se utiliza
             * en el tipo de campo imagen
             */
            var dataAlt = '';
            if(typeof config['data-alt'] != ud){
                dataAlt = ' data-alt="'+config['data-alt']+'"';
            }



            return  dataLabel + dataType + dataParse + required +
                multiple + minLength + maxLength + inputClass +
                inputName + inputVal + dataTitle + dataAlt + dataGroup +
                dataLive + dataTarget + dataTag + dataAttr + dataWidth + dataHeight
                + dataEmptyValue + dataEditor

        };

        ledPageBuilderFields.getParsedSettings = function(){
            var $fields = $('[data-led-parse="true"]');
            var params = [];

            $fields.each(function(index, element){
                params.push(ledPageBuilderFields.parseField($(element)));
            });

            return JSON.stringify({
                parametros: params,
                id: 0,
                idNwZona: 0,
                html: ''
            })

        };

        /**
         * Parseamos los inputs en busca de los valores
         * que componen el json de parámetros que enviamos
         * a la Web Service.
         * @param $input
         * @returns {{
         * label: null, name: null, type: null, value: null,
         * help: null, alt: null, title: null, idOrigen: null
         * }}
         *
         * */
        ledPageBuilderFields.parseField = function($input){

            var ud = 'undefined';
            /**
             * El label del input group
             * @type {null}
             */
            var label = null;
            if(typeof $input.data('led-label') != ud){
                label = $input.data('led-label');
            }
            /**
             * El nombre del input field
             * @type {null}
             */
            var name = null;
            if(typeof $input.attr('name') != ud){
                name = $input.attr('name');
            }

            /**
             * el tipo de campo: text,
             * select, etc.
             * @type {null}
             */
            var type = null;
            if(typeof $input.attr('type') != ud){
                type = $input.attr('type');
            }

            /**
             * El valor del campo
             * @type {null}
             */
            var value = null;
            if(typeof $input.val() != ud){
                value = $input.val();
            }

            /**
             * Un pequeño texto descriptivo
             * que va debajo del input. En la
             * versión 1 no lo estamos usando.
             * @type {null}
             */
            var help = null;
            if(typeof $input.data('led-help') != ud){
                help = $input.data('led-help');
            }

            /**
             * Se utiliza en el tipo de campo
             * imagen para establecer el texto
             * alternativo
             * @type {null}
             */
            var alt = null;
            if(typeof $input.data('led-alt') != ud){
                alt = $input.data('led-alt');
            }

            /**
             * El tipo de campo imagen utiliza
             * el título
             * @type {null}
             */
            var title = null;
            if(typeof $input.data('led-title') != ud){
                title = $input.data('led-title');
            }

            /**
             * Si no es un tipo de campo origen
             * la id llega en null
             * @type {null}
             */
            var idOrigen = null;


            var object = {
                label : label,
                name : name,
                type : type,
                value: value,
                help: help,
                alt: alt,
                title: title,
                idOrigen: idOrigen
            };

            return object;

        };

        /**
         * build - para crear de Json a Form.
         * encode - para crear de form a Json.
         * @param {json} json
         */
        this.buildFields = function(json){

            /**
             * Aquí siempre recibimos un json.
             * Fuera de lo que es el método estamos
             * llamando a ledHelper.isValidJson
             */
            var fields = JSON.parse(json);
            var htmlFields = [];


            if(fields.length > 0) {
                for (field in fields) {
                    var typo = ledHelper.ucFirst(fields[field].type);
                    var label = typeof fields[field].label == 'string' ? fields[field].label : '';

                    /**
                     * Cada field se genera en función de
                     * su tipo llamando al metodo correspondiente.
                     */
                    var call = ledPageBuilderFields['field'+typo];


                    var fieldConf = {};
                    var fieldData = fields[field];

                    /**
                     * Guardamos el tipo del input en un
                     * atributo data para luego poder
                     * recomponer el json
                     */
                    fieldConf['data-type'] = fieldData.type;

                    if(typeof call == 'function'){
                        
                        /**
                         * Comprobamos que tenga la propiedad emptyValue
                         * para dejar el valor vacio
                         */
                        if (typeof fieldData.emptyValue !== 'undefined' && (fieldData.value === fieldData.emptyValue)) {
                            fieldData.value = '';
                        }
                        
                        /**
                         * Generamos el input field llamando
                         * al método correspondiente
                         */
                        var inputField = call(fieldConf, fieldData);


                        /**
                         * Insertamos el field recientemente
                         * generado en un div class form-group
                         * y le agregamos el label
                         * @type {string}
                         */
                        var fieldsGroup = ledPageBuilderFields.addLabels(inputField, label);

                        /**
                         * Guaramos el html generado por cada método
                         * en un array para insertalos posteriormente
                         * en un target.
                         */
                        htmlFields.push(fieldsGroup);
                    }else{

                        /**
                         * Mostramos mensaje de error solo si el modo
                         * developer está activado.
                         */
                        console.log('El método ledPageBuilderFields.field'+typo+' no está definido')
                    }

                }
                /**
                 * Si no hay campos devolvemos un
                 * mensaje indicando que el componente
                 * no tiene ajustes.
                 */
            }else{
                htmlFields.push('' +
                    '<div class="alert alert-info ledAlert" role="alert">' +
                    '<i class="icon icon-edit"></i> ' +
                    'El componente que intentas editar no tiene ajustes' +
                    '</div>' +
                    '')
            }
            return htmlFields;
        };


        ledPageBuilderFields.eventsListener = function () {
            $(document)
                    .on('click', '.chooseImage', function () {

                        var $this = $(this);
                        var $input = $this.closest('.form-group').find('.ledImageType');
                        
                        /**
                         * Añadimos la clase currentEditing
                         * para referenciar a la que estamos editando
                         * y evitar posibles conflictos si hay mas campos 
                         * del mismo tipo
                         */
                        $('.currentEditing').removeClass('currentEditing');
                        $input.addClass('currentEditing').focus();

                        ledHelper.showModal({
                            /**
                             * El modal que queremos mostrar
                             * Usualmente se cargan en el pie
                             * de la página
                             */
                            modal: '#mainModal',
                            /**
                             * Si queremos cargar el contenido
                             * de una url mediante Ajax
                             */
                            content: window.location.origin + '/media',
                            /**
                             * El callback que se ejecuta al
                             * cerrar el modal
                             */
                            onModalClose: function () {
                                $('.currentEditing').removeClass('currentEditing');
                                $('#mainModal').removeAttr('data-width').removeAttr('data-height');
                                $('#modalTitle').html('');
                            },

                            onModalOpen: function () {
                                var $input = $('.currentEditing');
                                var width = $input.data('width');
                                var height = $input.data('height');
                                $('#mainModal').attr('data-width', width).attr('data-height', height);
                                $('#modalTitle').html('Media');
                            },

                            /**
                             * Callback que se ejecuta tras cargar
                             * el contenido or ajax.
                             */
                            onAjaxComplete: function () {

                                $(".media-items-container").selectable({
                                    filter: 'figure',
                                    selected: function (event, ui) {
                                        ledPageBuilderFields.buildFieldsIn('#mediaAttributesContent', ledPageBuilderFields.getImageParams($(ui.selected)));
                                    },
                                    unselected: function (event, ui) {
                                        ledPageBuilderFields.removeFieldsIn('#mediaAttributesContent');
                                    }
                                });
                                ledHelper.datePicker();
                            }
                        });

                    })
        };


        ledPageBuilderFields.removeFieldsIn = function(selector){
            $(selector).html('');
        };


        ledPageBuilderFields.getImageParams = function($selected){

            var title =
                ledHelper.stringFromFileName(
                ledHelper.baseName(
                $selected.find('img').attr('src')
            ));

            var alt =
                ledHelper.slugFromFileName(
                ledHelper.baseName(
                $selected.find('img').attr('src')
                ));


            return JSON.stringify([
                {
                    "label": "Título",
                    "name": "title",
                    "type": "text",
                    "value": typeof title != 'undefined' ? title : ''

                },
                {
                    "label": "Alt",
                    "name": "alt",
                    "type": "text",
                    "value": typeof alt != 'undefined' ? alt : ''
                },
                {
                    "label": null,
                    "name": "button",
                    "type": "submit",
                    "value": "Insertar",
                    "class": "btn btn-primary"
                }
            ]);
        };
        
        /**
         * Genera los fields en un selector
         * que puede ser una clase o bien una id
         * a partir de un json.
         * @param {string} selector
         * @param {json} json
         */
        ledPageBuilderFields.buildFieldsIn = function(selector, json){
            if(selector){
                var fields = ledPageBuilderFields.buildFields(json);
                $(selector).html(fields);
            }
        };


        /**
         * Enfoca el elemento que se está modificando
         * en el liveEditor
         * @param {jQuery} $element
         */
        ledPageBuilderFields.focusAt = function($element){
            var overlay = '<div class="live-edit-overlay"></div>';
            $('#ledBuilder').prepend(overlay);
            $element.addClass('ledLiveEdit');
        };


        /**
         *
         * @param {object} config
         * @param {object} data
         * @returns {string}
         */
        ledPageBuilderFields.fieldTextarea = function(config, data){

            config['parse'] = true;

            /**
             * Guardamos el valor antes de eliminar la
             * propiedad para evitar crear un tag que
             * no corresponde en un textarea: value=""
             * @type {string}
             */
            var value = typeof(data.value) == 'string' ? data.value.replace(new RegExp("\n","g"), "<br>") : '';
            delete data.value;
            return '<textarea'+ledPageBuilderFields.getAttributes(config, data)+' id="'+data.name+'">'+value+'</textarea>';
        };




        /**
         *
         * @param config
         * @param data son los datos que recibimos
         * de la base de datos o webservice y con
         * los cuales generamos el value=""
         * el name="" y el id=""
         * @returns {string}
         */
        ledPageBuilderFields.fieldText = function(config, data){
            config['parse'] = true;
            return '<input type="text"'+ledPageBuilderFields.getAttributes(config, data)+'/>';
        };


        /**
         *
         * @param config
         * @param data
         * @returns {string}
         */
        ledPageBuilderFields.fieldImage = function(config, data) {

            var ud = 'undefined';

            config['class'] = 'ledImageType';
            config['parse'] = true;

            /**
             * Asignamos data-alt unicamente si nos
             * llega el valor
             */
            if(typeof data.alt != ud && data.alt != ''){
                config['data-alt'] = data.alt;
            }

            if(typeof data.title != ud && data.title != ''){
                config['data-title'] = data.title;
            }
            
            /**
             * Damos la opcion de mostrar el input o no
             * 
             * Si es de tipo Pixel no mostraremos el boton de Seleccionar imagen
             */
            var showInput              = typeof data.showInput === 'undefined' || !data.showInput ? 'display:none;' : 'margin-bottom: 20px;';
            var showSpanLedImageType   = typeof data.showInput === 'undefined' || !data.showInput ? '' : ' display:none;';
            var selectImage            = typeof data.pixel === 'undefined' || !data.pixel ? '<div class="chooseImage">Selecciona otra imagen</div>' : '';
            var description            = typeof data.description === 'undefined' || !data.description ? '' : '<br/><small class="ledSmallDescription"><b>'+data.description+'</b></small>';
            
			dataValue = data.value ? 'URL: ' + data.value : '';						
            return description+
                   '<input type="text"'+ledPageBuilderFields.getAttributes(config, data)+' style="'+showInput+'"/>' +
                   '<div class="spanLedImageType" style="margin: 20px 0; font-size: 12px;color: #337ab7;'+showSpanLedImageType+'">'+dataValue+'</div>' +
				   selectImage
					;


        };


        ledPageBuilderFields.preview = function(config, data){
            console.log(data);
        };
        
        /**
         * @param config
         * @param data
         * @returns {string}
         */
        ledPageBuilderFields.fieldColor = function(config, data){
            config['class'] = 'colorpicker';
            config['parse'] = true;
            return '<input type="color"'+ledPageBuilderFields.getAttributes(config, data)+'/>';
        };
        
        /**
         *
         * @param config
         * @param data
         * @returns {string}
         */
        ledPageBuilderFields.fieldSubmit = function(config, data){
            config['class'] = 'btn btn-primary';

            var value = typeof data.value != 'undefined' ? data.value : 'Guardar';
            return '<button type="submit"'+ledPageBuilderFields.getAttributes(config, data)+'>'+value+'</button>';
        };

        /**
         *
         * @param input
         * @param label
         * @returns {string}
         */
        ledPageBuilderFields.addLabels = function(input, label){
            var label = typeof label  == 'string' ? label : '';
            return  '<div class="form-group">' +
                    '<label class="">'+label+'</label>' +
                     input +
                    '</div>'
        };
        
        /**
         * Actualizamos el valor de un campo del componente activo
         * 
         * @param string key
         * @param string value
         */
        ledPageBuilderFields.changeInputFieldsValue = function(name, value){
            var $activeComponent = $('.ledComponent.activeComponent');
            var fields           = $activeComponent.data('fields');
            
            //Nos aseguramos que fields sea un objeto
            fields = typeof fields === 'object' ? fields : JSON.parse(fields);
            
            //Cambiamos el campo value según su nombre
            fields.find(function(element, index){
                if(element.name === name){
                    fields[index].value = value;
                }
            });
            
            //Actualizamos el componente activo
            var fieldsJSON = JSON.stringify(fields);
            $activeComponent.attr('data-fields', fieldsJSON);
        }

        /**
         *
         * @returns {jQuery}
         */
        ledPageBuilderFields.init = function () {
            ledPageBuilderFields.eventsListener();
            return this;
        };
        
        /**
         * Eliminamos todas las instancias del CKEDITOR 
         */
        ledPageBuilderFields.destroyCKEditorInstances = function (){
            if (CKEDITOR.instances != 'undefined' && CKEDITOR.instances.length > 0) {
                for (name in CKEDITOR.instances)
                {
                    CKEDITOR.instances[name].destroy(true);
                }
            }
        };
        
        /**
         * Añadimos el editor y los eventos
         * Change: actualizamos los textos referentes al textarea
         * Focus : actualizamos visualmente
         */
        ledPageBuilderFields.addEditor = function(idEditor){
            idEditor = idEditor || 'editor';
            //Nos aseguramos de eliminar la instancia para actualizar el editor
            /*if(CKEDITOR.instances[idEditor]){
               CKEDITOR.instances[idEditor].destroy(true); 
            }*/
            
            //Variables para la configuracion del editor
            var strRemoveButtons = 'Save,NewPage,Preview,Print,Templates,'+
                                   'Cut,Copy,Paste,PasteText,PasteFromWord,'+
                                   'Undo,Redo,Replace,Find,SelectAll,Scayt,'+
                                   'Form,Checkbox,Radio,TextField,Textarea,'+
                                   'Select,Button,ImageButton,HiddenField,'+
                                   'Subscript,Superscript,CopyFormatting,'+
                                   'RemoveFormat,Outdent,Indent,Blockquote,'+
                                   'CreateDiv,BidiRtl,Language,Anchor,Image,'+
                                   'Flash,TextColor,HorizontalRule,Smiley,'+
                                   'SpecialChar,PageBreak,Iframe,Styles,'+
                                   'Format,Font,FontSize,BGColor,ShowBlocks,'+
                                   'Maximize,About,BidiLtr';
            var extraPlugins     = 'justify,table';
            var allowedContent   = '';
            var $editor = $('#'+idEditor);
            
            
            if($editor.length > 0){
                CKEDITOR.config.enterMode      = CKEDITOR.ENTER_BR;
                CKEDITOR.config.shiftEnterMode = CKEDITOR.ENTER_BR;
                CKEDITOR.config.autoParagraph  = false;
                
                //Comprobamos que no sea un enlace para no permitir los botones link no tabla
                if($('.activeComponent .ledLive'+$editor.data('live')+' > a > span').length > 0){
                    strRemoveButtons += ',Link,Unlink,Table'; 
                }
                
                //Damos la posibilidad de que pueda editar HTML
                if($('.activeComponent').data('editor-html') === 1){
                    allowedContent = 'h1 h2 h3 h4 h5 h6 p span div table tbody tfooter th td tr b br blockquote strong section em hr ol ul li dl dt dd i col caption;' +
                                     'a[!href];' +
                                     'img(left,right)[!src,alt,width,height]; *{*}; *[*]; *(*);';
                    extraPlugins += ',sourcedialog';
                }
                
                //Esta funcion es la que hace dinamico la configuracion del editor
                CKEDITOR.replace(idEditor, {
                    on: {
                        change: function (evt) {
                            var content = CKEDITOR.instances[idEditor].getData();
                            $editor.html(content);
                            $editor.val(content);
                            $editor.keyup();
                        },
                        focus: function(){
                            var $activeElement = $('.activeComponent').find('.ledLive' + $editor.attr('data-live'));
                            ledPageBuilderFields.activateField($activeElement);
                        }
                    },
                    removeButtons: strRemoveButtons,
                    disallowedContent: 'table{border-style}[cellspacing]',
                    extraPlugins: extraPlugins,
                    allowedContent: allowedContent
                });
                
                //Eventos del editor
                CKEDITOR.on( 'dialogDefinition', function( ev ) {
                    
                    var dialogName = ev.data.name;
                    var dialogDefinition = ev.data.definition;
                    
                    if ( dialogName == 'table' ) {
                        
                        var infoTab = dialogDefinition.getContents( 'info' );
                        infoTab.remove('txtBorderColor');
                        txtWidth = infoTab.get( 'txtWidth' );
                        txtWidth['default'] = '100%';
                        infoTab.get( 'txtCellSpace' ).default = '';
                    }
                });
            }
        };
        
        /**
         * Añadimos todos los editores necesarios
         */
        ledPageBuilderFields.addAllEditors = function(){
            $textareasWithEditor = $('textarea[data-editor=true]');
            $textareasWithEditor.each(function(){
                $textarea = $(this);
                ledPageBuilderFields.addEditor($textarea.attr('id'));
            });
        }
        
        /**
         * Desactivamos los campos
         */
        ledPageBuilderFields.disableFields = function(){
            $('.ledLive').removeClass('activeField');
        }
        
        /**
         * Activamos el estado del campo que estamos editando
         * 
         * @param object $activeElement
         */
        ledPageBuilderFields.activateField = function($activeElement){
            ledPageBuilderFields.disableFields();
            $activeElement.addClass('activeField');
        }

        return ledPageBuilderFields.init();
    };
    
    $.fn.ledPageBuilderFields.defaults = {};
})(jQuery);