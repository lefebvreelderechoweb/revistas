(function ($) {
    $.fn.ledMedia = function (params) {
        var options     = $.extend({}, $.fn.ledMedia.defaults, params);
        var ledMedia    = this;
        var ledCore     = options.ledCore;

        ledMedia.init = function () {
            ledMedia.eventsListener();
            return this;
        };


        ledMedia.showOptions = function () {
            ledFields.buildFieldsIn('#mediaAttributesContent', ledMedia.getSettings());
        };

        ledMedia.eventsListener = function () {
			
            $(document)
            /**
             * Cuando hacemos click sobre cualquiera
             * de las carpetas en el media library
             *
             */

                .on('click', '.media-dir > span', function () {
                    var $this = $(this);
                    ledMedia.selectDir($this);
                    ledMedia.loadContent($this);
                })

                /**
                 * Cuando hacemos click sobre cualquiera
                 * de las imagesnes que se muestran dentro
                 * del editor.
                 */
                .on('click', '.ledAddMedia', function (e) {

                    e.preventDefault();

                    var $this = $(this);
                    var $modal = $('#mainModal');
					
					modalAux		= new ledModalLoading('html');
					modalAux.setAnimationTime(500);
					modalAux.show();

                    if ($modal.find('.media-item.active').length <= 0) {
                        $('.error-no-image').removeClass('hidden');
                    }else{
						var $input = $('.currentEditing');
						
						$modal.modal('hide');
						 
						$input.val($this.data('image'));
						// Pendiente maquetación
						$span = $input.parent().find('.spanLedImageType');
						$span.html($this.data('image'));
						
						$imgActiveComponent = $('.activeComponent').find('.ledLive' + ledCore.ucFirst($input.data('group'))).find('img');						
						
                        //Creamos un id aleatorio para que no cachee la imagen
                        var randomId	= $.now();
                        var randomId    = '&v='+String(randomId) + String(Math.floor((Math.random() * 10000000) + 1));
                        
						src = $this.data('image') + randomId;
						$imgActiveComponent.attr('src', src );

						$modal.modal('hide');
					}
					
					modalAux.hide();
                })
				
				.on('click', '.applyPaginationFilterMedia', function (e) {
					e.stopPropagation();
					page = $(this).attr('data-page');
					$('#paginationMedia').val(page);
					$('#isPagination').val(1);
					$('.filter-search-all').click();					
				})
				.on('click', '.filter-search', function (e) {

					e.preventDefault();
			
					modalAux = new ledModalLoading('html');
					modalAux.setAnimationTime(500);
					modalAux.show();
			
					// Limpiamos filtros
					clearFilter = $(this).data('clear') ? true : false;
					
					// Comprobamos si está paginando
					isPagination	= parseInt($('#isPagination').val()) ? true : false;
					
					if( clearFilter ){
						$('#filterTitle').val('');
						$('#dateFrom').val('');
						$('#dateTo').val('');
						$('#paginationMedia').val(1);
					}
					
					if( !isPagination ){
						$('#paginationMedia').val(1);
					}
					
					$('#isPagination').val(0);
			
					// Filtros
					searchTxt	= $('#filterTitle').val() ? $('#filterTitle').val() : null;
					dateFrom	= $('#dateFrom').val() ? $('#dateFrom').val() : null;
					dateTo		= $('#dateTo').val() ? $('#dateTo').val() : null;
					page		= parseInt($('#paginationMedia').val());
					
					if( searchTxt ){
						searchTxt = encodeURIComponent( searchTxt );
					}
					
					if( dateFrom ){
						d			= new Date(dateFrom.split("/").reverse().join("-"));
						dd			= d.getDate() < 10 ?  '0' + d.getDate() : d.getDate();
						mm			= ( d.getMonth()+1 ) < 10 ? '0' + (d.getMonth()+1) : (d.getMonth()+1);
						yy			= d.getFullYear();
						dateFrom	= parseInt( yy + '' + mm + '' + dd );
					}
					
					if( dateTo ){
						d			= new Date(dateTo.split("/").reverse().join("-"));
						dd			= d.getDate() < 10 ?  '0' + d.getDate() : d.getDate();
						mm			= ( d.getMonth()+1 ) < 10 ? '0' + (d.getMonth()+1) : (d.getMonth()+1);
						yy			= d.getFullYear();
						dateTo		= parseInt( yy + '' + mm + '' + dd );
					}
					
					if( searchTxt || dateFrom || dateTo ){
						$('#clearFilterMedia').removeClass('hide');
					}else{
						$('#clearFilterMedia').addClass('hide');
					}
					
					searchTxt	= searchTxt ? searchTxt : 'null';
					dateFrom	= dateFrom ? dateFrom : 'null';
					dateTo		= dateTo ? dateTo : 'null';
					
					path		= $( 'ul.content-tree li.active a' ).data( 'path' );
				   
				   	/*
					 * Renderizamos la vista con los datos (images) actualizadas
					 */
					var url		= APP.base_url + 'media' + '/index/' + path + '/' + searchTxt + '/' + dateFrom + '/' + dateTo + '/' + page + '/';
                    console.log(url);
					$( '#media-main-content-parent' ).load( url + ' #media-main-content', function(){
						modalAux.hide();
					});
					
                })
				/**
                 * Evento 'click' para subir fichero
				 * al repositorio, realizamos un submit del formulario
				 * para un correcto envío de la imagen
                 */
				.on('change', '#ledUploadMedia', function () {
					var $this			= $( this );					
					var file			= $this[0].files[0];		
					var subFolder		= $('ul.content-tree li.active a').data('path');
                    var existImgName    = false;
                    var $form           = $('#mediaUploadForm'); 
                    var fileSize        = parseFloat((file.size / 1048576).toFixed(2));
                    var limitUpload     = $this.data('limit-size');
                    
                    //Limitamos la subida de imagenes a un maximo de 8MB
                    if(fileSize < limitUpload){
                        //Comprobamos el tamaño del nombre de la imagen y mostramos un mensaje para los que tenga mas de 60 caracteres
                        if(file.name.length <= 60){

                            var clearImageName    = ledMedia.clearNameImage(file.name);
                            var urlIssetImageName = window.location.origin+'/media/issetImageName/';
                                $.ajax({
                                    url: urlIssetImageName,
                                    type: "POST",
                                    data: {
                                        'subFolder': subFolder,
                                        'imageName': clearImageName
                                    },
                                    success: function (data) {
                                        data = JSON.parse(data);
                                        if (data.issetImage === false) {
                                            // Subimos fichero si es tipo imagen
                                            if (file.type == 'image/png' || file.type == 'image/jpg' || file.type == 'image/jpeg') {

                                                var formData = new FormData();
                                                formData.append('file', file);
                                                formData.append('subFolder', subFolder);


                                                var response = $.ajax({
                                                    processData: false,
                                                    contentType: false,
                                                    type: 'POST',
                                                    url: window.location.origin + '/media/uploadFile',
                                                    data: formData
                                                });

                                                response.done(function (msg) {
                                                    console.log(msg);

                                                    var path = $('ul.content-tree li.active a').data('path');
                                                    var url = APP.base_url + 'media' + '/index/' + path;
                                                    $('#media-main-content').load(url + ' #media-main-content > div', function () {
                                                        $('#msgInfoOkMedia').html('La imagen ha sido subida correctamente');
                                                        $('#divInfoOkMedia').removeClass('hide');

                                                        setTimeout(function () {
                                                            $('#divInfoOkMedia').addClass('hide');
                                                            $('#msgInfoOkMedia').html('');
                                                        }, 4000);
                                                    });

                                                });
                                                response.fail(function (error) {

                                                    if (error.status == 413) {
                                                        msg = 'La imagen tiene un tamaño muy grande.';
                                                    } else {
                                                        msg = 'Ha ocurrido un error. Pruebe con otra imagen';
                                                    }
                                                    ledMedia.showErrorMsg(msg);
                                                });
                                            } else {
                                                msg = "El fichero a subir no es tipo imagen";
                                                ledMedia.showErrorMsg(msg);
                                            }

                                        } else {
                                            msg = "Ya existe una imagen con el mismo nombre";
                                            ledMedia.showErrorMsg(msg);
                                        }
                                    },
                                    error: function (xhr, ajaxOptions, thrownError) {
                                        console.log(xhr.status);
                                        console.log(thrownError);
                                    }
                                });

                        }else{
                            msg = "El nombre de la imagen es demasiado largo (máximo 60 carácteres)";
                            ledMedia.showErrorMsg(msg);
                        }
                    }else{
                        msg = "La imagen es demasiado grande (máximo de "+limitUpload+"MB)";
                        ledMedia.showErrorMsg(msg);
                    }
                    $('#ledUploadMedia').val('');
					
				})
				.on('mouseover', '.media-img', function () {
					
					$this = $(this);
                    var imgUrl = $this.data('url');
												
					var html = '' +
						  '<div class="ledControls">' +
							  '<ul>' +						
								  '<li>' +
                                      '<a href="#" class="ledEditCropImage" title="Editar recorte">' +
										  '<i class="icon icon-edit edit-crop-media-item" data-edit-crop="1"></i>' +
									  ' </a>' +
                                  '</li>' +
                                  '<li>' +
                                      '<a href="#" class="ledAddImage" title="Añadir imagen">' +
										  '<i class="icon icon-add edit-add-media-item"></i>' +
									  ' </a>' +
                                  '</li>' +
							  '</ul>' +
						  '</div>';

					$this.parent().append(html);
				})
				.on('mouseleave', '.media-item', function () {
					
					$this = $(this);
					$this.parent().find('.ledControls').remove();

				})
                .on('click', '.edit-crop-media-item', function (event) {
                    
                    event.stopPropagation();

                    /**
                     * Evitamos crear un jQuery
                     * object en cada acción del evento
                     */
                    var $this = $(this);
					
					var editCrop = $this.data( 'edit-crop' ) ? true : false;					

                    /**
                     * Cuando hacemos click sobre el botón
                     * seleccionar imagen, al abrir el modal
                     * agregamos la clase currentEditing y la
                     * eliminamos al cerrar el modal.
                     *
                     * Si vas hacer pruebas dentro del controlador
                     * media sin acceder al modal que se dispara en
                     * el controlador NewsletterController ten en
                     * cuenta que $input no lo vas a tener disponible.
                     */
                    var $input = $('.currentEditing');

                    /**
                     * El input guarda información sobre el
                     * tamaño de la imagen que espera
                     */
                    var width = $input.data('width');

                    /**
                     * Lo mismo que arriba pero con el
                     * alto de la imagen
                     */
                    var height = $input.data('height');

                    /**
                     * Lo usamos para hacer una búsqueda
                     * por el atributo data a la hora de
                     * seleccionar una imagen y saber si
                     * existe el tamaño o tenemos que llamar
                     * el cropper.
                     */

                    /**
                     * La imagen del anchor media-img
                     */
					if( editCrop ){
						$mediaItem = $this.closest('.media-item');
						var $image = $mediaItem.find('.media-img');
					}else{
						var $image = $this.find('.media-img');
					}
                    

                    /**
                     * A partir del nombre del fichero
                     * sacamos el título por defecto de
                     * la imagen.
                     */
                    var title = ledMedia.getFileTitle($image);

                    /**
                     * A partir del nombre del fichero sacamos
                     * el alt de la imagen. Ten en cuenta que
                     * el atributo alt en una newsletter se muestra
                     * cuando el cliente de correo bloquea las imagenes.
                     */
                    var alt = ledMedia.getFileAlt($image);

                    /**
                     * Por defecto establecemos el tamaño
                     * original de la imagen.
                     * Todos los tamaños de la imagen se
                     * guardan dentro de atributos data
                     * de la siguiente forma
                     * data-size-800x300="url"
                     */
                    //var url = $image.data('size-original');
					var url = $image.data('crop-original');

                    ledMedia.getImageCropper($image, width, height);

                    /**
                     * Agregamos el borde azul
                     * al anchor que tiene la
                     * imagen dentro
                     */
                    $('.media-item.active').removeClass('active');
                    $this.addClass('active');

                    $('.error-no-image').addClass('hidden');

                    $('#media-title').val(title);
                    $('#alt-text').val(alt);
                    $('#other-details').val(url);	
                })
                .on('click', '.edit-add-media-item', function(){
                    $this     = $(this); 
                    $inputImg = $('.currentEditing');
                    $spanImg  = $inputImg.closest('.form-group').find('.spanLedImageType');

                    //Le añadimos una version a la imagen para que no cachee
                    $img    = $this.closest('.media-item').find('.media-img');
                    $imgSrc = $img.attr('src') + '&v=' + ledCore.getRandomInt();
                    
                    $inputImg.attr('value', $imgSrc);
                    $spanImg.text('URL: ' + $imgSrc);

                    $('#mainModal').modal('toggle');
                    $inputImg.keyup();
                    
                })
				.on('click', '#closeEditCropper', function (e) {
				
					e.preventDefault();
					e.stopPropagation();
					var $plugin		= $('#led-cropper-plugin');
					var $cropper	= $('.image-container-crop');			
				   
					$plugin.hide();
			
				});

        };


        ledMedia.getImageCropper = function ($image, width, height) {
            var $plugin		= $('#led-cropper-plugin');
            var $cropper	= $('.image-container-crop');			
            //$plugin.slideDown('slow');
			$plugin.show();
				
			//$cropper.html('<img src="' + $image.data('size-original') + '" class="cropping-image">');
			$cropper.html('<img src="' + $image.data('url') + '" class="cropping-image">');

			var $img = $('.cropping-image');
			$img.attr( 'data-name', $image.data('name') );

			$img.cropper({
				autoCropArea: 0,
				strict: true,
				dragCrop: true,
				cropBoxMovable: true,
				cropBoxResizable: false,				
				aspectRatio: width / height,
				resizable: false,
				minCropBoxWidth: width,
				minCropBoxHeight: height,			

				autoCropArea: 0,
				guides: false,

				//built: function () { $img.cropper("setCropBoxData", { width: 650, height: 118 });},
				crop: function (data) {
					/**
					 * Evita sobrecargar el navegador
					 * Este callback se dispara en el
					 * evento onMouseChange que revienta
					 * el navegador
					 */
				},
				ready: function () {					
					$img.cropper("setCropBoxData", { width: width, height: height });
				}
			});			
            

            $('#crop-action').on('click', function(e){
				
				modalAux		= new ledModalLoading('html');
				modalAux.setAnimationTime(500);
				modalAux.show();
                
				subFolder		= $('ul.content-tree li.active a').data('path');
				
				cropBoxData		= $img.cropper('getCropBoxData');
				imgData			= $img.cropper('getImageData');
				cropData		= $img.cropper('getData');
				canvasData		= $img.cropper('getCanvasData');
				path			= $( 'ul.content-tree li.active a' ).data( 'path' );
				fileName		= $img.attr('data-name');
				
                var response = ledCore.ajaxCall({
                    url: window.location.origin+'/media/upload',
                    data: {
                        imgSrc: $img.attr('src'),
						subFolder: subFolder,
						imgName: fileName,						
						path: $( 'ul.content-tree li.active a' ).data( 'path' ),						
                        cropBoxData: cropBoxData,
						cropData: cropData,
						canvasData: canvasData,
						imgData: imgData
                    }
                });
				
				response.done( function(data) {
                     /*
					 * Renderizamos la vista con los datos (images) actualizadas
					 */
                    data = JSON.parse(data);
                    
                    if (data.img) {
                        $inputImg = $('.currentEditing');
                        $spanImg = $inputImg.closest('.form-group').find('.spanLedImageType');
                        
                        //Le añadimos una version a la imagen para que no cachee
                        $imgSrc = data.img + '&v=' + ledCore.getRandomInt();
                        $inputImg.attr('value', $imgSrc);
                        $spanImg.text('URL: ' + $imgSrc);

                        $('#mainModal').modal('toggle');
                        $inputImg.keyup();
                    }
                    modalAux.remove();		
                });
                
                response.fail(function (error) {
                    modalAux.remove();
                    
                    $('.back-button').click();
                    msg = "Error al conectar con el servicio. Pruebe más tarde";
                    ledMedia.showErrorMsg(msg);
					
                    e.stopPropagation();
                    e.preventDefault();
                    return false;
                });
            });

            $('#zoom-in').on('click', function () {
                $img.cropper("zoom", 0.1);
            });

            $('#zoom-out').on('click', function () {
                $img.cropper("zoom", -0.1);
            });


        };


        ledMedia.getFileTitle = function ($img) {
            var title = $img.data('name');
            title = title.replace($img.data('extension'), '');
            title = title.replace(/[\.\-\_\:]/g, ' ').toLowerCase();
            title = title.charAt(0).toUpperCase() + title.slice(1);
            return title;
        };


        ledMedia.getFileAlt = function ($img) {
            var alt = ledMedia.getFileTitle($img);
            return alt.replace(/ /g, '-');
        };


        ledMedia.loadContent = function ($dir) {

            //ledCore.showWait('#media-content-images-container', 'Cargando contenido');
            var baseurl = ledCore.getVar('baseUrl') + 'media/index/';
            var relpath = $dir.closest('a').data('path').replace(/\\/g, '/');

            console.log(baseurl + relpath + '/');

            $('#media-content-images-container').load(baseurl + relpath + '/' + " #media-content-images");
        };


        ledMedia.getDirPath = function ($element) {
            var $parent = $element.prev('a').addClass('prevTest');
            var route;

            /*
            $parent.find('a').each(function(index, element){
                var $element = $(element);
                route += '/'+$element.data('name')
            });
            */

            console.log($parent);
        };


        ledMedia.selectDir = function ($clickedAnchor) {
            $('.media-dir').closest('li').removeClass('active');
            if (!$clickedAnchor.closest('li').hasClass('active')) {
                $clickedAnchor.closest('li').addClass('active');
            }
        };
        
        /**
         * Limpiamos el string con la misma lógica que en php
         * para comparar el nombre de las imagenes, tanto las 
         * ya subidas como las que vamos a subir
         */
        ledMedia.clearNameImage = function (string){
            //Lo dejamos comentado ya que normalize no es compatible con IE
            //string = string.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
            
            //Remplazamos carateres no alfanumericos
            string = string.replace(/[º~`#@|!·$%&/()?'¡¿[^+}{´><;,:]/g, '')
                    .replace(/ /g, '-')
                    .replace(/"/g, '')
                    .replace(/'/g, '')
                    .toLowerCase();
            return string;
        };
        
        /**
         * Mostramos un mensaje de error
         */
        ledMedia.showErrorMsg = function(msg){
            $('#msgInfoErrorMedia').html( msg );
            $('#divInfoErrorMedia').removeClass('hide');

            setTimeout(function(){ 
                $('#divInfoErrorMedia').addClass('hide');
                $('#msgInfoErrorMedia').html(''); 
            }, 4000);
        };

        return ledMedia.init();
    };


    $.fn.ledMedia.defaults = {
        test: null
    };

    $(window).ledMedia({
        ledCore : $(window).ledHelper()
    });
})(jQuery);


jQuery.fn.putCursorAtEnd = function () {

    return this.each(function () {

        // Cache references
        var $el = $(this),
            el = this;

        // Only focus if input isn't already
        if (!$el.is(":focus")) {
            $el.focus();
        }

        // If this function exists... (IE 9+)
        if (el.setSelectionRange) {

            // Double the length because Opera is inconsistent about whether a carriage return is one character or two.
            var len = $el.val().length * 2;

            // Timeout seems to be required for Blink
            setTimeout(function () {
                el.setSelectionRange(len, len);
            }, 1);

        } else {

            // As a fallback, replace the contents with itself
            // Doesn't work in Chrome, but Chrome supports setSelectionRange
            $el.val($el.val());

        }

        // Scroll to the bottom, in case we're in a tall textarea
        // (Necessary for Firefox and Chrome)
        this.scrollTop = 999999;

    });

};