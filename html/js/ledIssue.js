(function ($) {

    $.fn.ledIssue = function () {
        
        var ledIssue  = this;
        var ledHelper = $(window).ledHelper();
        var BASE_URL  = window.location.origin;
        
        /**
         * Iniciamos la funcionalidad
         */
        ledIssue.init = function () {
            ledIssue.selectComponentTab();
        };
        
        /**
         * Mostramos el contenido segun la pestana de opciones
         * desde la configuración del contenido del componente
         */
        ledIssue.selectComponentTab = function(){
            $(document).on('click', '.ledSettingsTab', function(){
                var $tab = $(this);
                ledIssue.statusComponentTab($tab);
            }); 
        };
        
        /**
         * Cambia el estado segun tab activo
         * 
         * @param element $tab
         */
        ledIssue.statusComponentTab = function ($tab){
            //Ocultamos y mostramos el contenido correspondiente
            $('.ledSettingsTabContent').addClass('hidden');
            var contentId = $tab.data('target');
            $(contentId).removeClass('hidden');
            
            //Activamos el tab correspondiente
            $('.ledSettingsTab').removeClass('active');
            $tab.addClass('active');
        };
        return ledIssue.init();
    };

    $(window).ledIssue();
})(jQuery);